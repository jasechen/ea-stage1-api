<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Package\Jsonponse\Jsonponse;

use App\Mail\ForgotPassword;
use App\Services\UserServ;
use App\Services\UserCheckedOptionServ;
use App\Services\SessionServ;


class UserPassword extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->userCheckedOptionServ = app(UserCheckedOptionServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Notify
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function notify(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $account = $request->input('account');

        if (empty($account)) {
            $code = 400;
            $comment = 'account empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $accountValidator = Validator::make(['account' => $account],
            ['account' => 'email']
        );

        if ($accountValidator->fails()) {
            $code = 422;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findByAccount($account);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userDatum->first()->status != config('tbl_users.USERS_STATUS_ENABLE')) {
            $code = 422;
            $comment = 'user status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'forgot_password');

        if ($userCheckedOptionDatum->isEmpty()) {
            $userCheckedOptionDatum = $this->userCheckedOptionServ->create($userDatum->first()->id, 'forgot_password');
        } else {
            $userCheckedOptionDatum = $this->userCheckedOptionServ->resetCodeAndStatus($userCheckedOptionDatum->first()->id);
        } // END if


        $to = $userDatum->first()->account;
        $emailFields = ['account' => $to,
                        'code' => $userCheckedOptionDatum->first()->code,
                        'link' => env('WEB_URL') . '/rescue/' . $userDatum->first()->id];
        Mail::to($to)->send(new ForgotPassword($emailFields));


        $resultData = ['session' => $sessionCode];

        Jsonponse::success('notify success', $resultData, 202);
    } // END function


    /**
     * Verify
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function reset(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $account = $request->input('account');

        if (empty($account)) {
            $code = 400;
            $comment = 'account empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $password = $request->input('password');
        $passwordRepeat = $request->input('password_repeat');

        if (empty($password) or empty($passwordRepeat)) {
            $code = 400;
            $comment = 'password / password_repeat empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $checkCode = $request->input('check_code');

        if (empty($checkCode)) {
            $code = 400;
            $comment = 'check_code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $accountValidator = Validator::make(['account' => $account],
            ['account' => 'email']
        );

        if ($accountValidator->fails()) {
            $code = 422;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if

        // $passwordValidator = Validator::make(['password' => $password],
        //     ['password' => 'size:' . config('app.PASSWORD_LENGTH_LIMIT')]
        // );

        // if ($passwordValidator->fails()) {
        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 422;
            $comment = 'password\' length < ' . config('app.PASSWORD_LENGTH_LIMIT');

            Jsonponse::fail($comment, $code);
        } // END if

        if ($password != $passwordRepeat) {
            $code = 422;
            $comment = 'password / password_repeat is NOT equal';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findByAccount($account);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userDatum->first()->status != config('tbl_users.USERS_STATUS_ENABLE')) {
            $code = 422;
            $comment = 'user status error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userDatum->first()->id != $id) {
            $code = 422;
            $comment = 'user account error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userPasswordCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'forgot_password');

        if ($userPasswordCheckedOptionDatum->isEmpty()) {
            $code = 404;
            $comment = 'user password check error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userPasswordCheckedOptionDatum->first()->status != config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT')) {
            $code = 422;
            $comment = 'user check status error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userPasswordCheckedOptionDatum->first()->code != $checkCode) {
            $code = 422;
            $comment = 'user check code error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFields = ['status' => config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED')];
        $updateWhere = ['id' => $userPasswordCheckedOptionDatum->first()->id];
        $userPasswordCheckedOptionDatum = $this->userCheckedOptionServ->update($updateFields, $updateWhere);

        if ($userPasswordCheckedOptionDatum->isEmpty()) {
            $code = 500;
            $comment = 'update check status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->updatePassword($password, $userDatum->first()->id);

        if ($userDatum->isEmpty()) {
            $code = 500;
            $comment = 'update password error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode];

        Jsonponse::success('reset success', $resultData, 201);
    } // END function


} // END class
