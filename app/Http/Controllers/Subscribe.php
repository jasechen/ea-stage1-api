<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\SubscriberServ;


class Subscribe extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ   = app(SessionServ::class);
        $this->subscriberServ = app(SubscriberServ::class);
    } // END function


    /**
     * create
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $name
     * @param  $email
     *
     * @throws
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $name = $request->input('name');

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $email = $request->input('email');

        if (empty($email)) {
            $code = 400;
            $comment = 'email empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $token = $request->header('token');

        if (!empty($token)) {

            $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

            if (empty($isValid)) {
                $code = 422;
                $comment = 'session token error';

                Jsonponse::fail($comment, $code);
            } // END if

            $userDatum = $this->userServ->findByToken($token);
            $ownerId = $userDatum->isNotEmpty() ? $userDatum->first()->id : 0;
        } // END if

        $emailValidator = Validator::make(['email' => $email],
            ['email' => 'email']
        );

        if ($emailValidator->fails()) {
            $code = 422;
            $comment = 'email format error';

            Jsonponse::fail($comment, $code);
        } // END if


        $subscriberDatum = $this->subscriberServ->findByEmail($email);

        if ($subscriberDatum->isNotEmpty()) {
            if ($subscriberDatum->first()->status == config('tbl_subscribers.SUBSCRIBERS_STATUS_UNSUBSCRIBE')) {
                $this->subscriberServ->switchStatusByEmail($email);
            } else {
                $code = 409;
                $comment = 'email already exist';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $sessionDatum = $this->sessionServ->findByCode($sessionCode);
        $ownerId = empty($ownerId) ? 0 : $ownerId;

        $subscriberDatum = $this->subscriberServ->create($sessionDatum->first()->id, $name, $email, $ownerId);

        if ($subscriberDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'subscriber_id' => $subscriberDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * findBySubscribe
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByStatusSubscribe(Request $request, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];
        $status = config('tbl_subscribers.SUBSCRIBERS_STATUS_SUBSCRIBE');

        $subscribeData = $this->subscriberServ->findByStatus($status, $orderby, $page);

        if ($subscribeData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'subscribers' => $subscribeData->all()];

        Jsonponse::success('find success', $resultData);
    } // END function

} // END class
