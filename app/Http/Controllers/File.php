<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\UserServ;

class File extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ   = app(SessionServ::class);
        $this->dirServ = app(DirServ::class);
        $this->fileServ = app(FileServ::class);
        $this->userServ = app(UserServ::class);
    } // END function


    /**
     * Upload file.
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $grand_type
     * @param   $grand_id
     * @param   $type
     * @param   $parent_type
     * @param   $parent_id
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function upload(Request $request)
    {

        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $grandType = $request->input('grand_type', 'user'); // postcategory, blog, user, dir
        $grandId   = $request->input('grand_id');

        if (empty($grandId)) {
            $code = 400;
            $comment = 'grand_id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $type = $request->input('type', 'no_dir'); // no_dir, avatar, cover, others

        if (empty($type)) {
            $code = 400;
            $comment = 'type is empty';

            $this->failResponse($comment, $code);
        } // END if

        $parentType = $request->input('parent_type', 'post'); // post, dir
        $parentId   = $request->input('parent_id', 0);

        $privacyStatus = $request->input('privacy_status', 'public');
        $status = $request->input('status', 'enable');

        $ownerId = $request->input('owner_id');

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if


        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        if (!empty($ownerId)) {
            $ownerDatum = $this->userServ->findById($ownerId);
            $ownerDatum = ($ownerDatum->isEmpty()) ? $creatorDatum : $ownerDatum;
        } else {
            $ownerDatum = $creatorDatum;
        } // END if

        $ownerId = $ownerDatum->first()->id;

        if ($creatorId != $ownerId) {

            $roleDatum = $this->roleServ->findByTypeAndCode('general', 'manager');
            $userRoleDatum = $this->userRoleServ->findByUserIdAndRoleId($creatorId, $roleDatum->first()->id);

            if ($userRoleDatum->isEmpty()) {
                $code = 422;
                $comment = 'creator role error';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $grandTypeValidator = Validator::make(['grand_type' => $grandType],
            ['grand_type' => ['in:' . implode(',', config('tbl_dirs.DIRS_PARENT_TYPES'))]]
        );

        if ($grandTypeValidator->fails()) {
            $code = 422;
            $comment = 'grand_type error';

            $this->failResponse($comment, $code);
        } // END if

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:' . implode(',', config('tbl_dirs.DIRS_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentTypeValidator = Validator::make(['parent_type' => $parentType],
            ['parent_type' => ['in:' . implode(',', config('tbl_files.FILES_PARENT_TYPES'))]]
        );

        if ($parentTypeValidator->fails()) {
            $code = 422;
            $comment = 'parent_type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_files.FILES_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($grandType == 'blog') {
            $grandDatum = $this->blogServ->findById($grandId);
        } else if ($grandType == 'user') {
            $grandDatum = $this->userServ->findById($grandId);
        } else if ($grandType == 'dir') {
            $grandDatum = $this->dirServ->findById($grandId);
        } // END if else

        if ($grandDatum->isEmpty()) {
            $code = 404;
            $comment = 'grand error';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($parentId)) {
            if ($parentType == 'post') {
                $parentDatum = $this->postServ->findById($parentId);
            } else if ($parentType == 'user') {
                $parentDatum = $this->userServ->findById($parentId);
            } // END if else

            if ($parentDatum->isEmpty()) {
                $code = 404;
                $comment = 'parent error';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if

        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($grandType, $grandDatum->first()->id, $type, $ownerId);

        if ($dirDatum->isEmpty()) {
            $code = 404;
            $comment = 'dir error';

            Jsonponse::fail($comment, $code);
        } // END if


        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $grandDatum->first()->id, $grandType, $type);

        $fileDatum = $this->fileServ->create($dirDatum->first()->id, $parentType, $parentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $ownerId, false, $fileType, $privacyStatus, $status, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $fileLinks = $this->fileServ->findLinks($filename);


        $resultData = ['session'  => $sessionCode, 'file_id'  => $fileDatum->first()->id, 'filename' => $fileDatum->first()->filename, 'file_links' => $fileLinks];

        Jsonponse::success('upload success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $filename
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $filename)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($filename)) {
            $code = 400;
            $comment = 'filename empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $fileDatum = $this->fileServ->findByFilename($filename);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($fileDatum->first()->status == config('tbl_files.FILES_STATUS_DELETE')) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFields = ['status' => config('tbl_files.FILES_STATUS_DELETE')];
        $updateWhere = ['id' => $fileDatum->first()->id];
        $updateFileDatum = $this->fileServ->update($updateFields, $updateWhere);

        if ($updateFileDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'filename' => $filename];

        Jsonponse::success('delete success', $resultData);
    } // END function


} // END class
