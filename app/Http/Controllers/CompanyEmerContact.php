<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\CompanyServ;
use App\Services\CompanyEmerContactServ;
use App\Services\SessionServ;


class CompanyEmerContact extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyEmerContactServ = app(CompanyEmerContactServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyId = $request->input('company_id');
        $username = $request->input('username');
        $phone = $request->input('phone');
        $email = $request->input('email');

        if (empty($companyId)) {
            $code = 400;
            $comment = 'company_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($username)) {
            $code = 400;
            $comment = 'username empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($phone)) {
            $code = 400;
            $comment = 'phone empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($email)) {
            $code = 400;
            $comment = 'email empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyDatum = $this->companyServ->findById($companyId);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if


        $createCompanyEmerContactDatum = $this->companyEmerContactServ->create($companyId, $username, $phone, $email);

        if ($createCompanyEmerContactDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_emer_id' => $createCompanyEmerContactDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $company_emer_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_emer_id)) {
            $code = 400;
            $comment = 'company_emer_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyEmerId = $company_emer_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyEmerContactDatum = $this->companyEmerContactServ->findById($companyEmerId);

        if ($companyEmerContactDatum->isEmpty()) {
            $code = 404;
            $comment = 'company emer error';

            Jsonponse::fail($comment, $code);
        } // END if


        $username = $request->input('username');
        $phone = $request->input('phone');
        $email = $request->input('email');

        $updateData = [];

       if (!empty($username) AND $username != $companyEmerContactDatum->first()->username) {
            $updateData['username'] = $username;
        } // END if

       if (!empty($phone) AND $phone != $companyEmerContactDatum->first()->phone) {
            $updateData['phone'] = $phone;
        } // END if

       if (!empty($email) AND $email != $companyEmerContactDatum->first()->email) {
            $updateData['email'] = $email;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $companyEmerContactDatum->first()->id];
        $updateCompanyEmerContactDatum = $this->companyEmerContactServ->update($updateData, $updateWhere);

        if ($updateCompanyEmerContactDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_emer_id' => $companyEmerId];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $company_emer_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_emer_id)) {
            $code = 400;
            $comment = 'company_emer_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyEmerId = $company_emer_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $companyEmerContactDatum = $this->companyEmerContactServ->findById($companyEmerId);

        if ($companyEmerContactDatum->isEmpty()) {
            $code = 404;
            $comment = 'company emer error';

            Jsonponse::fail($comment, $code);
        } // END if


        $result = $this->companyEmerContactServ->delete(['id' => $companyEmerId]);

        if (empty($result)) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_emer_id' => $companyEmerId];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $company_emer_id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($company_emer_id)) {
            $code = 400;
            $comment = 'company_emer_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyEmerId = $company_emer_id;


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $companyEmerContactDatum = $this->companyEmerContactServ->findById($companyEmerId);

        if ($companyEmerContactDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_emer' => $companyEmerContactDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByCompanyId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByCompanyId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderby = ['id' => $orderWay];

        $companyEmerContactData = $this->companyEmerContactServ->findByCompanyId($id, $orderby, $page);

        if ($companyEmerContactData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_emers' => $companyEmerContactData->all()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
