<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\LocalesServ;
use App\Services\LocalesContentServ;
use App\Services\SessionServ;


class Locales extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->localesServ   = app(LocalesServ::class);
        $this->localesContentServ   = app(LocalesContentServ::class);
        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $localesDatum = $this->localesServ->findById($id);

        if ($localesDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        $localesId = $localesDatum->first()->id;
        $localesContentdata = $this->localesContentServ->findByLocalesId($localesId);

        $localesDatum->first()->contents = [];

        if ($localesContentdata->isNotEmpty()) {
            foreach ($localesContentdata->all() as $localesContentDatum) {
                $localesDatum->first()->contents[$localesContentDatum->lang] = $localesContentDatum;
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'locales' => $localesDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $localesData = $this->localesServ->findList($orderby, $page);

        if ($localesData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($localesData->all() as $localesDatum) {
            $localesId = $localesDatum->id;
            $localesContentdata = $this->localesContentServ->findByLocalesId($localesId);

            $localesDatum->contents = [];

            if ($localesContentdata->isNotEmpty()) {
                foreach ($localesContentdata->all() as $localesContentDatum) {
                    $localesDatum->contents[$localesContentDatum->lang] = $localesContentDatum;
                } // END foreach
            } // END if

            array_push ($finalData, $localesDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'locales' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByCode
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $code
     *
     * @throws
     * @return
     */
    public function findByCode(Request $request, $code)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $localesDatum = $this->localesServ->findByCode($code);

        if ($localesDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        $localesId = $localesDatum->first()->id;
        $localesContentdata = $this->localesContentServ->findByLocalesId($localesId);

        $localesDatum->first()->contents = [];

        if ($localesContentdata->isNotEmpty()) {
            foreach ($localesContentdata->all() as $localesContentDatum) {
                $localesDatum->first()->contents[$localesContentDatum->lang] = $localesContentDatum;
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'locales' => $localesDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function



    /**
     * findByLang
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $lang
     *
     * @throws
     * @return
     */
    public function findByLang(Request $request, $lang, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $langValidator = Validator::make(['lang' => $lang],
            ['lang' => ['in:' . implode(',', config('tbl_locales_contents.LOCALES_CONTENTS_LANGS'))]]
        );

        if ($langValidator->fails()) {
            $code = 422;
            $comment = 'lang error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $localesContentData = $this->localesContentServ->findByLang($lang, $orderby, $page);

        if ($localesContentData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($localesContentData->all() as $localesContentDatum) {

            $localesDatum = $this->localesServ->findById($localesContentDatum->locales_id);
            if ($localesDatum->isEmpty()) {continue;}

            $localesDatum->first()->contents = [];
            $localesDatum->first()->contents[$lang] = $localesContentDatum;

            array_push ($finalData, $localesDatum->first());
        } // END foreach


        $resultData = ['session' => $sessionCode, 'locales' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByLangAndCode
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $lang
     * @param  $code
     *
     * @throws
     * @return
     */
    public function findByLangAndCode(Request $request, $lang, $code)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $langValidator = Validator::make(['lang' => $lang],
            ['lang' => ['in:' . implode(',', config('tbl_locales_contents.LOCALES_CONTENTS_LANGS'))]]
        );

        if ($langValidator->fails()) {
            $code = 422;
            $comment = 'lang error';

            Jsonponse::fail($comment, $code);
        } // END if


        $localesDatum = $this->localesServ->findByCode($code);

        if ($localesDatum->isEmpty()) {
            $code = 404;
            $comment = 'code error';

            Jsonponse::fail($comment, $code);
        } // END if

        $localesDatum->first()->contents = [];

        $localesId = $localesDatum->first()->id;
        $localesContentDatum = $this->localesContentServ->findByLocalesIdAndLang($localesId, $lang);

        if ($localesContentDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $localesDatum->first()->contents[$lang] = $localesContentDatum->first();

        $resultData = ['session' => $sessionCode, 'locales' => $localesDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
