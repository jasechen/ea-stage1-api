<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\CompanyServ;
use App\Services\UserServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\SessionServ;
use App\Services\LocalesServ;


class Company extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->companyServ    = app(CompanyServ::class);
        $this->userServ    = app(UserServ::class);
        $this->dirServ    = app(DirServ::class);
        $this->fileServ    = app(FileServ::class);
        $this->sessionServ = app(SessionServ::class);
        $this->localesServ = app(LocalesServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $name = $request->input('name');
        $principal = $request->input('principal');
        $incorporationDate = $request->input('incorporation_date');
        $guiNumber = $request->input('gui_number');
        $cover  = $request->input('cover');
        $status = $request->input('status', config('tbl_companies.DEFAULT_COMPANIES_STATUS'));

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($principal)) {
            $code = 400;
            $comment = 'principal empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($incorporationDate)) {
            $code = 400;
            $comment = 'incorporation_date empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($guiNumber)) {
            $code = 400;
            $comment = 'gui_number empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($cover)) {
            $code = 400;
            $comment = 'cover empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_companies.COMPANIES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyDatum = $this->companyServ->findByGuiNumber($guiNumber);

        if ($companyDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'gui_number error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findByFilename($cover);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'cover error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileId = $fileDatum->first()->id;
        $fileParentType = $fileDatum->first()->parent_type;
        $fileParentId = $fileDatum->first()->parent_id;
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_COMPANY');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirCreatorId = 0;


        $companyDatum = $this->companyServ->create($name, $principal, $incorporationDate, $guiNumber, $cover, $status, $creatorId);

        if ($companyDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyId = $companyDatum->first()->id;

        // cover
        $dirParentId = $companyId;
        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

        if ($dirDatum->isEmpty()) {
            $dirDatum = $this->dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
        } // END if

        $dirId = $dirDatum->first()->id;

        $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
        $pathExists = Storage::disk('gallery')->exists($tPath);
        if (empty($pathExists)) {
            Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
        } // END if


        $updatedata = ['dir_id' => $dirId, 'parent_id' => $companyId, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        $updateWhere = ['id' => $fileId];
        $this->fileServ->update($updatedata, $updateWhere);

        $this->fileServ->moveTo($cover, $tPath);


        $resultData = ['session' => $sessionCode, 'company_id' => $companyId];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $companyDatum = $this->companyServ->findById($id);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if


        $name = $request->input('name');
        $principal = $request->input('principal');
        $incorporationDate = $request->input('incorporation_date');
        $guiNumber = $request->input('gui_number');
        $cover  = $request->input('cover');
        $status = $request->input('status');

        $updateData = [];

        if (!empty($name) AND $name != $companyDatum->first()->name) {
            $updateData['name'] = $name;
        } // END if

        if (!empty($principal) AND $principal != $companyDatum->first()->principal) {
            $updateData['principal'] = $principal;
        } // END if

        if (!empty($incorporationDate) AND $incorporationDate != $companyDatum->first()->incorporation_date) {
            $updateData['incorporation_date'] = $incorporationDate;
        } // END if

        if (!empty($guiNumber) AND $guiNumber != $companyDatum->first()->gui_number) {
            $updateData['gui_number'] = $guiNumber;
        } // END if

        if (!empty($cover) AND $cover != $companyDatum->first()->cover) {
            $fileDatum = $this->fileServ->findByFilename($cover);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'cover error';

                Jsonponse::fail($comment, $code);
            } // END if

            $dirParentId = $companyDatum->first()->id;
            $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_COMPANY');
            $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
            $dirOwnerId = 0;
            $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
            $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
            $dirCreatorId = 0;

            $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

            if ($dirDatum->isEmpty()) {
                $dirDatum = $this->dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
            } // END if

            $dirId = $dirDatum->first()->id;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $pathExists = Storage::disk('gallery')->exists($tPath);
            if (empty($pathExists)) {
                Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
            } // END if

            $updateData['cover'] = $cover;
        } // END if

        if (!empty($status) AND $status != $companyDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_companies.COMPANIES_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateCompanyDatum = $this->companyServ->update($updateData, ['id' => $id]);

        if ($updateCompanyDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($updateData['cover'])) {
            if (!empty($companyDatum->first()->cover)) {
                $coverDatum = $this->fileServ->findByFilename($companyDatum->first()->cover);

                if ($coverDatum->isNotEmpty()) {
                    $updatedata = ['status' => config('tbl_files.FILES_STATUS_DELETE')];
                    $updateWhere = ['id' => $coverDatum->first()->id];
                    $this->fileServ->update($updatedata, $updateWhere);

                    $dirId = empty($dirId) ? $coverDatum->first()->dir_id : $dirId;
                } // END if
            } // END if

            $updatedata = ['dir_id' => $dirId, 'parent_id' => $companyDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
            $updateWhere = ['id' => $fileDatum->first()->id];
            $this->fileServ->update($updatedata, $updateWhere);

            $this->fileServ->moveTo($cover, $dirParentType . '_' . $dirParentId . '/' . $dirType);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $companyDatum = $this->companyServ->findById($id);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($companyDatum->first()->status == config('tbl_companies.COMPANIES_STATUS_DELETE')) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFields = ['status' => config('tbl_companies.COMPANIES_STATUS_DELETE')];
        $updateWhere = ['id' => $id];
        $deleteCompanyDatum = $this->companyServ->update($updateFields, $updateWhere);

        if ($deleteCompanyDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $companyDatum = $this->companyServ->findById($id);

        if ($companyDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $companyDatum->first()->cover_links = [];
        if (!empty($companyDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($companyDatum->first()->cover);
            $companyDatum->first()->cover_links = $coverLinks;
        } // END if else


        $companyDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $companyDatum->first()->url_path = empty($companyDatum->first()->slug) ? '' : '/' . $companyDatum->first()->slug . '-c' . $companyDatum->first()->id;


        $resultData = ['session' => $sessionCode, 'company' => $companyDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findList
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $status = 'enable', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_companies.COMPANIES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];
        $status = $status == 'all' ? '' : $status;

        $companyData = $this->companyServ->findList($status, $orderby, $page);

        if ($companyData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_COMPANY');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirOwnerId = 0;
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');

        $finalData = [];
        foreach ($companyData->all() as $companyDatum) {

            $companyDatum->cover_links = [];
            if (!empty($companyDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($companyDatum->cover);
                $companyDatum->cover_links = $coverLinks;
            } // END if else


            $companyDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $companyDatum->url_path = empty($companyDatum->slug) ? '' : '/' . $companyDatum->slug . '-c' . $companyDatum->id;

            array_push($finalData, $companyDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'companies' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * Upload Cover
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function uploadCover(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $ownerId = $request->input('owner_id', '0');
        $grandId = $parentId = '0';

        $grandType = config('tbl_dirs.DIRS_PARENT_TYPE_COMPANY');
        $type = config('tbl_dirs.DIRS_TYPE_COVER');

        $parentType = config('tbl_files.FILES_PARENT_TYPE_COMPANY');
        $privacyStatus = $request->input('privacy_status', config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC'));
        $status = $request->input('status', config('tbl_files.FILES_STATUS_DISABLE'));

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_files.FILES_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dirId = 0;

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $grandId, $grandType, $type);


        $fileDatum = $this->fileServ->create($dirId, $parentType, $parentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $ownerId, true, $fileType, $privacyStatus, $status, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $coverLinks = $this->fileServ->findLinks($filename);

        $resultData = ['session' => $sessionCode, 'filename' => $filename, 'cover_links' => $coverLinks];

        Jsonponse::success('upload cover success', $resultData, 201);
    } // END function


    /**
     * findCountries
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findCountries(Request $request)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $prefix = 'country_name_';
        $countries = config('global_countries.FAMILIAR_COUNTRIES');

        $finalData = [];

        foreach ($countries as $code) {

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);

            $finalData[$code] = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'countries' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findCities
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findCities(Request $request)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $prefix = 'city_name_';
        $subPrefix = 'TW';
        $locations = config('global_cities.SERVICE_LOACTIONS');

        $finalData = [];

        foreach ($locations as $code) {

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $finalData[$code] = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'locations' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findLocations
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findLocations(Request $request)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $prefix = 'city_name_';
        $subPrefix = 'TW';
        $locations = config('global_cities.SERVICE_LOACTIONS');

        $finalData = [];

        foreach ($locations as $code) {

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $finalData[$code] = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'locations' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
