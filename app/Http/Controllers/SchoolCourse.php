<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\SchoolServ;
use App\Services\SchoolCourseServ;
use App\Services\SessionServ;
use App\Services\LocalesServ;


class SchoolCourse extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->schoolServ = app(SchoolServ::class);
        $this->schoolCourseServ = app(SchoolCourseServ::class);
        $this->sessionServ = app(SessionServ::class);
        $this->localesServ = app(LocalesServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolId = $request->input('school_id');
        $name = $request->input('name');
        $description = $request->input('description');
        $ta = $request->input('ta');

        if (empty($schoolId)) {
            $code = 400;
            $comment = 'school_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ta)) {
            $code = 400;
            $comment = 'ta empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        // $taValidator = Validator::make(['ta' => $ta],
        //     ['ta' => ['in:' . implode(',', config('tbl_school_courses.SCHOOL_COURSES_TAS'))]]
        // );

        // if ($taValidator->fails()) {
        //     $code = 422;
        //     $comment = 'ta error';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        $schoolDatum = $this->schoolServ->findById($schoolId);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolCourseDatum = $this->schoolCourseServ->create($schoolId, $name, $ta, $description);

        if ($schoolCourseDatum->isEmpty()) {
            $code = 500;
            $comment = 'create school course error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_course_id' => $schoolCourseDatum->first()->id];

        Jsonponse::success('create school course success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $school_course_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_course_id)) {
            $code = 400;
            $comment = 'school_course_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolCourseId = $school_course_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        // $schoolDatum = $this->schoolServ->findById($id);

        // if ($schoolDatum->isEmpty()) {
        //     $code = 404;
        //     $comment = 'school error';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        $schoolCourseDatum = $this->schoolCourseServ->findById($schoolCourseId);

        if ($schoolCourseDatum->isEmpty()) {
            $code = 404;
            $comment = 'school course error';

            Jsonponse::fail($comment, $code);
        } // END if


        $name = $request->input('name');
        $description = $request->input('description');
        $ta = $request->input('ta');

        $updateData = [];

       if (!empty($name) AND $name != $schoolCourseDatum->first()->name) {
            $updateData['name'] = $name;
        } // END if

        if (!empty($description) AND $description != $schoolCourseDatum->first()->description) {
            $updateData['description'] = $description;
        } // END if

        if (!empty($ta) AND $ta != $schoolCourseDatum->first()->ta) {
            // $taValidator = Validator::make(['ta' => $ta],
            //     ['ta' => ['in:' . implode(',', config('tbl_school_courses.SCHOOL_COURSES_TAS'))]]
            // );

            // if ($taValidator->fails()) {
            //     $code = 422;
            //     $comment = 'ta error';

            //     Jsonponse::fail($comment, $code);
            // } // END if

            $updateData['ta'] = $ta;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $schoolCourseDatum->first()->id];
        $updateSchoolCourseDatum = $this->schoolCourseServ->update($updateData, $updateWhere);

        if ($updateSchoolCourseDatum->isEmpty()) {
            $code = 500;
            $comment = 'update school course error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_course_id' => $schoolCourseId];

        Jsonponse::success('update school course success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $school_course_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_course_id)) {
            $code = 400;
            $comment = 'school_course_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolCourseId = $school_course_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolCourseDatum = $this->schoolCourseServ->findById($schoolCourseId);

        if ($schoolCourseDatum->isEmpty()) {
            $code = 404;
            $comment = 'school course error';

            Jsonponse::fail($comment, $code);
        } // END if


        $result = $this->schoolCourseServ->delete(['id' => $schoolCourseId]);

        if (empty($result)) {
            $code = 500;
            $comment = 'delete school course error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_course_id' => $schoolCourseId];

        Jsonponse::success('delete school course success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $school_course_id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($school_course_id)) {
            $code = 400;
            $comment = 'school_course_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolCourseId = $school_course_id;


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $schoolCourseDatum = $this->schoolCourseServ->findById($schoolCourseId);

        if ($schoolCourseDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $prefix = 'school_program_ta_';
        $schoolCourseDatum->first()->ta_langs = [];

        if (!empty($schoolCourseDatum->first()->ta)) {
            $tas = explode(',', $schoolCourseDatum->first()->ta);

            foreach ($tas as $ta) {
                $ta = trim($ta);
                $langContent = $this->localesServ->findCodeLangsByCode($ta, $prefix);
                $schoolCourseDatum->first()->ta_langs[$ta] = $langContent;
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'school_course' => $schoolCourseDatum->first()];

        Jsonponse::success('fetch school course success', $resultData);
    } // END function


    /**
     * findBySchoolId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findBySchoolId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderby = ['id' => $orderWay];

        $schoolCourseData = $this->schoolCourseServ->findBySchoolId($id, $orderby, $page);

        if ($schoolCourseData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $prefix = 'school_program_ta_';

        $finalData = [];

        foreach ($schoolCourseData->all() as $schoolCourseDatum) {

            $schoolCourseDatum->ta_langs = [];

            $tas = explode(',', $schoolCourseDatum->ta);

            foreach ($tas as $ta) {
                $ta = trim($ta);
                $langContent = $this->localesServ->findCodeLangsByCode($ta, $prefix);
                $schoolCourseDatum->ta_langs[$ta] = $langContent;
            } // END foreach

            array_push($finalData, $schoolCourseDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'courses' => $finalData];

        Jsonponse::success('fetch courses success', $resultData);
    } // END function


    /**
     * findTAs
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findTAs(Request $request)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $prefix = 'school_program_ta_';
        $tas = config('tbl_school_courses.SCHOOL_COURSES_TAS');

        $finalData = [];

        foreach ($tas as $code) {

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);

            $finalData[$code] = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'tas' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
