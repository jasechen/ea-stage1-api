<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\UserProfileServ;
use App\Services\UserCheckedOptionServ;
use App\Services\SessionServ;

use Aloha\Twilio\Support\Laravel\Facade as Twilio;


class UserMobile extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->userProfileServ = app(UserProfileServ::class);
        $this->userCheckedOptionServ = app(UserCheckedOptionServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Notify
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function notify(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userDatum->first()->status != config('tbl_users.USERS_STATUS_INIT')) {
            $code = 422;
            $comment = 'user status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userMobileCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'mobile_phone');

        if ($userMobileCheckedOptionDatum->isEmpty()) {
            $code = 404;
            $comment = 'user mobile check error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userMobileCheckedOptionDatum->first()->status != config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT')) {
            $code = 422;
            $comment = 'user check status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userMobileCheckedOptionDatum = $this->userCheckedOptionServ->resetCodeAndStatus($userMobileCheckedOptionDatum->first()->id);


        $userProfileDatum = $this->userProfileServ->findByUserId($userDatum->first()->id);
        $mobileCountryCode = $userProfileDatum->first()->mobile_country_code;
        $mobilePhone = $userProfileDatum->first()->mobile_phone;

        $phone = '+' . $mobileCountryCode . $mobilePhone;
        $message = '您的 English.Agency「註冊驗證碼」為 ' . $userMobileCheckedOptionDatum->first()->code;
        Twilio::message($phone, $message);


        $resultData = ['session' => $sessionCode];

        Jsonponse::success('notify success', $resultData, 202);
    } // END function


    /**
     * Verify
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function verify(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $checkCode = $request->input('check_code');

        if (empty($checkCode)) {
            $code = 400;
            $comment = 'check_code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userDatum->first()->status != config('tbl_users.USERS_STATUS_INIT')) {
            $code = 422;
            $comment = 'user status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userMobileCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'mobile_phone');

        if ($userMobileCheckedOptionDatum->isEmpty()) {
            $code = 404;
            $comment = 'user mobile check error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userMobileCheckedOptionDatum->first()->status != config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT')) {
            $code = 422;
            $comment = 'user check status error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userMobileCheckedOptionDatum->first()->code != $checkCode) {
            $code = 422;
            $comment = 'user check code error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFields = ['status' => config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED')];
        $updateWhere = ['id' => $userMobileCheckedOptionDatum->first()->id];
        $userMobileCheckedOptionDatum = $this->userCheckedOptionServ->update($updateFields, $updateWhere);

        $userMobileChecked = $userMobileCheckedOptionDatum->first()->status == config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED') ? true : false;

        $userEmailCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'email');
        $userEmailChecked = $userEmailCheckedOptionDatum->first()->status == config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED') ? true : false;

        if (!empty($userEmailChecked) AND !empty($userMobileChecked)) {
            $updateFields = ['status' => config('tbl_users.USERS_STATUS_ENABLE')];
            $updateWhere = ['id' => $userDatum->first()->id];
            $this->userServ->update($updateFields, $updateWhere);

            $sessionDatum = $this->sessionServ->findByCode($sessionCode);
            $sessionFields = ['status' => config('tbl_sessions.SESSIONS_STATUS_LOGIN'), 'owner_id' => $userDatum->first()->id];
            $sessionWhere = ['id' => $sessionDatum->first()->id];
            $this->sessionServ->update($sessionFields, $sessionWhere);
        } // END if


        $resultData = ['session' => $sessionCode];

        Jsonponse::success('verify success', $resultData, 201);
    } // END function


} // END class
