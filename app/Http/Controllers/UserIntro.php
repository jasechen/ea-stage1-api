<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\CompanyServ;
use App\Services\CompanyUserServ;
use App\Services\UserIntroServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\SessionServ;


class UserIntro extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyUserServ = app(CompanyUserServ::class);
        $this->userIntroServ = app(UserIntroServ::class);
        $this->dirServ    = app(DirServ::class);
        $this->fileServ    = app(FileServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $userId = $request->input('user_id');
        $title = $request->input('title');
        $content = $request->input('content');
        $image = $request->input('image');
        $imageTitle = $request->input('image_title');
        $imageAlt = $request->input('image_alt');

        if (empty($userId)) {
            $code = 400;
            $comment = 'user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($title)) {
            $code = 400;
            $comment = 'title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($content)) {
            $code = 400;
            $comment = 'content empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($image)) {
            $code = 400;
            $comment = 'image empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($imageTitle)) {
            $code = 400;
            $comment = 'image_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($imageAlt)) {
            $code = 400;
            $comment = 'image_alt empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userDatum = $this->userServ->findById($userId);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($userId);

        if ($cuData->isEmpty()) {
            $code = 409;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findByFilename($image);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'image error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileId = $fileDatum->first()->id;
        $fileParentType = $fileDatum->first()->parent_type;
        $fileParentId = $fileDatum->first()->parent_id;
        $dirId = $fileDatum->first()->dir_id;

        $dirDatum = $this->dirServ->findById($dirId);

        if ($dirDatum->isEmpty()) {
            $code = 404;
            $comment = 'dir error';

            Jsonponse::fail($comment, $code);
        } // END if

        $dirParentType = $dirDatum->first()->parent_type;
        $dirParentId = $dirDatum->first()->parent_id;
        $dirType = $dirDatum->first()->type;

        $createUserIntroDatum = $this->userIntroServ->create($userId, $title, $content, $image, $imageTitle, $imageAlt);

        if ($createUserIntroDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updatedata = ['parent_id' => $userId, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        $updateWhere = ['id' => $fileId];
        $this->fileServ->update($updatedata, $updateWhere);

        $updatedata = ['parent_id' => $userId, 'status' => config('tbl_dirs.DIRS_STATUS_ENABLE')];
        $updateWhere = ['id' => $dirId];
        $this->dirServ->update($updatedata, $updateWhere);


        $tPath = $dirParentType . '_' . $userId . '/' . $dirType;
        $pathExists = Storage::disk('gallery')->exists($tPath);
        if (empty($pathExists)) {
            Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
        } // END if

        $this->fileServ->moveTo($image, $tPath);


        $resultData = ['session' => $sessionCode, 'user_intro_id' => $createUserIntroDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $user_intro_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($user_intro_id)) {
            $code = 400;
            $comment = 'user_intro_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $userIntroId = $user_intro_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userIntroDatum = $this->userIntroServ->findById($userIntroId);

        if ($userIntroDatum->isEmpty()) {
            $code = 404;
            $comment = 'user intro error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userId = $userIntroDatum->first()->user_id;

        $title = $request->input('title');
        $content = $request->input('content');
        $image = $request->input('image');
        $imageTitle = $request->input('image_title');
        $imageAlt = $request->input('image_alt');

        $updateData = [];

        if (!empty($title) AND $title != $userIntroDatum->first()->title) {
            $updateData['title'] = $title;
        } // END if

        if (!empty($content) AND $content != $userIntroDatum->first()->content) {
            $updateData['content'] = $content;
        } // END if

        if (!empty($image) AND $image != $userIntroDatum->first()->image) {
            $fileDatum = $this->fileServ->findByFilename($image);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'image error';

                Jsonponse::fail($comment, $code);
            } // END if

            $dirDatum = $this->dirServ->findById($fileDatum->first()->dir_id);

            $dirParentType = $dirDatum->first()->parent_type;
            $dirParentId = $dirDatum->first()->parent_id;
            $dirType = $dirDatum->first()->type;

            $updateData['image'] = $image;
        } // END if

        if (!empty($imageTitle) AND $imageTitle != $userIntroDatum->first()->image_title) {
            $updateData['image_title'] = $imageTitle;
        } // END if

        if (!empty($imageAlt) AND $imageAlt != $userIntroDatum->first()->image_alt) {
            $updateData['image_alt'] = $imageAlt;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $userIntroDatum->first()->id];
        $updateUserIntroDatum = $this->userIntroServ->update($updateData, $updateWhere);

        if ($updateUserIntroDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($updateData['image'])) {
            $updatedata = ['parent_id' => $userId, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
            $updateWhere = ['id' => $fileDatum->first()->id];
            $this->fileServ->update($updatedata, $updateWhere);

            $this->fileServ->moveTo($image, $dirParentType . '_' . $dirParentId . '/' . $dirType);

            if (!empty($userIntroDatum->first()->image)) {
                $imageDatum = $this->fileServ->findByFilename($userIntroDatum->first()->image);

                if ($imageDatum->isNotEmpty()) {
                    $updatedata = ['status' => config('tbl_files.FILES_STATUS_DELETE')];
                    $updateWhere = ['id' => $imageDatum->first()->id];
                    $this->fileServ->update($updatedata, $updateWhere);
                } // END if
            } // END if
        } // END if


        $resultData = ['session' => $sessionCode, 'user_intro_id' => $userIntroId];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $user_intro_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($user_intro_id)) {
            $code = 400;
            $comment = 'user_intro_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $userIntroId = $user_intro_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userIntroDatum = $this->userIntroServ->findById($userIntroId);

        if ($userIntroDatum->isEmpty()) {
            $code = 404;
            $comment = 'user intro error';

            Jsonponse::fail($comment, $code);
        } // END if


        $deleteUserIntroDatum = $this->userIntroServ->delete(['id' => $userIntroId]);

        if (empty($deleteUserIntroDatum)) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_intro_id' => $userIntroId];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $user_intro_id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($user_intro_id)) {
            $code = 400;
            $comment = 'user_intro_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $userIntroId = $user_intro_id;


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $userIntroDatum = $this->userIntroServ->findById($userIntroId);

        if ($userIntroDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $userIntroDatum->first()->image_links = [];
        if (!empty($userIntroDatum->first()->image)) {
            $imageLinks = $this->fileServ->findLinks($userIntroDatum->first()->image);
            $userIntroDatum->first()->image_links = $imageLinks;
        } // END if else


        $resultData = ['session' => $sessionCode, 'user_intro' => $userIntroDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByUserId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByUserId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($id);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $userIntroData = $this->userIntroServ->findByUserId($id, $orderby, $page);

        if ($userIntroData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($userIntroData->all() as $userIntroDatum) {

            $userIntroDatum->image_links = [];

            if (!empty($userIntroDatum->image)) {
                $imageLinks = $this->fileServ->findLinks($userIntroDatum->image);
                $userIntroDatum->image_links = $imageLinks;
            } // END if else

            array_push($finalData, $userIntroDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'user_intros' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * Upload Image
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function uploadImage(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $ownerId = $request->input('owner_id', '0');
        $grandId = $parentId = '0';

        $grandType = config('tbl_dirs.DIRS_PARENT_TYPE_USER');
        $type = config('tbl_dirs.DIRS_TYPE_OTHERS');
        $parentType = config('tbl_files.FILES_PARENT_TYPE_USER');

        $privacyStatus = $request->input('privacy_status', config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC'));
        $status = $request->input('status', config('tbl_files.FILES_STATUS_DISABLE'));

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_files.FILES_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($grandType, $grandId, $type, $ownerId);

        if ($dirDatum->isEmpty()) {
            $dirDatum = $this->dirServ->initDefaultAlbum($grandType, $grandId, $type, $ownerId, $privacyStatus, $status, $creatorId);
        } // END if

        $dirId = $dirDatum->first()->id;

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $grandId, $grandType, $type);


        $fileDatum = $this->fileServ->create($dirId, $parentType, $parentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $ownerId, false, $fileType, $privacyStatus, $status, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $imageLinks = $this->fileServ->findLinks($filename);

        $resultData = ['session' => $sessionCode, 'filename' => $filename, 'image_links' => $imageLinks];

        Jsonponse::success('upload cover success', $resultData, 201);
    } // END function


} // END class
