<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\UserSeoServ;
use App\Services\UserProfileServ;
use App\Services\RoleServ;
use App\Services\SchoolServ;
use App\Services\SchoolUserServ;
use App\Services\FileServ;
use App\Services\SessionServ;


class SchoolUser extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->userSeoServ = app(UserSeoServ::class);
        $this->userProfileServ = app(UserProfileServ::class);
        $this->roleServ = app(RoleServ::class);
        $this->schoolServ = app(SchoolServ::class);
        $this->schoolUserServ = app(SchoolUserServ::class);
        $this->fileServ = app(FileServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolId = $request->input('school_id');
        $userId = $request->input('user_id');
        $roleId = $request->input('role_id');

        if (empty($schoolId)) {
            $code = 400;
            $comment = 'school_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($userId)) {
            $code = 400;
            $comment = 'user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($roleId)) {
            $code = 400;
            $comment = 'role_id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolDatum = $this->schoolServ->findById($schoolId);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userDatum = $this->userServ->findById($userId);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $roleDatum = $this->roleServ->findById($roleId);

        if ($roleDatum->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            Jsonponse::fail($comment, $code);
        } // END if

        $roleType = $roleDatum->first()->type;

        if ($roleType == config('tbl_roles.ROLES_TYPE_GENERAL')) {
            $schoolUserType = config('tbl_school_users.SCHOOL_USERS_TYPE_STUDENT');
        } else if ($roleType == config('tbl_roles.ROLES_TYPE_COMPANY')) {
            $schoolUserType = config('tbl_school_users.SCHOOL_USERS_TYPE_COMPANY');
        } else if ($roleType == config('tbl_roles.ROLES_TYPE_SCHOOL')) {
            $schoolUserType = config('tbl_school_users.SCHOOL_USERS_TYPE_SCHOOL');
        } // END if


        $schoolUserDatum = $this->schoolUserServ->findBySchoolIdAndUserIdAndRoleId($schoolId, $userId, $roleId);

        if ($schoolUserDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'school user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolUserDatum = $this->schoolUserServ->create($schoolId, $userId, $roleId, $schoolUserType, $creatorId);

        if ($schoolUserDatum->isEmpty()) {
            $code = 500;
            $comment = 'create school user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_user_id' => $schoolUserDatum->first()->id];

        Jsonponse::success('create school user success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $school_user_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_user_id)) {
            $code = 400;
            $comment = 'school_user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolUserId = $school_user_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolUserDatum = $this->schoolUserServ->findById($schoolUserId);

        if ($schoolUserDatum->isEmpty()) {
            $code = 404;
            $comment = 'school user error';

            Jsonponse::fail($comment, $code);
        } // END if


        // $schoolId = $request->input('school_id');
        $userId = $request->input('user_id');
        $roleId = $request->input('role_id');

        $updateData = [];

        // if (!empty($schoolId) AND $schoolId != $schoolUserDatum->first()->school_id) {
        //     $schoolDatum = $this->schoolServ->findById($schoolId);

        //     if ($schoolDatum->isEmpty()) {
        //         $code = 404;
        //         $comment = 'school error';

        //         Jsonponse::fail($comment, $code);
        //     } // END if

        //     $updateData['school_id'] = $schoolId;
        // } // END if

        if (!empty($userId) AND $userId != $schoolUserDatum->first()->user_id) {
            $userDatum = $this->userServ->findById($userId);

            if ($userDatum->isEmpty()) {
                $code = 404;
                $comment = 'user error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['user_id'] = $userId;
        } // END if

        if (!empty($roleId) AND $roleId != $schoolUserDatum->first()->role_id) {
            $roleDatum = $this->roleServ->findById($roleId);

            if ($roleDatum->isEmpty()) {
                $code = 404;
                $comment = 'role error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['role_id'] = $roleId;

            $roleType = $roleDatum->first()->type;

            if ($roleType == config('tbl_roles.ROLES_TYPE_GENERAL')) {
                $schoolUserType = config('tbl_school_users.SCHOOL_USERS_TYPE_STUDENT');
            } else if ($roleType == config('tbl_roles.ROLES_TYPE_COMPANY')) {
                $schoolUserType = config('tbl_school_users.SCHOOL_USERS_TYPE_COMPANY');
            } else if ($roleType == config('tbl_roles.ROLES_TYPE_SCHOOL')) {
                $schoolUserType = config('tbl_school_users.SCHOOL_USERS_TYPE_SCHOOL');
            } // END if

            $updateData['type'] = $schoolUserType;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $schoolUserId];
        $updateSchoolUserDatum = $this->schoolUserServ->update($updateData, $updateWhere);

        if ($updateSchoolUserDatum->isEmpty()) {
            $code = 500;
            $comment = 'update school user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_user_id' => $schoolUserId];

        Jsonponse::success('update school user success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $school_user_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_user_id)) {
            $code = 400;
            $comment = 'school_user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolUserId = $school_user_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolUserDatum = $this->schoolUserServ->findById($schoolUserId);

        if ($schoolUserDatum->isEmpty()) {
            $code = 404;
            $comment = 'school user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $result = $this->schoolUserServ->delete(['id' => $schoolUserId]);

        if (!empty($result)) {
            $code = 500;
            $comment = 'delete school user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_user_id' => $schoolUserId];

        Jsonponse::success('delete school user success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $school_user_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_user_id)) {
            $code = 400;
            $comment = 'school_user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolUserId = $school_user_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolUserDatum = $this->schoolUserServ->findById($schoolUserId);

        if ($schoolUserDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_user' => $schoolUserDatum->first()];

        Jsonponse::success('fetch school user success', $resultData);
    } // END function


    /**
     * findBySchoolId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findBySchoolId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderby = ['id' => $orderWay];

        $schoolUserData = $this->schoolUserServ->findBySchoolId($id, $orderby, $page);

        if ($schoolUserData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_users' => $schoolUserData->all()];

        Jsonponse::success('fetch school users success', $resultData);
    } // END function


    /**
     * findBySchoolIdAndType
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findBySchoolIdAndType(Request $request, $id, $type, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($type)) {
            $code = 400;
            $comment = 'type empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:'.implode(',',config('tbl_school_users.SCHOOL_USERS_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderby = ['id' => $orderWay];

        $schoolUserData = $this->schoolUserServ->findBySchoolIdAndType($id, $type, $orderby, $page);

        if ($schoolUserData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($schoolUserData->all() as $schoolUserDatum) {

            $profileDatum = $this->userProfileServ->findByUserId($schoolUserDatum->user_id);

            $profileDatum->first()->avatar_links = [];
            if (!empty($profileDatum->first()->avatar)) {
                $avatarLinks = $this->fileServ->findLinks($profileDatum->first()->avatar);
                $profileDatum->first()->avatar_links = $avatarLinks;
            } // END if else

            $schoolUserDatum->profile = $profileDatum->first();

            $seoDatum = $this->userSeoServ->findByUserId($schoolUserDatum->user_id);
            $schoolUserDatum->seo = $seoDatum->first();

            array_push($finalData, $schoolUserDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'school_users' => $finalData];

        Jsonponse::success('fetch school users success', $resultData);
    } // END function


} // END class
