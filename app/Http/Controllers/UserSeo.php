<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\UserSeoServ;

use App\Services\SessionServ;


class UserSeo extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->userSeoServ = app(UserSeoServ::class);

        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userSeoDatum = $this->userSeoServ->findByUserId($userDatum->first()->id);

        if ($userSeoDatum->isEmpty()) {
            $code = 404;
            $comment = 'user seo error';

            Jsonponse::fail($comment, $code);
        } // END if


        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $avatarTitle = $request->input('avatar_title');
        $avatarAlt = $request->input('avatar_alt');


        $updateData = [];

        if (!empty($slug) AND $slug != $userSeoDatum->first()->slug) {
            $usDatum = $this->userSeoServ->findBySlug($slug);

            if ($usDatum->isNotEmpty() AND $usDatum->first()->user_id != $id) {
                $code = 409;
                $comment = 'slug error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['slug'] = $slug;
        } // END if

        if (!empty($excerpt) AND $excerpt != $userSeoDatum->first()->excerpt) {
            $updateData['excerpt'] = $excerpt;
        } // END if

        if (!empty($ogTitle) AND $ogTitle != $userSeoDatum->first()->og_title) {
            $updateData['og_title'] = $ogTitle;
        } // END if

        if (!empty($ogDescription) AND $ogDescription != $userSeoDatum->first()->og_description) {
            $updateData['og_description'] = $ogDescription;
        } // END if

        if (!empty($metaTitle) AND $metaTitle != $userSeoDatum->first()->meta_title) {
            $updateData['meta_title'] = $metaTitle;
        } // END if

        if (!empty($metaDescription) AND $metaDescription != $userSeoDatum->first()->meta_description) {
            $updateData['meta_description'] = $metaDescription;
        } // END if

        if (!empty($avatarTitle) AND $avatarTitle != $userSeoDatum->first()->avatar_title) {
            $updateData['avatar_title'] = $avatarTitle;
        } // END if

        if (!empty($avatarAlt) AND $avatarAlt != $userSeoDatum->first()->avatar_alt) {
            $updateData['avatar_alt'] = $avatarAlt;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $userSeoDatum->first()->id];
        $userSeoDatum = $this->userSeoServ->update($updateData, $updateWhere);

        if ($userSeoDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('update seo success', $resultData, 201);
    } // END function


} // END class
