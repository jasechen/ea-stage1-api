<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\SchoolServ;
use App\Services\FilterServ;
use App\Services\FilterSchoolServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\SessionServ;


class SchoolFilter extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->schoolServ = app(SchoolServ::class);
        $this->filterServ = app(FilterServ::class);
        $this->filterSchoolServ = app(FilterSchoolServ::class);
        $this->dirServ = app(DirServ::class);
        $this->fileServ = app(FileServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolId = $request->input('school_id');
        $filterIds = $request->input('filter_ids');

        if (empty($schoolId)) {
            $code = 400;
            $comment = 'school_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($filterIds)) {
            $code = 400;
            $comment = 'filter_ids empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $schoolDatum = $this->schoolServ->findById($schoolId);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        $realFilterIds = [];
        foreach ($filterIds as $filterId) {

            $filterdatum = $this->filterServ->findById($filterId);

            if ($filterdatum->isEmpty()) {
                continue;
            } // END if

            array_push ($realFilterIds, $filterId);
        } // END foreach

        if (empty($realFilterIds)) {
            $code = 404;
            $comment = 'filter error';

            Jsonponse::fail($comment, $code);
        } // END if


        $filterSchoolIds = [];

        foreach ($realFilterIds as $filterId) {
            $filterSchoolDatum = $this->filterSchoolServ->create($filterId, $schoolId, $creatorId);

            if ($filterSchoolDatum->isNotEmpty()) {
                array_push ($filterSchoolIds, $filterSchoolDatum->first()->id);
            } // ENd if
        } // END foreach

        if (empty($filterSchoolIds)) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'filter_school_ids' => $filterSchoolIds];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $filterIds  = $request->input('filter_ids');

        if (empty($filterIds)) {
            $code = 400;
            $comment = 'filter_ids empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $schoolDatum = $this->schoolServ->findById($id);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if


        $realFilterIds = [];
        foreach ($filterIds as $filterId) {

            $filterdatum = $this->filterServ->findById($filterId);

            if ($filterdatum->isEmpty()) {
                continue;
            } // END if

            array_push ($realFilterIds, $filterId);
        } // END foreach

        if (empty($realFilterIds)) {
            $code = 404;
            $comment = 'filter error';

            Jsonponse::fail($comment, $code);
        } // END if


        $filterSchoolData = $this->filterSchoolServ->findBySchoolId($id);

        if ($filterSchoolData->isNotEmpty()) {
            foreach ($filterSchoolData->all() as $filterSchoolDatum) {
                $this->filterSchoolServ->delete(['id' => $filterSchoolDatum->id]);
            } // END foreach
        } // END if


        $filterSchoolIds = [];

        foreach ($realFilterIds as $filterId) {
            $filterSchoolDatum = $this->filterSchoolServ->create($filterId, $id, $creatorId);

            if ($filterSchoolDatum->isNotEmpty()) {
                array_push ($filterSchoolIds, $filterSchoolDatum->first()->id);
            } // ENd if
        } // END foreach

        if (empty($filterSchoolIds)) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'filter_school_ids' => $filterSchoolIds];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolDatum = $this->schoolServ->findById($id);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        $filterSchoolData = $this->filterSchoolServ->findBySchoolId($id);

        if ($filterSchoolData->isEmpty()) {
            $code = 404;
            $comment = 'filter school error';

            Jsonponse::fail($comment, $code);
        } // END if


        $deleteSuccess = [];
        foreach ($filterSchoolData->all() as $filterSchoolDatum) {
            $result = $this->filterSchoolServ->delete(['id' => $filterSchoolDatum->id]);

            if (!empty($result)) {
                array_push ($deleteSuccess, $filterSchoolDatum->id);
            } // END if
        } // END foreach

        if (empty($deleteSuccess)) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'filter_school_ids' => $deleteSuccess];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolDatum = $this->schoolServ->findById($id);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        $filterSchoolData = $this->filterSchoolServ->findBySchoolId($id);

        if ($filterSchoolData->isEmpty()) {
            $code = 404;
            $comment = 'school filter error';

            Jsonponse::fail($comment, $code);
        } // END if


        $filderIds = [];

        foreach ($filterSchoolData->all() as $filterSchoolDatum) {

            $filterDatum = $this->filterServ->findById($filterSchoolDatum->filter_id);
            if ($filterDatum->isEmpty()) {continue;}

            array_push ($filderIds, $filterDatum->first()->id);
        } // END foreach

        if (empty($filderIds)) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'filder_ids' => $filderIds];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * Search
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     *
     * @return
     */
    public function search(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $filterIds = $request->input('filter_ids');

        if (empty($filterIds)) {
            $code = 400;
            $comment = 'filter_ids empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $dbParenTypes = config('tbl_filters.FILTERS_PARENT_TYPES');
        $parenTypes = ['country', 'location', 'schoolType', 'programType', 'program', 'programTa'];

        foreach ($parenTypes as $parenType) {
            ${$parenType.'FilterIds'} = [];
        } // END foreach

        foreach ($filterIds as $filterId) {
            $filterdatum = $this->filterServ->findById($filterId);

            if ($filterdatum->isNotEmpty()) {

                foreach ($dbParenTypes as $k => $dbParenType) {
                    if ($filterdatum->first()->parent_type == $dbParenType) {
                        array_push (${$parenTypes[$k].'FilterIds'}, $filterId);
                    }
                }

            } // END if
        } // END foreach

        if (empty($countryFilterIds) AND empty($locationFilterIds) AND empty($schoolTypeFilterIds) AND
            empty($programTypeFilterIds) AND empty($programFilterIds) AND empty($programTaFilterIds)) {
            $code = 404;
            $comment = 'filter error';

            Jsonponse::fail($comment, $code);
        } // END if


        $totalSchoolIds = [];

        foreach ($parenTypes as $parenType) {
            ${$parenType.'SchoolIds'} = [];

            foreach (${$parenType.'FilterIds'} as $filterId) {

                $filterSchoolData = $this->filterSchoolServ->findByFilterId($filterId);
                if ($filterSchoolData->isEmpty()) {continue;}

                foreach ($filterSchoolData->all() as $filterSchoolDatum) {
                    array_push (${$parenType.'SchoolIds'}, $filterSchoolDatum->school_id);
                } // END foreach
            } // END foreach

            if (!empty(${$parenType.'SchoolIds'})) {
                array_push($totalSchoolIds, array_unique(${$parenType.'SchoolIds'}));
            } // END if
        } // END foreach


        $schoolIds = call_user_func_array('array_intersect', $totalSchoolIds);

        if (empty($schoolIds)) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirOwnerId = 0;
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');

        foreach ($schoolIds as $schoolId) {

            $schoolDatum = $this->schoolServ->getById($schoolId);
            if ($schoolDatum->isEmpty()) {continue;}

            if ($schoolDatum->first()->status != config('tbl_schools.SCHOOLS_STATUS_ENABLE')) {continue;}

            $schoolDatum->first()->cover_links = [];
            if (!empty($schoolDatum->first()->cover)) {
                $schoolDatum->first()->cover_links = $this->fileServ->findLinks($schoolDatum->first()->cover);
            } // END if else


            $schoolDatum->first()->image_links = [];

            $dirParentId = $schoolDatum->first()->id;
            $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirStatus, $dirOwnerId);

            if ($dirDatum->isNotEmpty()) {
                $dirId = $dirDatum->first()->id;

                $fileData = $this->fileServ->findByDirId($dirId, $fileStatus);
                if ($fileData->isNotEmpty()) {
                    foreach ($fileData->all() as $fileDatum) {
                        $imageLinks = $this->fileServ->findLinks($fileDatum->filename);
                        if (!empty($imageLinks)) {
                            array_push($schoolDatum->first()->image_links, $imageLinks);
                        } // END if
                    } // END foreach
                } // END if
            } // END if


            array_push ($finalData, $schoolDatum->first());
        } // END foreach


        $resultData = ['session' => $sessionCode, 'schools' => $finalData];

        Jsonponse::success('search success', $resultData);
    } // END function


} // END class
