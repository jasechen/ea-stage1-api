<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\CompanyServ;
use App\Services\CompanyLocationServ;
use App\Services\SessionServ;
use App\Services\LocalesServ;


class CompanyLocation extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyLocationServ = app(CompanyLocationServ::class);
        $this->sessionServ = app(SessionServ::class);
        $this->localesServ = app(LocalesServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyId = $request->input('company_id');
        $locationCode = $request->input('code');

        if (empty($companyId)) {
            $code = 400;
            $comment = 'company_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($locationCode)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyDatum = $this->companyServ->findById($companyId);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if


        $createCompanyLocationDatum = $this->companyLocationServ->create($companyId, $locationCode);

        if ($createCompanyLocationDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_location_id' => $createCompanyLocationDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $company_location_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_location_id)) {
            $code = 400;
            $comment = 'company_location_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyLocationId = $company_location_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyLocationDatum = $this->companyLocationServ->findById($companyLocationId);

        if ($companyLocationDatum->isEmpty()) {
            $code = 404;
            $comment = 'company location error';

            Jsonponse::fail($comment, $code);
        } // END if


        $locationCode = $request->input('code');

        $updateData = [];

       if (!empty($locationCode) AND $locationCode != $companyLocationDatum->first()->code) {
            $updateData['code'] = $locationCode;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $companyLocationDatum->first()->id];
        $updateCompanyLocationDatum = $this->companyLocationServ->update($updateData, $updateWhere);

        if ($updateCompanyLocationDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_location_id' => $companyLocationId];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $company_location_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_location_id)) {
            $code = 400;
            $comment = 'company_location_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyLocationId = $company_location_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $companyLocationDatum = $this->companyLocationServ->findById($companyLocationId);

        if ($companyLocationDatum->isEmpty()) {
            $code = 404;
            $comment = 'company location error';

            Jsonponse::fail($comment, $code);
        } // END if


        $result = $this->companyLocationServ->delete(['id' => $companyLocationId]);

        if (empty($result)) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_location_id' => $companyLocationId];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $company_location_id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($company_location_id)) {
            $code = 400;
            $comment = 'company_location_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyLocationId = $company_location_id;


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $companyLocationDatum = $this->companyLocationServ->findById($companyLocationId);

        if ($companyLocationDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $code = $companyLocationDatum->first()->code;

        $prefix = 'city_name_';
        $subPrefix = 'TW';

        $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

        $companyLocationDatum->first()->code_langs = $langContent;


        $resultData = ['session' => $sessionCode, 'company_location' => $companyLocationDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByCompanyId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByCompanyId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderby = ['id' => $orderWay];

        $companyLocationData = $this->companyLocationServ->findByCompanyId($id, $orderby, $page);

        if ($companyLocationData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $prefix = 'city_name_';
        $subPrefix = 'TW';

        $finalData = [];

        foreach ($companyLocationData->all() as $companyLocationDatum) {

            $code = $companyLocationDatum->code;

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $companyLocationDatum->code_langs = $langContent;

            array_push($finalData, $companyLocationDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'company_locations' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
