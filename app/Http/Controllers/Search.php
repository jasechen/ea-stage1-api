<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\PostServ;
use App\Services\PostSeoServ;
use App\Services\PostcategoryServ;
use App\Services\UserServ;
use App\Services\UserSeoServ;
use App\Services\UserProfileServ;
use App\Services\FileServ;

use App\Services\SessionServ;


class Search extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->postServ   = app(PostServ::class);
        $this->postSeoServ   = app(PostSeoServ::class);
        $this->postcategoryServ   = app(PostcategoryServ::class);
        $this->userServ   = app(UserServ::class);
        $this->userSeoServ   = app(UserSeoServ::class);
        $this->userProfileServ   = app(UserProfileServ::class);
        $this->fileServ   = app(FileServ::class);
        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * findByTerm
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $term
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByTerm(Request $request, $term, $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($term)) {
            $code = 400;
            $comment = 'term empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $term = urldecode($term);
        $search = ['&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&ndash;', '&lt;', '&gt;', '&middot;', '&hellip;'];
        $finalPostData = $finalUserData = [];

        $postStatus = config('tbl_posts.POSTS_STATUS_PUBLISH');

        $postData = $this->postServ->getByTerm($term, $postStatus, ['p.id' => 'DESC'], $page);

        if ($postData->isNotEmpty()) {
            foreach ($postData->all() as $postDatum) {

                $postDatum->content = strip_tags(trim($postDatum->content));
                $postDatum->content = str_replace($search, '', $postDatum->content);

                $postcategoryDatum = $this->postcategoryServ->getById($postDatum->postcategory_id);
                $postcategoryDatum->first()->cover_links = [];
                if (!empty($postcategoryDatum->first()->cover)) {
                    $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
                    $postcategoryDatum->first()->cover_links = $coverLinks;
                } // END if
                $postDatum->postcategory = $postcategoryDatum->first();

                $ownerDatum = $this->userServ->findById($postDatum->owner_id);
                $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
                $ownerProfileDatum->first()->avatar_links = [];
                if (!empty($ownerProfileDatum->first()->avatar)) {
                    $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
                    $ownerProfileDatum->first()->avatar_links = $avatarLinks;
                } // END if
                $ownerDatum->first()->profile = $ownerProfileDatum->first();
                unset($ownerDatum->first()->password);
                $postDatum->owner = $ownerDatum->first();

                $postDatum->cover_links = [];
                if (!empty($postDatum->cover)) {
                    $coverLinks = $this->fileServ->findLinks($postDatum->cover);
                    $postDatum->cover_links = $coverLinks;
                } // END if

                $postDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
                $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

                array_push($finalPostData, $postDatum);
            } // ENd foreach
        } // END if


        $userStatus = config('tbl_users.USERS_STATUS_ENABLE');

        $userData = $this->userServ->getByTerm($term, $userStatus, ['u.id' => 'DESC'], 1, 1);

        if ($userData->isNotEmpty()) {
            foreach ($userData->all() as $userDatum) {

                $postData = $this->postServ->getByOwnerId($userDatum->id, $postStatus, ['p.id' => 'DESC'], $page);

                if ($postData->isNotEmpty()) {
                    foreach ($postData->all() as $postDatum) {

                        $postDatum->content = strip_tags(trim($postDatum->content));
                        $postDatum->content = str_replace($search, '', $postDatum->content);

                        $postcategoryDatum = $this->postcategoryServ->getById($postDatum->postcategory_id);
                        $postcategoryDatum->first()->cover_links = [];
                        if (!empty($postcategoryDatum->first()->cover)) {
                            $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
                            $postcategoryDatum->first()->cover_links = $coverLinks;
                        } // END if
                        $postDatum->postcategory = $postcategoryDatum->first();

                        $ownerDatum = $this->userServ->findById($postDatum->owner_id);
                        $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
                        $ownerProfileDatum->first()->avatar_links = [];
                        if (!empty($ownerProfileDatum->first()->avatar)) {
                            $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
                            $ownerProfileDatum->first()->avatar_links = $avatarLinks;
                        } // END if
                        $ownerDatum->first()->profile = $ownerProfileDatum->first();
                        unset($ownerDatum->first()->password);
                        $postDatum->owner = $ownerDatum->first();

                        $postDatum->cover_links = [];
                        if (!empty($postDatum->cover)) {
                            $coverLinks = $this->fileServ->findLinks($postDatum->cover);
                            $postDatum->cover_links = $coverLinks;
                        } // END if

                        $postDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
                        $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

                        array_push($finalUserData, $postDatum);
                    } // ENd foreach
                } // END if



            } // ENd foreach
        } // END if


        if (empty($finalPostData) AND empty($finalUserData)) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'posts' => $finalPostData, 'users' => $finalUserData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
