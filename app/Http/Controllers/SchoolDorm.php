<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\SchoolServ;
use App\Services\SchoolDormServ;
use App\Services\SchoolDormRoomServ;
use App\Services\SessionServ;
use App\Services\LocalesServ;


class SchoolDorm extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->schoolServ = app(SchoolServ::class);
        $this->schoolDormServ = app(SchoolDormServ::class);
        $this->schoolDormRoomServ = app(SchoolDormRoomServ::class);
        $this->sessionServ = app(SessionServ::class);
        $this->localesServ = app(LocalesServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolId = $request->input('school_id');
        $type = $request->input('type', config('tbl_school_dorms.DEFAULT_SCHOOL_DORMS_TYPE'));
        $service = $request->input('service');
        $facility = $request->input('facility');
        $rooms = $request->input('rooms');

        if (empty($schoolId)) {
            $code = 400;
            $comment = 'school_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($service)) {
            $code = 400;
            $comment = 'service empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($facility)) {
            $code = 400;
            $comment = 'facility empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($rooms)) {
            $code = 400;
            $comment = 'rooms empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:' . implode(',', config('tbl_school_dorms.SCHOOL_DORMS_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $finalRooms = [];
        foreach ($rooms as $room) {
            $room = (object) $room;

            $roomTypeValidator = Validator::make(['room_type' => $room->type],
                ['room_type' => ['in:' . implode(',', config('tbl_school_dorm_rooms.SCHOOL_DORM_ROOMS_TYPES'))]]
            );

            if (!$roomTypeValidator->fails()) {
                $room->accessible = empty($room->accessible) ? "false" : $room->accessible;
                $room->smoking = empty($room->smoking) ? "false" : $room->smoking;

                array_push ($finalRooms, $room);
            } // END if
        } // ENd foreach

        if (empty($finalRooms)) {
            $code = 422;
            $comment = 'all room type error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolDatum = $this->schoolServ->findById($schoolId);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolDormDatum = $this->schoolDormServ->findBySchoolIdAndType($schoolId, $type);

        if ($schoolDormDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'school dorm error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolDormDatum = $this->schoolDormServ->create($schoolId, $type, $service, $facility);

        if ($schoolDormDatum->isEmpty()) {
            $code = 500;
            $comment = 'create school dorm error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dormId = $schoolDormDatum->first()->id;

        foreach ($finalRooms as $room) {
            $roomAccessible = $room->accessible == 'false' ? false : true;
            $roomSmoking = $room->smoking == 'false' ? false : true;

            $dormRoomDatum = $this->schoolDormRoomServ->findByDormIdAndType($dormId, $room->type);
            if ($dormRoomDatum->isEmpty()) {
                $this->schoolDormRoomServ->create($schoolId, $dormId, $room->type, $roomAccessible, $roomSmoking);
            } // END if
        } // END foreach


        $resultData = ['session' => $sessionCode, 'school_dorm_id' => $schoolDormDatum->first()->id];

        Jsonponse::success('create school dorm success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $school_dorm_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_dorm_id)) {
            $code = 400;
            $comment = 'school_dorm_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolDormId = $school_dorm_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolDormDatum = $this->schoolDormServ->findById($schoolDormId);

        if ($schoolDormDatum->isEmpty()) {
            $code = 404;
            $comment = 'school dorm error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolId = $schoolDormDatum->first()->school_id;
        $dormId = $schoolDormDatum->first()->id;

        $type = $request->input('type');
        $service = $request->input('service');
        $facility = $request->input('facility');
        $rooms = $request->input('rooms');

        $updateData = $updateRoomData = [];

        if (!empty($type) AND $type != $schoolDormDatum->first()->type) {
            $typeValidator = Validator::make(['type' => $type],
                ['type' => ['in:' . implode(',', config('tbl_school_dorms.SCHOOL_DORMS_TYPES'))]]
            );

            if ($typeValidator->fails()) {
                $code = 422;
                $comment = 'type error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['type'] = $type;
        } // END if

        if (!empty($service) AND $service != $schoolDormDatum->first()->service) {
            $updateData['service'] = $service;
        } // END if

        if (!empty($facility) AND $facility != $schoolDormDatum->first()->facility) {
            $updateData['facility'] = $facility;
        } // END if

        if (!empty($rooms)) {
            foreach ($rooms as $room) {
                $room = (object) $room;

                $roomTypeValidator = Validator::make(['room_type' => $room->type],
                    ['room_type' => ['in:' . implode(',', config('tbl_school_dorm_rooms.SCHOOL_DORM_ROOMS_TYPES'))]]
                );

                if (!$roomTypeValidator->fails()) {
                    array_push ($updateRoomData, $room);
                } // END if
            } // ENd foreach
        } // END if


        if (empty($updateData) AND empty($updateRoomData)) {
            $code = 400;
            $comment = 'updateData / updateRoomData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!empty($updateData)) {
            $updateWhere = ['id' => $dormId];
            $updateSchoolDormDatum = $this->schoolDormServ->update($updateData, $updateWhere);

            if ($updateSchoolDormDatum->isEmpty()) {
                $code = 500;
                $comment = 'update school dorm error';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        if (!empty($updateRoomData)) {

            $roomData = $this->schoolDormRoomServ->findByDormId($dormId);

            if ($roomData->isNotEmpty()) {
                foreach ($roomData->all() as $roomDatum) {
                    $this->schoolDormRoomServ->delete(['id' => $roomDatum->id]);
                } // END foreach
            } // END if

            foreach ($updateRoomData as $room) {
                $roomAccessible = $room->accessible == 'false' ? false : true;
                $roomSmoking = $room->smoking == 'false' ? false : true;

                $dormRoomDatum = $this->schoolDormRoomServ->findByDormIdAndType($dormId, $room->type);
                if ($dormRoomDatum->isEmpty()) {
                    $this->schoolDormRoomServ->create($schoolId, $dormId, $room->type, $roomAccessible, $roomSmoking);
                } // END if
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'school_dorm_id' => $schoolDormId];

        Jsonponse::success('update school dorm success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $school_dorm_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_dorm_id)) {
            $code = 400;
            $comment = 'school_dorm_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolDormId = $school_dorm_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolDormDatum = $this->schoolDormServ->findById($schoolDormId);

        if ($schoolDormDatum->isEmpty()) {
            $code = 404;
            $comment = 'school dorm error';

            Jsonponse::fail($comment, $code);
        } // END if


        $result = $this->schoolDormServ->delete(['id' => $schoolDormId]);

        if (empty($result)) {
            $code = 500;
            $comment = 'delete school dorm error';

            Jsonponse::fail($comment, $code);
        } // END if

        $roomData = $this->schoolDormRoomServ->findByDormId($schoolDormId);
        if ($roomData->isNotEmpty()) {
            foreach ($roomData->all() as $roomDatum) {
                $this->schoolDormRoomServ->delete(['id' => $roomDatum->id]);
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'school_dorm_id' => $schoolDormId];

        Jsonponse::success('delete school dorm success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $school_dorm_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($school_dorm_id)) {
            $code = 400;
            $comment = 'school_dorm_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolDormId = $school_dorm_id;

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolDormDatum = $this->schoolDormServ->getById($schoolDormId);

        if ($schoolDormDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $prefix = 'dorm_type_';

        $code = $schoolDormDatum->first()->type;

        $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);

        $schoolDormDatum->first()->type_langs = $langContent;


        $roomPrefix = 'dorm_room_type_';

        $finalRooms = [];

        foreach ($schoolDormDatum->first()->rooms as $room) {

            $code = $room->type;

            $langContent = $this->localesServ->findCodeLangsByCode($code, $roomPrefix);

            $room->type_langs = $langContent;

            array_push ($finalRooms, $room);
        } // END foreach

        $schoolDormDatum->first()->rooms = $finalRooms;


        $resultData = ['session' => $sessionCode, 'school_dorm' => $schoolDormDatum->first()];

        Jsonponse::success('fetch school dorm success', $resultData);
    } // END function


    /**
     * findBySchoolId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findBySchoolId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $schoolDormData = $this->schoolDormServ->getBySchoolId($id, $orderby, $page);

        if ($schoolDormData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $prefix = 'dorm_type_';

        $finalData = [];

        foreach ($schoolDormData->all() as $schoolDormDatum) {

            $code = $schoolDormDatum->type;

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);

            $schoolDormDatum->type_langs = $langContent;


            $roomPrefix = 'dorm_room_type_';

            $finalRooms = [];

            foreach ($schoolDormDatum->rooms as $room) {

                $code = $room->type;

                $langContent = $this->localesServ->findCodeLangsByCode($code, $roomPrefix);

                $room->type_langs = $langContent;

                array_push ($finalRooms, $room);
            } // END foreach

            $schoolDormDatum->rooms = $finalRooms;

            array_push ($finalData, $schoolDormDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'school_dorms' => $finalData];

        Jsonponse::success('fetch school dorms success', $resultData);
    } // END function


    /**
     * findRoomTypes
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findRoomTypes(Request $request)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $prefix = 'dorm_room_type_';
        $types = config('tbl_school_dorm_rooms.SCHOOL_DORM_ROOMS_TYPES');

        $finalData = [];

        foreach ($types as $code) {

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);

            $finalData[$code] = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'room_types' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
