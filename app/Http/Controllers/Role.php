<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\RoleServ;
use App\Services\RolePermissionServ;
use App\Services\PermissionServ;
use App\Services\SessionServ;


class Role extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->roleServ   = app(RoleServ::class);
        $this->rolePermissionServ   = app(RolePermissionServ::class);
        $this->permissionServ   = app(PermissionServ::class);
        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $roleDatum = $this->roleServ->findById($id);

        if ($roleDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $RPData = $this->rolePermissionServ->findByRoleId($id);

        foreach ($RPData->all() as $RPDatum) {

            $permissionId = $RPDatum->permission_id;
            $permissionDatum = $this->permissionServ->findById($permissionId);

            $roleDatum->first()->permissions[] = $permissionDatum->first();
        } // END foreach


        $resultData = ['session' => $sessionCode, 'role' => $roleDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $roleData = $this->roleServ->findList($orderby, $page);

        if ($roleData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($roleData->all() as $roleDatum) {

            $RPData = $this->rolePermissionServ->findByRoleId($roleDatum->id);

            foreach ($RPData->all() as $RPDatum) {

                $permissionId = $RPDatum->permission_id;
                $permissionDatum = $this->permissionServ->findById($permissionId);

                $roleDatum->permissions[] = $permissionDatum->first();
            } // END foreach

            array_push($finalData, $roleDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'roles' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByType
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $type
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByType(Request $request, $type, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($type)) {
            $code = 400;
            $comment = 'type empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:',implode(','.config('tbl_roles.ROLES_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $roleData = $this->roleServ->findByType($type, $orderby, $page);

        if ($roleData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($roleData->all() as $roleDatum) {

            $RPData = $this->rolePermissionServ->findByRoleId($roleDatum->id);

            foreach ($RPData->all() as $RPDatum) {

                $permissionId = $RPDatum->permission_id;
                $permissionDatum = $this->permissionServ->findById($permissionId);

                $roleDatum->permissions[] = $permissionDatum->first();
            } // END foreach

            array_push($finalData, $roleDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'roles' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByCode
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $code
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByCode(Request $request, $code, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $roleData = $this->roleServ->findByCode($code, $orderby, $page);

        if ($roleData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($roleData->all() as $roleDatum) {

            $RPData = $this->rolePermissionServ->findByRoleId($roleDatum->id);

            foreach ($RPData->all() as $RPDatum) {

                $permissionId = $RPDatum->permission_id;
                $permissionDatum = $this->permissionServ->findById($permissionId);

                $roleDatum->permissions[] = $permissionDatum->first();
            } // END foreach

            array_push($finalData, $roleDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'roles' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByTypeAndCode
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $type
     * @param  $code
     *
     * @throws
     * @return
     */
    public function findByTypeAndCode(Request $request, $type, $code)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($type)) {
            $code = 400;
            $comment = 'type empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:',implode(','.config('tbl_roles.ROLES_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if


        $roleDatum = $this->roleServ->findByTypeAndCode($type, $code);

        if ($roleDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $RPData = $this->rolePermissionServ->findByRoleId($roleDatum->first()->id);

        foreach ($RPData->all() as $RPDatum) {

            $permissionId = $RPDatum->permission_id;
            $permissionDatum = $this->permissionServ->findById($permissionId);

            $roleDatum->first()->permissions[] = $permissionDatum->first();
        } // END foreach


        $resultData = ['session' => $sessionCode, 'role' => $roleDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
