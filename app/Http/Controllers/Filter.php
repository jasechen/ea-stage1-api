<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\LocalesServ;
use App\Services\FilterServ;
use App\Services\SessionServ;


class Filter extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->localesServ = app(LocalesServ::class);
        $this->filterServ   = app(FilterServ::class);
        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $filterDatum = $this->filterServ->findById($id);

        if ($filterDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $code = $filterDatum->first()->code;
        $type = $filterDatum->first()->type;
        $parentId = $filterDatum->first()->id;
        $parentType = $filterDatum->first()->parent_type;

        $subPrefix = '';

        if ($parentType == 'country') {
            $prefix = 'country_name_';
        } else if ($parentType == 'location') {
            $prefix = 'city_name_';

            $parentDatum = $this->filterServ->findById($parentId);
            $parentCode = $parentDatum->first()->code;

            $subPrefix = $parentCode;
        } else if ($parentType == 'school-type') {
            $prefix = 'school_type_title_';
        } else if ($parentType == 'program-type') {
            $prefix = 'school_program_type_';
        } else if ($parentType == 'program') {
            $prefix = 'school_program_name_';
        } else if ($parentType == 'program-ta') {
            $prefix = 'school_program_ta_';
        } // END if

        $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

        $filterDatum->first()->code_langs = $langContent;


        $filterDatum->first()->child = [];

        $childFilterData = $this->filterServ->findByTypeAndParentId($type, $parentId);

        if ($childFilterData->isNotEmpty()) {

            $finalData = [];

            foreach ($childFilterData->all() as $childFilterDatum) {

                $code = $childFilterDatum->code;
                $parentId = $childFilterDatum->parent_id;
                $parentType = $childFilterDatum->parent_type;

                $subPrefix = '';

                if ($parentType == 'country') {
                    $prefix = 'country_name_';
                } else if ($parentType == 'location') {
                    $prefix = 'city_name_';

                    $parentDatum = $this->filterServ->findById($parentId);
                    $parentCode = $parentDatum->first()->code;

                    $subPrefix = $parentCode;
                } else if ($parentType == 'school-type') {
                    $prefix = 'school_type_title_';
                } else if ($parentType == 'program-type') {
                    $prefix = 'school_program_type_';
                } else if ($parentType == 'program') {
                    $prefix = 'school_program_name_';
                } else if ($parentType == 'program-ta') {
                    $prefix = 'school_program_ta_';
                } // END if

                $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

                $childFilterDatum->code_langs = $langContent;

                array_push ($finalData, $childFilterDatum);
            } // END foreach

            $filterDatum->first()->child = $finalData;
        } // END if


        $resultData = ['session' => $sessionCode, 'filter' => $filterDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $filterData = $this->filterServ->findList($orderby, $page);

        if ($filterData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($filterData->all() as $filterDatum) {

            $code = $filterDatum->code;
            $type = $filterDatum->type;
            $parentId = $filterDatum->id;
            $parentType = $filterDatum->parent_type;

            $subPrefix = '';

            if ($parentType == 'country') {
                $prefix = 'country_name_';
            } else if ($parentType == 'location') {
                $prefix = 'city_name_';

                $parentDatum = $this->filterServ->findById($parentId);
                $parentCode = $parentDatum->first()->code;

                $subPrefix = $parentCode;
            } else if ($parentType == 'school-type') {
                $prefix = 'school_type_title_';
            } else if ($parentType == 'program-type') {
                $prefix = 'school_program_type_';
            } else if ($parentType == 'program') {
                $prefix = 'school_program_name_';
            } else if ($parentType == 'program-ta') {
                $prefix = 'school_program_ta_';
            } // END if

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $filterDatum->code_langs = $langContent;

            $filterDatum->child = [];

            $childFilterData = $this->filterServ->findByTypeAndParentId($type, $parentId);

            if ($childFilterData->isNotEmpty()) {
                $finalData = [];

                foreach ($childFilterData->all() as $childFilterDatum) {

                    $code = $childFilterDatum->code;
                    $parentId = $childFilterDatum->parent_id;
                    $parentType = $childFilterDatum->parent_type;

                    $subPrefix = '';

                    if ($parentType == 'country') {
                        $prefix = 'country_name_';
                    } else if ($parentType == 'location') {
                        $prefix = 'city_name_';

                        $parentDatum = $this->filterServ->findById($parentId);
                        $parentCode = $parentDatum->first()->code;

                        $subPrefix = $parentCode;
                    } else if ($parentType == 'school-type') {
                        $prefix = 'school_type_title_';
                    } else if ($parentType == 'program-type') {
                        $prefix = 'school_program_type_';
                    } else if ($parentType == 'program') {
                        $prefix = 'school_program_name_';
                    } else if ($parentType == 'program-ta') {
                        $prefix = 'school_program_ta_';
                    } // END if

                    $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

                    $childFilterDatum->code_langs = $langContent;

                    array_push ($finalData, $childFilterDatum);
                } // END foreach

                $filterDatum->child = $finalData;
            } // END if

            array_push($finalData, $filterDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'filters' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findSchoolByCode
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $code
     *
     * @throws
     * @return
     */
    public function findSchoolByCode(Request $request, $code, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $type = config('tbl_filters.FILTERS_TYPE_SCHOOL');
        $orderby = ['id' => $orderWay];

        $filterData = $this->filterServ->findByTypeAndCode($type, $code, $orderby, $page);

        if ($filterData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($filterData->all() as $filterDatum) {

            $code = $filterDatum->code;
            $type = $filterDatum->type;
            $parentId = $filterDatum->parent_id;
            $parentType = $filterDatum->parent_type;

            $subPrefix = '';

            if ($parentType == 'country') {
                $prefix = 'country_name_';
            } else if ($parentType == 'location') {
                $prefix = 'city_name_';

                $parentDatum = $this->filterServ->findById($parentId);
                $parentCode = $parentDatum->first()->code;

                $subPrefix = $parentCode;
            } else if ($parentType == 'school-type') {
                $prefix = 'school_type_title_';
            } else if ($parentType == 'program-type') {
                $prefix = 'school_program_type_';
            } else if ($parentType == 'program') {
                $prefix = 'school_program_name_';
            } else if ($parentType == 'program-ta') {
                $prefix = 'school_program_ta_';
            } // END if

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $filterDatum->code_langs = $langContent;


            $filterDatum->child = [];

            $childFilterData = $this->filterServ->findByTypeAndParentId($type, $parentId);

            if ($childFilterData->isNotEmpty()) {

                $finalChildData = [];

                foreach ($childFilterData->all() as $childFilterDatum) {

                    $code = $childFilterDatum->code;
                    $parentId = $childFilterDatum->parent_id;
                    $parentType = $childFilterDatum->parent_type;

                    $subPrefix = '';

                    if ($parentType == 'country') {
                        $prefix = 'country_name_';
                    } else if ($parentType == 'location') {
                        $prefix = 'city_name_';

                        $parentDatum = $this->filterServ->findById($parentId);
                        $parentCode = $parentDatum->first()->code;

                        $subPrefix = $parentCode;
                    } else if ($parentType == 'school-type') {
                        $prefix = 'school_type_title_';
                    } else if ($parentType == 'program-type') {
                        $prefix = 'school_program_type_';
                    } else if ($parentType == 'program') {
                        $prefix = 'school_program_name_';
                    } else if ($parentType == 'program-ta') {
                        $prefix = 'school_program_ta_';
                    } // END if

                    $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

                    $childFilterDatum->code_langs = $langContent;

                    array_push ($finalChildData, $childFilterDatum);
                } // END foreach

                $filterDatum->child = $finalChildData;
            } // END if

            array_push ($finalData, $filterDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'filter' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findSchoolByParentType
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $parent_type
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findSchoolByParentType(Request $request, $parent_type, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($parent_type)) {
            $code = 400;
            $comment = 'parent_type empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentType = $parent_type;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentTypeValidator = Validator::make(['parent_type' => $parentType],
            ['parent_type' => ['in:'.implode(',', config('tbl_filters.FILTERS_PARENT_TYPES'))]]
        );

        if ($parentTypeValidator->fails()) {
            $code = 422;
            $comment = 'parent_type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $type = config('tbl_filters.FILTERS_TYPE_SCHOOL');
        $orderby = ['id' => $orderWay];

        $filterData = $this->filterServ->findByTypeAndParentType($type, $parentType, $orderby, $page);

        if ($filterData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($filterData->all() as $filterDatum) {

            $code = $filterDatum->code;
            $parentId = $filterDatum->parent_id;
            $parentType = $filterDatum->parent_type;

            $subPrefix = '';

            if ($parentType == 'country') {
                $prefix = 'country_name_';
            } else if ($parentType == 'location') {
                $prefix = 'city_name_';

                $parentDatum = $this->filterServ->findById($parentId);
                $parentCode = $parentDatum->first()->code;

                $subPrefix = $parentCode;
            } else if ($parentType == 'school-type') {
                $prefix = 'school_type_title_';
            } else if ($parentType == 'program-type') {
                $prefix = 'school_program_type_';
            } else if ($parentType == 'program') {
                $prefix = 'school_program_name_';
            } else if ($parentType == 'program-ta') {
                $prefix = 'school_program_ta_';
            } // END if

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $filterDatum->code_langs = $langContent;

            array_push ($finalData, $filterDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'filters' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findSchoolByParentTypeAndParentId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $parent_type
     * @param  $parent_id
     *
     * @throws
     * @return
     */
    public function findSchoolByParentTypeAndParentId(Request $request, $parent_type, $parent_id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($parent_type)) {
            $code = 400;
            $comment = 'parent_type empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentType = $parent_type;

        if (empty($parent_id)) {
            $code = 400;
            $comment = 'parent_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentId = $parent_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentTypeValidator = Validator::make(['parent_type' => $parentType],
            ['parent_type' => ['in:'.implode(',', config('tbl_filters.FILTERS_PARENT_TYPES'))]]
        );

        if ($parentTypeValidator->fails()) {
            $code = 422;
            $comment = 'parent_type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $filterData = $this->filterServ->findByParentTypeAndParentId($parentType, $parentId, $orderby, $page);

        if ($filterData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($filterData->all() as $filterDatum) {

            $code = $filterDatum->code;
            $parentId = $filterDatum->parent_id;
            $parentType = $filterDatum->parent_type;

            $subPrefix = '';

            if ($parentType == 'country') {
                $prefix = 'country_name_';
            } else if ($parentType == 'location') {
                $prefix = 'city_name_';

                $parentDatum = $this->filterServ->findById($parentId);
                $parentCode = $parentDatum->first()->code;

                $subPrefix = $parentCode;
            } else if ($parentType == 'school-type') {
                $prefix = 'school_type_title_';
            } else if ($parentType == 'program-type') {
                $prefix = 'school_program_type_';
            } else if ($parentType == 'program') {
                $prefix = 'school_program_name_';
            } else if ($parentType == 'program-ta') {
                $prefix = 'school_program_ta_';
            } // END if

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $filterDatum->code_langs = $langContent;

            array_push ($finalData, $filterDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'filters' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findSchoolByParentTypeAndCode
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $parent_type
     * @param  $code
     *
     * @throws
     * @return
     */
    public function findSchoolByParentTypeAndCode(Request $request, $parent_type, $code)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($parent_type)) {
            $code = 400;
            $comment = 'parent_type empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentType = $parent_type;

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $parentTypeValidator = Validator::make(['parent_type' => $parentType],
            ['parent_type' => ['in:'.implode(',', config('tbl_filters.FILTERS_PARENT_TYPES'))]]
        );

        if ($parentTypeValidator->fails()) {
            $code = 422;
            $comment = 'parent_type error';

            Jsonponse::fail($comment, $code);
        } // END if


        $type = config('tbl_filters.FILTERS_TYPE_SCHOOL');
        $filterDatum = $filterServ->findByTypeAndParentTypeAndCode($type, $parentType, $code);

        if ($filterDatum->isEmpty()) {
            $code = 404;
            $comment = 'code error';

            Jsonponse::fail($comment, $code);
        } // END if


        $parentId = $filterDatum->first()->id;

        $filterData = $this->filterServ->findByParentTypeAndParentId($parentType, $parentId);

        if ($filterData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];

        foreach ($filterData->all() as $filterDatum) {

            $code = $filterDatum->code;
            $parentId = $filterDatum->parent_id;
            $parentType = $filterDatum->parent_type;

            $subPrefix = '';

            if ($parentType == 'country') {
                $prefix = 'country_name_';
            } else if ($parentType == 'location') {
                $prefix = 'city_name_';

                $parentDatum = $this->filterServ->findById($parentId);
                $parentCode = $parentDatum->first()->code;

                $subPrefix = $parentCode;
            } else if ($parentType == 'school-type') {
                $prefix = 'school_type_title_';
            } else if ($parentType == 'program-type') {
                $prefix = 'school_program_type_';
            } else if ($parentType == 'program') {
                $prefix = 'school_program_name_';
            } else if ($parentType == 'program-ta') {
                $prefix = 'school_program_ta_';
            } // END if

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $filterDatum->code_langs = $langContent;

            array_push ($finalData, $filterDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'filters' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
