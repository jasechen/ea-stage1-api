<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;


class Logout extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ  = app(SessionServ::class);
    } // END function


    /**
     * index
     *
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function index(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $token  = $request->input('token');

        if (!empty($token)) {
            $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

            if (empty($isValid)) {
                $code = 422;
                $comment = 'session token error';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $sessionDatum = $this->sessionServ->findByCode($sessionCode);

        if ($sessionDatum->isNotEmpty()) {
            $sessionFields = ['status' => config('tbl_sessions.SESSIONS_STATUS_LOGOUT')];
            $sessionWhere = ['id' => $sessionDatum->first()->id];
            $this->sessionServ->update($sessionFields, $sessionWhere);
        } // END if


        $request->session()->flush();
        $request->session()->getHandler()->destroy($sessionCode);


        $resultData = ['session' => $sessionCode];

        Jsonponse::success('logout success', $resultData, 200);
    } // END function


} // END class
