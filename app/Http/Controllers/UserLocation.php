<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\CompanyServ;
use App\Services\CompanyUserServ;
use App\Services\UserLocationServ;
use App\Services\SessionServ;


class UserLocation extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyUserServ = app(CompanyUserServ::class);
        $this->userLocationServ = app(UserLocationServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $userId = $request->input('user_id');
        $locations = $request->input('locations');

        if (empty($userId)) {
            $code = 400;
            $comment = 'user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($locations)) {
            $code = 400;
            $comment = 'locations empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userDatum = $this->userServ->findById($userId);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($userId);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        foreach ($locations as $location) {
            $userLocationDatum = $this->userLocationServ->findByUserIdAndCode($userId, $location);
            if ($userLocationDatum->isEmpty()) {
                $this->userLocationServ->create($userId, $location);
            } // END if
        } // END foreach


        $resultData = ['session' => $sessionCode, 'user_id' => $userId];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($id);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userLocationData = $this->userLocationServ->findByUserId($id);

        if ($userLocationData->isEmpty()) {
            $code = 404;
            $comment = 'user location error';

            Jsonponse::fail($comment, $code);
        } // END if


        $locations = $request->input('locations');

        $updateData = [];

        if (!empty($locations)) {
            foreach ($locations as $location) {
                array_push ($updateData, $location);
            } // ENd foreach
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!empty($updateData)) {

            foreach ($userLocationData->all() as $userLocationDatum) {
                $this->userLocationServ->delete(['id' => $userLocationDatum->id]);
            } // END foreach

            foreach ($updateData as $location) {
                $userLocationDatum = $this->userLocationServ->findByUserIdAndCode($id, $location);
                if ($userLocationDatum->isEmpty()) {
                    $this->userLocationServ->create($id, $location);
                } // END if
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($id);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userLocationData = $this->userLocationServ->findByUserId($id);

        if ($userLocationData->isEmpty()) {
            $code = 404;
            $comment = 'user location error';

            Jsonponse::fail($comment, $code);
        } // END if


        foreach ($userLocationData->all() as $userLocationDatum) {
            $this->userLocationServ->delete(['id' => $userLocationDatum->id]);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * findByUserId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByUserId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($id);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $userLocationData = $this->userLocationServ->findByUserId($id, $orderby, $page);

        if ($userLocationData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_locations' => $userLocationData->all()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
