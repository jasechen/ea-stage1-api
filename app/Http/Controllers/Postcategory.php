<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\PostcategoryServ;
use App\Services\PostcategorySeoServ;
use App\Services\UserServ;
use App\Services\DirServ;
use App\Services\FileServ;

use App\Services\SessionServ;


class Postcategory extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->postcategoryServ    = app(PostcategoryServ::class);
        $this->postcategorySeoServ = app(PostcategorySeoServ::class);
        $this->userServ    = app(UserServ::class);
        $this->dirServ    = app(DirServ::class);
        $this->fileServ    = app(FileServ::class);

        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $type   = $request->input('type', config('tbl_postcategories.DEFAULT_POSTCATEGORIES_TYPE'));
        $name   = $request->input('name');
        $cover   = $request->input('cover');
        $status   = $request->input('status', config('tbl_postcategories.DEFAULT_POSTCATEGORIES_STATUS'));

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');
        $ownerId = $request->input('owner_id', '0');

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($cover)) {
            $code = 400;
            $comment = 'cover empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($excerpt)) {
            $code = 400;
            $comment = 'excerpt empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogTitle)) {
            $code = 400;
            $comment = 'og_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogDescription)) {
            $code = 400;
            $comment = 'og_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaTitle)) {
            $code = 400;
            $comment = 'meta_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaDescription)) {
            $code = 400;
            $comment = 'meta_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverTitle)) {
            $code = 400;
            $comment = 'cover_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverAlt)) {
            $code = 400;
            $comment = 'cover_alt empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:' . implode(',', config('tbl_postcategories.POSTCATEGORIES_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_postcategories.POSTCATEGORIES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $postcategorySeoDatum = $this->postcategorySeoServ->findBySlug($slug);

        if ($postcategorySeoDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'slug error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findByFilename($cover);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'cover error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileId = $fileDatum->first()->id;
        $fileParentType = $fileDatum->first()->parent_type;
        $fileParentId = $fileDatum->first()->parent_id;
        $dirId = $fileDatum->first()->dir_id;

        $dirDatum = $this->dirServ->findById($dirId);

        if ($dirDatum->isEmpty()) {
            $code = 404;
            $comment = 'dir error';

            Jsonponse::fail($comment, $code);
        } // END if

        $dirParentType = $dirDatum->first()->parent_type;
        $dirParentId = $dirDatum->first()->parent_id;
        $dirType = $dirDatum->first()->type;

        $postcategoryDatum = $this->postcategoryServ->findByName($name);

        if ($postcategoryDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'name error';

            Jsonponse::fail($comment, $code);
        } // END if


        $postcategoryDatum = $this->postcategoryServ->create($type, $name, $cover, $status, $ownerId, $creatorId);

        if ($postcategoryDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $postcategoryId = $postcategoryDatum->first()->id;

        $this->postcategorySeoServ->create($postcategoryId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);


        $updatedata = ['parent_id' => $postcategoryId, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        $updateWhere = ['id' => $fileId];
        $this->fileServ->update($updatedata, $updateWhere);

        $updatedata = ['parent_id' => $postcategoryId, 'status' => config('tbl_dirs.DIRS_STATUS_ENABLE')];
        $updateWhere = ['id' => $dirId];
        $this->dirServ->update($updatedata, $updateWhere);


        $tPath = $dirParentType . '_' . $postcategoryId . '/' . $dirType;
        $pathExists = Storage::disk('gallery')->exists($tPath);
        if (empty($pathExists)) {
            Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
        } // END if

        $this->fileServ->moveTo($cover, $tPath);


        $resultData = ['session' => $sessionCode, 'postcategory_id' => $postcategoryId];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $postcategoryDatum = $this->postcategoryServ->getById($id);

        if ($postcategoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'postcategory error';

            Jsonponse::fail($comment, $code);
        } // END if


        $type = $request->input('type');
        $name = $request->input('name');
        $cover  = $request->input('cover');
        $status = $request->input('status');

        $updateData = [];

        if (!empty($type) AND $type != $postcategoryDatum->first()->type) {
            $typeValidator = Validator::make(['type' => $type],
                ['type' => ['in:' . implode(',', config('tbl_postcategories.POSTCATEGORIES_TYPES'))]]
            );

            if ($typeValidator->fails()) {
                $code = 422;
                $comment = 'type error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['type'] = $type;
        } // END if

        if (!empty($name) AND $name != $postcategoryDatum->first()->name) {
            $updateData['name'] = $name;
        } // END if

        if (!empty($cover) AND $cover != $postcategoryDatum->first()->cover) {
            $fileDatum = $this->fileServ->findByFilename($cover);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'cover error';

                Jsonponse::fail($comment, $code);
            } // END if

            $dirDatum = $this->dirServ->findById($fileDatum->first()->dir_id);

            $dirParentType = $dirDatum->first()->parent_type;
            $dirParentId = $dirDatum->first()->parent_id;
            $dirType = $dirDatum->first()->type;

            $updateData['cover'] = $cover;
        } // END if

        if (!empty($status) AND $status != $postcategoryDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_postcategories.POSTCATEGORIES_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $postcategoryDatum = $this->postcategoryServ->update($updateData, ['id' => $id]);

        if ($postcategoryDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($updateData['cover'])) {
            $updatedata = ['parent_id' => $postcategoryDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
            $updateWhere = ['id' => $fileDatum->first()->id];
            $this->fileServ->update($updatedata, $updateWhere);

            $this->fileServ->moveTo($cover, $dirParentType . '_' . $dirParentId . '/' . $dirType);
        } // END if


        $resultData = ['session' => $sessionCode, 'postcategory_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $postcategoryDatum = $this->postcategoryServ->findById($id);

        if ($postcategoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'postcategory error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($postcategoryDatum->first()->status == config('tbl_postcategories.POSTCATEGORIES_STATUS_DELETE')) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFields = ['status' => config('tbl_postcategories.POSTCATEGORIES_STATUS_DELETE')];
        $updateWhere = ['id' => $id];
        $postcategoryDatum = $this->postcategoryServ->update($updateFields, $updateWhere);

        if ($postcategoryDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'postcategory_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $postcategoryDatum = $this->postcategoryServ->getById($id);

        if ($postcategoryDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $postcategoryDatum->first()->cover_links = [];
        if (!empty($postcategoryDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
            $postcategoryDatum->first()->cover_links = $coverLinks;
        } // END if else

        $postcategoryDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $postcategoryDatum->first()->url_path = empty($postcategoryDatum->first()->slug) ? '' : '/' . $postcategoryDatum->first()->slug . '-c' . $postcategoryDatum->first()->id;


        $resultData = ['session' => $sessionCode, 'postcategory' => $postcategoryDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findList
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $status = 'enable', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_postcategories.POSTCATEGORIES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['c.id' => $orderWay];
        $status = $status == 'all' ? '' : $status;

        $postcategoryData = $this->postcategoryServ->getList($status, $orderby, $page);

        if ($postcategoryData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postcategoryData->all() as $postcategoryDatum) {

            $postcategoryDatum->cover_links = [];
            if (!empty($postcategoryDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->cover);
                $postcategoryDatum->cover_links = $coverLinks;
            } // END if else

            $postcategoryDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postcategoryDatum->url_path = empty($postcategoryDatum->slug) ? '' : '/' . $postcategoryDatum->slug . '-c' . $postcategoryDatum->id;

            array_push($finalData, $postcategoryDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'postcategories' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findBySlug
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     *
     * @throws
     * @return
     */
    public function findBySlug(Request $request, $slug)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $postcategorySeoDatum = $this->postcategorySeoServ->findBySlug($slug);

        if ($postcategorySeoDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $postcategoryDatum = $this->postcategoryServ->getById($postcategorySeoDatum->first()->postcategory_id);

        $postcategoryDatum->first()->cover_links = [];
        if (!empty($postcategoryDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
            $postcategoryDatum->first()->cover_links = $coverLinks;
        } // END if else

        $postcategoryDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $postcategoryDatum->first()->url_path = empty($postcategoryDatum->first()->slug) ? '' : '/' . $postcategoryDatum->first()->slug . '-c' . $postcategoryDatum->first()->id;


        $resultData = ['session' => $sessionCode, 'postcategory' => $postcategoryDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByType
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $type
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByType(Request $request, $type, $status = 'enable', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:'.implode(',',config('tbl_postcategories.POSTCATEGORIES_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_postcategories.POSTCATEGORIES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['c.id' => $orderWay];
        $status = $status == 'all' ? '' : $status;

        $postcategoryData = $this->postcategoryServ->getByType($type, $status, $orderby, $page);

        if ($postcategoryData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postcategoryData->all() as $postcategoryDatum) {

            $postcategoryDatum->cover_links = [];
            if (!empty($postcategoryDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->cover);
                $postcategoryDatum->cover_links = $coverLinks;
            } // END if else

            $postcategoryDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postcategoryDatum->url_path = empty($postcategoryDatum->slug) ? '' : '/' . $postcategoryDatum->slug . '-c' . $postcategoryDatum->id;

            array_push($finalData, $postcategoryDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'postcategories' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByOwnerId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $owner_id
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByOwnerId(Request $request, $owner_id, $status = 'enable', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_postcategories.POSTCATEGORIES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['c.id' => $orderWay];
        $ownerId = $owner_id;
        $status = $status == 'all' ? '' : $status;

        $postcategoryData = $this->postcategoryServ->getByOwnerId($ownerId, $status, $orderby, $page);

        if ($postcategoryData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postcategoryData->all() as $postcategoryDatum) {

            $postcategoryDatum->cover_links = [];
            if (!empty($postcategoryDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->cover);
                $postcategoryDatum->cover_links = $coverLinks;
            } // END if else

            $postcategoryDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postcategoryDatum->url_path = empty($postcategoryDatum->slug) ? '' : '/' . $postcategoryDatum->slug . '-c' . $postcategoryDatum->id;

            array_push($finalData, $postcategoryDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'postcategories' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByTypeAndOwnerId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $type
     * @param  $owner_id
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByTypeAndOwnerId(Request $request, $type, $owner_id, $status = 'enable', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:'.implode(',',config('tbl_postcategories.POSTCATEGORIES_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 422;
            $comment = 'type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_postcategories.POSTCATEGORIES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['c.id' => $orderWay];
        $ownerId = $owner_id;
        $status = $status == 'all' ? '' : $status;

        $postcategoryData = $this->postcategoryServ->getByTypeAndOwnerId($type, $ownerId, $status, $orderby, $page);

        if ($postcategoryData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postcategoryData->all() as $postcategoryDatum) {

            $postcategoryDatum->cover_links = [];
            if (!empty($postcategoryDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->cover);
                $postcategoryDatum->cover_links = $coverLinks;
            } // END if else

            $postcategoryDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postcategoryDatum->url_path = empty($postcategoryDatum->slug) ? '' : '/' . $postcategoryDatum->slug . '-c' . $postcategoryDatum->id;

            array_push($finalData, $postcategoryDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'postcategories' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function



    /**
     * Upload Cover
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function uploadCover(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $ownerId = $request->input('owner_id', '0');
        $grandId = $parentId = '0';

        $grandType = config('tbl_dirs.DIRS_PARENT_TYPE_POSTCATEGORY');
        $type = config('tbl_dirs.DIRS_TYPE_COVER');
        $parentType = config('tbl_files.FILES_PARENT_TYPE_POSTCATEGORY');

        $privacyStatus = $request->input('privacy_status', config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC'));
        $status = $request->input('status', config('tbl_files.FILES_STATUS_DISABLE'));

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_files.FILES_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($grandType, $grandId, $type, $ownerId);

        if ($dirDatum->isEmpty()) {
            $dirDatum = $this->dirServ->initDefaultAlbum($grandType, $grandId, $type, $ownerId, $privacyStatus, $status, $creatorId);
        } // END if

        $dirId = $dirDatum->first()->id;

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $grandId, $grandType, $type);


        $fileDatum = $this->fileServ->create($dirId, $parentType, $parentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $ownerId, false, $fileType, $privacyStatus, $status, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $coverLinks = $this->fileServ->findLinks($filename);

        $resultData = ['session' => $sessionCode, 'filename' => $filename, 'cover_links' => $coverLinks];

        Jsonponse::success('upload cover success', $resultData, 201);
    } // END function


} // END class
