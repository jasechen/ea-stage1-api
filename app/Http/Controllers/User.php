<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Mail\VerifyEmail;
use App\Services\UserServ;
use App\Services\UserSeoServ;
use App\Services\UserProfileServ;
use App\Services\UserRoleServ;
use App\Services\UserCheckedOptionServ;
use App\Services\RoleServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\BlogServ;
use App\Services\SessionServ;

use Aloha\Twilio\Support\Laravel\Facade as Twilio;


class User extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->userSeoServ = app(UserSeoServ::class);
        $this->userProfileServ = app(UserProfileServ::class);
        $this->userRoleServ = app(UserRoleServ::class);
        $this->userCheckedOptionServ = app(UserCheckedOptionServ::class);
        $this->roleServ = app(RoleServ::class);
        $this->dirServ = app(DirServ::class);
        $this->fileServ = app(FileServ::class);
        $this->blogServ = app(BlogServ::class);
        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param
     * @param
     * @param
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $account  = $request->input('account');
        $password = $request->input('password');
        $passwordRepeat = $request->input('password_repeat');
        $status   = $request->input('status', config('tbl_users.DEFAULT_USERS_STATUS'));

        $firstName = $request->input('first_name');
        $lastName  = $request->input('last_name');
        $mobileCountryCode = $request->input('mobile_country_code');
        $mobilePhone = $request->input('mobile_phone');

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $avatarTitle = $request->input('avatar_title');
        $avatarAlt = $request->input('avatar_alt');

        $roleIds = $request->input('role_ids', []);

        if (empty($account)) {
            $code = 400;
            $comment = 'account empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($password) or empty($passwordRepeat)) {
            $code = 400;
            $comment = 'password / password_repeat empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($firstName)) {
            $code = 400;
            $comment = 'first_name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($lastName)) {
            $code = 400;
            $comment = 'last_name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($mobileCountryCode)) {
            $code = 400;
            $comment = 'mobile_country_code empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($mobilePhone)) {
            $code = 400;
            $comment = 'mobile_phone is empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($excerpt)) {
            $code = 400;
            $comment = 'excerpt empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogTitle)) {
            $code = 400;
            $comment = 'og_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogDescription)) {
            $code = 400;
            $comment = 'og_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaTitle)) {
            $code = 400;
            $comment = 'meta_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaDescription)) {
            $code = 400;
            $comment = 'meta_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($avatarTitle)) {
            $code = 400;
            $comment = 'avatar_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($avatarAlt)) {
            $code = 400;
            $comment = 'avatar_alt empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $accountValidator = Validator::make(['account' => $account],
            ['account' => 'email']
        );

        if ($accountValidator->fails()) {
            $code = 422;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if

        // $passwordValidator = Validator::make(['password' => $password],
        //     ['password' => 'size:' . config('app.PASSWORD_LENGTH_LIMIT')]
        // );

        // if ($passwordValidator->fails()) {
        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 422;
            $comment = 'password\' length < ' . config('app.PASSWORD_LENGTH_LIMIT');

            Jsonponse::fail($comment, $code);
        } // END if

        if ($password != $passwordRepeat) {
            $code = 422;
            $comment = 'password / password_repeat is NOT equal';

            Jsonponse::fail($comment, $code);
        } // END if

        $phone = '+' . $mobileCountryCode . $mobilePhone;
        $mobilePhoneValidator = Validator::make(['mobile_phone' => $phone],
            ['mobile_phone' => 'phone:AUTO,mobile']
        );

        if ($mobilePhoneValidator->fails()) {
            $code = 422;
            $comment = 'mobile_phone format error';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_users.USERS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userSeoDatum = $this->userSeoServ->findBySlug($slug);

        if ($userSeoDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'slug error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findByAccount($account);

        if ($userDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userProfileDatum = $this->userProfileServ->findByMobilePhone($mobilePhone);

        if ($userProfileDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'mobile_phone error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->create($account, $password, $status, $creatorId);

        if ($userDatum->isEmpty()) {
            $code = 500;
            $comment = 'create user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userSeoDatum = $this->userSeoServ->findByUserId($userDatum->first()->id);
        if ($userSeoDatum->isEmpty()) {
            $this->userSeoServ->create($userDatum->first()->id, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $avatarTitle, $avatarAlt);
        } // END if


        $userProfileDatum = $this->userProfileServ->findByUserId($userDatum->first()->id);

        if ($userProfileDatum->isEmpty()) {
            $userProfileDatum = $this->userProfileServ->create($userDatum->first()->id, $account, $mobileCountryCode, $mobilePhone, $firstName, $lastName);
        } // END if

        $userDatum->first()->profile = $userProfileDatum->first();

        $roleDatum = $this->roleServ->findByTypeAndCode('general', 'member');

        if (!in_array($roleDatum->first()->id, $roleIds)) {
            $roleIds[] = $roleDatum->first()->id;
        }  // END if

        foreach ($roleIds as $roleId) {
            $roleDatum = $this->roleServ->findById($roleId);
            if ($roleDatum->isEmpty()) {continue;}

            $userRoleDatum = $this->userRoleServ->findByUserIdAndRoleId($userDatum->first()->id, $roleId);
            if ($userRoleDatum->isNotEmpty()) {continue;}

            $this->userRoleServ->create($userDatum->first()->id, $roleId, $creatorId);

            if ($roleDatum->first()->type == 'general' AND $roleDatum->first()->code == 'author') {
                $this->blogServ->create($userDatum->first()->id, $creatorId);
            }  // END if
        } // END foreach


        $userEmailCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'email');

        if ($userEmailCheckedOptionDatum->isEmpty()) {
            $emailCheckedStatus = $status == config('tbl_users.USERS_STATUS_ENABLE') ? config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED') : config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT');
            $userEmailCheckedOptionDatum = $this->userCheckedOptionServ->create($userDatum->first()->id, 'email', $emailCheckedStatus);

            if ($emailCheckedStatus == config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT')) {
                $to = $userDatum->first()->account;

                $emailFields = ['user' => $userDatum->first(),
                                'code' => $userEmailCheckedOptionDatum->first()->code,
                                'link' => env('WEB_URL') . '/verify/' . $userDatum->first()->id];
                Mail::to($to)->send(new VerifyEmail($emailFields));
            } // END if
        } // END if

        $userMobilePhoneCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'mobile_phone');

        if ($userMobilePhoneCheckedOptionDatum->isEmpty()) {
            $mobileCheckedStatus = $status == config('tbl_users.USERS_STATUS_ENABLE') ? config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED') : config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT');
            $userMobilePhoneCheckedOptionDatum = $this->userCheckedOptionServ->create($userDatum->first()->id, 'mobile_phone', $mobileCheckedStatus);

            if ($mobileCheckedStatus == config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT')) {
                $phone = '+' . $mobileCountryCode . $mobilePhone;
                $message = '您的 English.Agency「註冊驗證碼」為 ' . $userMobilePhoneCheckedOptionDatum->first()->code;
                Twilio::message($phone, $message);
            } // END if
        } // END if


        $noDir  = $this->dirServ->initDefaultDir('user', $userDatum->first()->id, $userDatum->first()->id);
        $noPath = $noDir->first()->parent_type . '_' . $noDir->first()->parent_id . '/' . $noDir->first()->type;
        Storage::disk('gallery')->makeDirectory($noPath, 0755, true);
        // Storage::disk('s3-gallery')->makeDirectory($noPath, 0755, true);

        $avatarDir  = $this->dirServ->initDefaultAlbum('user', $userDatum->first()->id, 'avatar', $userDatum->first()->id);
        $avatarPath = $avatarDir->first()->parent_type . '_' . $userDatum->first()->id . '/' . $avatarDir->first()->type;
        Storage::disk('gallery')->makeDirectory($avatarPath, 0755, true);
        // Storage::disk('s3-gallery')->makeDirectory($avatarPath, 0755, true);


        $resultData = ['session' => $sessionCode, 'user_id' => $userDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $userDatum = $this->userServ->getById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $status = $request->input('status');

        $updateData = [];

        if (!empty($status) AND $status != $userDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_users.USERS_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->update($updateData, ['id' => $id]);

        if ($userDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFields = ['status' => config('tbl_users.USERS_STATUS_DELETE')];
        $updateWhere = ['id' => $id];
        $userDatum = $this->userServ->update($updateFields, $updateWhere);

        if ($userDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->getById($id);

        if ($userDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $profileDatum = $this->userProfileServ->findByUserId($id);

        $profileDatum->first()->avatar_links = [];
        if (!empty($profileDatum->first()->avatar)) {
            $avatarLinks = $this->fileServ->findLinks($profileDatum->first()->avatar);
            $profileDatum->first()->avatar_links = $avatarLinks;
        } // END if else

        $userDatum->first()->profile = $profileDatum->first();


        $userDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $userDatum->first()->url_path = empty($userDatum->first()->slug) ? '' : '/' . $userDatum->first()->slug . '-u' . $userDatum->first()->id;
        unset($userDatum->first()->password);


        $resultData = ['session' => $sessionCode, 'user' => $userDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findList
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $status = 'enable', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_users.USERS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];
        $status = $status == 'all' ? '' : $status;

        $userData = $this->userServ->getList($status, $orderby, $page);

        if ($userData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($userData->all() as $userDatum) {

            $profileDatum = $this->userProfileServ->findByUserId($userDatum->id);

            $profileDatum->first()->avatar_links = [];
            if (!empty($profileDatum->first()->avatar)) {
                $avatarLinks = $this->fileServ->findLinks($profileDatum->first()->avatar);
                $profileDatum->first()->avatar_links = $avatarLinks;
            } // END if else

            $userDatum->profile = $profileDatum->first();

            $userDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $userDatum->url_path = empty($userDatum->slug) ? '' : '/' . $userDatum->slug . '-u' . $userDatum->id;
            unset($userDatum->password);

            array_push($finalData, $userDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'users' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findBySlug
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     *
     * @throws
     * @return
     */
    public function findBySlug(Request $request, $slug)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $userSeoDatum = $this->userSeoServ->findBySlug($slug);

        if ($userSeoDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $userDatum = $this->userServ->getById($userSeoDatum->first()->user_id);


        $profileDatum = $this->userProfileServ->findByUserId($userDatum->first()->id);

        $profileDatum->first()->avatar_links = [];
        if (!empty($profileDatum->first()->avatar)) {
            $avatarLinks = $this->fileServ->findLinks($profileDatum->first()->avatar);
            $profileDatum->first()->avatar_links = $avatarLinks;
        } // END if else

        $userDatum->first()->profile = $profileDatum->first();


        $userDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $userDatum->first()->url_path = empty($userDatum->first()->slug) ? '' : '/' . $userDatum->first()->slug . '-u' . $userDatum->first()->id;
        unset($userDatum->first()->password);


        $resultData = ['session' => $sessionCode, 'user' => $userDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByToken
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $token
     *
     * @throws
     * @return
     */
    public function findByToken(Request $request, $token)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findByToken($token);

        if ($userDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $profileDatum = $this->userProfileServ->findByUserId($userDatum->first()->id);

        $profileDatum->first()->avatar_links = [];
        if (!empty($profileDatum->first()->avatar)) {
            $avatarLinks = $this->fileServ->findLinks($profileDatum->first()->avatar);
            $profileDatum->first()->avatar_links = $avatarLinks;
        } // END if else

        $userDatum->first()->profile = $profileDatum->first();


        $UECODatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'email');
        $userDatum->first()->email_checked = $UECODatum->first()->status == config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED') ? true : false;
        $UMCODatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'mobile_phone');
        $userDatum->first()->mobile_checked = $UMCODatum->first()->status == config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_CHECKED') ? true : false;


        $userDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $userDatum->first()->url_path = empty($userDatum->first()->slug) ? '' : '/' . $userDatum->first()->slug . '-u' . $userDatum->first()->id;
        unset($userDatum->first()->password);

        $userDatum->first()->roles = [];
        $userRoleData = $this->userRoleServ->findByUserId($userDatum->first()->id);
        if ($userRoleData->isNotEmpty()) {
            foreach ($userRoleData->all() as $userRoleDatum) {

                $roleDatum = $this->roleServ->findById($userRoleDatum->role_id);
                if ($roleDatum->isEmpty()) continue;
                array_push ($userDatum->first()->roles, ['type' => $roleDatum->first()->type, 'code' => $roleDatum->first()->code]);
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'user' => $userDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * Update password
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function updatePassword(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $userDatum = $this->userServ->getById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $password = $request->input('password');
        $passwordRepeat = $request->input('password_repeat');

        $updateData = [];

        if (!empty($password) AND !empty($passwordRepeat)) {
            $passwordValidator = Validator::make(['password' => $password],
                ['password' => 'size:' . config('app.PASSWORD_LENGTH_LIMIT')]
            );

            if ($passwordValidator->fails()) {
                $code = 422;
                $comment = 'password\' length < ' . config('app.PASSWORD_LENGTH_LIMIT');

                Jsonponse::fail($comment, $code);
            } // END if

            if ($password != $passwordRepeat) {
                $code = 422;
                $comment = 'password / password_repeat is NOT equal';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['password'] = $password;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->updatePassword($updateData['password'], $id);

        if ($userDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('update password success', $resultData, 201);
    } // END function



} // END class
