<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\CompanyServ;
use App\Services\CompanyUserServ;
use App\Services\AgentProfileServ;
use App\Services\LocalesServ;
use App\Services\SessionServ;


class AgentProfile extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyUserServ = app(CompanyUserServ::class);
        $this->agentProfileServ = app(AgentProfileServ::class);
        $this->localesServ = app(LocalesServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $userId = $request->input('user_id');
        $schoolTypes = $request->input('expertly_school_types');
        $countries = $request->input('familiar_countries');
        $locations = $request->input('service_locations');
        $serviceYears = $request->input('service_years');
        $experience = $request->input('experience');

        if (empty($userId)) {
            $code = 400;
            $comment = 'user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($schoolTypes)) {
            $code = 400;
            $comment = 'expertly_school_types empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($countries)) {
            $code = 400;
            $comment = 'familiar_countries empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($locations)) {
            $code = 400;
            $comment = 'service_locations empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($serviceYears)) {
            $code = 400;
            $comment = 'service_years empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($experience)) {
            $code = 400;
            $comment = 'experience empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userDatum = $this->userServ->findById($userId);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($userId);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $createAgentProfileDatum = $this->agentProfileServ->create($userId, $schoolTypes, $countries, $locations, $serviceYears, $experience);

        if ($createAgentProfileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $userId];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($id);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $agentProfileDatum = $this->agentProfileServ->findByUserId($id);

        if ($agentProfileDatum->isEmpty()) {
            $code = 404;
            $comment = 'agent profile error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolTypes = $request->input('expertly_school_types');
        $countries = $request->input('familiar_countries');
        $locations = $request->input('service_locations');
        $serviceYears = $request->input('service_years');
        $experience = $request->input('experience');

        $updateData = [];

        if (!empty($schoolTypes) AND $schoolTypes != $agentProfileDatum->first()->expertly_school_types) {
            $updateData['expertly_school_types'] = $schoolTypes;
        } // END if

        if (!empty($countries) AND $countries != $agentProfileDatum->first()->familiar_countries) {
            $updateData['familiar_countries'] = $countries;
        } // END if

        if (!empty($locations) AND $locations != $agentProfileDatum->first()->service_locations) {
            $updateData['service_locations'] = $locations;
        } // END if

        if (!empty($serviceYears) AND $serviceYears != $agentProfileDatum->first()->service_years) {
            $updateData['service_years'] = $serviceYears;
        } // END if

        if (!empty($experience) AND $experience != $agentProfileDatum->first()->experience) {
            $updateData['experience'] = $experience;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $agentProfileDatum->first()->id];
        $updateAgentProfileDatum = $this->agentProfileServ->update($updateData, $updateWhere);

        if ($updateAgentProfileDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $cuData = $this->companyUserServ->findByUserId($id);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $agentProfileDatum = $this->agentProfileServ->findByUserId($id);

        if ($agentProfileDatum->isEmpty()) {
            $code = 404;
            $comment = 'agent profile error';

            Jsonponse::fail($comment, $code);
        } // END if

        $agentProfileId = $agentProfileDatum->first()->id;


        $deleteAgentProfileDatum = $this->agentProfileServ->delete(['id' => $agentProfileId]);

        if (empty($deleteAgentProfileDatum)) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * findByUserId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByUserId(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $cuData = $this->companyUserServ->findByUserId($id);

        if ($cuData->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $agentProfileDatum = $this->agentProfileServ->findByUserId($id);

        if ($agentProfileDatum->isEmpty()) {
            $code = 404;
            $comment = 'agent profile error';

            Jsonponse::fail($comment, $code);
        } // END if


        $prefix = 'country_name_';
        $agentProfileDatum->first()->familiar_country_langs = [];
        $countries = $agentProfileDatum->first()->familiar_countries;

        if (!empty($countries)) {
            $countries = explode(',', $countries);

            foreach ($countries as $code) {
                $code = trim($code);
                $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);
                $agentProfileDatum->first()->familiar_country_langs[$code] = $langContent;
            } // END foreach
        }

        $prefix = 'city_name_';
        $subPrefix = 'tw';
        $agentProfileDatum->first()->service_location_langs = [];
        $locations = $agentProfileDatum->first()->service_locations;

        if (!empty($locations)) {
            $locations = explode(',', $locations);

            foreach ($locations as $code) {
                $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);
                $agentProfileDatum->first()->service_location_langs[$code] = $langContent;
            } // END foreach
        }


        $resultData = ['session' => $sessionCode, 'agent_profile' => $agentProfileDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findFamiliarCountries
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findFamiliarCountries(Request $request)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $prefix = 'country_name_';
        $countries = config('global_countries.FAMILIAR_COUNTRIES');

        $finalData = [];

        foreach ($countries as $code) {

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);

            $finalData[$code] = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'countries' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findServiceLocations
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findServiceLocations(Request $request)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $prefix = 'city_name_';
        $subPrefix = 'TW';
        $locations = config('global_cities.SERVICE_LOACTIONS');

        $finalData = [];

        foreach ($locations as $code) {

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $finalData[$code] = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'locations' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function

} // END class
