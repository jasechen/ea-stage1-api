<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Mail\VerifyEmail;
use App\Services\UserServ;
use App\Services\UserProfileServ;
use App\Services\UserCheckedOptionServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\SessionServ;

use Aloha\Twilio\Support\Laravel\Facade as Twilio;


class UserProfile extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->userProfileServ = app(UserProfileServ::class);
        $this->userCheckedOptionServ = app(UserCheckedOptionServ::class);
        $this->dirServ = app(DirServ::class);
        $this->fileServ = app(FileServ::class);

        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $userDatum = $this->userServ->getById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userProfileDatum = $this->userProfileServ->findByUserId($userDatum->first()->id);

        if ($userProfileDatum->isEmpty()) {
            $code = 404;
            $comment = 'user profile error';

            Jsonponse::fail($comment, $code);
        } // END if


        $firstName = $request->input('first_name');
        $lastName  = $request->input('last_name');
        $nickname  = $request->input('nickname');
        $email  = $request->input('email');
        $mobileCountryCode = $request->input('mobile_country_code');
        $mobilePhone = $request->input('mobile_phone');
        $line = $request->input('line');
        // $avatar  = $request->input('avatar');
        $birth  = $request->input('birth');
        $bio  = $request->input('bio');
        $experience = $request->input('experience');
        $serviceYears = $request->input('service_years');

        $updateData = [];

        if (!empty($firstName) AND $firstName != $userProfileDatum->first()->first_name) {
            $updateData['first_name'] = $firstName;
        } // END if

        if (!empty($lastName) AND $lastName != $userProfileDatum->first()->last_name) {
            $updateData['last_name'] = $lastName;
        } // END if

        if (!empty($nickname) AND $nickname != $userProfileDatum->first()->nickname) {
            $updateData['nickname'] = $nickname;
        } // END if

        if (!empty($email) AND $email != $userProfileDatum->first()->email) {
            $emailValidator = Validator::make(['email' => $email],
                ['email' => 'email']
            );

            if ($emailValidator->fails()) {
                $code = 422;
                $comment = 'email error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['email'] = $email;
        } // END if

        if (!empty($mobileCountryCode) AND $mobileCountryCode != $userProfileDatum->first()->mobile_country_code) {
            $updateData['mobile_country_code'] = $mobileCountryCode;
        } // END if

        if (!empty($mobilePhone) AND $mobilePhone != $userProfileDatum->first()->mobile_phone) {

            $profileDatum = $this->userProfileServ->findByMobilePhone($mobilePhone);

            if ($profileDatum->isNotEmpty() AND $profileDatum->first()->id != $userProfileDatum->first()->id) {
                $code = 409;
                $comment = 'mobile_phone error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['mobile_phone'] = $mobilePhone;
        } // END if

        if (!empty($updateData['mobile_country_code']) OR !empty($updateData['mobile_phone'])) {

            $mcc = empty($updateData['mobile_country_code']) ? $userProfileDatum->first()->mobile_country_code : $updateData['mobile_country_code'];
            $mp  = empty($updateData['mobile_phone']) ? $userProfileDatum->first()->mobile_phone : $updateData['mobile_phone'];

            $phone = '+' . $mcc . $mp;
            $mobilePhoneValidator = Validator::make(['mobile_phone' => $phone],
                ['mobile_phone' => 'phone:AUTO,mobile']
            );

            if ($mobilePhoneValidator->fails()) {
                $code = 422;
                $comment = 'mobile_phone format error';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        if (!empty($line) AND $line != $userProfileDatum->first()->line) {
            $updateData['line'] = $line;
        } // END if

        // if (!empty($avatar) AND $avatar != $userProfileDatum->first()->avatar) {
        //     $fileDatum = $this->fileServ->findByFilename($avatar);

        //     if ($fileDatum->isEmpty()) {
        //         $code = 404;
        //         $comment = 'avatar error';

        //         Jsonponse::fail($comment, $code);
        //     } // END if

        //     $dirDatum = $this->dirServ->findById($fileDatum->first()->dir_id);

        //     $updateData['avatar'] = $avatar;
        // } // END if

        if (!empty($birth) AND $birth != $userProfileDatum->first()->birth) {
            $birthValidator = Validator::make(['birth' => $birth],
                ['birth' => ['before:10 years ago']]
            );

            if ($birthValidator->fails()) {
                $code = 422;
                $comment = 'birth error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['birth'] = $birth;
        } // END if

        if (!empty($bio) AND $bio != $userProfileDatum->first()->bio) {
            $updateData['bio'] = $bio;
        } // END if

        if (!empty($experience) AND $experience != $userProfileDatum->first()->experience) {
            $updateData['experience'] = $experience;
        } // END if

        if (!empty($serviceYears) AND $serviceYears != $userProfileDatum->first()->service_years) {
            $updateData['service_years'] = $serviceYears;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $userProfileDatum->first()->id];
        $userProfileDatum = $this->userProfileServ->update($updateData, $updateWhere);

        if ($userProfileDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($updateData['email'])) {
            $userEmailCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'email');

            if ($userEmailCheckedOptionDatum->isNotEmpty()) {
                $userEmailCheckedOptionDatum = $this->userCheckedOptionServ->resetCodeAndStatus($userEmailCheckedOptionDatum->first()->id);

                if ($userEmailCheckedOptionDatum->isNotEmpty()) {
                    $userDatum->first()->profile = $userProfileDatum->first();

                    $emailFields = ['user' => $userDatum->first(),
                                    'code' => $userEmailCheckedOptionDatum->first()->code,
                                    'link' => env('WEB_URL') . '/verify/' . $userDatum->first()->id];
                    Mail::to($updateData['email'])->send(new VerifyEmail($emailFields));
                } // END if
            } // END if
        } // END if

        if (!empty($updateData['mobile_country_code']) OR !empty($updateData['mobile_phone'])) {
            $userMobilePhoneCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'mobile_phone');

            if ($userMobilePhoneCheckedOptionDatum->isNotEmpty()) {
                $userMobilePhoneCheckedOptionDatum = $this->userCheckedOptionServ->resetCodeAndStatus($userMobilePhoneCheckedOptionDatum->first()->id);

                if ($userMobilePhoneCheckedOptionDatum->isNotEmpty()) {
                    $mobileCountryCode = $userProfileDatum->first()->mobile_country_code;
                    $mobilePhone  = $userProfileDatum->first()->mobile_phone;

                    $phone = '+' . $mobileCountryCode . $mobilePhone;
                    $message = '您的 English.Agency「註冊驗證碼」為 ' . $userMobilePhoneCheckedOptionDatum->first()->code;
                    Twilio::message($phone, $message);
                } // END if
            } // END if
        } // END if

        // if (!empty($updateData['avatar'])) {
        //     $updateFields = ['parent_id' => $userDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        //     $updateWhere = ['id' => $fileDatum->first()->id];
        //     $this->fileServ->update($updateFields, $updateWhere);

        //     $this->fileServ->moveTo($avatar, 'user_' . $userDatum->first()->id . '/' . $dirDatum->first()->type);
        // } // END if


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('update profile success', $resultData, 201);
    } // END function


    /**
     * Upload Avatar
     *
     * @method  PUT
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function updateAvatar(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $ownerId = $id;
        $grandId = $parentId = $ownerId;

        $grandType = config('tbl_dirs.DIRS_PARENT_TYPE_USER');
        $type = config('tbl_dirs.DIRS_TYPE_AVATAR');
        $parentType = config('tbl_files.FILES_PARENT_TYPE_USER');

        $privacyStatus = $request->input('privacy_status', config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC'));
        $status = $request->input('status', config('tbl_files.FILES_STATUS_DISABLE'));


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_files.FILES_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $ownerDatum = $this->userServ->findById($ownerId);

        if ($ownerDatum->isEmpty()) {
            $code = 404;
            $comment = 'owner error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($ownerDatum->first()->status != config('tbl_users.USERS_STATUS_ENABLE')) {
            $code = 422;
            $comment = 'owner status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($grandType, $grandId, $type, $ownerId);

        if ($dirDatum->isEmpty()) {
            $dirDatum = $this->dirServ->initDefaultAlbum($grandType, $grandId, $type, $ownerId, $privacyStatus, $status, $creatorId);
        } // END if

        $dirId = $dirDatum->first()->id;

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $grandId, $grandType, $type);


        $fileDatum = $this->fileServ->create($dirId, $parentType, $parentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $ownerId, false, $fileType, $privacyStatus, $status, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userProfileDatum = $this->userProfileServ->findByUserId($ownerId);

        $updateFields = ['avatar' => $filename];
        $updateWhere = ['id' => $userProfileDatum->first()->id];
        $this->userProfileServ->update($updateFields, $updateWhere);


        $avatarLinks = $this->fileServ->findLinks($filename);

        $resultData = ['session' => $sessionCode, 'filename' => $filename, 'avatar_links' => $avatarLinks];

        Jsonponse::success('upload avatar success', $resultData, 201);
    } // END function


} // END class
