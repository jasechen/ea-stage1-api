<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\CompanyServ;
use App\Services\CompanyContactServ;
use App\Services\SessionServ;
use App\Services\LocalesServ;


class CompanyContact extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyContactServ = app(CompanyContactServ::class);
        $this->sessionServ = app(SessionServ::class);
        $this->localesServ = app(LocalesServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyId = $request->input('company_id');
        $username = $request->input('username');
        $country = $request->input('country');
        $state = $request->input('state');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $fax = $request->input('fax');

        if (empty($companyId)) {
            $code = 400;
            $comment = 'company_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($username)) {
            $code = 400;
            $comment = 'username empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($country)) {
            $code = 400;
            $comment = 'country empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($state)) {
            $code = 400;
            $comment = 'state empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($address)) {
            $code = 400;
            $comment = 'address empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($phone)) {
            $code = 400;
            $comment = 'phone empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($fax)) {
            $code = 400;
            $comment = 'fax empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyDatum = $this->companyServ->findById($companyId);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if


        $createCompanyContactDatum = $this->companyContactServ->create($companyId, $username, $country, $state, $address, $phone, $fax);

        if ($createCompanyContactDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_contact_id' => $createCompanyContactDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $company_contact_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_contact_id)) {
            $code = 400;
            $comment = 'company_contact_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyContactId = $company_contact_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyContactDatum = $this->companyContactServ->findById($companyContactId);

        if ($companyContactDatum->isEmpty()) {
            $code = 404;
            $comment = 'company contact error';

            Jsonponse::fail($comment, $code);
        } // END if


        $username = $request->input('username');
        $country = $request->input('country');
        $state = $request->input('state');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $fax = $request->input('fax');

        $updateData = [];

       if (!empty($username) AND $username != $companyContactDatum->first()->username) {
            $updateData['username'] = $username;
        } // END if

       if (!empty($country) AND $country != $companyContactDatum->first()->country) {
            $updateData['country'] = $country;
        } // END if

       if (!empty($state) AND $state != $companyContactDatum->first()->state) {
            $updateData['state'] = $state;
        } // END if

       if (!empty($address) AND $address != $companyContactDatum->first()->address) {
            $updateData['address'] = $address;
        } // END if

       if (!empty($phone) AND $phone != $companyContactDatum->first()->phone) {
            $updateData['phone'] = $phone;
        } // END if

       if (!empty($fax) AND $fax != $companyContactDatum->first()->fax) {
            $updateData['fax'] = $fax;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $companyContactDatum->first()->id];
        $updateCompanyContactDatum = $this->companyContactServ->update($updateData, $updateWhere);

        if ($updateCompanyContactDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_contact_id' => $companyContactId];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $company_contact_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_contact_id)) {
            $code = 400;
            $comment = 'company_contact_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyContactId = $company_contact_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $companyContactDatum = $this->companyContactServ->findById($companyContactId);

        if ($companyContactDatum->isEmpty()) {
            $code = 404;
            $comment = 'company contact error';

            Jsonponse::fail($comment, $code);
        } // END if


        $result = $this->companyContactServ->delete(['id' => $companyContactId]);

        if (empty($result)) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_contact_id' => $companyContactId];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $company_contact_id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($company_contact_id)) {
            $code = 400;
            $comment = 'company_contact_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyContactId = $company_contact_id;


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $companyContactDatum = $this->companyContactServ->findById($companyContactId);

        if ($companyContactDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $codes = ['country', 'state'];

        foreach ($codes as $codeName) {

            $code = $companyContactDatum->first()->{$codeName};

            $prefix = $codeName == 'state' ? 'city_name_' : 'country_name_';
            $subPrefix = $codeName == 'state' ? 'TW' : '';

            $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

            $companyContactDatum->first()->{$codeName . '_langs'} = $langContent;
        } // END foreach


        $resultData = ['session' => $sessionCode, 'company_contact' => $companyContactDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByCompanyId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByCompanyId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderby = ['id' => $orderWay];

        $companyContactData = $this->companyContactServ->findByCompanyId($id, $orderby, $page);

        if ($companyContactData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $current_languages = config('global_languages.CURRENT_LANGUAGES');

        $finalData = [];

        foreach ($companyContactData->all() as $companyContactDatum) {

            $codes = ['country', 'state'];

            foreach ($codes as $codeName) {

                $code = $companyContactDatum->{$codeName};

                $prefix = $codeName == 'state' ? 'city_name_' : 'country_name_';
                $subPrefix = $codeName == 'state' ? 'TW' : '';

                $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix, $subPrefix);

                $companyContactDatum->{$codeName.'_langs'} = $langContent;
            } // END foreach

            array_push($finalData, $companyContactDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'company_contacts' => $finalData];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
