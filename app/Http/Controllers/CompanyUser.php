<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\RoleServ;
use App\Services\CompanyServ;
use App\Services\CompanyUserServ;
use App\Services\SessionServ;


class CompanyUser extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->roleServ = app(RoleServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyUserServ = app(CompanyUserServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyId = $request->input('company_id');
        $userId = $request->input('user_id');
        $roleId = $request->input('role_id');

        if (empty($companyId)) {
            $code = 400;
            $comment = 'company_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($userId)) {
            $code = 400;
            $comment = 'user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($roleId)) {
            $code = 400;
            $comment = 'role_id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyDatum = $this->companyServ->findById($companyId);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userDatum = $this->userServ->findById($userId);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if

        $roleDatum = $this->roleServ->findById($roleId);

        if ($roleDatum->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            Jsonponse::fail($comment, $code);
        } // END if

        $roleType = $roleDatum->first()->type;

        if ($roleType != config('tbl_roles.ROLES_TYPE_COMPANY')) {
            $code = 404;
            $comment = 'role type error';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyUserType = $roleDatum->first()->code;

        $companyUserDatum = $this->companyUserServ->findByCompanyIdAndUserId($companyId, $userId);

        if ($companyUserDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $createCompanyUserDatum = $this->companyUserServ->create($companyId, $userId, $roleId, $companyUserType, $creatorId);

        if ($createCompanyUserDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_user_id' => $createCompanyUserDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $company_user_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_user_id)) {
            $code = 400;
            $comment = 'company_user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyUserId = $company_user_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyUserDatum = $this->companyUserServ->findById($companyUserId);

        if ($companyUserDatum->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userId = $request->input('user_id');
        $roleId = $request->input('role_id');

        $updateData = [];

       if (!empty($userId) AND $userId != $companyUserDatum->first()->user_id) {
            $updateData['user_id'] = $userId;
        } // END if

       if (!empty($roleId) AND $roleId != $companyUserDatum->first()->role_id) {
            $updateData['role_id'] = $roleId;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $companyUserDatum->first()->id];
        $updateCompanyUserDatum = $this->companyUserServ->update($updateData, $updateWhere);

        if ($updateCompanyUserDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_user_id' => $companyUserId];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $company_user_id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($company_user_id)) {
            $code = 400;
            $comment = 'company_user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyUserId = $company_user_id;


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $companyUserDatum = $this->companyUserServ->findById($companyUserId);

        if ($companyUserDatum->isEmpty()) {
            $code = 404;
            $comment = 'company user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $result = $this->companyUserServ->delete(['id' => $companyUserId]);

        if (empty($result)) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_user_id' => $companyUserId];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $company_user_id)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($company_user_id)) {
            $code = 400;
            $comment = 'company_user_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyUserId = $company_user_id;


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $companyUserDatum = $this->companyUserServ->findById($companyUserId);

        if ($companyUserDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        // $companyUserId = $companyUserDatum->first()->id;
        // $companyUserLocationData = $this->companyUserLocationServ->findByCompanyUserId($companyUserId);

        // $companyUserDatum->first()->locations = [];

        // if ($companyUserLocationData->isNotEmpty()) {
        //     foreach($companyUserLocationData->all() as $companyUserLocationDatum) {
        //         array_push ($companyUserDatum->first()->locations, $companyUserLocationDatum->code);
        //     } // END foreach
        // } // END if

        $resultData = ['session' => $sessionCode, 'company_user' => $companyUserDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findByCompanyId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function findByCompanyId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderby = ['id' => $orderWay];

        $companyUserData = $this->companyUserServ->findByCompanyId($id, $orderby, $page);

        if ($companyUserData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        // $finalData = [];

        // foreach ($companyUserData->all() as $companyUserDatum {

        //     $companyUserId = $companyUserDatum->id;
        //     $companyUserLocationData = $this->companyUserLocationServ->findByCompanyUserId($companyUserId);

        //     $companyUserDatum->locations = [];

        //     if ($companyUserLocationData->isNotEmpty()) {
        //         foreach($companyUserLocationData->all() as $companyUserLocationDatum) {
        //             array_push ($companyUserDatum->locations, $companyUserLocationDatum->code);
        //         } // END foreach
        //     } // END if

        //     array_push ($finalData, $companyUserDatum);
        // } // END foreach


        // $resultData = ['session' => $sessionCode, 'company_users' => $finalData];
        $resultData = ['session' => $sessionCode, 'company_users' => $companyUserData->all()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
