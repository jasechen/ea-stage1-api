<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\PostcategoryServ;
use App\Services\PostcategorySeoServ;
use App\Services\SessionServ;


class PostcategorySeo extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->postcategoryServ = app(PostcategoryServ::class);
        $this->postcategorySeoServ = app(PostcategorySeoServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $postcategoryDatum = $this->postcategoryServ->findById($id);

        if ($postcategoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'postcategory error';

            Jsonponse::fail($comment, $code);
        } // END if

        $postcategorySeoDatum = $this->postcategorySeoServ->findByPostcategoryId($postcategoryDatum->first()->id);

        if ($postcategorySeoDatum->isEmpty()) {
            $code = 404;
            $comment = 'postcategory seo error';

            Jsonponse::fail($comment, $code);
        } // END if


        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');


        $updateData = [];

       if (!empty($slug) AND $slug != $postcategorySeoDatum->first()->slug) {
            $psDatum = $this->postcategorySeoServ->findBySlug($slug);

            if ($psDatum->isNotEmpty() AND $psDatum->first()->postcategory_id != $id) {
                $code = 409;
                $comment = 'slug error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['slug'] = $slug;
        } // END if

        if (!empty($excerpt) AND $excerpt != $postcategorySeoDatum->first()->excerpt) {
            $updateData['excerpt'] = $excerpt;
        } // END if

        if (!empty($ogTitle) AND $ogTitle != $postcategorySeoDatum->first()->og_title) {
            $updateData['og_title'] = $ogTitle;
        } // END if

        if (!empty($ogDescription) AND $ogDescription != $postcategorySeoDatum->first()->og_description) {
            $updateData['og_description'] = $ogDescription;
        } // END if

        if (!empty($metaTitle) AND $metaTitle != $postcategorySeoDatum->first()->meta_title) {
            $updateData['meta_title'] = $metaTitle;
        } // END if

        if (!empty($metaDescription) AND $metaDescription != $postcategorySeoDatum->first()->meta_description) {
            $updateData['meta_description'] = $metaDescription;
        } // END if

        if (!empty($coverTitle) AND $coverTitle != $postcategorySeoDatum->first()->cover_title) {
            $updateData['cover_title'] = $coverTitle;
        } // END if

        if (!empty($coverAlt) AND $coverAlt != $postcategorySeoDatum->first()->cover_alt) {
            $updateData['cover_alt'] = $coverAlt;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $postcategorySeoDatum->first()->id];
        $postcategorySeoDatum = $this->postcategorySeoServ->update($updateData, $updateWhere);

        if ($postcategorySeoDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'postcategory_id' => $id];

        Jsonponse::success('update seo success', $resultData, 201);
    } // END function


} // END class
