<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\SchoolServ;
use App\Services\SchoolFeeServ;
use App\Services\SessionServ;


class SchoolFee extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->schoolServ = app(SchoolServ::class);
        $this->schoolFeeServ = app(SchoolFeeServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolId = $request->input('school_id');
        $tuition = $request->input('tuition', 0);
        $dorm = $request->input('dorm', 0);
        $unit = $request->input('unit');
        $currency = $request->input('currency', config('tbl_school_fees.DEFAULT_SCHOOL_FEES_CURRENCY'));

        if (empty($schoolId)) {
            $code = 400;
            $comment = 'school_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($tuition == '') {
            $code = 400;
            $comment = 'tuition empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($dorm == '') {
            $code = 400;
            $comment = 'dorm empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($unit)) {
            $code = 400;
            $comment = 'unit empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        // $unitValidator = Validator::make(['unit' => $unit],
        //     ['unit' => ['in:' . implode(',', config('tbl_school_fees.SCHOOL_FEES_UNITS'))]]
        // );

        // if ($unitValidator->fails()) {
        //     $code = 422;
        //     $comment = 'unit error';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        $currencyValidator = Validator::make(['currency' => $currency],
            ['currency' => ['in:' . implode(',', config('tbl_school_fees.SCHOOL_FEES_CURRENCYS'))]]
        );

        if ($currencyValidator->fails()) {
            $code = 422;
            $comment = 'currency error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolDatum = $this->schoolServ->findById($schoolId);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolFeeDatum = $this->schoolFeeServ->findBySchoolId($schoolId);

        if ($schoolFeeDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'school fee error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolFeeDatum = $this->schoolFeeServ->create($schoolId, $tuition, $dorm, $unit, $currency);

        if ($schoolFeeDatum->isEmpty()) {
            $code = 500;
            $comment = 'create fee error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_id' => $schoolId];

        Jsonponse::success('create fee success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolDatum = $this->schoolServ->findById($id);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolFeeDatum = $this->schoolFeeServ->findBySchoolId($schoolDatum->first()->id);

        if ($schoolFeeDatum->isEmpty()) {
            $code = 404;
            $comment = 'school fee error';

            Jsonponse::fail($comment, $code);
        } // END if


        $tuition = $request->input('tuition');
        $dorm = $request->input('dorm');
        $unit = $request->input('unit');
        $currency = $request->input('currency');

        $updateData = [];

       if ($tuition != '' AND $tuition != $schoolFeeDatum->first()->tuition) {
            $updateData['tuition'] = $tuition;
        } // END if

        if ($dorm != '' AND $dorm != $schoolFeeDatum->first()->dorm) {
            $updateData['dorm'] = $dorm;
        } // END if

        if (!empty($unit) AND $unit != $schoolFeeDatum->first()->unit) {
            // $unitValidator = Validator::make(['unit' => $unit],
            //     ['unit' => ['in:' . implode(',', config('tbl_school_fees.SCHOOL_FEES_UNITS'))]]
            // );

            // if ($unitValidator->fails()) {
            //     $code = 422;
            //     $comment = 'unit error';

            //     Jsonponse::fail($comment, $code);
            // } // END if

            $updateData['unit'] = $unit;
        } // END if

        if (!empty($currency) AND $currency != $schoolFeeDatum->first()->currency) {
            $currencyValidator = Validator::make(['currency' => $currency],
                ['currency' => ['in:' . implode(',', config('tbl_school_fees.SCHOOL_FEES_CURRENCYS'))]]
            );

            if ($currencyValidator->fails()) {
                $code = 422;
                $comment = 'currency error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['currency'] = $currency;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $schoolFeeDatum->first()->id];
        $updateSchoolFeeDatum = $this->schoolFeeServ->update($updateData, $updateWhere);

        if ($updateSchoolFeeDatum->isEmpty()) {
            $code = 500;
            $comment = 'update fee error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_id' => $id];

        Jsonponse::success('update fee success', $resultData, 201);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolDatum = $this->schoolServ->getById($id);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolFeeDatum = $this->schoolFeeServ->findBySchoolId($schoolDatum->first()->id);

        if ($schoolFeeDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_fee' => $schoolFeeDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
