<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Mail\ApplySchoolAgent;
use App\Mail\ApplySchoolApplicant;
use App\Services\SchoolServ;
use App\Services\SchoolSeoServ;
use App\Services\SchoolDormServ;
use App\Services\SchoolUserServ;
use App\Services\SchoolApplicantServ;
use App\Services\UserServ;
use App\Services\UserProfileServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\FilterServ;
use App\Services\FilterSchoolServ;
use App\Services\SessionServ;
use App\Services\LocalesServ;
use App\Services\CompanyUserServ;


class School extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->schoolServ    = app(SchoolServ::class);
        $this->schoolSeoServ = app(SchoolSeoServ::class);
        $this->schoolDormServ = app(SchoolDormServ::class);
        $this->schoolUserServ = app(SchoolUserServ::class);
        $this->schoolApplicantServ = app(SchoolApplicantServ::class);
        $this->userServ    = app(UserServ::class);
        $this->userProfileServ    = app(UserProfileServ::class);
        $this->dirServ    = app(DirServ::class);
        $this->fileServ    = app(FileServ::class);
        $this->filterServ = app(FilterServ::class);
        $this->filterSchoolServ    = app(FilterSchoolServ::class);
        $this->sessionServ = app(SessionServ::class);
        $this->localesServ = app(LocalesServ::class);
        $this->companyUserServ = app(CompanyUserServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $name = $request->input('name');
        $description = $request->input('description');
        $facility = $request->input('facility');
        $cover  = $request->input('cover');
        $status = $request->input('status', config('tbl_schools.DEFAULT_SCHOOLS_STATUS'));

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($cover)) {
            $code = 400;
            $comment = 'cover empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($excerpt)) {
            $code = 400;
            $comment = 'excerpt empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogTitle)) {
            $code = 400;
            $comment = 'og_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogDescription)) {
            $code = 400;
            $comment = 'og_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaTitle)) {
            $code = 400;
            $comment = 'meta_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaDescription)) {
            $code = 400;
            $comment = 'meta_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverTitle)) {
            $code = 400;
            $comment = 'cover_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverAlt)) {
            $code = 400;
            $comment = 'cover_alt empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_schools.SCHOOLS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolSeoDatum = $this->schoolSeoServ->findBySlug($slug);

        if ($schoolSeoDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'slug error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findByFilename($cover);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'cover error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileId = $fileDatum->first()->id;
        $fileParentType = $fileDatum->first()->parent_type;
        $fileParentId = $fileDatum->first()->parent_id;
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirCreatorId = 0;


        $schoolDatum = $this->schoolServ->create($name, $description, $facility, $cover, $status, $creatorId);

        if ($schoolDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolId = $schoolDatum->first()->id;

        // seo
        $this->schoolSeoServ->create($schoolId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);

        // cover
        $dirParentId = $schoolId;
        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

        if ($dirDatum->isEmpty()) {
            $dirDatum = $this->dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
        } // END if

        $dirId = $dirDatum->first()->id;

        $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
        $pathExists = Storage::disk('gallery')->exists($tPath);
        if (empty($pathExists)) {
            Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
        } // END if


        $updatedata = ['dir_id' => $dirId, 'parent_id' => $schoolId, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        $updateWhere = ['id' => $fileId];
        $this->fileServ->update($updatedata, $updateWhere);

        $this->fileServ->moveTo($cover, $tPath);


        $resultData = ['session' => $sessionCode, 'school_id' => $schoolId];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolDatum = $this->schoolServ->getById($id);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if


        $name = $request->input('name');
        $description = $request->input('description');
        $facility = $request->input('facility');
        $cover  = $request->input('cover');
        $status = $request->input('status');

        $updateData = [];

        if (!empty($name) AND $name != $schoolDatum->first()->name) {
            $updateData['name'] = $name;
        } // END if

        if (!empty($description) AND $description != $schoolDatum->first()->description) {
            $updateData['description'] = $description;
        } // END if

        if (!empty($facility) AND $facility != $schoolDatum->first()->facility) {
            $updateData['facility'] = $facility;
        } // END if

        if (!empty($cover) AND $cover != $schoolDatum->first()->cover) {
            $fileDatum = $this->fileServ->findByFilename($cover);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'cover error';

                Jsonponse::fail($comment, $code);
            } // END if

            $dirParentId = $schoolDatum->first()->id;
            $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
            $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
            $dirOwnerId = 0;
            $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
            $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
            $dirCreatorId = 0;

            $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

            if ($dirDatum->isEmpty()) {
                $dirDatum = $this->dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
            } // END if

            $dirId = $dirDatum->first()->id;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $pathExists = Storage::disk('gallery')->exists($tPath);
            if (empty($pathExists)) {
                Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
            } // END if

            $updateData['cover'] = $cover;
        } // END if

        if (!empty($status) AND $status != $schoolDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_schools.SCHOOLS_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateSchoolDatum = $this->schoolServ->update($updateData, ['id' => $id]);

        if ($updateSchoolDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($updateData['cover'])) {
            if (!empty($schoolDatum->first()->cover)) {
                $coverDatum = $this->fileServ->findByFilename($schoolDatum->first()->cover);

                if ($coverDatum->isNotEmpty()) {
                    $updatedata = ['status' => config('tbl_files.FILES_STATUS_DELETE')];
                    $updateWhere = ['id' => $coverDatum->first()->id];
                    $this->fileServ->update($updatedata, $updateWhere);

                    $dirId = empty($dirId) ? $coverDatum->first()->dir_id : $dirId;
                } // END if
            } // END if

            $updatedata = ['dir_id' => $dirId, 'parent_id' => $schoolDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
            $updateWhere = ['id' => $fileDatum->first()->id];
            $this->fileServ->update($updatedata, $updateWhere);

            $this->fileServ->moveTo($cover, $dirParentType . '_' . $dirParentId . '/' . $dirType);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * Delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $schoolDatum = $this->schoolServ->findById($id);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($schoolDatum->first()->status == config('tbl_schools.SCHOOLS_STATUS_DELETE')) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFields = ['status' => config('tbl_schools.SCHOOLS_STATUS_DELETE')];
        $updateWhere = ['id' => $id];
        $schoolDatum = $this->schoolServ->update($updateFields, $updateWhere);

        if ($schoolDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'school_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolDatum = $this->schoolServ->getById($id);

        if ($schoolDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $schoolDatum->first()->cover_links = [];
        if (!empty($schoolDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($schoolDatum->first()->cover);
            $schoolDatum->first()->cover_links = $coverLinks;
        } // END if else


        $schoolDatum->first()->image_links = [];

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $dirParentId = $schoolDatum->first()->id;
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirOwnerId = 0;
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');

        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirStatus, $dirOwnerId);

        if ($dirDatum->isNotEmpty()) {
            $dirId = $dirDatum->first()->id;
            $fileData = $this->fileServ->findByDirId($dirId, $fileStatus);
            if ($fileData->isNotEmpty()) {
                foreach ($fileData->all() as $fileDatum) {
                    $imageLinks = $this->fileServ->findLinks($fileDatum->filename);
                    if (!empty($imageLinks)) {
                        array_push($schoolDatum->first()->image_links, $imageLinks);
                    } // END if
                } // END foreach
            } // END if
        } // END if


        $schoolDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $schoolDatum->first()->url_path = empty($schoolDatum->first()->slug) ? '' : '/' . $schoolDatum->first()->slug . '-s' . $schoolDatum->first()->id;


        $resultData = ['session' => $sessionCode, 'school' => $schoolDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findList
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $status = 'enable', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_schools.SCHOOLS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['c.id' => $orderWay];
        $status = $status == 'all' ? '' : $status;

        $schoolData = $this->schoolServ->getList($status, $orderby, $page);

        if ($schoolData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirOwnerId = 0;
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');

        $finalData = [];
        foreach ($schoolData->all() as $schoolDatum) {

            $schoolDatum->cover_links = [];
            if (!empty($schoolDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($schoolDatum->cover);
                $schoolDatum->cover_links = $coverLinks;
            } // END if else


            $schoolDatum->image_links = [];

            $dirParentId = $schoolDatum->id;
            $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirStatus, $dirOwnerId);

            if ($dirDatum->isNotEmpty()) {
                $dirId = $dirDatum->first()->id;

                $fileData = $this->fileServ->findByDirId($dirId, $fileStatus);
                if ($fileData->isNotEmpty()) {
                    foreach ($fileData->all() as $fileDatum) {
                        $imageLinks = $this->fileServ->findLinks($fileDatum->filename);
                        if (!empty($imageLinks)) {
                            array_push($schoolDatum->image_links, $imageLinks);
                        } // END if
                    } // END foreach
                } // END if
            } // END if


            $schoolDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $schoolDatum->url_path = empty($schoolDatum->slug) ? '' : '/' . $schoolDatum->slug . '-s' . $schoolDatum->id;

            array_push($finalData, $schoolDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'schools' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findBySlug
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     *
     * @throws
     * @return
     */
    public function findBySlug(Request $request, $slug)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $schoolSeoDatum = $this->schoolSeoServ->findBySlug($slug);

        if ($schoolSeoDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $schoolDatum = $this->schoolServ->getById($schoolSeoDatum->first()->school_id);


        $coverLinks = [];
        if (!empty($schoolDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($schoolDatum->first()->cover);
        } // END if
        $schoolDatum->first()->cover_links = $coverLinks;


        $prefix = 'dorm_type_';
        $roomPrefix = 'dorm_room_type_';

        $dorms = [];
        $schoolDormData = $this->schoolDormServ->getBySchoolId($schoolDatum->first()->id);
        if ($schoolDormData->isNotEmpty()) {
            foreach ($schoolDormData->all() as $schoolDormDatum) {
                $rooms = [];
                if (empty($schoolDormDatum->rooms)) {continue;}
                foreach ($schoolDormDatum->rooms as $schoolDormRoomDatum) {

                    $code = $schoolDormRoomDatum->type;
                    $langContent = $this->localesServ->findCodeLangsByCode($code, $roomPrefix);
                    $schoolDormRoomDatum->type_langs = $langContent;

                    array_push($rooms, ['type' => $schoolDormRoomDatum->type, 'accessible' => $schoolDormRoomDatum->accessible, 'smoking' => $schoolDormRoomDatum->smoking,'type_langs' => $schoolDormRoomDatum->type_langs]);
                } // END foreach

                $code = $schoolDormDatum->type;
                $langContent = $this->localesServ->findCodeLangsByCode($code, $prefix);
                $schoolDormDatum->type_langs = $langContent;

                array_push($dorms, ['type' => $schoolDormDatum->type, 'service' => $schoolDormDatum->service, 'facility' => $schoolDormDatum->facility, 'rooms' => $rooms, 'type_langs' => $schoolDormDatum->type_langs]);
            } // END foreach
        } // END if
        $schoolDatum->first()->dorms = $dorms;

        $schoolLocationPrefix = 'city_name_';
        $locations = $schoolLocationLangs = [];
        $filterData = $this->filterSchoolServ->getLocationsBySchoolId($schoolDatum->first()->id);
        if ($filterData->isNotEmpty()) {
            foreach ($filterData->all() as $filterDatum) {
                $code = $filterDatum->code;
                array_push ($locations, $code);

                $parentFilterdatum = $this->filterServ->findById($filterDatum->parent_id);
                $parentCode = $parentFilterdatum->isEmpty() ? 'tw' : $parentFilterdatum->first()->code;

                $schoolLocationSubPrefix = $parentCode;
                $langContent = $this->localesServ->findCodeLangsByCode($code, $schoolLocationPrefix, $schoolLocationSubPrefix);

                $schoolLocationLangs[$code] = $langContent;
            } // END foreach
        } // END if
        $schoolDatum->first()->locations = $locations;
        $schoolDatum->first()->location_langs = $schoolLocationLangs;

        $schoolTypePrefix = 'school_type_title_';
        $schoolTypes = $schoolTypeLangs = [];
        $filterData = $this->filterSchoolServ->getSchoolTypesBySchoolId($schoolDatum->first()->id);
        if ($filterData->isNotEmpty()) {
            foreach ($filterData->all() as $filterDatum) {
                $code = $filterDatum->code;
                array_push ($schoolTypes, $code);

                $langContent = $this->localesServ->findCodeLangsByCode($code, $schoolTypePrefix);
                $schoolTypeLangs[$code] = $langContent;
            } // END foreach
        } // END if
        $schoolDatum->first()->types = $schoolTypes;
        $schoolDatum->first()->type_langs = $schoolTypeLangs;

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $dirParentId = $schoolDatum->first()->id;
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirOwnerId = 0;
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');

        $schoolDatum->first()->image_links = [];
        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirStatus, $dirOwnerId);

        if ($dirDatum->isNotEmpty()) {
            $dirId = $dirDatum->first()->id;
            $fileData = $this->fileServ->findByDirId($dirId, $fileStatus);
            if ($fileData->isNotEmpty()) {
                foreach ($fileData->all() as $fileDatum) {
                    $imageLinks = $this->fileServ->findLinks($fileDatum->filename);
                    if (!empty($imageLinks)) {
                        array_push($schoolDatum->first()->image_links, $imageLinks);
                    } // END if
                } // END foreach
            } // END if
        } // END if


        $schoolDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $schoolDatum->first()->url_path = empty($schoolDatum->first()->slug) ? '' : '/' . $schoolDatum->first()->slug . '-s' . $schoolDatum->first()->id;


        $resultData = ['session' => $sessionCode, 'school' => $schoolDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * Upload Cover
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function uploadCover(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $ownerId = $request->input('owner_id', '0');
        $grandId = $parentId = '0';

        $grandType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $type = config('tbl_dirs.DIRS_TYPE_COVER');

        $parentType = config('tbl_files.FILES_PARENT_TYPE_SCHOOL');
        $privacyStatus = $request->input('privacy_status', config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC'));
        $status = $request->input('status', config('tbl_files.FILES_STATUS_DISABLE'));

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_files.FILES_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dirId = 0;

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $grandId, $grandType, $type);


        $fileDatum = $this->fileServ->create($dirId, $parentType, $parentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $ownerId, true, $fileType, $privacyStatus, $status, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $coverLinks = $this->fileServ->findLinks($filename);

        $resultData = ['session' => $sessionCode, 'filename' => $filename, 'cover_links' => $coverLinks];

        Jsonponse::success('upload cover success', $resultData, 201);
    } // END function


    /**
     * Upload Image
     *
     * @method  PUT
     * @param   \Illuminate\Http\Request  $request
     * @param   $id
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function uploadImage(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $dirParentId = $id;
        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        // $dirCreatorId = 0;

        $fileParentId = $id;
        $fileParentType = config('tbl_files.FILES_PARENT_TYPE_SCHOOL');
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');
        $filePrivacyStatus = config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC');
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');
        $fileOwnerId = 0;
        // $fileCreatorId = 0;
        $fileIsCover = false;

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

        if ($dirDatum->isEmpty()) {
            $dirDatum = $this->dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $creatorId);
        } // END if

        $dirId = $dirDatum->first()->id;

        $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
        $pathExists = Storage::disk('gallery')->exists($tPath);
        if (empty($pathExists)) {
            Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
        } // END if

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $dirParentId, $dirParentType, $dirType);


        $fileDatum = $this->fileServ->create($dirId, $fileParentType, $fileParentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $fileOwnerId, $fileIsCover, $fileType, $filePrivacyStatus, $fileStatus, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $imageLinks = $this->fileServ->findLinks($filename);

        $resultData = ['session' => $sessionCode, 'filename' => $filename, 'image_links' => $imageLinks];

        Jsonponse::success('upload image success', $resultData, 201);
    } // END function


    /**
     * Apply
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function apply(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolId = $request->input('school_id');
        $agentId = $request->input('agent_id');

        if (empty($schoolId)) {
            $code = 400;
            $comment = 'school_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($agentId)) {
            $code = 400;
            $comment = 'agent_id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $schoolDatum = $this->schoolServ->findById($schoolId);

        if ($schoolDatum->isEmpty()) {
            $code = 404;
            $comment = 'school error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($schoolDatum->first()->status != config('tbl_schools.SCHOOLS_STATUS_ENABLE')) {
            $code = 404;
            $comment = 'school status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $agentDatum = $this->userServ->findById($agentId);

        if ($agentDatum->isEmpty()) {
            $code = 404;
            $comment = 'agent error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($agentDatum->first()->status != config('tbl_users.USERS_STATUS_ENABLE')) {
            $code = 404;
            $comment = 'agent status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyAgentData = $this->companyUserServ->findByUserId($agentId);

        if ($companyAgentData->isEmpty()) {
            $code = 404;
            $comment = 'agent company error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolUserType = config('tbl_school_users.SCHOOL_USERS_TYPE_COMPANY');
        $schoolAgentData = $this->schoolUserServ->findBySchoolIdAndUserIdAndType($schoolId, $agentId, $schoolUserType);

        if ($schoolAgentData->isEmpty()) {
            $code = 404;
            $comment = 'agent school error';

            Jsonponse::fail($comment, $code);
        } // END if


        $schoolApplicantDatum = $this->schoolApplicantServ->create($schoolId, $agentId, $creatorId);

        if ($schoolApplicantDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $schoolApplicantId = $schoolApplicantDatum->first()->id;


        $agentProfileDatum = $this->userProfileServ->findByUserId($agentId);
        $agentDatum->first()->profile = $agentProfileDatum->first();

        $applicantProfileDatum = $this->userProfileServ->findByUserId($creatorId);
        $creatorDatum->first()->profile = $applicantProfileDatum->first();
        $creatorDatum->first()->school_name = $schoolDatum->first()->name;

        $fields = ['agent' => $agentDatum->first(), 'applicant' => $creatorDatum->first()];

        $agentEmail = $agentProfileDatum->first()->email;
        Mail::to($agentEmail)->send(new ApplySchoolAgent($fields));

        $applicantEmail = $applicantProfileDatum->first()->email;
        Mail::to($applicantEmail)->send(new ApplySchoolApplicant($fields));


        $updateFields = ['status' => config('tbl_school_applicants.SCHOOL_APPLICANTS_STATUS_NOTIFY')];
        $updateWhere = ['id' => $schoolApplicantId];
        $this->schoolApplicantServ->update($updateFields, $updateWhere);


        $resultData = ['session' => $sessionCode, 'school_applicant_id' => $schoolApplicantId];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


} // END class
