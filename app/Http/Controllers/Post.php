<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\PostServ;
use App\Services\PostSeoServ;
use App\Services\PostcategoryServ;
use App\Services\PostcategorySeoServ;
use App\Services\UserServ;
use App\Services\UserProfileServ;
use App\Services\BlogServ;
use App\Services\DirServ;
use App\Services\FileServ;
use App\Services\SessionServ;


class Post extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->postServ = app(PostServ::class);
        $this->postSeoServ = app(PostSeoServ::class);
        $this->postcategoryServ = app(PostcategoryServ::class);
        $this->postcategorySeoServ = app(PostcategorySeoServ::class);
        $this->userServ = app(UserServ::class);
        $this->userProfileServ = app(UserProfileServ::class);
        $this->blogServ = app(BlogServ::class);
        $this->dirServ = app(DirServ::class);
        $this->fileServ = app(FileServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * create
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            $this->failResponse($comment, $code);
        } // END if

        $title   = $request->input('title');
        $content = $request->input('content');
        $cover   = $request->input('cover');
        $privacyStatus = $request->input('privacy_status', config('tbl_posts.DEFAULT_POSTS_PRIVACY_STATUS'));
        $status  = $request->input('status', config('tbl_posts.DEFAULT_POSTS_STATUS'));
        $recommended = $request->input('recommended', 'false');
        // $blogId = $request->input('blog_id');
        $postcategoryId = $request->input('postcategory_id');
        $ownerId   = $request->input('owner_id');

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $canonicalUrl = $request->input('canonical_url');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');

        if (empty($title)) {
            $code = 400;
            $comment = 'title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($content)) {
            $code = 400;
            $comment = 'content empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($cover)) {
            $code = 400;
            $comment = 'cover empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($postcategoryId)) {
            $code = 400;
            $comment = 'postcategory_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ownerId)) {
            $code = 400;
            $comment = 'owner_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($excerpt)) {
            $code = 400;
            $comment = 'excerpt empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogTitle)) {
            $code = 400;
            $comment = 'og_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogDescription)) {
            $code = 400;
            $comment = 'og_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaTitle)) {
            $code = 400;
            $comment = 'meta_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaDescription)) {
            $code = 400;
            $comment = 'meta_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverTitle)) {
            $code = 400;
            $comment = 'cover_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverAlt)) {
            $code = 400;
            $comment = 'cover_alt empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_posts.POSTS_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_posts.POSTS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $recommendedValidator = Validator::make(['recommended' => $recommended],
            ['recommended' => ['in:true,false']]
        );

        if ($recommendedValidator->fails()) {
            $code = 422;
            $comment = 'recommended error';

            Jsonponse::fail($comment, $code);
        } // END if
        $recommended = $recommended == "true" ? true : false;

        $ownerDatum = $this->userServ->findById($ownerId);

        if ($ownerDatum->isEmpty()) {
            $code = 404;
            $comment = 'owner error';

            Jsonponse::fail($comment, $code);
        } // END if

        $postcategoryDatum = $this->postcategoryServ->getById($postcategoryId);

        if ($postcategoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'postcategory error';

            Jsonponse::fail($comment, $code);
        } // END if

        $postSeoDatum = $this->postSeoServ->findBySlug($slug);

        if ($postSeoDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'slug error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findByFilename($cover);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'cover error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileId = $fileDatum->first()->id;
        $dirId  = $fileDatum->first()->dir_id;

        $dirDatum = $this->dirServ->findById($dirId);

        $blogDatum = $this->blogServ->findByOwnerId($ownerId);
        $blogId = $blogDatum->first()->id;

        $postDatum = $this->postServ->create($title, $content, $blogId, $postcategoryId, $ownerId, $cover, $status, $recommended, $creatorId);

        if ($postDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $this->postSeoServ->create($postDatum->first()->id, $slug, $excerpt, $canonicalUrl, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);


        // update num_posts in category
        if ($status == config('tbl_posts.POSTS_STATUS_PUBLISH')) {
            $this->postcategoryServ->updateNumItems($postcategoryId);
        } // END if


        $updateFields = ['parent_id' => $postDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        $updateWhere  = ['id' => $fileId];
        $fileDatum = $this->fileServ->update($updateFields, $updateWhere);

        $updateFields = ['parent_id' => $postDatum->first()->id, 'status' => config('tbl_dirs.DIRS_STATUS_ENABLE')];
        $updateWhere  = ['id' => $dirId];
        $dirDatum = $this->dirServ->update($updateFields, $updateWhere);

        $dirParentType = $dirDatum->first()->parent_type;
        $dirParentId = $dirDatum->first()->parent_id;
        $dirType = $dirDatum->first()->type;

        $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
        Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
        $this->fileServ->moveTo($cover, $tPath);


        $resultData = ['session' => $sessionCode, 'post_id' => $postDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if


        $title   = $request->input('title');
        $content = $request->input('content');
        $cover   = $request->input('cover');
        $privacyStatus  = $request->input('privacy_status');
        $status  = $request->input('status');
        $recommended  = $request->input('recommended');
        $postcategoryId = $request->input('postcategory_id');
        $ownerId   = $request->input('owner_id');


        $updateData = [];

        if (!empty($title) AND $title != $postDatum->first()->title) {
            $updateData['title'] = $title;
        } // END if

        if (!empty($content) AND $content != $postDatum->first()->content) {
            $updateData['content'] = $content;
        } // END if

        if (!empty($cover) AND $cover != $postDatum->first()->cover) {
            $fileDatum = $this->fileServ->findByFilename($cover);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'cover error';

                Jsonponse::fail($comment, $code);
            } // END if

            $fileId = $fileDatum->first()->id;
            $dirId  = $fileDatum->first()->dir_id;

            $updateData['cover'] = $cover;
        } // END if

        if (!empty($privacyStatus) AND $privacyStatus != $postDatum->first()->privacy_status) {
            $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
                ['privacy_status' => ['in:' . implode(',', config('tbl_posts.POSTS_PRIVACY_STATUS'))]]
            );

            if ($privacyStatusValidator->fails()) {
                $code = 422;
                $comment = 'privacy_status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['privacy_status'] = $privacyStatus;
        } // END if

        if (!empty($status) AND $status != $postDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_posts.POSTS_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if

        $recommendedChecked = empty($postDatum->first()->recommended) ? "false" : "true";
        if (!empty($recommended) AND $recommended != $recommendedChecked) {
            $recommendedValidator = Validator::make(['recommended' => $recommended],
                ['recommended' => ['in:true,false']]
            );

            if ($recommendedValidator->fails()) {
                $code = 422;
                $comment = 'recommended error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['recommended'] = $recommended == "false" ? false : true;
        } // END if

        if (!empty($ownerId) AND $ownerId != $postDatum->first()->owner_id) {
            $ownerDatum = $this->userServ->findById($ownerId);

            if ($ownerDatum->isEmpty()) {
                $code = 404;
                $comment = 'owner error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['owner_id'] = $ownerId;

            $blogDatum = $this->blogServ->findByOwnerId($ownerId);
            $blogId = $blogDatum->first()->id;

            $updateData['blog_id'] = $blogId;
        } // END if

        if (!empty($postcategoryId) AND $postcategoryId != $postDatum->first()->postcategory_id) {
            $postcategoryDatum = $this->postcategoryServ->findById($postcategoryId);

            if ($postcategoryDatum->isEmpty()) {
                $code = 404;
                $comment = 'postcategory error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['postcategory_id'] = $postcategoryId;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updatePostDatum = $this->postServ->update($updateData, ['id' => $id]);

        if ($updatePostDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        // update num_posts in category
        if (!empty($updateData['postcategory_id']) AND $updateData['postcategory_id'] != $postDatum->first()->postcategory_id) {

            if ($postDatum->first()->status == config('tbl_posts.POSTS_STATUS_PUBLISH')) {
                $this->postategoryServ->updateNumItems($postDatum->first()->postcategory_id, -1);
            } // END if

            if ($updateData['status'] == config('tbl_posts.POSTS_STATUS_PUBLISH')) {
                $this->postategoryServ->updateNumItems($updateData['postcategory_id'], 1);
            } // END if

        } else if (!empty($updateData['postcategory_id']) AND $updateData['postcategory_id'] == $postDatum->first()->postcategory_id) {

            if (!empty($updateData['status']) AND $updateData['status'] == config('tbl_posts.POSTS_STATUS_PUBLISH')) {
                if ($postDatum->first()->status != config('tbl_posts.POSTS_STATUS_PUBLISH')) {
                    $this->postategoryServ->updateNumItems($postDatum->first()->postcategory_id, 1);
                } // END if
            } // END if

            if (!empty($updateData['status']) AND $updateData['status'] != config('tbl_posts.POSTS_STATUS_PUBLISH')) {
                if ($postDatum->first()->status == config('tbl_posts.POSTS_STATUS_PUBLISH')) {
                    $this->postategoryServ->updateNumItems($postDatum->first()->postcategory_id, -1);
                } // END if
            } // END if

        } // END if


        if (!empty($updateData['cover']) AND $updateData['cover'] != $postDatum->first()->cover) {
            $updatedata = ['parent_id' => $postDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
            $updateWhere = ['id' => $fileId];
            $fileDatum = $this->fileServ->update($updatedata, $updateWhere);

            $updatedata = ['parent_id' => $postDatum->first()->id, 'status' => config('tbl_dirs.DIRS_STATUS_ENABLE')];
            $updateWhere = ['id' => $dirId];
            $dirDatum = $this->dirServ->update($updatedata, $updateWhere);

            $dirParentType = $dirDatum->first()->parent_type;
            $dirParentId = $dirDatum->first()->parent_id;
            $dirType = $dirDatum->first()->type;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $this->fileServ->moveTo($cover, $tPath);
        } // END if


        $resultData = ['session' => $sessionCode, 'post_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($postDatum->first()->status == config('tbl_posts.POSTS_STATUS_DELETE')) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateData = ['status' => config('tbl_posts.POSTS_STATUS_DELETE')];
        $updatePostDatum = $this->postServ->update($updateData, ['id' => $id]);

        if ($updatePostDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'post_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $postDatum = $this->postServ->getById($id);

        if ($postDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $postcategoryDatum = $this->postcategoryServ->getById($postDatum->first()->postcategory_id);
        $postcategoryDatum->first()->cover_links = [];
        if (!empty($postcategoryDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
            $postcategoryDatum->first()->cover_links = $coverLinks;
        } // END if
        $postDatum->first()->postcategory = $postcategoryDatum->first();

        $ownerDatum = $this->userServ->getById($postDatum->first()->owner_id);
        $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
        $ownerProfileDatum->first()->avatar_links = [];
        if (!empty($ownerProfileDatum->first()->avatar)) {
            $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
            $ownerProfileDatum->first()->avatar_links = $avatarLinks;
        } // END if
        $ownerDatum->first()->profile = $ownerProfileDatum->first();
        unset($ownerDatum->first()->password);
        $postDatum->first()->owner = $ownerDatum->first();

        $postDatum->first()->cover_links = [];
        if (!empty($postDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($postDatum->first()->cover);
            $postDatum->first()->cover_links = $coverLinks;
        } // END if

        $postDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $postDatum->first()->url_path = empty($postDatum->first()->slug) ? '' : '/' . $postDatum->first()->slug . '-p' . $postDatum->first()->id;


        $resultData = ['session' => $sessionCode, 'post' => $postDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findList
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findList(Request $request, $status = 'publish', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_posts.POSTS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['p.id' => $orderWay];
        $status = $status == 'all' ? '' : $status;

        $postData = $this->postServ->getList($status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {

            $postcategoryDatum = $this->postcategoryServ->getById($postDatum->postcategory_id);
            $postcategoryDatum->first()->cover_links = [];
            if (!empty($postcategoryDatum->first()->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
                $postcategoryDatum->first()->cover_links = $coverLinks;
            } // END if
            $postDatum->postcategory = $postcategoryDatum->first();

            $ownerDatum = $this->userServ->getById($postDatum->owner_id);
            $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
            $ownerDatum->first()->avatar_links = [];
            if (!empty($ownerProfileDatum->first()->avatar)) {
                $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
                $ownerProfileDatum->first()->avatar_links = $avatarLinks;
            } // END if
            $ownerDatum->first()->profile = $ownerProfileDatum->first();
            unset($ownerDatum->first()->password);
            $postDatum->owner = $ownerDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinks;
            } // END if

            $postDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findBySlug
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     *
     * @throws
     * @return
     */
    public function findBySlug(Request $request, $slug)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $postSeoDatum = $this->postSeoServ->findBySlug($slug);

        if ($postSeoDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $postDatum = $this->postServ->getById($postSeoDatum->first()->post_id);

        $postcategoryDatum = $this->postcategoryServ->getById($postDatum->first()->postcategory_id);
        $postcategoryDatum->first()->cover_links = [];
        if (!empty($postcategoryDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
            $postcategoryDatum->first()->cover_links = $coverLinks;
        } // END if
        $postDatum->first()->postcategory = $postcategoryDatum->first();

        $ownerDatum = $this->userServ->findById($postDatum->first()->owner_id);
        $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
        $ownerProfileDatum->first()->avatar_links = [];
        if (!empty($ownerProfileDatum->first()->avatar)) {
            $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
            $ownerProfileDatum->first()->avatar_links = $avatarLinks;
        } // END if
        $ownerDatum->first()->profile = $ownerProfileDatum->first();
        unset($ownerDatum->first()->password);
        $postDatum->first()->owner = $ownerDatum->first();

        $postDatum->first()->cover_links = [];
        if (!empty($postDatum->first()->cover)) {
            $coverLinks = $this->fileServ->findLinks($postDatum->first()->cover);
            $postDatum->first()->cover_links = $coverLinks;
        } // END if

        $postDatum->first()->s3_url = env('AWS_GALLERY_BUCKET_URL');
        $postDatum->first()->url_path = empty($postDatum->first()->slug) ? '' : '/' . $postDatum->first()->slug . '-p' . $postDatum->first()->id;


        $resultData = ['session' => $sessionCode, 'post' => $postDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


    /**
     * findRecommendedByPostcategoryId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $postcategory_id
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findRecommendedByPostcategoryId(Request $request, $postcategory_id, $status = 'publish', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($postcategory_id)) {
            $code = 400;
            $comment = 'postcategory_id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($sessionCode)) {
            $isAlive = $this->sessionServ->isAlive($sessionCode);

            if (empty($isAlive)) {
                $code = 410;
                $comment = 'session is NOT alive';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_posts.POSTS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];
        $status = $status == 'all' ? '' : $status;

        $postData = $this->postServ->getRecommendedByPostcategoryId($postcategory_id, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {

            $postcategoryDatum = $this->postcategoryServ->getById($postDatum->postcategory_id);
            $postcategoryDatum->first()->cover_links = [];
            if (!empty($postcategoryDatum->first()->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
                $postcategoryDatum->first()->cover_links = $coverLinks;
            } // END if
            $postDatum->postcategory = $postcategoryDatum->first();

            $ownerDatum = $this->userServ->findById($postDatum->owner_id);
            $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
            $ownerProfileDatum->first()->avatar_links = [];
            if (!empty($ownerProfileDatum->first()->avatar)) {
                $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
                $ownerProfileDatum->first()->avatar_links = $avatarLinks;
            } // END if
            $ownerDatum->first()->profile = $ownerProfileDatum->first();
            unset($ownerDatum->first()->password);
            $postDatum->owner = $ownerDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinks;
            } // END if

            $postDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByPostcategoryId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $postcategory_id
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByPostcategoryId(Request $request, $postcategory_id, $status = 'publish', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($postcategory_id)) {
            $code = 400;
            $comment = 'postcategory_id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_posts.POSTS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];
        $postcategoryId = $postcategory_id;
        $status = $status == 'all' ? '' : $status;

        $postData = $this->postServ->getByPostcategoryId($postcategoryId, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {

            $postcategoryDatum = $this->postcategoryServ->getById($postDatum->postcategory_id);
            $postcategoryDatum->first()->cover_links = [];
            if (!empty($postcategoryDatum->first()->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
                $postcategoryDatum->first()->cover_links = $coverLinks;
            } // END if
            $postDatum->postcategory = $postcategoryDatum->first();

            $ownerDatum = $this->userServ->findById($postDatum->owner_id);
            $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
            $ownerProfileDatum->first()->avatar_links = [];
            if (!empty($ownerProfileDatum->first()->avatar)) {
                $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
                $ownerProfileDatum->first()->avatar_links = $avatarLinks;
            } // END if
            $ownerDatum->first()->profile = $ownerProfileDatum->first();
            unset($ownerDatum->first()->password);
            $postDatum->owner = $ownerDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinks;
            } // END if

            $postDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByPostcategorySlug
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $postcategory_slug
     * @param  $status
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByPostcategorySlug(Request $request, $postcategory_slug, $status = 'publish', $order_way = 'ASC', $page = -1)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($postcategory_slug)) {
            $code = 400;
            $comment = 'postcategory_slug empty';

            Jsonponse::fail($comment, $code);
        } // END if
        $postcategorySlug = $postcategory_slug;

        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:all,'.implode(',',config('tbl_posts.POSTS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $postcategorySeoDatum = $this->postcategorySeoServ->findBySlug($postcategorySlug);

        if ($postcategorySeoDatum->isEmpty()) {
            $code = 404;
            $comment = 'postcategory_slug error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];
        $status = $status == 'all' ? '' : $status;
        $postcategoryId = $postcategorySeoDatum->first()->postcategory_id;

        $postData = $this->postServ->getByPostcategoryId($postcategoryId, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {

            $postcategoryDatum = $this->postcategoryServ->getById($postDatum->postcategory_id);
            $postcategoryDatum->first()->cover_links = [];
            if (!empty($postcategoryDatum->first()->cover)) {
                $coverLinks = $this->fileServ->findLinks($postcategoryDatum->first()->cover);
                $postcategoryDatum->first()->cover_links = $coverLinks;
            } // END if
            $postDatum->postcategory = $postcategoryDatum->first();

            $ownerDatum = $this->userServ->getById($postDatum->owner_id);
            $ownerProfileDatum = $this->userProfileServ->findByUserId($ownerDatum->first()->id);
            $ownerProfileDatum->first()->avatar_links = [];
            if (!empty($ownerProfileDatum->first()->avatar)) {
                $avatarLinks = $this->fileServ->findLinks($ownerProfileDatum->first()->avatar);
                $ownerProfileDatum->first()->avatar_links = $avatarLinks;
            } // END if
            $ownerDatum->first()->profile = $ownerProfileDatum->first();
            unset($ownerDatum->first()->password);
            $postDatum->owner = $ownerDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinks = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinks;
            } // END if

            $postDatum->s3_url = env('AWS_GALLERY_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * Upload Cover
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function uploadCover(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $ownerId = $request->input('owner_id');

        if (empty($ownerId)) {
            $code = 400;
            $comment = 'owner_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $grandId = $parentId = '0';

        $grandType = config('tbl_dirs.DIRS_PARENT_TYPE_POST');
        $type = config('tbl_dirs.DIRS_TYPE_COVER');
        $parentType = config('tbl_files.FILES_PARENT_TYPE_POST');

        $privacyStatus = $request->input('privacy_status', config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC'));
        $status = $request->input('status', config('tbl_files.FILES_STATUS_DISABLE'));

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:' . implode(',', config('tbl_files.FILES_PRIVACY_STATUS'))]]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 422;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if

        $ownerDatum = $this->userServ->findById($ownerId);

        if ($ownerDatum->isEmpty()) {
            $code = 404;
            $comment = 'owner_id error';

            Jsonponse::fail($comment, $code);
        } // END if


        $dirDatum = $this->dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($grandType, $grandId, $type, $ownerId);

        if ($dirDatum->isEmpty()) {
            $dirDatum = $this->dirServ->initDefaultAlbum($grandType, $grandId, $type, $ownerId, $privacyStatus, $status, $creatorId);
        } // END if

        $dirId = $dirDatum->first()->id;

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';

        $filename = $this->fileServ->convertImage($fileUpload, $grandId, $grandType, $type);


        $fileDatum = $this->fileServ->create($dirId, $parentType, $parentId, $filename, $fileExtension, $fileMimeType, $fileSize, $fileOriginalName, '', $ownerId, false, $fileType, $privacyStatus, $status, $creatorId);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $coverLinks = $this->fileServ->findLinks($filename);

        $resultData = ['session' => $sessionCode, 'filename' => $filename, 'cover_links' => $coverLinks];

        Jsonponse::success('upload cover success', $resultData, 201);
    } // END function



} // END class
