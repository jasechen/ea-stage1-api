<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Mail\VerifyEmail;
use App\Services\UserServ;
use App\Services\UserSeoServ;
use App\Services\UserProfileServ;
use App\Services\UserRoleServ;
use App\Services\UserCheckedOptionServ;
use App\Services\DirServ;
use App\Services\SessionServ;

use Aloha\Twilio\Support\Laravel\Facade as Twilio;


class Register extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ     = app(UserServ::class);
        $this->userSeoServ = app(UserSeoServ::class);
        $this->userProfileServ = app(UserProfileServ::class);
        $this->userRoleServ = app(UserRoleServ::class);
        $this->userCheckedOptionServ = app(UserCheckedOptionServ::class);
        $this->dirServ = app(DirServ::class);
        $this->sessionServ  = app(SessionServ::class);
    } // END function


    /**
     * index
     *
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function index(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $account  = $request->input('account');
        $password = $request->input('password');
        $passwordRepeat = $request->input('password_repeat');
        $firstName = $request->input('first_name');
        $lastName  = $request->input('last_name');
        $mobileCountryCode = $request->input('mobile_country_code');
        $mobilePhone = $request->input('mobile_phone');

        if (empty($account)) {
            $code = 400;
            $comment = 'account empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($password) or empty($passwordRepeat)) {
            $code = 400;
            $comment = 'password / password_repeat empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($firstName)) {
            $code = 400;
            $comment = 'first_name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($lastName)) {
            $code = 400;
            $comment = 'last_name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($mobileCountryCode)) {
            $code = 400;
            $comment = 'mobile_country_code empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($mobilePhone)) {
            $code = 400;
            $comment = 'mobile_phone is empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $accountValidator = Validator::make(['account' => $account],
            ['account' => 'email']
        );

        if ($accountValidator->fails()) {
            $code = 422;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if

        // $passwordValidator = Validator::make(['password' => $password],
        //     ['password' => 'size:' . config('app.PASSWORD_LENGTH_LIMIT')]
        // );

        // if ($passwordValidator->fails()) {
        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 422;
            $comment = 'password\' length < ' . config('app.PASSWORD_LENGTH_LIMIT');

            Jsonponse::fail($comment, $code);
        } // END if

        if ($password != $passwordRepeat) {
            $code = 422;
            $comment = 'password / password_repeat is NOT equal';

            Jsonponse::fail($comment, $code);
        } // END if

        $phone = '+' . $mobileCountryCode . $mobilePhone;
        $mobilePhoneValidator = Validator::make(['mobile_phone' => $phone],
            ['mobile_phone' => 'phone:AUTO,mobile']
        );

        if ($mobilePhoneValidator->fails()) {
            $code = 422;
            $comment = 'mobile_phone format error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findByAccount($account);

        if ($userDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if

        $userProfileDatum = $this->userProfileServ->findByMobilePhone($mobilePhone);

        if ($userProfileDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'mobile_phone error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->create($account, $password);

        if ($userDatum->isEmpty()) {
            $code = 500;
            $comment = 'create user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $userProfileDatum = $this->userProfileServ->findByUserId($userDatum->first()->id);

        if ($userProfileDatum->isEmpty()) {
            $userProfileDatum = $this->userProfileServ->create($userDatum->first()->id, $account, $mobileCountryCode, $mobilePhone, $firstName, $lastName);
        } // END if

        $userDatum->first()->profile = $userProfileDatum->first();


        $userRoleData = $this->userRoleServ->findByUserId($userDatum->first()->id);

        if ($userRoleData->isEmpty()) {
            $this->userRoleServ->initWebMember($userDatum->first()->id);
        } // END if


        $userSeoDatum = $this->userSeoServ->findByUserId($userDatum->first()->id);

        if ($userSeoDatum->isEmpty()) {
            $userSlug = str_replace(['@','.'], '-', $account);
            $this->userSeoServ->create($userDatum->first()->id, $userDatum->first()->id . '-' . $userSlug);
        } // END if


        $userEmailCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'email');

        if ($userEmailCheckedOptionDatum->isEmpty()) {
            $userEmailCheckedOptionDatum = $this->userCheckedOptionServ->create($userDatum->first()->id, 'email');
        } // END if

        $userMobilePhoneCheckedOptionDatum = $this->userCheckedOptionServ->findByUserIdAndOption($userDatum->first()->id, 'mobile_phone');

        if ($userMobilePhoneCheckedOptionDatum->isEmpty()) {
            $userMobilePhoneCheckedOptionDatum = $this->userCheckedOptionServ->create($userDatum->first()->id, 'mobile_phone');
        } // END if


        $noDir  = $this->dirServ->initDefaultDir('user', $userDatum->first()->id, $userDatum->first()->id);
        $noPath = $noDir->first()->parent_type . '_' . $noDir->first()->parent_id . '/' . $noDir->first()->type;
        Storage::disk('gallery')->makeDirectory($noPath, 0755, true);
        // Storage::disk('s3-gallery')->makeDirectory($noPath, 0755, true);

        $avatarDir  = $this->dirServ->initDefaultAlbum('user', $userDatum->first()->id, 'avatar', $userDatum->first()->id);
        $avatarPath = $avatarDir->first()->parent_type . '_' . $userDatum->first()->id . '/' . $avatarDir->first()->type;
        Storage::disk('gallery')->makeDirectory($avatarPath, 0755, true);
        // Storage::disk('s3-gallery')->makeDirectory($avatarPath, 0755, true);


        $to = $userDatum->first()->account;
        $emailFields = ['user' => $userDatum->first(),
                        'code' => $userEmailCheckedOptionDatum->first()->code,
                        'link' => env('WEB_URL') . '/verify/' . $userDatum->first()->id];
        Mail::to($to)->send(new VerifyEmail($emailFields));


        $message = '您的 English.Agency「註冊驗證碼」為 ' . $userMobilePhoneCheckedOptionDatum->first()->code;
        Twilio::message($phone, $message);


        $resultData = ['session' => $sessionCode, 'token' => $userDatum->first()->token];

        Jsonponse::success('register success', $resultData, 201);
    } // END function


} // END class
