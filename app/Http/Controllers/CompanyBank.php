<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\CompanyServ;
use App\Services\CompanyBankServ;
use App\Services\SessionServ;


class CompanyBank extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->companyServ = app(CompanyServ::class);
        $this->companyBankServ = app(CompanyBankServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Create
     *
     * @method  POST
     * @param   \Illuminate\Http\Request  $request
     * @param   $privacy_status
     * @param   $status
     * @param   $file
     *
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyId = $request->input('company_id');
        $name = $request->input('name');
        $branchName = $request->input('branch_name');
        $accountName = $request->input('account_name');
        $accountNumber = $request->input('account_number');

        if (empty($companyId)) {
            $code = 400;
            $comment = 'company_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($branchName)) {
            $code = 400;
            $comment = 'branch_name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($accountName)) {
            $code = 400;
            $comment = 'account_name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($accountNumber)) {
            $code = 400;
            $comment = 'account_number empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;

        $companyDatum = $this->companyServ->findById($companyId);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyBankDatum = $this->companyBankServ->findByCompanyId($companyId);

        if ($companyBankDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'company bank error';

            Jsonponse::fail($comment, $code);
        } // END if


        $createCompanyBankDatum = $this->companyBankServ->create($companyId, $name, $branchName, $accountName, $accountNumber);

        if ($createCompanyBankDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_bank_id' => $createCompanyBankDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $companyDatum = $this->companyServ->findById($id);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyBankDatum = $this->companyBankServ->findByCompanyId($companyDatum->first()->id);

        if ($companyBankDatum->isEmpty()) {
            $code = 404;
            $comment = 'company bank error';

            Jsonponse::fail($comment, $code);
        } // END if


        $name = $request->input('name');
        $branchName = $request->input('branch_name');
        $accountName = $request->input('account_name');
        $accountNumber = $request->input('account_number');

        $updateData = [];

       if (!empty($name) AND $name != $companyBankDatum->first()->name) {
            $updateData['name'] = $name;
        } // END if

        if (!empty($branchName) AND $branchName != $companyBankDatum->first()->branch_name) {
            $updateData['branch_name'] = $branchName;
        } // END if

       if (!empty($accountName) AND $accountName != $companyBankDatum->first()->account_name) {
            $updateData['account_name'] = $accountName;
        } // END if

        if (!empty($accountNumber) AND $accountNumber != $companyBankDatum->first()->account_number) {
            $updateData['account_number'] = $accountNumber;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $companyBankDatum->first()->id];
        $updateCompanyBankDatum = $this->companyBankServ->update($updateData, $updateWhere);

        if ($updateCompanyBankDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_bank_id' => $companyBankDatum->first()->id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $companyDatum = $this->companyServ->findById($id);

        if ($companyDatum->isEmpty()) {
            $code = 404;
            $comment = 'company error';

            Jsonponse::fail($comment, $code);
        } // END if


        $companyBankDatum = $this->companyBankServ->findByCompanyId($companyDatum->first()->id);

        if ($companyBankDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'company_bank' => $companyBankDatum->first()];

        Jsonponse::success('fetch success', $resultData);
    } // END function


} // END class
