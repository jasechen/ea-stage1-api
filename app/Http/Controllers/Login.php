<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\SessionServ;
use App\Services\LoginLogServ;


class Login extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ     = app(UserServ::class);
        $this->sessionServ  = app(SessionServ::class);
        $this->loginLogServ = app(LoginLogServ::class);
    } // END function


    /**
     * index
     *
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function index(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $account  = $request->input('account');
        $password = $request->input('password');

        if (empty($account)) {
            $code = 400;
            $comment = 'account empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($password)) {
            $code = 400;
            $comment = 'password empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $accountValidator = Validator::make(['account' => $account],
            ['account' => 'email']
        );

        if ($accountValidator->fails()) {
            $code = 422;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if

        // $passwordValidator = Validator::make(['password' => $password],
        //     ['password' => 'size:' . config('app.PASSWORD_LENGTH_LIMIT')]
        // );

        // if ($passwordValidator->fails()) {
        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 422;
            $comment = 'password\' length < ' . config('app.PASSWORD_LENGTH_LIMIT');

            Jsonponse::fail($comment, $code);
        } // END if


        $userDatum = $this->userServ->findByAccount($account);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'account error';

            Jsonponse::fail($comment, $code);
        } // END if

        if (!password_verify($password, $userDatum->first()->password)) {
            $code = 422;
            $comment = 'password error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($userDatum->first()->status != config('tbl_users.USERS_STATUS_ENABLE')) {
            $code = 422;
            $comment = 'user status error';
            $resultData = ['token' => $userDatum->first()->token];

            Jsonponse::fail($comment, $code, $resultData);
        } // END if


        $sessionDatum = $this->sessionServ->findByCode($sessionCode);

        $sessionFields = ['status' => config('tbl_sessions.SESSIONS_STATUS_LOGIN'),
                          'owner_id' => $userDatum->first()->id];
        $sessionWhere = ['id' => $sessionDatum->first()->id];
        $this->sessionServ->update($sessionFields, $sessionWhere);


        $this->loginLogServ->create($sessionDatum->first()->id, $sessionDatum->first()->device_id, $userDatum->first()->id);


        $resultData = ['session' => $sessionCode, 'token' => $userDatum->first()->token];

        Jsonponse::success('login success', $resultData, 200);
    } // END function


} // END class
