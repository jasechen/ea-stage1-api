<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\PostServ;
use App\Services\PostSeoServ;
use App\Services\SessionServ;


class PostSeo extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->postServ = app(PostServ::class);
        $this->postSeoServ = app(PostSeoServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if

        $postSeoDatum = $this->postSeoServ->findByPostId($postDatum->first()->id);

        if ($postSeoDatum->isEmpty()) {
            $code = 404;
            $comment = 'post seo error';

            Jsonponse::fail($comment, $code);
        } // END if


        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $canonicalUrl = $request->input('canonical_url');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');


        $updateData = [];

       if (!empty($slug) AND $slug != $postSeoDatum->first()->slug) {
            $psDatum = $this->postSeoServ->findBySlug($slug);

            if ($psDatum->isNotEmpty() AND $psDatum->first()->post_id != $id) {
                $code = 409;
                $comment = 'slug error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['slug'] = $slug;
        } // END if

        if (!empty($excerpt) AND $excerpt != $postSeoDatum->first()->excerpt) {
            $updateData['excerpt'] = $excerpt;
        } // END if

        if (!empty($canonicalUrl) AND $canonicalUrl != $postSeoDatum->first()->canonical_url) {
            $updateData['canonical_url'] = $canonicalUrl;
        } // END if

        if (!empty($ogTitle) AND $ogTitle != $postSeoDatum->first()->og_title) {
            $updateData['og_title'] = $ogTitle;
        } // END if

        if (!empty($ogDescription) AND $ogDescription != $postSeoDatum->first()->og_description) {
            $updateData['og_description'] = $ogDescription;
        } // END if

        if (!empty($metaTitle) AND $metaTitle != $postSeoDatum->first()->meta_title) {
            $updateData['meta_title'] = $metaTitle;
        } // END if

        if (!empty($metaDescription) AND $metaDescription != $postSeoDatum->first()->meta_description) {
            $updateData['meta_description'] = $metaDescription;
        } // END if

        if (!empty($coverTitle) AND $coverTitle != $postSeoDatum->first()->cover_title) {
            $updateData['cover_title'] = $coverTitle;
        } // END if

        if (!empty($coverAlt) AND $coverAlt != $postSeoDatum->first()->cover_alt) {
            $updateData['cover_alt'] = $coverAlt;
        } // END if


        if (empty($updateData)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateWhere = ['id' => $postSeoDatum->first()->id];
        $updatePostSeoDatum = $this->postSeoServ->update($updateData, $updateWhere);

        if ($updatePostSeoDatum->isEmpty()) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'post_id' => $id];

        Jsonponse::success('update seo success', $resultData, 201);
    } // END function


} // END class
