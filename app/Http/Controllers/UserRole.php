<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\UserServ;
use App\Services\UserRoleServ;
use App\Services\RoleServ;
use App\Services\SessionServ;


class UserRole extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->userServ = app(UserServ::class);
        $this->userRoleServ = app(UserRoleServ::class);
        $this->roleServ = app(RoleServ::class);
        $this->sessionServ = app(SessionServ::class);
    } // END function


    /**
     * Update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @param  $role_ids
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $roleIds  = $request->input('role_ids');

        if (empty($roleIds)) {
            $code = 400;
            $comment = 'role_ids empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $isValid = $this->sessionServ->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 422;
            $comment = 'session token error';

            Jsonponse::fail($comment, $code);
        } // END if

        $creatorDatum = $this->userServ->findByToken($token);
        $creatorId = $creatorDatum->first()->id;


        $userDatum = $this->userServ->findById($id);

        if ($userDatum->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateRoleIds = [];

        foreach ($roleIds as $roleId) {
            $roleDatum = $this->roleServ->findById($roleId);
            if ($roleDatum->isEmpty()) continue;
            array_push ($updateRoleIds, $roleId);
        } // END foreach

        if (empty($updateRoleIds)) {
            $code = 400;
            $comment = 'updateData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $userRoleData = $this->userRoleServ->findByUserId($id);

        foreach ($userRoleData as $userRoleDatum) {
            $this->userRoleServ->delete(['id' => $userRoleDatum->id]);
        } // END foreach


        foreach ($updateRoleIds as $updateRoleId) {
            $this->userRoleServ->create($id, $updateRoleId, $creatorId);
        } // END foreach


        $resultData = ['session' => $sessionCode, 'user_id' => $id];

        Jsonponse::success('update user role success', $resultData, 201);
    } // END function


} // END class
