<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplySchoolAgent extends Mailable
{
    use Queueable, SerializesModels;

    public $fields;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $fields)
    {
        //
        $this->fields = $fields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $agent = $this->fields['agent'];

        $subject = "Hi ".$agent->profile->last_name.$agent->profile->first_name."，又一位學生需要您的專業協助。";

        return $this->from(config('mail.from.address'), config('mail.from.name'))
                    ->subject( $subject )
                    ->view('email.apply_school_agent')
                    ->with([
                        'agent' => $this->fields['agent'],
                        'applicant' => $this->fields['applicant']
                    ]);
    }
}
