<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $fields;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $fields)
    {
        //
        $this->fields = $fields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
                    ->subject( env('APP_NAME') . ' 忘記密碼通知信!!')
                    ->view('email.forgot_password')
                    ->with([
                        'account' => $this->fields['account'],
                        'code' => $this->fields['code'],
                        'link' => $this->fields['link'],
                    ]);
    }
}
