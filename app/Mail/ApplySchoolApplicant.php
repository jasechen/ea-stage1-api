<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplySchoolApplicant extends Mailable
{
    use Queueable, SerializesModels;

    public $fields;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $fields)
    {
        //
        $this->fields = $fields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $applicant = $this->fields['applicant'];

        $subject = "Hi ".$applicant->profile->last_name.$applicant->profile->first_name."，我們已收到你想去遊學的詢問邀請。";

        return $this->from(config('mail.from.address'), config('mail.from.name'))
                    ->subject( $subject )
                    ->view('email.apply_school_applicant')
                    ->with([
                        'agent' => $this->fields['agent'],
                        'applicant' => $this->fields['applicant']
                    ]);
    }
}
