<?php

namespace App\Services;

use App\Repos\CompanyLocationRepo;


/**
 * Class CompanyLocationServ
 *
 * @package namespace App\Services;
 */
class CompanyLocationServ
{


    public function __construct()
    {

        $this->companyLocationRepo = new CompanyLocationRepo();
    } // END function


    /*
     * create
     *
     * @param $companyId
     * @param $code
     *
     * @return
     */
    public function create($companyId, $code)
    {
        $data = ['company_id' => $companyId,
                 'code' => $code
        ];

        return $this->companyLocationRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyLocationRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyLocationRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        return $this->companyLocationRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyIdAndCode
     *
     * @param $companyId
     * @param $code
     *
     * @return
     */
    public function findByCompanyIdAndCode($companyId, $code)
    {
        $where = ['company_id' => $companyId, 'code' => $code];

        return $this->companyLocationRepo->fetchDatum($where);
    } // END function


    /*
     * findByCompanyId
     *
     * @param $companyId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByCompanyId($companyId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['company_id' => $companyId];

        return $this->companyLocationRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->companyLocationRepo->fetchDatum($where);
    } // END function


}
