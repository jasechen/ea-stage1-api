<?php

namespace App\Services;

use App\Repos\DirRepo;


/**
 * Class DirServ
 *
 * @package namespace App\Services;
 */
class DirServ
{


    public function __construct()
    {

        $this->dirRepo = new DirRepo();
    } // END function


    /*
     * initDir
     *
     * @param $parentType
     * @param $parentId
     * @param $title
     * @param $description
     * @param $ownerId
     * @param $privacyStatus
     * @param $status
     * @param $creatorId
     *
     * @return
     */
    public function initDir($parentType, $parentId, $title, $description, $ownerId, $privacyStatus = 'public', $status = 'enable', $creatorId = 0)
    {
        $type = 'others';
        $cover = '';
        $isAlbum = false;
        $isDefault = false;

        return $this->create($parentType, $parentId, $type, $title, $description, $cover, $privacyStatus, $status, $isAlbum, $isDefault, $ownerId, $creatorId);
    } // END function


    /*
     * initAlbum
     *
     * @param $parentType
     * @param $parentId
     * @param $title
     * @param $description
     * @param $ownerId
     * @param $privacyStatus
     * @param $status
     * @param $creatorId
     *
     * @return
     */
    public function initAlbum($parentType, $parentId, $title, $description, $ownerId, $privacyStatus = 'public', $status = 'enable', $creatorId = 0)
    {
        $type = 'others';
        $cover = '';
        $isAlbum = true;
        $isDefault = false;

        return $this->create($parentType, $parentId, $type, $title, $description, $cover, $privacyStatus, $status, $isAlbum, $isDefault, $ownerId, $creatorId);
    } // END function


    /*
     * initDefaultDir
     *
     * @param $parentType
     * @param $parentId
     * @param $ownerId
     * @param $privacyStatus
     * @param $status
     * @param $creatorId
     *
     * @return
     */
    public function initDefaultDir($parentType, $parentId, $ownerId, $privacyStatus = 'public', $status = 'enable', $creatorId = 0)
    {
        $type = 'no_dir';
        $title = 'Default Dir';
        $description = '';
        $cover = '';
        $isAlbum = false;
        $isDefault = true;

        return $this->create($parentType, $parentId, $type, $title, $description, $cover, $privacyStatus, $status, $isAlbum, $isDefault, $ownerId, $creatorId);
    } // END function


    /*
     * initDefaultAlbum
     *
     * @param $parentType
     * @param $parentId
     * @param $type
     * @param $ownerId
     * @param $privacyStatus
     * @param $status
     * @param $creatorId
     *
     * @return
     */
    public function initDefaultAlbum($parentType, $parentId, $type, $ownerId, $privacyStatus = 'public', $status = 'enable', $creatorId = 0)
    {
        $title = $parentType . ' ' . $type;
        $description = '';
        $cover = '';
        $isAlbum = true;
        $isDefault = true;

        return $this->create($parentType, $parentId, $type, $title, $description, $cover, $privacyStatus, $status, $isAlbum, $isDefault, $ownerId, $creatorId);
    } // END function


    /*
     * create
     *
     * @param $parentType
     * @param $parentId
     * @param $type
     * @param $title
     * @param $description
     * @param $cover
     * @param $isAlbum
     * @param $isDefault
     * @param $ownerId
     * @param $creatorId
     *
     * @return
     */
    public function create($parentType, $parentId, $type, $title, $description, $cover, $privacyStatus, $status, $isAlbum, $isDefault, $ownerId, $creatorId)
    {
        $data = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'type'  => $type,
            'title'   => $title,
            'description' => $description,
            'cover'        => $cover,
            'privacy_status'        => $privacyStatus,
            'status'        => $status,
            'is_album'     => $isAlbum,
            'is_default' => $isDefault,
            'owner_id'  => $ownerId,
            'creator_id'  => $creatorId
        ];

        return $this->dirRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->dirRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->dirRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->dirRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByParentTypeAndParentIdAndTypeAndOwnerId
     *
     * @param $parentType
     * @param $parentId
     * @param $type
     * @param $ownerId
     *
     * @return
     */
    public function findByParentTypeAndParentIdAndTypeAndOwnerId($parentType, $parentId, $type, $ownerId)
    {
        $where = ['parent_type' => $parentType, 'parent_id' => $parentId, 'type' => $type, 'owner_id' => $ownerId];

        return $this->dirRepo->fetchDatum($where);
    } // END function


    /*
     * findByParentTypeAndParentIdAndType
     *
     * @param $parentType
     * @param $parentId
     * @param $type
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByParentTypeAndParentIdAndType($parentType, $parentId, $type, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['parent_type' => $parentType, 'parent_id' => $parentId, 'type' => $type];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->dirRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByParentTypeAndParentId
     *
     * @param $parentType
     * @param $parentId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByParentTypeAndParentId($parentType, $parentId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['parent_type' => $parentType, 'parent_id' => $parentId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->dirRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByOwnerId
     *
     * @param $ownerId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByOwnerId($ownerId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['owner_id' => $ownerId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->dirRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->dirRepo->fetchDatum($where);
    } // END function

}
