<?php

namespace App\Services;

use App\Repos\SchoolSeoRepo;


/**
 * Class SchoolSeoServ
 *
 * @package namespace App\Services;
 */
class SchoolSeoServ
{


    public function __construct()
    {

        $this->schoolSeoRepo = new SchoolSeoRepo();
    } // END function


    /*
     * create
     *
     * @param $schoolId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $coverTitle
     * @param $coverAlt
     *
     * @return
     */
    public function create($schoolId, $slug, $excerpt = '', $ogTitle = '', $ogDescription = '', $metaTitle = '', $metaDescription = '', $coverTitle = '', $coverAlt = '')
    {
        $data = ['school_id' => $schoolId,
                 'slug' => $slug,
                 'excerpt' => $excerpt,
                 'og_title' => $ogTitle,
                 'og_description' => $ogDescription,
                 'meta_title' => $metaTitle,
                 'meta_description' => $metaDescription,
                 'cover_title' => $coverTitle,
                 'cover_alt' => $coverAlt
        ];

        return $this->schoolSeoRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolSeoRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolSeoRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->schoolSeoRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySlug
     *
     * @param $slug
     *
     * @return
     */
    public function findBySlug($slug)
    {
        $where = ['slug' => $slug];

        return $this->schoolSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolId($schoolId)
    {
        $where = ['school_id' => $schoolId];

        return $this->schoolSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolSeoRepo->fetchDatum($where);
    } // END function

}
