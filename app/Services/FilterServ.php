<?php

namespace App\Services;

use App\Repos\FilterRepo;


/**
 * Class FilterServ
 *
 * @package namespace App\Services;
 */
class FilterServ
{


    public function __construct()
    {

        $this->filterRepo = new FilterRepo();
    } // END function


    /*
     * findSchoolLocationByCountryCode
     *
     * @param $programTypeCode
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findSchoolProgramByProgramTypeCode($code, $orderby = [], $page = -1, $numItems = 20)
    {
        $type = config('tbl_filters.FILTERS_TYPE_SCHOOL');
        $parentType = config('tbl_filters.FILTERS_PARENT_TYPE_PROGRAM_TYPE');

        $datum = $this->findByTypeAndParentTypeAndCode($type, $parentType, $code);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $parentType = config('tbl_filters.FILTERS_PARENT_TYPE_PROGRAM');
        $parentId = $datum->first()->id;

        return $this->findByTypeAndParentTypeAndParentId($type, $parentType, $parentId, $orderby, $page, $numItems);
    } // END function


    /*
     * findSchoolLocationByCountryCode
     *
     * @param $countryCode
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findSchoolLocationByCountryCode($code, $orderby = [], $page = -1, $numItems = 20)
    {
        $type = config('tbl_filters.FILTERS_TYPE_SCHOOL');
        $parentType = config('tbl_filters.FILTERS_PARENT_TYPE_COUNTRY');

        $datum = $this->findByTypeAndParentTypeAndCode($type, $parentType, $code);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $parentType = config('tbl_filters.FILTERS_PARENT_TYPE_LOCATION');
        $parentId = $datum->first()->id;

        return $this->findByTypeAndParentTypeAndParentId($type, $parentType, $parentId, $orderby, $page, $numItems);
    } // END function


    /*
     * create
     *
     * @param $code
     * @param $parentType
     * @param $parentId
     * @param $type
     * @param $creatorId
     *
     * @return
     */
    public function create($code, $parentType = 'country', $parentId = 0, $type = 'school', $creatorId = 0)
    {
        $data = ['code' => $code,
                 'parent_type' => $parentType,
                 'parent_id' => $parentId,
                 'type' => $type,
                 'creator_id' => $creatorId];

        return $this->filterRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->filterRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->filterRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->filterRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByTypeAndParentTypeAndCode
     *
     * @param $type
     * @param $parentType
     * @param $code
     *
     * @return
     */
    public function findByTypeAndParentTypeAndCode($type, $parentType, $code)
    {
        $where = ['type' => $type, 'parent_type' => $parentType, 'code' => $code];

        return $this->filterRepo->fetchDatum($where);
    } // END function


    /*
     * findByTypeAndParentTypeAndParentId
     *
     * @param $type
     * @param $parentType
     * @param $parentId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByTypeAndParentTypeAndParentId($type, $parentType, $parentId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type, 'parent_type' => $parentType, 'parent_id' => $parentId];

        return $this->filterRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByTypeAndCode
     *
     * @param $type
     * @param $code
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByTypeAndCode($type, $code, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type, 'code' => $code];

        return $this->filterRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByTypeAndParentId
     *
     * @param $type
     * @param $parentId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByTypeAndParentId($type, $parentId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type, 'parent_id' => $parentId];

        return $this->filterRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByTypeAndParentType
     *
     * @param $type
     * @param $parentType
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByTypeAndParentType($type, $parentType, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type, 'parent_type' => $parentType];

        return $this->filterRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByParentTypeAndParentId
     *
     * @param $parentType
     * @param $parentId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByParentTypeAndParentId($parentType, $parentId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['parent_type' => $parentType, 'parent_id' => $parentId];

        return $this->filterRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByCode($code, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['code' => $code];

        return $this->filterRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->filterRepo->fetchDatum($where);
    } // END function

}
