<?php

namespace App\Services;

use App\Repos\UserProfileRepo;


/**
 * Class UserProfileServ
 *
 * @package namespace App\Services;
 */
class UserProfileServ
{


    public function __construct()
    {

        $this->userProfileRepo = new UserProfileRepo();
    } // END function


    /*
     * create
     *
     * @param $email
     *
     * @return
     */
    public function create($userId, $email, $mobileCountryCode, $mobilePhone, $firstName = '', $lastName = '', $nickname = '', $avatar = '', $birth = '', $line = '')
    {
        $data = ['user_id' => $userId,
                 'first_name' => $firstName,
                 'last_name' => $lastName,
                 'nickname' => $nickname,
                 'avatar' => $avatar,
                 'birth' => $birth,
                 'email' => $email,
                 'mobile_country_code' => $mobileCountryCode,
                 'mobile_phone' => $mobilePhone,
                 'line' => $line
        ];

        return $this->userProfileRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userProfileRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userProfileRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->userProfileRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByMobileCountryCodeAndPhone
     *
     * @param $mobileCountryCode
     * @param $mobilePhone
     *
     * @return
     */
    public function findByMobileCountryCodeAndPhone($mobileCountryCode, $mobilePhone)
    {
        $where = ['mobile_country_code' => $mobileCountryCode, 'mobile_phone' => $mobilePhone];

        return $this->userProfileRepo->fetchDatum($where);
    } // END function


    /*
     * findByNickname
     *
     * @param $nickname
     *
     * @return
     */
    public function findByNickname($nickname)
    {
        $where = ['nickname' => $nickname];

        return $this->userProfileRepo->fetchDatum($where);
    } // END function


    /*
     * findByMobilePhone
     *
     * @param $mobilePhone
     *
     * @return
     */
    public function findByMobilePhone($mobilePhone, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['mobile_phone' => $mobilePhone];

        return $this->userProfileRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function

    /*
     * findByUserId
     *
     * @param $userId
     *
     * @return
     */
    public function findByUserId($userId)
    {
        $where = ['user_id' => $userId];

        return $this->userProfileRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->userProfileRepo->fetchDatum($where);
    } // END function

}
