<?php

namespace App\Services;

use App\Repos\PostcategoryRepo;
use App\Services\PostcategorySeoServ;


/**
 * Class PostcategoryServ
 *
 * @package namespace App\Services;
 */
class PostcategoryServ
{


    public function __construct()
    {

        $this->postcategoryRepo = new PostcategoryRepo();
        $this->postcategorySeoServ = new PostcategorySeoServ();
    } // END function


    /*
     * updateNumItems
     *
     * @param $id
     * @param $num
     *
     * @return
     */
    public function updateNumItems($id, $num = 1)
    {

        $datum = $this->findById($id);

        if ($datum->isEmpty()) {
            return false;
        } // END if

        $data  = ['num_items' => intval($datum->first()->num_items) + $num];
        $where = ['id' => $id];

        return $this->update($data, $where);
    } // END function


    /*
     * create
     *
     * @param $type
     * @param $name
     * @param $cover
     * @param $status
     *
     * @return
     */
    public function create($type, $name, $cover = '', $status = 'enable', $ownerId = 0, $creatorId = 0)
    {
        $data = ['type' => $type,
                 'name' => $name,
                 'cover' => $cover,
                 'status' => $status,
                 'owner_id' => $ownerId,
                 'creator_id' => $creatorId
        ];

        return $this->postcategoryRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postcategoryRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postcategoryRepo->deleteData($where);
    } // END function


// ===
    /*
     * getList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM postcategories AS c ";
        $query .= "LEFT JOIN postcategory_seos AS s ON s.postcategory_id = c.id ";
        $query .= "WHERE 1 ";

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postcategoryRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByTypeAndNameAndOwnerId
     *
     * @param $type
     * @param $name
     * @param $ownerId
     *
     * @return
     */
    public function getByTypeAndNameAndOwnerId($type, $name, $ownerId)
    {
        $where = ['type' => $type, 'name' => $name, 'owner_id' => $ownerId];
        $datum = $this->postcategoryRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->postcategorySeoServ->findByPostcategoryId($datum->first()->id);

        if ($seoDatum->isNotEmpty()) {
            $datum->first()->slug    = $seoDatum->first()->slug;
            $datum->first()->excerpt = $seoDatum->first()->excerpt;
            $datum->first()->og_title       = $seoDatum->first()->og_title;
            $datum->first()->og_description = $seoDatum->first()->og_description;
            $datum->first()->meta_title       = $seoDatum->first()->meta_title;
            $datum->first()->meta_description = $seoDatum->first()->meta_description;
            $datum->first()->cover_title = $seoDatum->first()->cover_title;
            $datum->first()->cover_alt   = $seoDatum->first()->cover_alt;
        } // END if

        return $datum;
    } // END function


    /*
     * getByTypeAndName
     *
     * @param $type
     * @param $name
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByTypeAndName($type, $name, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM postcategories AS c ";
        $query .= "LEFT JOIN postcategory_seos AS s ON s.postcategory_id = c.id ";
        $query .= "WHERE c.type LIKE :type ";
        $query .= "AND c.name LIKE :name ";

        $bindValues['type'] = $type;
        $bindValues['name'] = $name;

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postcategoryRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByTypeAndOwnerId
     *
     * @param $type
     * @param $name
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByTypeAndOwnerId($type, $ownerId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM postcategories AS c ";
        $query .= "LEFT JOIN postcategory_seos AS s ON s.postcategory_id = c.id ";
        $query .= "WHERE c.type LIKE :type ";
        $query .= "AND c.owner_id LIKE :ownerId ";

        $bindValues['type'] = $type;
        $bindValues['ownerId'] = $ownerId;

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postcategoryRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByOwnerId
     *
     * @param $ownerId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByOwnerId($ownerId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM postcategories AS c ";
        $query .= "LEFT JOIN postcategory_seos AS s ON s.postcategory_id = c.id ";
        $query .= "WHERE c.owner_id LIKE :ownerId ";

        $bindValues['ownerId'] = $ownerId;

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postcategoryRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByName
     *
     * @param $name
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByName($name, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM postcategories AS c ";
        $query .= "LEFT JOIN postcategory_seos AS s ON s.postcategory_id = c.id ";
        $query .= "WHERE c.name LIKE :name ";

        $bindValues['name'] = $name;

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postcategoryRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByType
     *
     * @param $type
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByType($type, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM postcategories AS c ";
        $query .= "LEFT JOIN postcategory_seos AS s ON s.postcategory_id = c.id ";
        $query .= "WHERE c.type LIKE :type ";

        $bindValues['type'] = $type;

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postcategoryRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getById
     *
     * @param $id
     *
     * @return
     */
    public function getById($id)
    {
        $where = ['id' => $id];
        $datum = $this->postcategoryRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->postcategorySeoServ->findByPostcategoryId($id);

        if ($seoDatum->isNotEmpty()) {
            $datum->first()->slug    = $seoDatum->first()->slug;
            $datum->first()->excerpt = $seoDatum->first()->excerpt;
            $datum->first()->og_title       = $seoDatum->first()->og_title;
            $datum->first()->og_description = $seoDatum->first()->og_description;
            $datum->first()->meta_title       = $seoDatum->first()->meta_title;
            $datum->first()->meta_description = $seoDatum->first()->meta_description;
            $datum->first()->cover_title = $seoDatum->first()->cover_title;
            $datum->first()->cover_alt   = $seoDatum->first()->cover_alt;
        } // END if

        return $datum;
    } // END function


// ===
    /*
     * findList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postcategoryRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByTypeAndNameAndOwnerId
     *
     * @param $type
     * @param $name
     * @param $ownerId
     *
     * @return
     */
    public function findByTypeAndNameAndOwnerId($type, $name, $ownerId)
    {
        $where = ['type' => $type, 'name' => $name, 'owner_id' => $ownerId];

        return $this->postcategoryRepo->fetchDatum($where);
    } // END function


    /*
     * findByTypeAndName
     *
     * @param $type
     * @param $name
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByTypeAndName($type, $name, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type, 'name' => $name];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postcategoryRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByTypeAndOwnerId
     *
     * @param $type
     * @param $ownerId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByTypeAndOwnerId($type, $ownerId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type, 'owner_id' => $ownerId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postcategoryRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByName
     *
     * @param $name
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByName($name, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['name' => $name];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postcategoryRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByType
     *
     * @param $type
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByType($type, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postcategoryRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->postcategoryRepo->fetchDatum($where);
    } // END function


}
