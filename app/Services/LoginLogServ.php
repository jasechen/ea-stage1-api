<?php

namespace App\Services;

use App\Repos\LoginLogRepo;


/**
 * Class LoginLogServ
 *
 * @package namespace App\Services;
 */
class LoginLogServ
{


    public function __construct()
    {

        $this->loginLogRepo = new LoginLogRepo();
    } // END function


    /*
     * create
     *
     * @param $email
     *
     * @return
     */
    public function create($sessionId, $deviceId, $ownerId)
    {
        $data = ['session_id' => $sessionId,
                 'device_id'  => $deviceId,
                 'owner_id'   => $ownerId
        ];

        return $this->loginLogRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->loginLogRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->loginLogRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->loginLogRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByOwnerId
     *
     * @param $ownerId
     *
     * @return
     */
    public function findByOwnerId($ownerId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['owner_id' => $ownerId];

        return $this->loginLogRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByDeviceId
     *
     * @param $deviceId
     *
     * @return
     */
    public function findByDeviceId($deviceId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['device_id' => $deviceId];

        return $this->loginLogRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySessionId
     *
     * @param $sessionId
     *
     * @return
     */
    public function findBySessionId($sessionId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['session_id' => $sessionId];

        return $this->loginLogRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->loginLogRepo->fetchDatum($where);
    } // END function

}
