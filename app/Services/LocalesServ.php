<?php

namespace App\Services;

use App\Repos\LocalesRepo;
use App\Services\LocalesContentServ;

/**
 * Class LocalesServ
 *
 * @package namespace App\Services;
 */
class LocalesServ
{


    public function __construct()
    {

        $this->localesRepo = new LocalesRepo();
    } // END function


    public function findCodeLangsByCode($code, $prefix, $subPrefix = '')
    {
        $code = trim($code);
        $localesContentServ = app(LocalesContentServ::class);

        $langContent = [];
        $current_languages = config('global_languages.CURRENT_LANGUAGES');

        foreach ($current_languages as $lang) {
            $langContent[$lang] = $code;
        } // END foreach

        $code = ($prefix == 'country_name_') ? strtoupper($code) : $code;
        $code = ($prefix == 'city_name_') ? strtoupper($subPrefix) . '_' . strtoupper($code) : $code;

        $datum = $this->findByCode($prefix . $code);

        if ($datum->isEmpty()) {
            return $langContent;
        } // END if

        $id = $datum->first()->id;

        $contentData = $localesContentServ->findByLocalesId($id);
        if ($contentData->isEmpty()) {
            return $langContent;
        } // END if

        foreach ($contentData->all() as $contentDatum) {

            $lang = $contentDatum->lang;
            $content = $contentDatum->content;

            if (!in_array($lang, $current_languages)) {continue;}

            $langContent[$lang] = $content;
        } // END foreach

        return $langContent;
    } // END function


    /*
     * create
     *
     * @param $code
     * @param $note
     * @param $creatorId
     *
     * @return
     */
    public function create($code, $note, $creatorId = 0)
    {
        $data = ['code'    => $code,
                 'note'   => $note,
                 'creator_id' => $creatorId];

        return $this->localesRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->localesRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->localesRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->localesRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->localesRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->localesRepo->fetchDatum($where);
    } // END function

}
