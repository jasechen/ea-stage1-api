<?php

namespace App\Services;

use App\Repos\SchoolFeeRepo;


/**
 * Class SchoolFeeServ
 *
 * @package namespace App\Services;
 */
class SchoolFeeServ
{


    public function __construct()
    {

        $this->schoolFeeRepo = new SchoolFeeRepo();
    } // END function


    /*
     * create
     *
     * @param $schoolId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $coverTitle
     * @param $coverAlt
     *
     * @return
     */
    public function create($schoolId, $tuition, $dorm, $unit = '', $currency = 'usd')
    {
        $data = ['school_id' => $schoolId,
                 'tuition' => $tuition,
                 'dorm' => $dorm,
                 'unit' => $unit,
                 'currency' => $currency
        ];

        return $this->schoolFeeRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolFeeRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolFeeRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->schoolFeeRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolId($schoolId)
    {
        $where = ['school_id' => $schoolId];

        return $this->schoolFeeRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolFeeRepo->fetchDatum($where);
    } // END function

}
