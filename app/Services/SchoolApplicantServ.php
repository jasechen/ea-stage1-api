<?php

namespace App\Services;

use App\Repos\SchoolApplicantRepo;


/**
 * Class SchoolApplicantServ
 *
 * @package namespace App\Services;
 */
class SchoolApplicantServ
{


    public function __construct()
    {

        $this->schoolApplicantRepo = new SchoolApplicantRepo();
    } // END function


    /*
     * create
     *
     * @param $schoolId
     * @param $agentId
     * @param $applicantId
     *
     * @return
     */
    public function create($schoolId, $agentId, $applicantId)
    {
        $data = ['school_id' => $schoolId,
                 'agent_id' => $agentId,
                 'applicant_id' => $applicantId
        ];

        return $this->schoolApplicantRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolApplicantRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolApplicantRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->schoolApplicantRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolIdAndAgentIdAndApplicantId
     *
     * @param $schoolId
     * @param $agentId
     * @param $applicantId
     *
     * @return
     */
    public function findBySchoolIdAndAgentIdAndApplicantId($schoolId, $agentId, $applicantId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId, 'agent_id' => $agentId, 'applicant_id' => $applicantId];

        return $this->schoolApplicantRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByAgentIdAndApplicantId
     *
     * @param $agentId
     * @param $applicantId
     *
     * @return
     */
    public function findByAgentIdAndApplicantId($agentId, $applicantId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['agent_id' => $agentId, 'applicant_id' => $applicantId];

        return $this->schoolApplicantRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolIdAndApplicantId
     *
     * @param $schoolId
     * @param $applicantId
     *
     * @return
     */
    public function findBySchoolIdAndApplicantId($schoolId, $applicantId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId, 'applicant_id' => $applicantId];

        return $this->schoolApplicantRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolIdAndAgentId
     *
     * @param $schoolId
     * @param $agentId
     *
     * @return
     */
    public function findBySchoolIdAndAgentId($schoolId, $agentId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId, 'agent_id' => $agentId];

        return $this->schoolApplicantRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByApplicantId
     *
     * @param $applicantId
     *
     * @return
     */
    public function findByApplicantId($applicantId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['applicant_id' => $applicantId];

        return $this->schoolApplicantRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByAgentId
     *
     * @param $agentId
     *
     * @return
     */
    public function findByAgentId($agentId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['agent_id' => $agentId];

        return $this->schoolApplicantRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolId($schoolId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId];

        return $this->schoolApplicantRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolApplicantRepo->fetchDatum($where);
    } // END function

}
