<?php

namespace App\Services;

use App\Repos\UserSeoRepo;


/**
 * Class UserSeoServ
 *
 * @package namespace App\Services;
 */
class UserSeoServ
{


    public function __construct()
    {

        $this->userSeoRepo = new UserSeoRepo();
    } // END function


    /*
     * create
     *
     * @param $userId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $avatarTitle
     * @param $avatarAlt
     *
     * @return
     */
    public function create($userId, $slug, $excerpt = '', $ogTitle = '', $ogDescription = '', $metaTitle = '', $metaDescription = '', $avatarTitle = '', $avatarAlt = '')
    {
        $data = ['user_id' => $userId,
                 'slug' => $slug,
                 'excerpt' => $excerpt,
                 'og_title' => $ogTitle,
                 'og_description' => $ogDescription,
                 'meta_title' => $metaTitle,
                 'meta_description' => $metaDescription,
                 'avatar_title' => $avatarTitle,
                 'avatar_alt' => $avatarAlt
        ];

        return $this->userSeoRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userSeoRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userSeoRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->userSeoRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySlug
     *
     * @param $slug
     *
     * @return
     */
    public function findBySlug($slug)
    {
        $where = ['slug' => $slug];

        return $this->userSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findByUserId
     *
     * @param $userId
     *
     * @return
     */
    public function findByUserId($userId)
    {
        $where = ['user_id' => $userId];

        return $this->userSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->userSeoRepo->fetchDatum($where);
    } // END function

}
