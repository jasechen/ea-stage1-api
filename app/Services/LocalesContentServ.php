<?php

namespace App\Services;

use App\Repos\LocalesContentRepo;


/**
 * Class LocalesContentServ
 *
 * @package namespace App\Services;
 */
class LocalesContentServ
{


    public function __construct()
    {

        $this->localesContentRepo = new LocalesContentRepo();
    } // END function


    /*
     * create
     *
     * @param $localesId
     * @param $lang
     * @param $content
     * @param $creatorId
     *
     * @return
     */
    public function create($localesId, $lang, $content, $creatorId = 0)
    {
        $data = ['locales_id' => $localesId,
                 'lang' => $lang,
                 'content' => $content,
                 'creator_id' => $creatorId];

        return $this->localesContentRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->localesContentRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->localesContentRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->localesContentRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByLangAndContent
     *
     * @param $lang
     * @param $content
     *
     * @return
     */
    public function findByLangAndContent($lang, $content)
    {
        $where = ['lang' => $lang, 'content' => $content];

        return $this->localesContentRepo->fetchDatum($where);
    } // END function


    /*
     * findByLocalesIdAndLang
     *
     * @param $localesId
     * @param $lang
     *
     * @return
     */
    public function findByLocalesIdAndLang($localesId, $lang)
    {
        $where = ['locales_id' => $localesId, 'lang' => $lang];

        return $this->localesContentRepo->fetchDatum($where);
    } // END function


    /*
     * findByLang
     *
     * @param $lang
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByLang($lang, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['lang' => $lang];

        return $this->localesContentRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByLocalesId
     *
     * @param $localesId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByLocalesId($localesId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['locales_id' => $localesId];

        return $this->localesContentRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->localesContentRepo->fetchDatum($where);
    } // END function

}
