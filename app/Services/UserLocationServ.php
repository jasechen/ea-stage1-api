<?php

namespace App\Services;

use App\Repos\UserLocationRepo;


/**
 * Class UserLocationServ
 *
 * @package namespace App\Services;
 */
class UserLocationServ
{


    public function __construct()
    {

        $this->userLocationRepo = new UserLocationRepo();
    } // END function


    /*
     * create
     *
     * @param $userId
     * @param $code
     *
     * @return
     */
    public function create($userId, $code)
    {
        $data = ['user_id' => $userId,
                 'code' => $code
        ];

        return $this->userLocationRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userLocationRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userLocationRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        return $this->userLocationRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserIdAndCode
     *
     * @param $userId
     * @param $code
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByUserIdAndCode($userId, $code, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId, 'code' => $code];

        return $this->userLocationRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserId
     *
     * @param $userId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByUserId($userId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId];

        return $this->userLocationRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->userLocationRepo->fetchDatum($where);
    } // END function


}
