<?php

namespace App\Services;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

use App\Services\UserServ;
use App\Repos\SessionRepo;


/**
 * Class SessionServ
 *
 * @package namespace App\Services;
 */
class SessionServ
{


    public function __construct()
    {

        $this->userServ = new UserServ();
        $this->sessionRepo = new SessionRepo();
    } // END function


    /**
     *
     */
    public function isValidCodeAndToken($code, $token)
    {

        $userDatum = $this->userServ->findByToken($token);

        if ($userDatum->isEmpty()) {
            return false;
        } // END if


        $sessionDatum = $this->findByCodeAndOwnerId($code, $userDatum->first()->id);

        return $sessionDatum->isEmpty() ? false : true;
    } // END function


    /**
     *
     */
    public function initCode()
    {
        do {

            $code = md5(Uuid::generate(4)->string);
            $sessionDatum = $this->findByCode($code);

        } while ($sessionDatum->isNotEmpty());

        return $code;
    } // END function


    /**
     *
     */
    public function isAlive($code)
    {
        $sessionDatum = $this->findByCode($code);

        if ($sessionDatum->isEmpty()) {
            return false;
        } // END if

        $now = Carbon::now();

        $sessionUpdateAt = Carbon::parse($sessionDatum->first()->updated_at);
        $deadline = $sessionUpdateAt->timestamp + $sessionDatum->first()->expire_time;

        return ($deadline < $now->timestamp) ? false : true;
    } // END function


    /*
     * create
     *
     * @param $email
     *
     * @return
     */
    public function create($deviceId, $remoteAddress)
    {
        $data = ['code'        => $this->initCode(),
                 'device_id'   => $deviceId,
                 'expire_time' => config('session.lifetime'),
                 'remote_address' => $remoteAddress,
        ];

        return $this->sessionRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->sessionRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->sessionRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->sessionRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByCodeAndOwnerId
     *
     * @param $code
     * @param $ownerId
     *
     * @return
     */
    public function findByCodeAndOwnerId($code, $ownerId)
    {
        $where = ['code' => $code, 'owner_id' => $ownerId];

        return $this->sessionRepo->fetchDatum($where);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     *
     * @return
     */
    public function findByStatus($status, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['status' => $status];

        return $this->sessionRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     *
     * @return
     */
    public function findByDeviceId($deviceId)
    {
        $where = ['device_id' => $deviceId];

        return $this->sessionRepo->fetchDatum($where);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->sessionRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->sessionRepo->fetchDatum($where);
    } // END function

}
