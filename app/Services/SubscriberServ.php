<?php

namespace App\Services;

use App\Repos\SubscriberRepo;


/**
 * Class SubscriberServ
 *
 * @package namespace App\Services;
 */
class SubscriberServ
{


    public function __construct()
    {

        $this->subscriberRepo = new SubscriberRepo();
    } // END function


    /*
     * create
     *
     * @param $sessionId
     * @param $name
     * @param $email
     * @param $ownerId
     * @param $status
     *
     * @return
     */
    public function switchStatusByEmail($email)
    {
        $datum = $this->findByEmail($email);

        if ($datum->isEmpty()) {
            return $datum;
        }

        $status = $datum->first()->status == config('tbl_subscribers.SUBSCRIBERS_STATUS_SUBSCRIBE') ? config('tbl_subscribers.SUBSCRIBERS_STATUS_UNSUBSCRIBE') : config('tbl_subscribers.SUBSCRIBERS_STATUS_SUBSCRIBE');

        return $this->update(['status' => $status], ['id' => $datum->first()->id]);
    } // END function


    /*
     * create
     *
     * @param $sessionId
     * @param $name
     * @param $email
     * @param $ownerId
     * @param $status
     *
     * @return
     */
    public function create($sessionId, $name, $email, $ownerId = '', $status = 'subscribe')
    {
        $data = ['session_id' => $sessionId,
                 'name' => $name,
                 'email' => $email,
                 'owner_id' => $ownerId,
                 'status' => $status
        ];

        return $this->subscriberRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->subscriberRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->subscriberRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->subscriberRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     *
     * @return
     */
    public function findByStatus($status, $orderby = [], $page = -1, $numItems = 10)
    {
        $where = ['status' => $status];

        return $this->subscriberRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByEmail
     *
     * @param $email
     *
     * @return
     */
    public function findByEmail($email)
    {
        $where = ['email' => $email];

        return $this->subscriberRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->subscriberRepo->fetchDatum($where);
    } // END function

}
