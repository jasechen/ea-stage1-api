<?php

namespace App\Services;

use App\Repos\SchoolRepo;
use App\Services\SchoolSeoServ;


/**
 * Class SchoolServ
 *
 * @package namespace App\Services;
 */
class SchoolServ
{


    public function __construct()
    {

        $this->schoolRepo = new SchoolRepo();
        $this->schoolSeoServ = new SchoolSeoServ();
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $description
     * @param $cover
     * @param $status
     * @param $creatorId
     *
     * @return
     */
    public function create($name, $description = '', $facility = '', $cover = '', $status = 'enable', $creatorId = 0)
    {
        $data = ['name' => $name,
                 'description' => $description,
                 'facility' => $facility,
                 'cover' => $cover,
                 'status' => $status,
                 'creator_id' => $creatorId
        ];

        return $this->schoolRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolRepo->deleteData($where);
    } // END function


// ===
    /*
     * getList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM schools AS c ";
        $query .= "LEFT JOIN school_seos AS s ON s.school_id = c.id ";
        $query .= "WHERE 1 ";

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->schoolRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getById
     *
     * @param $id
     *
     * @return
     */
    public function getById($id)
    {
        $where = ['id' => $id];
        $datum = $this->schoolRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->schoolSeoServ->findBySchoolId($id);

        if ($seoDatum->isNotEmpty()) {
            $datum->first()->slug    = $seoDatum->first()->slug;
            $datum->first()->excerpt = $seoDatum->first()->excerpt;
            $datum->first()->og_title       = $seoDatum->first()->og_title;
            $datum->first()->og_description = $seoDatum->first()->og_description;
            $datum->first()->meta_title       = $seoDatum->first()->meta_title;
            $datum->first()->meta_description = $seoDatum->first()->meta_description;
            $datum->first()->cover_title = $seoDatum->first()->cover_title;
            $datum->first()->cover_alt   = $seoDatum->first()->cover_alt;
        } // END if

        return $datum;
    } // END function


// ===
    /*
     * findList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->schoolRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolRepo->fetchDatum($where);
    } // END function


}
