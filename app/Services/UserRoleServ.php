<?php

namespace App\Services;

use App\Repos\UserRoleRepo;
use App\Services\RoleServ;

/**
 * Class UserRoleServ
 *
 * @package namespace App\Services;
 */
class UserRoleServ
{


    public function __construct()
    {

        $this->userRoleRepo = new UserRoleRepo();
        $this->roleServ = new RoleServ();
    } // END function


    /*
     * initWebAdmin
     *
     * @param $userId
     * @param $creatorId
     *
     * @return
     */
    public function initWebAdmin($userId, $creatorId = 0)
    {
        $roleDatum = $this->roleServ->findByTypeAndCode('general', 'admin');

        return $this->create($userId, $roleDatum->first()->id, $creatorId);
    } // ENd function


    /*
     * initWebManager
     *
     * @param $userId
     * @param $creatorId
     *
     * @return
     */
    public function initWebManager($userId, $creatorId = 0)
    {
        $roleDatum = $this->roleServ->findByTypeAndCode('general', 'manager');

        return $this->create($userId, $roleDatum->first()->id, $creatorId);
    } // ENd function


    /*
     * initWebEditor
     *
     * @param $userId
     * @param $creatorId
     *
     * @return
     */
    public function initWebEditor($userId, $creatorId = 0)
    {
        $roleDatum = $this->roleServ->findByTypeAndCode('general', 'editor');

        return $this->create($userId, $roleDatum->first()->id, $creatorId);
    } // ENd function


    /*
     * initWebAuthor
     *
     * @param $userId
     * @param $creatorId
     *
     * @return
     */
    public function initWebAuthor($userId, $creatorId = 0)
    {
        $roleDatum = $this->roleServ->findByTypeAndCode('general', 'author');

        return $this->create($userId, $roleDatum->first()->id, $creatorId);
    } // ENd function


    /*
     * initWebMember
     *
     * @param $userId
     * @param $creatorId
     *
     * @return
     */
    public function initWebMember($userId, $creatorId = 0)
    {
        $roleDatum = $this->roleServ->findByTypeAndCode('general', 'member');

        return $this->create($userId, $roleDatum->first()->id, $creatorId);
    } // ENd function


    /*
     * create
     *
     * @param $userId
     * @param $roleId
     * @param $creatorId
     *
     * @return
     */
    public function create($userId, $roleId, $creatorId = 0)
    {
        $data = ['user_id'   => $userId,
                 'role_id'    => $roleId,
                 'creator_id' => $creatorId];

        return $this->userRoleRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userRoleRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userRoleRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->userRoleRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserIdAndRoleId
     *
     * @param $userId
     * @param $roleId
     *
     * @return
     */
    public function findByUserIdAndRoleId($userId, $roleId)
    {
        $where = ['user_id' => $userId, 'role_id' => $roleId];

        return $this->userRoleRepo->fetchDatum($where);
    } // END function


    /*
     * findByRoleId
     *
     * @param $roleId
     *
     * @return
     */
    public function findByRoleId($roleId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['role_id' => $roleId];

        return $this->userRoleRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserId
     *
     * @param $userId
     *
     * @return
     */
    public function findByUserId($userId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId];

        return $this->userRoleRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->userRoleRepo->fetchDatum($where);
    } // END function

}
