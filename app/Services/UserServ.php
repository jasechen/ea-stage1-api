<?php

namespace App\Services;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

use App\Repos\UserRepo;
use App\Services\UserSeoServ;

/**
 * Class UserServ
 *
 * @package namespace App\Services;
 */
class UserServ
{


    public function __construct()
    {

        $this->userRepo = new UserRepo();
        $this->userSeoServ = new UserSeoServ();
    } // END function


    /*
     * update password
     *
     * @param $password
     * @param $id
     *
     * @return
     */
    public function updatePassword($password, $id)
    {
        $datum = $this->findById($id);

        if ($datum->isEmpty()) {
            return false;
        } // END if

        $data = ['password'   => password_hash($password, PASSWORD_BCRYPT)];
        $where = ['id' => $id];

        return $this->update($data, $where);
    } // END function


    /**
     *
     */
    public function initToken($account = '')
    {
        $now = Carbon::now();
        $account = empty($account) ? mt_rand(10000000, 99999999) : $account;

        do {
            $tokenName = $account . "_" . $now->timestamp;
            $token = Uuid::generate(5, $tokenName, Uuid::NS_DNS)->string;

            $userDatum = $this->findByToken($token);
        } while ($userDatum->isNotEmpty());

        return $token;
    } // END function


    /*
     * create
     *
     * @param $email
     *
     * @return
     */
    public function create($account, $password, $status = 'init', $creatorId = 0)
    {
        $data = ['account'    => $account,
                 'password'   => password_hash($password, PASSWORD_BCRYPT),
                 'token'      => $this->initToken($account),
                 'status'     => $status,
                 'creator_id' => $creatorId
        ];

        return $this->userRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userRepo->deleteData($where);
    } // END function


// ===
    /*
     * getByTerm
     *
     * @param $term
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByTerm($term, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT u.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.avatar_title, s.avatar_alt ";
        $query .= "FROM users AS u ";
        $query .= "LEFT JOIN user_seos AS s ON s.user_id = u.id ";
        $query .= "LEFT JOIN user_profiles AS p ON p.user_id = u.id ";
        $query .= "WHERE (s.slug LIKE :slug ";
        $query .= "OR p.nickname LIKE :nickname) ";

        $bindValues['slug'] = '%' . $term . '%';
        $bindValues['nickname'] = '%' . $term . '%';

        if (!empty($status)) {
            $query .= "AND u.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->userRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT u.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.avatar_title, s.avatar_alt ";
        $query .= "FROM users AS u ";
        $query .= "LEFT JOIN user_seos AS s ON s.user_id = u.id ";
        $query .= "WHERE 1 ";

        if (!empty($status)) {
            $query .= "AND u.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->userRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByCreatorId
     *
     * @param $creatorId
     *
     * @return
     */
    public function getByCreatorId($creatorId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT u.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.avatar_title, s.avatar_alt ";
        $query .= "FROM users AS u ";
        $query .= "LEFT JOIN user_seos AS s ON s.user_id = u.id ";
        $query .= "WHERE u.creator_id = :creatorId ";

        $bindValues['creatorId'] = $creatorId;

        if (!empty($status)) {
            $query .= "AND u.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->userRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByStatus
     *
     * @param $status
     *
     * @return
     */
    public function getByStatus($status, $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT u.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.avatar_title, s.avatar_alt ";
        $query .= "FROM users AS u ";
        $query .= "LEFT JOIN user_seos AS s ON s.user_id = u.id ";
        $query .= "WHERE u.status = :status ";

        $bindValues['status'] = $status;

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->userRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByToken
     *
     * @param $token
     *
     * @return
     */
    public function getByToken($token)
    {
        $where = ['token' => $token];

        $datum = $this->userRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->userSeoServ->findByUserId($datum->first()->id);

        $datum->first()->slug    = $seoDatum->first()->slug;
        $datum->first()->excerpt = $seoDatum->first()->excerpt;
        $datum->first()->og_title       = $seoDatum->first()->og_title;
        $datum->first()->og_description = $seoDatum->first()->og_description;
        $datum->first()->meta_title       = $seoDatum->first()->meta_title;
        $datum->first()->meta_description = $seoDatum->first()->meta_description;
        $datum->first()->avatar_title = $seoDatum->first()->avatar_title;
        $datum->first()->avatar_alt   = $seoDatum->first()->avatar_alt;

        return $datum;
    } // END function


    /*
     * getByAccount
     *
     * @param $account
     *
     * @return
     */
    public function getByAccount($account)
    {
        $where = ['account' => $account];

        $datum = $this->userRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->userSeoServ->findByUserId($datum->first()->id);

        $datum->first()->slug    = $seoDatum->first()->slug;
        $datum->first()->excerpt = $seoDatum->first()->excerpt;
        $datum->first()->og_title       = $seoDatum->first()->og_title;
        $datum->first()->og_description = $seoDatum->first()->og_description;
        $datum->first()->meta_title       = $seoDatum->first()->meta_title;
        $datum->first()->meta_description = $seoDatum->first()->meta_description;
        $datum->first()->avatar_title = $seoDatum->first()->avatar_title;
        $datum->first()->avatar_alt   = $seoDatum->first()->avatar_alt;

        return $datum;
    } // END function


    /*
     * getById
     *
     * @param $id
     *
     * @return
     */
    public function getById($id)
    {
        $where = ['id' => $id];

        $datum = $this->userRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->userSeoServ->findByUserId($id);

        $datum->first()->slug    = $seoDatum->first()->slug;
        $datum->first()->excerpt = $seoDatum->first()->excerpt;
        $datum->first()->og_title       = $seoDatum->first()->og_title;
        $datum->first()->og_description = $seoDatum->first()->og_description;
        $datum->first()->meta_title       = $seoDatum->first()->meta_title;
        $datum->first()->meta_description = $seoDatum->first()->meta_description;
        $datum->first()->avatar_title = $seoDatum->first()->avatar_title;
        $datum->first()->avatar_alt   = $seoDatum->first()->avatar_alt;

        return $datum;
    } // END function


// ==
    /*
     * findList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->userRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCreatorId
     *
     * @param $creatorId
     *
     * @return
     */
    public function findByCreatorId($creatorId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['creator_id' => $creatorId];

        return $this->userRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     *
     * @return
     */
    public function findByStatus($status, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['status' => $status];

        return $this->userRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByToken
     *
     * @param $token
     *
     * @return
     */
    public function findByToken($token)
    {
        $where = ['token' => $token];

        return $this->userRepo->fetchDatum($where);
    } // END function


    /*
     * findByAccount
     *
     * @param $account
     *
     * @return
     */
    public function findByAccount($account)
    {
        $where = ['account' => $account];

        return $this->userRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->userRepo->fetchDatum($where);
    } // END function

}
