<?php

namespace App\Services;

use Illuminate\Support\Collection;

use App\Repos\SchoolDormRepo;

use App\Services\SchoolDormRoomServ;


/**
 * Class SchoolDormServ
 *
 * @package namespace App\Services;
 */
class SchoolDormServ
{


    public function __construct()
    {

        $this->schoolDormRepo = new SchoolDormRepo();
        $this->schoolDormRoomServ = new SchoolDormRoomServ();
    } // END function


    /*
     * create
     *
     * @param $schoolId
     * @param $type
     * @param $service
     * @param $facility
     *
     * @return
     */
    public function create($schoolId, $type, $service, $facility)
    {
        $data = ['school_id' => $schoolId,
                 'type' => $type,
                 'service' => $service,
                 'facility' => $facility
        ];

        return $this->schoolDormRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolDormRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolDormRepo->deleteData($where);
    } // END function


    /*
     * getList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getList($orderby = [], $page = -1, $numItems = 20)
    {
        $data = $this->findList($orderby, $page, $numItems);

        if ($data->isEmpty()) {
            return $data;
        } // END if

        $finalData = [];
        foreach ($data->all() as $datum) {

            $datum->rooms = [];

            $roomData = $this->schoolDormRoomServ->findBySchoolIdAndDormId($schoolId, $datum->id);

            if ($roomData->isNotEmpty()) {
                foreach ($roomData->all() as $roomDatum) {
                    array_push ($datum->rooms, $roomDatum);
                } // END foreach
            } // END if

            array_push ($finalData, $datum);
        } // END foreach

        return Collection::make($finalData);
    } // END function


    /*
     * getBySchoolIdAndType
     *
     * @param $schoolId
     * @param $type
     *
     * @return
     */
    public function getBySchoolIdAndType($schoolId, $type)
    {
        $datum = $this->findBySchoolIdAndType($schoolId, $type);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $datum->first()->rooms = [];

        $schoolId = $datum->first()->school_id;
        $roomData = $this->schoolDormRoomServ->findBySchoolIdAndDormId($schoolId, $id);

        if ($roomData->isNotEmpty()) {
            foreach ($roomData->all() as $roomDatum) {
                array_push ($datum->first()->rooms, $roomDatum);
            } // END foreach
        } // END if

        return $datum;
    } // END function


    /*
     * getBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function getBySchoolId($schoolId, $orderby = [], $page = -1, $numItems = 20)
    {
        $data = $this->findBySchoolId($schoolId, $orderby, $page, $numItems);

        if ($data->isEmpty()) {
            return $data;
        } // END if

        $finalData = [];
        foreach ($data->all() as $datum) {

            $datum->rooms = [];

            $roomData = $this->schoolDormRoomServ->findBySchoolIdAndDormId($schoolId, $datum->id);

            if ($roomData->isNotEmpty()) {
                foreach ($roomData->all() as $roomDatum) {
                    array_push ($datum->rooms, $roomDatum);
                } // END foreach
            } // END if

            array_push ($finalData, $datum);
        } // END foreach

        return Collection::make($finalData);
    } // END function


    /*
     * getById
     *
     * @param $id
     *
     * @return
     */
    public function getById($id)
    {
        $datum = $this->findById($id);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $datum->first()->rooms = [];

        $schoolId = $datum->first()->school_id;
        $roomData = $this->schoolDormRoomServ->findBySchoolIdAndDormId($schoolId, $id);

        if ($roomData->isNotEmpty()) {
            foreach ($roomData->all() as $roomDatum) {
                array_push ($datum->first()->rooms, $roomDatum);
            } // END foreach
        } // END if

        return $datum;
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->schoolDormRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolIdAndType
     *
     * @param $schoolId
     * @param $type
     *
     * @return
     */
    public function findBySchoolIdAndType($schoolId, $type)
    {
        $where = ['school_id' => $schoolId, 'type' => $type];

        return $this->schoolDormRepo->fetchDatum($where);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolId($schoolId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId];

        return $this->schoolDormRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolDormRepo->fetchDatum($where);
    } // END function

}
