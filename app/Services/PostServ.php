<?php

namespace App\Services;

use App\Repos\PostRepo;
use App\Services\PostSeoServ;


/**
 * Class PostServ
 *
 * @package namespace App\Services;
 */
class PostServ
{


    public function __construct()
    {

        $this->postRepo = new PostRepo();
        $this->postSeoServ = new PostSeoServ();
    } // END function


    /*
     * create
     *
     * @param $title
     * @param $content
     * @param $blogId
     * @param $postcategoryId
     * @param $ownerId
     * @param $creatorId
     * @param $cover
     * @param $status
     *
     * @return
     */
    public function create($title, $content, $blogId, $postcategoryId, $ownerId, $cover = '', $status = 'draft', $recommended = false, $creatorId = 0)
    {
        $data = ['title' => $title,
                 'content' => $content,
                 'cover' => $cover,
                 'status' => $status,
                 'recommended' => $recommended,
                 'blog_id' => $blogId,
                 'postcategory_id' => $postcategoryId,
                 'owner_id' => $ownerId,
                 'creator_id' => $creatorId,
        ];

        return $this->postRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postRepo->deleteData($where);
    } // END function


// ===
    /*
     * getByTerm
     *
     * @param $term
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByTerm($term, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE (p.title LIKE :p_term ";
        $query .= "OR s.slug LIKE :s_term) ";

        $bindValues['p_term'] = '%' . $term . '%';
        $bindValues['s_term'] = '%' . $term . '%';

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE 1 ";

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByBlogId
     *
     * @param $blogId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByBlogId($blogId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.blog_id = :blogId ";

        $bindValues['blogId'] = $blogId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getRecommendedByPostcategoryId
     *
     * @param $postcategoryId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getRecommendedByPostcategoryId($postcategoryId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.postcategory_id = :postcategoryId ";
        $query .= "AND p.recommended = 1 ";

        $bindValues['postcategoryId'] = $postcategoryId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByPostcategoryId
     *
     * @param $postcategoryId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByPostcategoryId($postcategoryId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.postcategory_id = :postcategoryId ";

        $bindValues['postcategoryId'] = $postcategoryId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getByOwnerId
     *
     * @param $ownerId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getByOwnerId($ownerId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.owner_id = :ownerId ";

        $bindValues['ownerId'] = $ownerId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getById
     *
     * @param $id
     *
     * @return
     */
    public function getById($id)
    {
        $where = ['id' => $id];
        $datum = $this->postRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->postSeoServ->findByPostId($id);

        if ($seoDatum->isNotEmpty()) {
            $datum->first()->slug    = $seoDatum->first()->slug;
            $datum->first()->excerpt = $seoDatum->first()->excerpt;
            $datum->first()->og_title       = $seoDatum->first()->og_title;
            $datum->first()->og_description = $seoDatum->first()->og_description;
            $datum->first()->meta_title       = $seoDatum->first()->meta_title;
            $datum->first()->meta_description = $seoDatum->first()->meta_description;
            $datum->first()->cover_title = $seoDatum->first()->cover_title;
            $datum->first()->cover_alt   = $seoDatum->first()->cover_alt;
        } // END if

        return $datum;
    } // END function


// ===
    /*
     * findList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByBlogId
     *
     * @param $blogId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByBlogId($blogId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['blog_id' => $blogId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findRecommendedByPostcategoryId
     *
     * @param $siteCategoryId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findRecommendedByPostcategoryId($postcategoryId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['recommended' => 1, 'postcategory_id' => $postcategoryId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByPostcategoryId
     *
     * @param $postcategoryId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByPostcategoryId($postcategoryId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['postcategory_id' => $postcategoryId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByOwnerId
     *
     * @param $ownerId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByOwnerId($ownerId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['owner_id' => $ownerId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->postRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->postRepo->fetchDatum($where);
    } // END function

}
