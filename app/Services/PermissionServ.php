<?php

namespace App\Services;

use App\Repos\PermissionRepo;


/**
 * Class PermissionServ
 *
 * @package namespace App\Services;
 */
class PermissionServ
{


    public function __construct()
    {

        $this->permissionRepo = new PermissionRepo();
    } // END function


    /*
     * create
     *
     * @param $type
     * @param $code
     * @param $note
     * @param $creatorId
     *
     * @return
     */
    public function create($type, $code, $note, $creatorId = 0)
    {
        $data = ['type'    => $type,
                 'code'    => $code,
                 'note'   => $note,
                 'creator_id' => $creatorId];

        return $this->permissionRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->permissionRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->permissionRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->permissionRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByTypeAndCode
     *
     * @param $type
     * @param $code
     *
     * @return
     */
    public function findByTypeAndCode($type, $code)
    {
        $where = ['type' => $type, 'code' => $code];

        return $this->permissionRepo->fetchDatum($where);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->permissionRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByType
     *
     * @param $type
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByType($type, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type];

        return $this->permissionRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->permissionRepo->fetchDatum($where);
    } // END function

}
