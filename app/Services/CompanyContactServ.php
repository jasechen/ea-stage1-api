<?php

namespace App\Services;

use App\Repos\CompanyContactRepo;


/**
 * Class CompanyContactServ
 *
 * @package namespace App\Services;
 */
class CompanyContactServ
{


    public function __construct()
    {

        $this->companyContactRepo = new CompanyContactRepo();
    } // END function


    /*
     * create
     *
     * @param $companyId
     * @param $username
     * @param $country
     * @param $state
     * @param $address
     * @param $phone
     * @param $fax
     *
     * @return
     */
    public function create($companyId, $username, $country, $state, $address, $phone, $fax)
    {
        $data = ['company_id' => $companyId,
                 'username' => $username,
                 'country' => $country,
                 'state' => $state,
                 'address' => $address,
                 'phone' => $phone,
                 'fax' => $fax
        ];

        return $this->companyContactRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyContactRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyContactRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        return $this->companyContactRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyId
     *
     * @param $companyId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByCompanyId($companyId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['company_id' => $companyId];

        return $this->companyContactRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->companyContactRepo->fetchDatum($where);
    } // END function


}
