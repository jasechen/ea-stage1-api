<?php

namespace App\Services;

use App\Repos\AgentProfileRepo;


/**
 * Class AgentProfileServ
 *
 * @package namespace App\Services;
 */
class AgentProfileServ
{


    public function __construct()
    {

        $this->agentProfileRepo = new AgentProfileRepo();
    } // END function


    /*
     * create
     *
     * @param $email
     *
     * @return
     */
    public function create($userId, $expertlySchoolTypes, $familiarCountries, $serviceLocations, $serviceYears, $experience)
    {
        $data = ['user_id' => $userId,
                 'expertly_school_types' => $expertlySchoolTypes,
                 'familiar_countries' => $familiarCountries,
                 'service_locations' => $serviceLocations,
                 'service_years' => $serviceYears,
                 'experience' => $experience
        ];

        return $this->agentProfileRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->agentProfileRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->agentProfileRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->agentProfileRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserId
     *
     * @param $userId
     *
     * @return
     */
    public function findByUserId($userId)
    {
        $where = ['user_id' => $userId];

        return $this->agentProfileRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->agentProfileRepo->fetchDatum($where);
    } // END function

}
