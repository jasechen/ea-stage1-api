<?php

namespace App\Services;

use App\Repos\SchoolDormRoomRepo;


/**
 * Class SchoolDormRoomServ
 *
 * @package namespace App\Services;
 */
class SchoolDormRoomServ
{


    public function __construct()
    {

        $this->schoolDormRoomRepo = new SchoolDormRoomRepo();
    } // END function


    /*
     * create
     *
     * @param $schoolId
     * @param $dormId
     * @param $type
     * @param $accessible
     * @param $smoking
     *
     * @return
     */
    public function create($schoolId, $dormId, $type, $accessible = false, $smoking = false)
    {
        $data = ['school_id' => $schoolId,
                 'dorm_id' => $dormId,
                 'type' => $type,
                 'accessible' => $accessible,
                 'smoking' => $smoking
        ];

        return $this->schoolDormRoomRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolDormRoomRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolDormRoomRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->schoolDormRoomRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByDormIdAndType
     *
     * @param $dormId
     * @param $type
     *
     * @return
     */
    public function findByDormIdAndType($dormId, $type)
    {
        $where = ['dorm_id' => $dormId, 'type' => $type];

        return $this->schoolDormRoomRepo->fetchDatum($where);
    } // END function


    /*
     * findBySchoolIdAndDormId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolIdAndDormId($schoolId, $dormId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId, 'dorm_id' => $dormId];

        return $this->schoolDormRoomRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByDormId
     *
     * @param $dormId
     *
     * @return
     */
    public function findByDormId($dormId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['dorm_id' => $dormId];

        return $this->schoolDormRoomRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolId($schoolId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId];

        return $this->schoolDormRoomRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolDormRoomRepo->fetchDatum($where);
    } // END function

}
