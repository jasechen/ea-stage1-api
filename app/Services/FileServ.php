<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

use App\Repos\FileRepo;
use App\Services\DirServ;


/**
 * Class FileServ
 *
 * @package namespace App\Services;
 */
class FileServ
{


    public function __construct()
    {

        $this->fileRepo = new FileRepo();
        $this->dirServ = app(DirServ::class);
    } // END function


    /**
     * Move file to
     *
     * @param  $parentType
     * @param  \Illuminate\Filesystem\Filesystem  $file
     *
     * @throws
     * @return string   $filename
     */
    public function moveTo($filename, $target, $source = '')
    {

        $fileDatum = $this->findByFilename($filename);

        if ($fileDatum->isEmpty()) {
            return false;
        } // END if

        $sizes = config('image.sizes');
        $sizes['o'] = 0;

        $fileExtension = $fileDatum->first()->extension;

        $source .= empty($source) ? '' : '/';

        foreach ($sizes as $idx => $size) {

            $file = $filename . '_' . $idx . '.' . $fileExtension;

            $fileExists = Storage::disk('gallery')->exists($source . $file);

            if (empty($fileExists)) {continue;}

            Storage::disk('gallery')->move($source . $file, $target . '/'. $file);
        } // END foreach

        return true;
    } // END function


    /**
     * Covert image
     *
     * @param  $parentType
     * @param  \Illuminate\Filesystem\Filesystem  $file
     *
     * @throws
     * @return string   $filename
     */
    public function convertImage($file, $parentId, $parentType = 'blog', $type = 'no_dir')
    {

        $sizes = config('image.sizes');
        $sizes['o'] = 0;
        $path = empty($parentId) ? '' : $parentType . '_' . $parentId . '/' . $type;
        $savePath  = public_path('gallery/' . $path);

        $filename = Carbon::now()->timestamp . mt_rand(100000, 999999);
        $fileExtension = (is_string($file) AND is_file($file)) ? pathinfo($file, PATHINFO_EXTENSION) : $file->extension();

        $fileContent = file_get_contents($file);

        if ($fileContent === FALSE) {
            return false;
        } // END if


        foreach ($sizes as $idx => $size) {
            $img = Image::make($fileContent);

            $imgWidth = $img->width();

            if (empty($img->filesize()) AND (empty($imgWidth) OR $imgWidth > 4096)) {
                continue;
            } // END if

            if ($idx != 'o') {
                if ($imgWidth < $size['width']) {
                    continue;
                } // END if

                if ($type == 'cover' and $size['width'] < 800) {
                    continue;
                }

                if ($type == 'avatar' and $size['width'] > 150) {
                    continue;
                }

                if ($size['width'] == $size['height']) {
                    $img->fit($size['width']);
                } else {
                    $img->resize($size['width'], $size['height'], function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } // END if else
            } // END if

            $imageName = $filename . '_' . $idx . '.' . $fileExtension;

            $filePath = $savePath . '/' . $imageName;
            $img->save($filePath);

            // $imageContent = file_get_contents($filePath);
            // Storage::disk('s3-gallery')->putFileAs($path, $imageContent, $imageName, 'public');
        } // END foreach

        return $filename;
    } // END function


    public function findLinks($filename)
    {

        $fileDatum = $this->findByFilename($filename);

        if ($fileDatum->isEmpty()) {
            return [];
        } // END if

        $links = [];
        $sizes = config('image.sizes');
        $sizes['o'] = 0;

        $dirDatum = $this->dirServ->findById($fileDatum->first()->dir_id);
        if ($dirDatum->isNotEmpty()) {
            $dirParentType = $dirDatum->first()->parent_type;
            $dirParentId = $dirDatum->first()->parent_id;
            $dirType = $dirDatum->first()->type;
        } // END if

        $filename = $fileDatum->first()->filename;
        $fileExtension = $fileDatum->first()->extension;

        $path  = empty($dirParentId) ? '' : $dirParentType .'_' . $dirParentId . '/'. $dirType . '/';

        foreach ($sizes as $idx => $size) {
            $file =  $filename . '_' . $idx . '.' . $fileExtension;

            $exists = Storage::disk('gallery')->exists($path . $file);
            if (empty($exists)) {continue;}

            $links[$idx] = 'gallery/' . $path . $file;
        } // END foreach


        return $links;
    } // END function


    /*
     * create
     *
     * @param $dirId
     * @param $filename
     * @param $extension
     * @param $mimeType
     * @param $size
     * @param $title
     * @param $description
     * @param $ownerId
     * @param $isCover
     * @param $type
     * @param $privacyStatus
     * @param $status
     * @param $creatorId
     *
     * @return
     */
    public function create($dirId, $parentType, $parentId, $filename, $extension, $mimeType, $size, $title, $description, $ownerId, $isCover = false, $type = 'image', $privacyStatus = 'public', $status = 'enable', $creatorId = 0)
    {
        $data = [
            'dir_id' => $dirId,
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'filename'  => $filename,
            'extension' => $extension,
            'mime_type' => $mimeType,
            'size'      => $size,
            'title'     => $title,
            'description' => $description,
            'type'       => $type,
            'privacy_status' => $privacyStatus,
            'status' => $status,
            'is_cover'   => $isCover,
            'owner_id'   => $ownerId,
            'creator_id' => $creatorId
        ];

        return $this->fileRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->fileRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->fileRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->fileRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByDirId
     *
     * @param $dirId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByDirId($dirId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['dir_id' => $dirId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->fileRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByOwnerId
     *
     * @param $ownerId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByOwnerId($ownerId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['owner_id' => $ownerId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->fileRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByFilename
     *
     * @param $filename
     *
     * @return
     */
    public function findByFilename($filename)
    {
        $where = ['filename' => $filename];

        return $this->fileRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->fileRepo->fetchDatum($where);
    } // END function

}
