<?php

namespace App\Services;

use App\Repos\SchoolUserRepo;


/**
 * Class SchoolUserServ
 *
 * @package namespace App\Services;
 */
class SchoolUserServ
{


    public function __construct()
    {

        $this->schoolUserRepo = new SchoolUserRepo();
    } // END function


    /*
     * create
     *
     * @param $schoolId
     * @param $userId
     * @param $roleId
     * @param $type
     * @param $creatorId
     *
     * @return
     */
    public function create($schoolId, $userId, $roleId, $type = 'school', $creatorId = 0)
    {
        $data = ['school_id' => $schoolId,
                 'user_id' => $userId,
                 'role_id' => $roleId,
                 'type' => $type,
                 'creator_id' => $creatorId
        ];

        return $this->schoolUserRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolUserRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolUserRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->schoolUserRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolIdAndUserIdAndType
     *
     * @param $schoolId
     * @param $userId
     * @param $type
     *
     * @return
     */
    public function findBySchoolIdAndUserIdAndType($schoolId, $userId, $type)
    {
        $where = ['school_id' => $schoolId, 'user_id' => $userId, 'type' => $type];

        return $this->schoolUserRepo->fetchDatum($where);
    } // END function


    /*
     * findBySchoolIdAndUserIdAndRoleId
     *
     * @param $schoolId
     * @param $userId
     * @param $roleId
     *
     * @return
     */
    public function findBySchoolIdAndUserIdAndRoleId($schoolId, $userId, $roleId)
    {
        $where = ['school_id' => $schoolId, 'user_id' => $userId, 'role_id' => $roleId];

        return $this->schoolUserRepo->fetchDatum($where);
    } // END function


    /*
     * findByUserIdAndRoleId
     *
     * @param $userId
     * @param $roleId
     *
     * @return
     */
    public function findByUserIdAndRoleId($userId, $roleId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId, 'role_id' => $roleId];

        return $this->schoolUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolIdAndType
     *
     * @param $schoolId
     * @param $type
     *
     * @return
     */
    public function findBySchoolIdAndType($schoolId, $type, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId, 'type' => $type];

        return $this->schoolUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolIdAndRoleId
     *
     * @param $schoolId
     * @param $roleId
     *
     * @return
     */
    public function findBySchoolIdAndRoleId($schoolId, $roleId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId, 'role_id' => $roleId];

        return $this->schoolUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByRoleId
     *
     * @param $roleId
     *
     * @return
     */
    public function findByRoleId($roleId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['role_id' => $roleId];

        return $this->schoolUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolId($schoolId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId];

        return $this->schoolUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolUserRepo->fetchDatum($where);
    } // END function

}
