<?php

namespace App\Services;

use App\Repos\CompanyRepo;


/**
 * Class CompanyServ
 *
 * @package namespace App\Services;
 */
class CompanyServ
{


    public function __construct()
    {

        $this->companyRepo = new CompanyRepo();
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $principal
     * @param $incorporationDate
     * @param $guiNumber
     * @param $cover
     * @param $status
     * @param $creatorId
     *
     * @return
     */
    public function create($name, $principal, $incorporationDate, $guiNumber, $cover = '', $status = 'enable', $creatorId = 0)
    {
        $data = ['name' => $name,
                 'principal' => $principal,
                 'incorporation_date' => $incorporationDate,
                 'gui_number' => $guiNumber,
                 'cover' => $cover,
                 'status' => $status,
                 'creator_id' => $creatorId
        ];

        return $this->companyRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->companyRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByGuiNumber
     *
     * @param $id
     *
     * @return
     */
    public function findByGuiNumber($guiNumber)
    {
        $where = ['gui_number' => $guiNumber];

        return $this->companyRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->companyRepo->fetchDatum($where);
    } // END function


}
