<?php

namespace App\Services;

use App\Repos\PostcategorySeoRepo;


/**
 * Class PostcategorySeoServ
 *
 * @package namespace App\Services;
 */
class PostcategorySeoServ
{


    public function __construct()
    {

        $this->postcategorySeoRepo = new PostcategorySeoRepo();
    } // END function


    /*
     * create
     *
     * @param $postcategoryId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $coverTitle
     * @param $coverAlt
     *
     * @return
     */
    public function create($postcategoryId, $slug, $excerpt = '', $ogTitle = '', $ogDescription = '', $metaTitle = '', $metaDescription = '', $coverTitle = '', $coverAlt = '')
    {
        $data = ['postcategory_id' => $postcategoryId,
                 'slug' => $slug,
                 'excerpt' => $excerpt,
                 'og_title' => $ogTitle,
                 'og_description' => $ogDescription,
                 'meta_title' => $metaTitle,
                 'meta_description' => $metaDescription,
                 'cover_title' => $coverTitle,
                 'cover_alt' => $coverAlt
        ];

        return $this->postcategorySeoRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postcategorySeoRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postcategorySeoRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->postcategorySeoRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySlug
     *
     * @param $slug
     *
     * @return
     */
    public function findBySlug($slug)
    {
        $where = ['slug' => $slug];

        return $this->postcategorySeoRepo->fetchDatum($where);
    } // END function


    /*
     * findByPostcategoryId
     *
     * @param $postcategoryId
     *
     * @return
     */
    public function findByPostcategoryId($postcategoryId)
    {
        $where = ['postcategory_id' => $postcategoryId];

        return $this->postcategorySeoRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->postcategorySeoRepo->fetchDatum($where);
    } // END function

}
