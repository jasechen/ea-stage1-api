<?php

namespace App\Services;

use App\Repos\BlogRepo;


/**
 * Class BlogServ
 *
 * @package namespace App\Services;
 */
class BlogServ
{


    public function __construct()
    {

        $this->blogRepo = new BlogRepo();
    } // END function


    /*
     * create
     *
     * @param $ownerId
     * @param $creatorId
     * @param $status
     * @param $intro
     * @param $cover
     *
     * @return
     */
    public function create($ownerId, $creatorId = 0, $status = 'enable', $intro = '', $cover = '')
    {
        $data = ['intro' => $intro,
                 'cover' => $cover,
                 'status' => $status,
                 'owner_id' => $ownerId,
                 'creator_id' => $creatorId];

        return $this->blogRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->blogRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->blogRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->blogRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByOwnerId
     *
     * @param $ownerId
     *
     * @return
     */
    public function findByOwnerId($ownerId)
    {
        $where = ['owner_id' => $ownerId];

        return $this->blogRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->blogRepo->fetchDatum($where);
    } // END function

}
