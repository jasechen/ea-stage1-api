<?php

namespace App\Services;

use App\Repos\CompanyBankRepo;


/**
 * Class CompanyBankServ
 *
 * @package namespace App\Services;
 */
class CompanyBankServ
{


    public function __construct()
    {

        $this->companyBankRepo = new CompanyBankRepo();
    } // END function


    /*
     * create
     *
     * @param $companyId
     * @param $name
     * @param $branchName
     * @param $accountName
     * @param $accountNumber
     *
     * @return
     */
    public function create($companyId, $name, $branchName, $accountName, $accountNumber)
    {
        $data = ['company_id' => $companyId,
                 'name' => $name,
                 'branch_name' => $branchName,
                 'account_name' => $accountName,
                 'account_number' => $accountNumber
        ];

        return $this->companyBankRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyBankRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyBankRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        return $this->companyBankRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyId
     *
     * @param $companyUserId
     *
     * @return
     */
    public function findByCompanyId($companyId)
    {
        $where = ['company_id' => $companyId];

        return $this->companyBankRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->companyBankRepo->fetchDatum($where);
    } // END function


}
