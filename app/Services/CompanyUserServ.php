<?php

namespace App\Services;

use App\Repos\CompanyUserRepo;


/**
 * Class CompanyUserServ
 *
 * @package namespace App\Services;
 */
class CompanyUserServ
{


    public function __construct()
    {

        $this->companyUserRepo = new CompanyUserRepo();
    } // END function


    /*
     * create
     *
     * @param $companyId
     * @param $userId
     * @param $roleId
     * @param $type
     * @param $creatorId
     *
     * @return
     */
    public function create($companyId, $userId, $roleId, $type = 'agent', $creatorId = 0)
    {
        $data = ['company_id' => $companyId,
                 'user_id' => $userId,
                 'role_id' => $roleId,
                 'type' => $type,
                 'creator_id' => $creatorId
        ];

        return $this->companyUserRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyUserRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyUserRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        return $this->companyUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserIdAndType
     *
     * @param $userId
     * @param $type
     *
     * @return
     */
    public function findByUserIdAndType($userId, $type, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId, 'type' => $type];

        return $this->companyUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserIdAndRoleId
     *
     * @param $userId
     * @param $roleId
     *
     * @return
     */
    public function findByUserIdAndRoleId($userId, $roleId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId, 'role_id' => $roleId];

        return $this->companyUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyIdAndType
     *
     * @param $companyId
     * @param $type
     *
     * @return
     */
    public function findByCompanyIdAndType($companyId, $type, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['company_id' => $companyId, 'type' => $type];

        return $this->companyUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyIdAndRoleId
     *
     * @param $companyId
     * @param $roleId
     *
     * @return
     */
    public function findByCompanyIdAndRoleId($companyId, $roleId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['company_id' => $companyId, 'role_id' => $roleId];

        return $this->companyUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyIdAndUserId
     *
     * @param $companyId
     * @param $userId
     *
     * @return
     */
    public function findByCompanyIdAndUserId($companyId, $userId)
    {
        $where = ['company_id' => $companyId, 'user_id' => $userId];

        return $this->companyUserRepo->fetchDatum($where);
    } // END function


    /*
     * findByUserId
     *
     * @param $userId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByUserId($userId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId];

        return $this->companyUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyId
     *
     * @param $companyId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByCompanyId($companyId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['company_id' => $companyId];

        return $this->companyUserRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->companyUserRepo->fetchDatum($where);
    } // END function


}
