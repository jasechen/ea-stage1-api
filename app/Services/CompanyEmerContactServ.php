<?php

namespace App\Services;

use App\Repos\CompanyEmerContactRepo;


/**
 * Class CompanyEmerContactServ
 *
 * @package namespace App\Services;
 */
class CompanyEmerContactServ
{


    public function __construct()
    {

        $this->companyEmerContactRepo = new CompanyEmerContactRepo();
    } // END function


    /*
     * create
     *
     * @param $companyId
     * @param $username
     * @param $phone
     * @param $email
     *
     * @return
     */
    public function create($companyId, $username, $phone , $email)
    {
        $data = ['company_id' => $companyId,
                 'username' => $username,
                 'phone' => $phone,
                 'email' => $email
        ];

        return $this->companyEmerContactRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyEmerContactRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->companyEmerContactRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        return $this->companyEmerContactRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByCompanyId
     *
     * @param $companyUserId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByCompanyId($companyId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['company_id' => $companyId];

        return $this->companyEmerContactRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->companyEmerContactRepo->fetchDatum($where);
    } // END function


}
