<?php

namespace App\Services;

use App\Repos\FilterSchoolRepo;


/**
 * Class FilterSchoolServ
 *
 * @package namespace App\Services;
 */
class FilterSchoolServ
{


    public function __construct()
    {

        $this->filterSchoolRepo = new FilterSchoolRepo();
    } // END function


    /*
     * create
     *
     * @param $filterId
     * @param $schoolId
     * @param $creatorId
     *
     * @return
     */
    public function create($filterId, $schoolId, $creatorId = 0)
    {
        $data = ['filter_id' => $filterId,
                 'school_id' => $schoolId,
                 'creator_id' => $creatorId];

        return $this->filterSchoolRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->filterSchoolRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->filterSchoolRepo->deleteData($where);
    } // END function


    /*
     * getSchoolTypesBySchoolId
     *
     * @param $schoolId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getSchoolTypesBySchoolId($schoolId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT f.*, fs.`school_id` ";
        $query .= "FROM filters AS f ";
        $query .= "LEFT JOIN filter_schools AS fs ON fs.filter_id = f.id ";

        if (!empty($status)) {
            $query .= "LEFT JOIN schools AS s ON s.id = fs.school_id ";
        } // END if

        $query .= "WHERE fs.school_id = :school_id ";
        $query .= "AND f.parent_type = 'school-type' ";

        $bindValues['school_id'] = $schoolId;

        if (!empty($status)) {
            $query .= "AND s.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->filterSchoolRepo->fetch($query, $bindValues);
    } // END function


    /*
     * getLocationsBySchoolId
     *
     * @param $schoolId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function getLocationsBySchoolId($schoolId, $status = '', $orderby = [], $page = -1, $numItems = 20)
    {
        $bindValues = [];

        $query  = "SELECT f.*, fs.`school_id` ";
        $query .= "FROM filters AS f ";
        $query .= "LEFT JOIN filter_schools AS fs ON fs.filter_id = f.id ";

        if (!empty($status)) {
            $query .= "LEFT JOIN schools AS s ON s.id = fs.school_id ";
        } // END if

        $query .= "WHERE fs.school_id = :school_id ";
        $query .= "AND f.parent_type = 'location' ";

        $bindValues['school_id'] = $schoolId;

        if (!empty($status)) {
            $query .= "AND s.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->filterSchoolRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->filterSchoolRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByFilterIdAndSchoolId
     *
     * @param $filterId
     * @param $schoolId
     *
     * @return
     */
    public function findByFilterIdAndSchoolId($filterId, $schoolId)
    {
        $where = ['filter_id' => $filterId, 'school_id' => $schoolId];

        return $this->filterSchoolRepo->fetchDatum($where);
    } // END function


    /*
     * findByFilterId
     *
     * @param $schoolId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByFilterId($filterId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['filter_id' => $filterId];

        return $this->filterSchoolRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findBySchoolId($schoolId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId];

        return $this->filterSchoolRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->filterSchoolRepo->fetchDatum($where);
    } // END function

}
