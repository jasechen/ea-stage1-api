<?php

namespace App\Services;

use App\Repos\SchoolCourseRepo;


/**
 * Class SchoolCourseServ
 *
 * @package namespace App\Services;
 */
class SchoolCourseServ
{


    public function __construct()
    {

        $this->schoolCourseRepo = new SchoolCourseRepo();
    } // END function


    /*
     * create
     *
     * @param $schoolId
     * @param $name
     * @param $ta
     * @param $description
     *
     * @return
     */
    public function create($schoolId, $name, $ta, $description = '')
    {
        $data = ['school_id' => $schoolId,
                 'name' => $name,
                 'description' => $description,
                 'ta' => $ta
        ];

        return $this->schoolCourseRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolCourseRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->schoolCourseRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->schoolCourseRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySchoolId
     *
     * @param $schoolId
     *
     * @return
     */
    public function findBySchoolId($schoolId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['school_id' => $schoolId];

        return $this->schoolCourseRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->schoolCourseRepo->fetchDatum($where);
    } // END function

}
