<?php

namespace App\Services;

use App\Repos\CountryPropRepo;


/**
 * Class CountryPropServ
 *
 * @package namespace App\Services;
 */
class CountryPropServ
{


    public function __construct()
    {

        $this->countryPropRepo = new CountryPropRepo();
    } // END function


    /*
     * create
     *
     * @param $code
     * @param $callingCode
     * @param $tld
     * @param $lat
     * @param $lng
     *
     * @return
     */
    public function create($code, $callingCode, $tld, $lat, $lng)
    {
        $data = ['code' => $code,
                 'calling_code' => $callingCode,
                 'tld' => $tld,
                 'lat' => $lat,
                 'lng' => $lng];

        return $this->countryPropRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->countryPropRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->countryPropRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->countryPropRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->countryPropRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->countryPropRepo->fetchDatum($where);
    } // END function

}
