<?php

namespace App\Services;

use App\Repos\RolePermissionRepo;


/**
 * Class RolePermissionServ
 *
 * @package namespace App\Services;
 */
class RolePermissionServ
{


    public function __construct()
    {

        $this->rolePermissionRepo = new RolePermissionRepo();
    } // END function


    /*
     * create
     *
     * @param $roleId
     * @param $permissionId
     * @param $creatorId
     *
     * @return
     */
    public function create($roleId, $permissionId, $creatorId = 0)
    {
        $data = ['role_id'    => $roleId,
                 'permission_id'   => $permissionId,
                 'creator_id' => $creatorId];

        return $this->rolePermissionRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->rolePermissionRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->rolePermissionRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->rolePermissionRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByRoleIdAndPermissionId
     *
     * @param $roleId
     * @param $permissionId
     *
     * @return
     */
    public function findByRoleIdAndPermissionId($roleId, $permissionId)
    {
        $where = ['role_id' => $roleId, 'permission_id' => $permissionId];

        return $this->rolePermissionRepo->fetchDatum($where);
    } // END function


    /*
     * findByPermissionId
     *
     * @param $permissionId
     *
     * @return
     */
    public function findByPermissionId($permissionId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['permission_id' => $permissionId];

        return $this->rolePermissionRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByRoleId
     *
     * @param $roleId
     *
     * @return
     */
    public function findByRoleId($roleId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['role_id' => $roleId];

        return $this->rolePermissionRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->rolePermissionRepo->fetchDatum($where);
    } // END function

}
