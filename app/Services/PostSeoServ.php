<?php

namespace App\Services;

use App\Repos\PostSeoRepo;


/**
 * Class PostSeoServ
 *
 * @package namespace App\Services;
 */
class PostSeoServ
{


    public function __construct()
    {

        $this->postSeoRepo = new PostSeoRepo();
    } // END function


    /*
     * create
     *
     * @param $postId
     * @param $slug
     * @param $excerpt
     * @param $canonicalUrl
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $coverTitle
     * @param $coverAlt
     *
     * @return
     */
    public function create($postId, $slug, $excerpt = '', $canonicalUrl = '', $ogTitle = '', $ogDescription = '', $metaTitle = '', $metaDescription = '', $coverTitle = '', $coverAlt = '')
    {
        $data = ['post_id' => $postId,
                 'slug' => $slug,
                 'excerpt' => $excerpt,
                 'canonical_url' => $canonicalUrl,
                 'og_title' => $ogTitle,
                 'og_description' => $ogDescription,
                 'meta_title' => $metaTitle,
                 'meta_description' => $metaDescription,
                 'cover_title' => $coverTitle,
                 'cover_alt' => $coverAlt
        ];

        return $this->postSeoRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postSeoRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postSeoRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->postSeoRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySlug
     *
     * @param $slug
     *
     * @return
     */
    public function findBySlug($slug)
    {
        $where = ['slug' => $slug];

        return $this->postSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findByPostId
     *
     * @param $postId
     *
     * @return
     */
    public function findByPostId($postId)
    {
        $where = ['post_id' => $postId];

        return $this->postSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->postSeoRepo->fetchDatum($where);
    } // END function

}
