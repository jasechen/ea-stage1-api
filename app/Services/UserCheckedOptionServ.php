<?php

namespace App\Services;

use App\Repos\UserCheckedOptionRepo;


/**
 * Class UserCheckedOptionServ
 *
 * @package namespace App\Services;
 */
class UserCheckedOptionServ
{


    public function __construct()
    {

        $this->userCheckedOptionRepo = new UserCheckedOptionRepo();
    } // END function


    /*
     * resetCodeAndStatus
     *
     * @param $id
     *
     * @return
     */
    public function resetCodeAndStatus($id)
    {
        $where = ['id' => $id];
        $data  = ['code'   => mt_rand(100000, 999999),
                  'status' => config('tbl_user_checked_options.USER_CHECKED_OPTIONS_STATUS_INIT')];

        return $this->userCheckedOptionRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $userId
     * @param $option
     * @param $status
     *
     * @return
     */
    public function create($userId, $option, $status = 'init')
    {
        $data = ['user_id' => $userId,
                 'option' => $option,
                 'code' => mt_rand(100000, 999999),
                 'status' => $status];

        return $this->userCheckedOptionRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userCheckedOptionRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userCheckedOptionRepo->deleteData($where);
    } // END function


    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->userCheckedOptionRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserIdAndOption
     *
     * @param $userId
     * @param $option
     *
     * @return
     */
    public function findByUserIdAndOption($userId, $option)
    {
        $where = ['user_id' => $userId, 'option' => $option];

        return $this->userCheckedOptionRepo->fetchDatum($where);
    } // END function


    /*
     * findByUserId
     *
     * @param $userId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByUserId($userId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId];

        return $this->userCheckedOptionRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->userCheckedOptionRepo->fetchDatum($where);
    } // END function

}
