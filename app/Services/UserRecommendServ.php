<?php

namespace App\Services;

use App\Repos\UserRecommendRepo;


/**
 * Class UserRecommendServ
 *
 * @package namespace App\Services;
 */
class UserRecommendServ
{


    public function __construct()
    {

        $this->userRecommendRepo = new UserRecommendRepo();
    } // END function


    /*
     * create
     *
     * @param $userId
     * @param $title
     * @param $content
     * @param $image
     * @param $imageTitle
     * @param $imageAlt
     *
     * @return
     */
    public function create($userId, $title, $content, $image = '', $imageTitle = '', $imageAlt = '')
    {
        $data = ['user_id' => $userId,
                 'title' => $title,
                 'content' => $content,
                 'image' => $image,
                 'image_title' => $imageTitle,
                 'image_alt' => $imageAlt
        ];

        return $this->userRecommendRepo->createData($data);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userRecommendRepo->updateData($data, $where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->userRecommendRepo->deleteData($where);
    } // END function


// ===
    /*
     * findList
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findList($orderby = [], $page = -1, $numItems = 20)
    {
        $where = [];

        return $this->userRecommendRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByUserId
     *
     * @param $userId
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByUserId($userId, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['user_id' => $userId];

        return $this->userRecommendRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->userRecommendRepo->fetchDatum($where);
    } // END function


}
