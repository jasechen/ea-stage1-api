<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class SchoolDormRoom extends Model
{

    protected $connection = 'mysql';
    protected $readConnection = 'main-read';

    protected $table = "school_dorm_rooms";

    // protected $fillable = [];

    protected $guarded = ['created_at', 'updated_at'];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
}
