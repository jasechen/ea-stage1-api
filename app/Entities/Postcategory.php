<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class Postcategory extends Model
{

    protected $connection = 'mysql';
    protected $readConnection = 'main-read';

    protected $table = "postcategories";

    // protected $fillable = [];

    protected $guarded = ['created_at', 'updated_at'];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
}
