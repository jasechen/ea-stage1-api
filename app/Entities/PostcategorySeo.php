<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class PostcategorySeo extends Model
{

    protected $connection = 'mysql';
    protected $readConnection = 'main-read';

    protected $table = "postcategory_seos";

    // protected $fillable = [];

    protected $guarded = ['created_at', 'updated_at'];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
}
