<?php

namespace App\Docs;

    /**
     * 新增文章分類
     *
     * @api {POST} /postcategory 01. 新增文章分類
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增文章分類
     * @apiName PostPostcategoryCreate
     * @apiGroup Postcategory
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             Token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
     *
     * @apiParam {string=global,user}                               [type=global]       類型
     * @apiParam {string}                                           name                名稱
     * @apiParam {string}                                           cover                封面
     * @apiParam {string=enable,delete,disable}                     [status="enable"]     狀態
     * @apiParam {string}                                           slug                 位址
     * @apiParam {string}                                           excerpt              摘要
     * @apiParam {string}                                           og_title             og 標題
     * @apiParam {string}                                           og_description       og 描述
     * @apiParam {string}                                           meta_title           meta 標題
     * @apiParam {string}                                           meta_description     meta 描述
     * @apiParam {string}                                           cover_title          封面標題
     * @apiParam {string}                                           cover_alt            封面 alt
     * @apiParam {string}                                           [owner_id=0]          擁有者 id
     *
     * @apiParamExample {json} Request
{
    "type" : "global",
    "name" : "標題一",
    "description" : "test1test1test1test1test1test1test1\n\r test1test1test1test1test1",
    "cover" : "7876923123987",
    "status" : "enable",
    "slug" : "test1",
    "excerpt" : "test1test1test1test1test1",
    "og_title" : "標題一",
    "og_description" : "test1test1test1test1test1",
    "meta_title" : "標題一",
    "meta_description" : "test1test1test1test1test1",
    "cover_title" : "標題一",
    "cover_alt" : "標題一",
    "owner_id" : "0",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.postcategory_id    文章分類 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "name empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "cover empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
     *
     * @apiErrorExample {json}  Response: 400.06
{
    "status": "fail",
    "code": 400,
    "comment": "excerpt empty"
}
     *
     * @apiErrorExample {json}  Response: 400.07
{
    "status": "fail",
    "code": 400,
    "comment": "og_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.08
{
    "status": "fail",
    "code": 400,
    "comment": "og_decription empty"
}
     *
     * @apiErrorExample {json}  Response: 400.09
{
    "status": "fail",
    "code": 400,
    "comment": "meta_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.10
{
    "status": "fail",
    "code": 400,
    "comment": "meta_description empty"
}
     *
     * @apiErrorExample {json}  Response: 400.11
{
    "status": "fail",
    "code": 400,
    "comment": "cover_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.12
{
    "status": "fail",
    "code": 400,
    "comment": "cover_alt empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "dir error"
}
     *
     * @apiErrorExample {json}  Response: 409.02
{
    "status": "fail",
    "code": 409,
    "comment": "name error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */


