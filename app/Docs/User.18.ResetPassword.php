<?php

namespace App\Docs;

    /**
     * 重設密碼
     *
     * @api {PUT} /user/{id}/password/reset 18. 重設密碼
     * @apiVersion 0.1.0
     * @apiDescription ・ 重設密碼
     * @apiName PutUserResetPassword
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                id                     使用者 ID
     * @apiParam {string}                account          使用者帳號
     * @apiParam {string}                password          密碼
     * @apiParam {string}                password_repeat          重設密碼
     * @apiParam {string}                check_code               驗證碼
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
    "account" : "account@abc.com",
    "password" : "987654321",
    "password_repeat" : "987654321",
    "check_code" : "123456",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "reset success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "account empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "check_code empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "password\' length < 8"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "password / password_repeat is NOT equal"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 422.04
{
    "status": "fail",
    "code": 422,
    "comment": "user status error"
}
     *
     * @apiErrorExample {json}  Response: 422.05
{
    "status": "fail",
    "code": 422,
    "comment": "user account error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "user password check error"
}
     *
     * @apiErrorExample {json}  Response: 422.06
{
    "status": "fail",
    "code": 422,
    "comment": "user check status error"
}
     *
     * @apiErrorExample {json}  Response: 422.07
{
    "status": "fail",
    "code": 422,
    "comment": "user check code error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "update check status error"
}
     *
     * @apiErrorExample {json}  Response: 500.02
{
    "status": "fail",
    "code": 500,
    "comment": "update password error"
}
     *
     */



