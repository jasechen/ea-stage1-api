<?php

namespace App\Docs;

    /**
     * 查詢 單一角色
     *
     * @api {GET} /role/{id} 04. 查詢 單一角色
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單一角色
     * @apiName GetRoleFind
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          角色 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.role   角色
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "role": {
            "id": "5324389482631168",
            "type": "general",
            "code": "admin",
            "note": "網站最高管理員",
            "creator_id": "0",
            "updated_at": "2018-03-15 16:37:13",
            "created_at": "2018-03-15 16:37:13",
            "permissions": [
                {
                    "id": "5324389491019776",
                    "type": "admin",
                    "code": "role_create",
                    "note": "新增角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389507796992",
                    "type": "admin",
                    "code": "role_update",
                    "note": "更新角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389545545728",
                    "type": "admin",
                    "code": "role_delete",
                    "note": "刪除角色",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389562322944",
                    "type": "admin",
                    "code": "locales_create",
                    "note": "新增語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389570711552",
                    "type": "admin",
                    "code": "locales_update",
                    "note": "更新語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                },
                {
                    "id": "5324389587488768",
                    "type": "admin",
                    "code": "locales_delete",
                    "note": "刪除語系",
                    "creator_id": "0",
                    "updated_at": "2018-03-15 16:37:13",
                    "created_at": "2018-03-15 16:37:13"
                }
            ]
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


