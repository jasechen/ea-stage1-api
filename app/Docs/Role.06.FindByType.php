<?php

namespace App\Docs;

    /**
     * 查詢 所有角色 By Type
     *
     * @api {GET} /role/type/{type}/{order_way?}/{page?} 06. 查詢 所有角色 By Type
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有角色 By Type
     * @apiName GetRoleFindByType
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=general,school,company}                     type          角色類型
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "type" : "general",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.roles   角色s
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "roles": [
            {
                "id": "5324389482631168",
                "type": "general",
                "code": "admin",
                "note": "網站最高管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389491019776",
                        "type": "admin",
                        "code": "role_create",
                        "note": "新增角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389507796992",
                        "type": "admin",
                        "code": "role_update",
                        "note": "更新角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389545545728",
                        "type": "admin",
                        "code": "role_delete",
                        "note": "刪除角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389562322944",
                        "type": "admin",
                        "code": "locales_create",
                        "note": "新增語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389570711552",
                        "type": "admin",
                        "code": "locales_update",
                        "note": "更新語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389587488768",
                        "type": "admin",
                        "code": "locales_delete",
                        "note": "刪除語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389604265984",
                "type": "general",
                "code": "manager",
                "note": "網站管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389625237504",
                        "type": "admin",
                        "code": "user_create",
                        "note": "新增使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389637820416",
                        "type": "admin",
                        "code": "user_update",
                        "note": "更新使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389658791936",
                        "type": "admin",
                        "code": "user_delete",
                        "note": "刪除使用者",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389671374848",
                        "type": "admin",
                        "code": "postcategory_create",
                        "note": "新增文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389688152064",
                        "type": "admin",
                        "code": "postcategory_update",
                        "note": "更新文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389709123584",
                        "type": "admin",
                        "code": "postcategory_delete",
                        "note": "刪除文章分類",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:28:36",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389721706496",
                "type": "general",
                "code": "editor",
                "note": "網站編輯",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389730095104",
                        "type": "admin",
                        "code": "blog_update",
                        "note": "更新網誌資料",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389742678016",
                        "type": "admin",
                        "code": "post_create",
                        "note": "新增文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389759455232",
                        "type": "admin",
                        "code": "post_update",
                        "note": "更新文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389772038144",
                        "type": "admin",
                        "code": "post_delete",
                        "note": "刪除文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389788815360",
                "type": "general",
                "code": "author",
                "note": "網站作者",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389797203968",
                        "type": "general",
                        "code": "post_create",
                        "note": "新增文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389813981184",
                        "type": "general",
                        "code": "post_update",
                        "note": "更新文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389830758400",
                        "type": "general",
                        "code": "post_delete",
                        "note": "刪除文章",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389847535616",
                "type": "general",
                "code": "member",
                "note": "網站會員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389851729920",
                        "type": "general",
                        "code": "member_update",
                        "note": "更新個人資料",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389868507136",
                        "type": "general",
                        "code": "file_upload",
                        "note": "上傳檔案",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389885284352",
                        "type": "general",
                        "code": "password_change",
                        "note": "變更密碼",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389897867264",
                        "type": "general",
                        "code": "quotation_fill",
                        "note": "填寫報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "type empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

