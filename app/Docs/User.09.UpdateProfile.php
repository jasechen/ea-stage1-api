<?php

namespace App\Docs;

    /**
     * 修改使用者資料
     *
     * @api {PUT} /user/{id}/profile 09. 修改使用者資料
     * @apiVersion 0.1.0
     * @apiDescription ・ 修改使用者資料
     * @apiName PutUserUpdateProfile
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             Token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
     *
     * @apiParam {string}    id                            使用者 ID
     * @apiParam {string}    [first_name]                    名
     * @apiParam {string}    [last_name]                     姓
     * @apiParam {string}    [nickname]                      暱稱
     * @apiParam {string}    [email]                         E-Mail
     * @apiParam {string}    [mobile_country_code]           手機號碼國碼
     * @apiParam {string}    [mobile_phone]                  手機號碼
     * @apiParam {string}    [birth]                         生日
     * @apiParam {string}    [bio]                           自我介紹
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
    "first_name" : "John",
    "last_name"   : "Wu",
    "email"   : "account@abc.com",
    "nickname"   : "Big John",
    "mobile_country_code" : "886",
    "mobile_phone" : "912345678",
    "birth"   : "1976-11-24",
    "bio"   : "...",

}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.user_id        使用者 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "update profile success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "user profile error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "email error"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "mobile_phone error"
}
     *
     * @apiErrorExample {json}  Response: 409.02
{
    "status": "fail",
    "code": 422,
    "comment": "mobile_phone format error"
}
     *
     * @apiErrorExample {json}  Response: 409.03
{
    "status": "fail",
    "code": 422,
    "comment": "birth error"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
     *
     */



