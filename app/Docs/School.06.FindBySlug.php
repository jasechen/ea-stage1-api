<?php

namespace App\Docs;

    /**
     * 查詢 單篇學校 By Slug
     *
     * @api {GET} /school/slug/{slug} 06. 查詢 單篇學校 By Slug
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單篇學校 By Slug
     * @apiName GetSchoolFindBySlug
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     slug          學校 Slug
     *
     * @apiParamExample {json} Request
{
    "slug" : "英國留遊學-倫敦大學金匠學院"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.school   學校
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school": {
            "id": "12658531849342976",
            "name": "test777",
            "description": "test9999test9999test9999test9999test9999test9999test9999test9999test9999",
            "facility": "qwerqwerqwerwqerqwerwqer",
            "cover": "1522851523251513",
            "status": "enable",
            "creator_id": "11042496696160256",
            "updated_at": "2018-04-04 22:20:29",
            "created_at": "2018-04-04 22:20:29",
            "slug": "test777",
            "excerpt": "分類三",
            "og_title": "分類三",
            "og_description": "分類三分類三分類三分類三分類三分類三",
            "meta_title": "分類三",
            "meta_description": "分類三分類三分類三分類三分類三分類三",
            "cover_title": "分類三",
            "cover_alt": "分類三",
            "cover_links": {
                "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
            },
            "image_links": [
                {
                    "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
                }
            ],
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/test777-s12658531849342976"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


