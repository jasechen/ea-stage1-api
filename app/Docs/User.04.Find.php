<?php

namespace App\Docs;

    /**
     * 查詢 單一使用者
     *
     * @api {GET} /user/{id} 04. 查詢 單一使用者
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單一使用者
     * @apiName GetUserFind
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          使用者 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.user   使用者
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user": {
            "id": "8217809167454208",
            "account": "admin@english.agency",
            "password": "$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG",
            "status": "enable",
            "token": "cd315b42-4108-52f2-92ce-86a76007dfe7",
            "creator_id": "0",
            "updated_at": "2018-03-23 16:14:38",
            "created_at": "2018-03-23 16:14:38",
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": ""
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


