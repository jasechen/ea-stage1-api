<?php

namespace App\Docs;

    /**
     * 搜尋學校 Filter
     *
     * @api {POST} /school/filter/search    33. 搜尋學校 Filter
     * @apiVersion 0.1.0
     * @apiDescription ・ 搜尋學校 Filter
     * @apiName PostSchoolSearchFilter
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
}
     *
     * @apiParam {string[]}                                         filter_ids         filter Ids
     *
     * @apiParamExample {json} Request
{
    "filter_ids" : [
        "1522851523251513",
        "1522851523251513",
        "1522851523251513"
    ]
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.schools        學校s
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "search success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "schools": [
{
                "id": "12658531849342976",
                "name": "test777",
                "description": "test9999test9999test9999test9999test9999test9999test9999test9999test9999",
                "facility": "qwerqwerqwerwqerqwerwqer",
                "cover": "1522851523251513",
                "status": "enable",
                "creator_id": "11042496696160256",
                "updated_at": "2018-04-04 22:20:29",
                "created_at": "2018-04-04 22:20:29",
                "slug": "test777",
                "excerpt": "分類三",
                "og_title": "分類三",
                "og_description": "分類三分類三分類三分類三分類三分類三",
                "meta_title": "分類三",
                "meta_description": "分類三分類三分類三分類三分類三分類三",
                "cover_title": "分類三",
                "cover_alt": "分類三"
            },
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "filter_ids empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "filter error"
}
     *
     */


