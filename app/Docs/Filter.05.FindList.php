<?php

namespace App\Docs;

    /**
     * 查詢 所有過濾條件
     *
     * @api {GET} /filter/list/{order_way?}/{page?}     05. 查詢 所有過濾條件
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有過濾條件
     * @apiName GetFilterFindList
     * @apiGroup Filter
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session      Session 代碼
     * @apiSuccess (Success) {string[]}   data.filters      過濾條件s
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filters": [
            {
                "id": "13292119993225216",
                "type": "school",
                "parent_type": "country",
                "parent_id": "0",
                "code": "ph",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08",
                "child": [
                    {
                        "id": "13292120068722688",
                        "type": "school",
                        "parent_type": "location",
                        "parent_id": "13292119993225216",
                        "code": "ceb",
                        "creator_id": "0",
                        "updated_at": "2018-04-06 16:18:08",
                        "created_at": "2018-04-06 16:18:08"
                    },
                    {
                        "id": "13292120081305600",
                        "type": "school",
                        "parent_type": "location",
                        "parent_id": "13292119993225216",
                        "code": "bag",
                        "creator_id": "0",
                        "updated_at": "2018-04-06 16:18:08",
                        "created_at": "2018-04-06 16:18:08"
                    },
                    {
                        "id": "13292120089694208",
                        "type": "school",
                        "parent_type": "location",
                        "parent_id": "13292119993225216",
                        "code": "crk",
                        "creator_id": "0",
                        "updated_at": "2018-04-06 16:18:08",
                        "created_at": "2018-04-06 16:18:08"
                    }
                ]
            },
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

