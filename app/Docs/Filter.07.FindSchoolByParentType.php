<?php

namespace App\Docs;

    /**
     * 查詢 所有過濾條件 By parent_type
     *
     * @api {GET} /filter/school/type/{parent_type}/{order_way?}/{page?}     07. 查詢 所有過濾條件 By parent_type
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有過濾條件 By parent_type
     * @apiName GetFilterFindByParentType
     * @apiGroup Filter
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=country,location,school-type,program-type,program,program-ta}                     parent_type          parent type
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "parent_type" : "school-type",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session      Session 代碼
     * @apiSuccess (Success) {string[]}   data.filters      過濾條件s
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filters": [
            {
                "id": "13292120144220160",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "middle",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120152608768",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "senior",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120160997376",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "collage",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120169385984",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "community",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120177774592",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "graduate",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            },
            {
                "id": "13292120181968896",
                "type": "school",
                "parent_type": "school-type",
                "parent_id": "0",
                "code": "language",
                "creator_id": "0",
                "updated_at": "2018-04-06 16:18:08",
                "created_at": "2018-04-06 16:18:08"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "parent_type empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "parent_type error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

