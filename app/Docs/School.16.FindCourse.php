<?php

namespace App\Docs;

    /**
     * 查詢 單篇學校課程
     *
     * @api {GET} /school/course/{school_course_id} 16. 查詢 單篇學校課程
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單篇學校課程
     * @apiName GetSchoolFindCourse
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     school_course_id          學校課程 ID
     *
     * @apiParamExample {json} Request
{
    "school_course_id" : "12668924801978368"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.school_course         學校課程
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch school course success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_course": {
            "id": "12668924801978368",
            "school_id": "12658531849342976",
            "name": "course1",
            "description": "course1course1course1course1course1course1course1course1course1course1course1",
            "ta": "parent-child",
            "updated_at": "2018-04-04 23:01:47",
            "created_at": "2018-04-04 23:01:47"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "school_course_id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


