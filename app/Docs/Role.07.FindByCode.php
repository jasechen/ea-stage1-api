<?php

namespace App\Docs;

    /**
     * 查詢 所有角色 By Code
     *
     * @api {GET} /role/code/{code}/{order_way?}/{page?} 07. 查詢 所有角色 By Code
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有角色 By Code
     * @apiName GetRoleFindByCode
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     code          角色代號
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "code" : "admin",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.roles   角色s
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "roles": [
            {
                "id": "5324389482631168",
                "type": "general",
                "code": "admin",
                "note": "網站最高管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389491019776",
                        "type": "admin",
                        "code": "role_create",
                        "note": "新增角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389507796992",
                        "type": "admin",
                        "code": "role_update",
                        "note": "更新角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389545545728",
                        "type": "admin",
                        "code": "role_delete",
                        "note": "刪除角色",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389562322944",
                        "type": "admin",
                        "code": "locales_create",
                        "note": "新增語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389570711552",
                        "type": "admin",
                        "code": "locales_update",
                        "note": "更新語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389587488768",
                        "type": "admin",
                        "code": "locales_delete",
                        "note": "刪除語系",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389914644480",
                "type": "company",
                "code": "admin",
                "note": "公司管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389918838784",
                        "type": "company",
                        "code": "report_show",
                        "note": "觀看統計數據",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:58",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389939810304",
                        "type": "company",
                        "code": "quotation_create",
                        "note": "新增學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324389952393216",
                        "type": "company",
                        "code": "quotation_show",
                        "note": "觀看每筆報名的狀態",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:39",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            },
            {
                "id": "5324389994336256",
                "type": "school",
                "code": "admin",
                "note": "學校管理員",
                "creator_id": "0",
                "updated_at": "2018-03-15 16:37:13",
                "created_at": "2018-03-15 16:37:13",
                "permissions": [
                    {
                        "id": "5324389998530560",
                        "type": "school",
                        "code": "report_show",
                        "note": "觀看統計數據",
                        "creator_id": "0",
                        "updated_at": "2018-03-22 22:29:39",
                        "created_at": "2018-03-15 16:37:13"
                    },
                    {
                        "id": "5324390011113472",
                        "type": "school",
                        "code": "quotation_verify",
                        "note": "審核學生報名報價單",
                        "creator_id": "0",
                        "updated_at": "2018-03-15 16:37:13",
                        "created_at": "2018-03-15 16:37:13"
                    }
                ]
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

