<?php

namespace App\Docs;

    /**
     * 修改使用者 SEO
     *
     * @api {PUT} /user/{id}/seo 11. 修改使用者 SEO
     * @apiVersion 0.1.0
     * @apiDescription ・ 修改使用者 SEO
     * @apiName PutUserUpdateSeo
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             Token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
     *
     * @apiParam {string}    id                            使用者 ID
     * @apiParam {string}                                           [slug]                 位址
     * @apiParam {string}                                           [excerpt]              摘要
     * @apiParam {string}                                           [og_title]             og 標題
     * @apiParam {string}                                           [og_description]       og 描述
     * @apiParam {string}                                           [meta_title]           meta 標題
     * @apiParam {string}                                           [meta_description]     meta 描述
     * @apiParam {string}                                           [avatar_title]         頭像標題
     * @apiParam {string}                                           [avatar_alt]           頭像 alt
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
    "slug" : "test1",
    "excerpt" : "test1test1test1test1test1",
    "og_title" : "標題一",
    "og_description" : "test1test1test1test1test1",
    "meta_title" : "標題一",
    "meta_description" : "test1test1test1test1test1",
    "avatar_title" : "標題一",
    "avatar_alt" : "標題一"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.user_id        使用者 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "update profile success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "user seo error"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
     *
     */



