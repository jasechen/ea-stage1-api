<?php

namespace App\Docs;

    /**
     * 查詢 session
     *
     * @api {GET} /session/{code} 02. 用 code 查詢 session
     * @apiVersion 0.1.0
     * @apiDescription ・ 用 code 查詢 session。
     * @apiName GetSessionFindByCode
     * @apiGroup Session
     *
     * @apiParam {string}    code       Session 代碼
     *
     * @apiParamExample {json} Request
{
    "code" : "8f3e973061800dc6ebcb367079b305d8",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     * @apiSuccess (Success) {string}   data.status    Session 狀態
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "status": "login"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "code empty"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "session error"
}
     *
     */
