<?php

namespace App\Docs;

    /**
     * 查詢 所有訂閱者
     *
     * @api {GET} /subscribe/subscribers 02. 查詢 所有訂閱者
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有訂閱者。
     * @apiName GetSubscribeFindByStatusSubscribe
     * @apiGroup Subscribe
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=ASC,DESC}            [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.subscribers    訂閱者
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "subscribers": [
            {
                "id": "1",
                "name": "John",
                "email": "jasechen@gmail.com",
                "status": "subscribe",
                "updated_at": "2018-01-22 11:28:55",
                "created_at": "2018-01-22 11:28:55"
            },
            {
                "id": "2",
                "name": "John",
                "email": "jase.chen@gmail.com",
                "status": "subscribe",
                "updated_at": "2018-01-22 11:28:55",
                "created_at": "2018-01-22 11:28:55"
            },
            ...
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */
