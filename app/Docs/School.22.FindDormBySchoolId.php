<?php

namespace App\Docs;

    /**
     * 查詢 學校宿舍 By School Id
     *
     * @api {GET} /school/{id}/dorm/{order_way?}/{page?} 22. 查詢 學校宿舍 By School Id
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 學校宿舍 By School Id
     * @apiName GetSchoolFindDormBySchoolId
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          學校 ID
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "id" : "12668924801978368",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.school_dorms         學校宿舍s
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch school dorms success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorms": [
            {
            "id": "12679249458761728",
            "school_id": "12658531849342976",
            "type": "dorm",
            "service": "serviceserviceserviceserviceserviceserviceservice",
            "facility": "facilityfacilityfacilityfacilityfacilityfacility",
            "updated_at": "2018-04-04 23:42:48",
            "created_at": "2018-04-04 23:42:48",
            "rooms": [
                {
                    "id": "12679249500704768",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "single",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                },
                {
                    "id": "12679249513287680",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "twin",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                }
            ]
            },
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */


