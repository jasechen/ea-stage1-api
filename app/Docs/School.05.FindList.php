<?php

namespace App\Docs;

    /**
     * 查詢 所有學校
     *
     * @api {GET} /school/list/{status?}/{order_way?}/{page?} 05. 查詢 所有學校
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有學校
     * @apiName GetSchoolFindList
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=all,enable,delete,disable}                     [status="enable"]          學校狀態
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "status" : "enable",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.schools   學校
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "schools": [
            {
                "id": "12658531849342976",
                "name": "test777",
                "description": "test9999test9999test9999test9999test9999test9999test9999test9999test9999",
                "facility": "qwerqwerqwerwqerqwerwqer",
                "cover": "1522851523251513",
                "status": "enable",
                "creator_id": "11042496696160256",
                "updated_at": "2018-04-04 22:20:29",
                "created_at": "2018-04-04 22:20:29",
                "slug": "test777",
                "excerpt": "分類三",
                "og_title": "分類三",
                "og_description": "分類三分類三分類三分類三分類三分類三",
                "meta_title": "分類三",
                "meta_description": "分類三分類三分類三分類三分類三分類三",
                "cover_title": "分類三",
                "cover_alt": "分類三",
                "cover_links": {
                    "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
                },
                "image_links": [
                    {
                        "o": "gallery/school_12658531849342976/cover/1522851523251513_o.jpeg"
                    }
                ],
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": "/test777-s12658531849342976"
            },
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

