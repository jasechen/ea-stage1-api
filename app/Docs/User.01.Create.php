<?php

namespace App\Docs;

    /**
     * 新增使用者
     *
     * @api {POST} /user 01. 新增使用者
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增使用者
     * @apiName PostUserCreate
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             Token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
     *
     * @apiParam {string}    account                           使用者帳號
     * @apiParam {string}    password                          密碼
     * @apiParam {string}    password_repeat                   確認密碼
     * @apiParam {string=init,enable,disable,delete}    [status=init]                      狀態
     * @apiParam {string}    first_name                        名
     * @apiParam {string}    last_name                         姓
     * @apiParam {string}    mobile_country_code               手機號碼國碼
     * @apiParam {string}    mobile_phone                      手機號碼
     * @apiParam {string}                                           slug                 位址
     * @apiParam {string}                                           excerpt              摘要
     * @apiParam {string}                                           og_title             og 標題
     * @apiParam {string}                                           og_description       og 描述
     * @apiParam {string}                                           meta_title           meta 標題
     * @apiParam {string}                                           meta_description     meta 描述
     * @apiParam {string}                                           avatar_title         頭像標題
     * @apiParam {string}                                           avatar_alt           頭像 alt
     * @apiParam {string[]}                                         [role_ids]           角色 IDs (default:type=general,code=mamber)
     *
     * @apiParamExample {json} Request
{
    "account"   : "service@showhi.co",
    "password"  : "12345678" ,
    "password_repeat" : "12345678",
    "first_name" : "John",
    "last_name"   : "Wu",
    "mobile_country_code" : "886",
    "mobile_phone" : "912345678",
    "status" : "init",
    "slug" : "test1",
    "excerpt" : "test1test1test1test1test1",
    "og_title" : "標題一",
    "og_description" : "test1test1test1test1test1",
    "meta_title" : "標題一",
    "meta_description" : "test1test1test1test1test1",
    "avatar_title" : "標題一",
    "avatar_alt" : "標題一",
     "role_ids" : [
        "5324389482631168",
        "5324389604265984",
        "5324389721706496",
        "5324389788815360",
        "5324389847535616"
     ]
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.user_id    使用者 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "account empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "first_name empty"
}
     *
     * @apiErrorExample {json}  Response: 400.06
{
    "status": "fail",
    "code": 400,
    "comment": "last_name empty"
}
     *
     * @apiErrorExample {json}  Response: 400.07
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_country_code empty"
}
     *
     * @apiErrorExample {json}  Response: 400.08
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_phone empty"
}
     *
     * @apiErrorExample {json}  Response: 400.09
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
     *
     * @apiErrorExample {json}  Response: 400.10
{
    "status": "fail",
    "code": 400,
    "comment": "excerpt empty"
}
     *
     * @apiErrorExample {json}  Response: 400.11
{
    "status": "fail",
    "code": 400,
    "comment": "og_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.12
{
    "status": "fail",
    "code": 400,
    "comment": "og_decription empty"
}
     *
     * @apiErrorExample {json}  Response: 400.013
{
    "status": "fail",
    "code": 400,
    "comment": "meta_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.14
{
    "status": "fail",
    "code": 400,
    "comment": "meta_description empty"
}
     *
     * @apiErrorExample {json}  Response: 400.15
{
    "status": "fail",
    "code": 400,
    "comment": "avatar_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.16
{
    "status": "fail",
    "code": 400,
    "comment": "avatar_alt empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "password' length < 8"
}
     *
     * @apiErrorExample {json}  Response: 422.04
{
    "status": "fail",
    "code": 422,
    "comment": "password / password_repeat is NOT equal"
}
     *
     * @apiErrorExample {json}  Response: 422.05
{
    "status": "fail",
    "code": 422,
    "comment": "mobile_phone format error"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "account error"
}
     *
     * @apiErrorExample {json}  Response: 409.02
{
    "status": "fail",
    "code": 409,
    "comment": "mobile_phone error"
}
     *
     * @apiErrorExample {json}  Response: 422.06
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 409.03
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create user error"
}
     *
     */


