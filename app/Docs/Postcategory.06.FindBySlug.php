<?php

namespace App\Docs;

    /**
     * 查詢 單篇文章分類 By Slug
     *
     * @api {GET} /postcategory/slug/{slug} 06. 查詢 單篇文章分類 By Slug
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單篇文章分類 By Slug
     * @apiName GetPostcategoryFindBySlug
     * @apiGroup Postcategory
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     slug          文章分類 Slug
     *
     * @apiParamExample {json} Request
{
    "slug" : "study-aboard-canada"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.postcategory   文章分類
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "postcategory": {
            "id": "7859246544850944",
            "type": "global",
            "name": "加拿大留學專欄",
            "cover": "1519872532384543",
            "status": "enable",
            "num_items": "0",
            "owner_id": "0",
            "creator_id": "0",
            "updated_at": "2018-03-22 16:29:50",
            "created_at": "2018-03-22 16:29:50",
            "slug": "study-aboard-canada",
            "excerpt": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
            "og_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
            "og_description": "EA針對相關許多加拿大留學相關資訊進行整理，針對加拿大大學排名、留學費用、大學獎學金相關說明，以及申請文件、履歷、讀書計畫、推薦函辦理流程資訊，又或是針對學生升學方面的流程，如加拿大高中、加拿大大學資訊，幫助每位學生打造自己的升學計畫。",
            "meta_title": "《加拿大留學》加拿大留學費用、代辦、簽證、移民申請資訊整理！",
            "meta_description": "study-aboard-canada",
            "cover_title": "加拿大留學",
            "cover_alt": "加拿大留學",
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/study-aboard-canada-c7859246544850944"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


