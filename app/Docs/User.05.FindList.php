<?php

namespace App\Docs;

    /**
     * 查詢 所有使用者
     *
     * @api {GET} /user/list/{status?}/{order_way?}/{page?} 05. 查詢 所有使用者
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有使用者
     * @apiName GetUserFindList
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=all,enable,delete,disable}                     [status="enable"]          使用者狀態
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "status" : "enable",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.users   使用者s
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "users": [
            {
                "id": "8217809167454208",
                "account": "admin@english.agency",
                "password": "$2y$10$yEX7E/.ueop/dbbgBP9qy.4vdobMHcR6diBG.pQKrOBiT61.cADaG",
                "status": "enable",
                "token": "cd315b42-4108-52f2-92ce-86a76007dfe7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217809599467520",
                "account": "alexs@english.agency",
                "password": "$2y$10$dTEwxbx0Q5pvkJhjyYndCehQRHk0epqGD/yWMUCcDjmm7HODAJD/q",
                "status": "enable",
                "token": "fcf7727b-1ea5-5549-a8cf-40c6a852c30d",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217809985343488",
                "account": "magic@english.agency",
                "password": "$2y$10$MRLFkU3Px7ckRBTxwj2DM.xcL70c.PwP2lqZGeYDKFAxL49is7Lzi",
                "status": "enable",
                "token": "5b01417c-522b-5fa4-bf80-1e86c2201832",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217810346053632",
                "account": "david@english.agency",
                "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                "status": "enable",
                "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217810706763776",
                "account": "vacca@english.agency",
                "password": "$2y$10$GH30Glfuxz0lrQiSemmKjuXHL/ay7TbO/o84bvbwPcdVv7JGdfVH.",
                "status": "enable",
                "token": "22874266-9ad8-5bc3-ae3d-6b20996ac576",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217811075862528",
                "account": "bacio@english.agency",
                "password": "$2y$10$3oV.jiNtXHnqgds39kaxOeiTNARl0A0Z2mfeEzQeP51krjZBRLTjC",
                "status": "enable",
                "token": "fdfe7acc-8097-519f-a8fd-60c49b5bc189",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217811432378368",
                "account": "gemora@english.agency",
                "password": "$2y$10$JJrQtFPbZAh.VssdUkchQeaRgWMA62HsTdzg.7eU2VQEPDiUM0yjC",
                "status": "enable",
                "token": "68308d20-f309-5d9f-baa3-528389747854",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217811793088512",
                "account": "rabo@english.agency",
                "password": "$2y$10$MLqwbWJYndct44a2Ti6Q4eRuwY97B84MG2qtXoDHzwAYvh9t9lG.O",
                "status": "enable",
                "token": "295a5e7a-0e14-5ebd-b8f3-7e84300848ce",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217812157992960",
                "account": "globaljobs@english.agency",
                "password": "$2y$10$KFhXj9W1nIJWOnkgadlhg.wtUxma0yutBlqFo0RPEtTu7cuAX590q",
                "status": "enable",
                "token": "e5405c9b-8a0e-5332-94de-0208cd92a3e7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217812506120192",
                "account": "toeic@english.agency",
                "password": "$2y$10$QuLXGH33wnx6S6s7PzSnw.buhFbDDhAbGq3pZ6K6M0.qD2osH52GS",
                "status": "enable",
                "token": "1a661b2d-db14-5540-ae65-bff88e20e654",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217812858441728",
                "account": "autumn@english.agency",
                "password": "$2y$10$XIw3Hm9wA.QAUWTpxH1GlOSEUrZcbvRcQ8OObU03YOEGlUn572sQ2",
                "status": "enable",
                "token": "05a12268-f836-502a-901b-fc5f06c2e566",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217813202374656",
                "account": "alya@english.agency",
                "password": "$2y$10$Fg84g9DsonIJCXpdLHdkC.qddD2jEj/UUzOgLe5kK3Nskx86/BFfC",
                "status": "enable",
                "token": "e2c63cf7-6ddb-5e84-bb32-31a31278cf4b",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217813571473408",
                "account": "toefl@english.agency",
                "password": "$2y$10$IEB8MrgmPaScblLyxzbLgeCnNOaEMKUBolM32hskDRgJLH91US73a",
                "status": "enable",
                "token": "1212d5ff-993e-5754-a609-0c020f98359c",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217813932183552",
                "account": "luna@english.agency",
                "password": "$2y$10$S8VRZ0odLFSyQYBKbRjwLO8LTJgEbXMk1LsyBNdNsuODoMX4dBLU6",
                "status": "enable",
                "token": "14bcc8af-eb01-5e81-b0e1-3d3bab4cea2f",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217814297088000",
                "account": "jasmine@english.agency",
                "password": "$2y$10$4XQn4RQ2N51qzPJ5V.ml1Om9TocKVx9/K/CYyRc4KY8y36xzgjNzO",
                "status": "enable",
                "token": "da6c3c3c-d8ae-56de-9e9f-d0feeb42571a",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217814678769664",
                "account": "claire@english.agency",
                "password": "$2y$10$b4RqTLd.CA4/0r1LPt5vZOWJ.j61tnh2v5aCi9ZOZNp5K60J2dK2W",
                "status": "enable",
                "token": "45713cc3-a826-5b4b-99f7-042f17c7e817",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            },
            {
                "id": "8217815043674112",
                "account": "winnie@english.agency",
                "password": "$2y$10$u4AjL6tK4dcwmRjtQHtNCO0XTxESMQXss2UiFuusyVCbwe12NTAna",
                "status": "enable",
                "token": "4d5bff58-b363-5010-88f1-9303e4a6fa40",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:39",
                "created_at": "2018-03-23 16:14:39",
                "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
                "url_path": ""
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

