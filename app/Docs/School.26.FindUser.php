<?php

namespace App\Docs;

    /**
     * 查詢 單篇學校使用者
     *
     * @api {GET} /school/user/{school_user_id} 26. 查詢 單篇學校使用者
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單篇學校使用者
     * @apiName GetSchoolFindUser
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     school_user_id          學校使用者 ID
     *
     * @apiParamExample {json} Request
{
    "school_user_id" : "12668924801978368"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.school_user         學校使用者
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch school user success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_user": {
            "id": "12691228839776256",
            "school_id": "12658531849342976",
            "user_id": "11042496696160256",
            "role_id": "5324389994336256",
            "type": "school",
            "creator_id": "11042496696160256",
            "updated_at": "2018-04-05 00:30:24",
            "created_at": "2018-04-05 00:30:24"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "school_user_id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


