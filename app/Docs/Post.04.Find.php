<?php

namespace App\Docs;

    /**
     * 查詢 單篇文章
     *
     * @api {GET} /post/{id} 04. 查詢 單篇文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單篇文章
     * @apiName GetPostFind
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          文章 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.post   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post": {
            "id": "8220321387778048",
            "title": "【英國留遊學】倫敦大學金匠學院",
            "content": "<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>\n<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>\n<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>\n<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>\n<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>\n<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>\n<p>Journalism</p>\n<p>Photography</p>\n<p>Filmmaking</p>\n<p>Illustration</p>\n<p>Script and Props</p>\n<p>Animation</p>\n<p>Radio</p>\n<p>Interactive Media</p>\n<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>\n<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>\n<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>\n<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>",
            "cover": "1519267601292148",
            "privacy_status": "public",
            "status": "publish",
            "recommended": "0",
            "blog_id": "8217810404773888",
            "postcategory_id": "7859246410633216",
            "owner_id": "8217810346053632",
            "creator_id": "0",
            "updated_at": "2018-03-23 16:24:37",
            "created_at": "2018-03-23 16:24:37",
            "slug": "英國留遊學-倫敦大學金匠學院",
            "excerpt": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "og_title": "【英國留遊學】倫敦大學金匠學院",
            "og_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "meta_title": "【英國留遊學】倫敦大學金匠學院",
            "meta_description": "如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。",
            "cover_title": "倫敦大學金匠學院",
            "cover_alt": "倫敦大學金匠學院",
            "postcategory": {
                "id": "7859246410633216",
                "type": "global",
                "name": "英國留學專欄",
                "cover": "1519872393361401",
                "status": "enable",
                "num_items": "0",
                "owner_id": "0",
                "creator_id": "0",
                "updated_at": "2018-03-22 16:29:50",
                "created_at": "2018-03-22 16:29:50",
                "slug": "study-aboard-UK",
                "excerpt": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "og_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                "og_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "meta_title": "《英國留學專欄》英國學校申請、碩士課程、簽證、留學準備總整理！",
                "meta_description": "出國留學是許多人心中的夢想，而英國留學正是大家出國唸書的選項之一，但要去英國唸書，有許多資料要查詢，EA整理了許多與英國留學有關的資訊，例如:英國留學費用、英國留學學程、英國留學碩士課程、又或是基本的交通、生活、以及英國留學學校推薦等等，幫助每位學員做好事前規劃，完成出發前的功課。",
                "cover_title": "出國留學",
                "cover_alt": "出國留學"
            },
            "owner": {
                "id": "8217810346053632",
                "account": "david@english.agency",
                "password": "$2y$10$5zcXhEAiHzYwfy5Vs7m5tObjcu3cgbvciFA5Myz4cmWgpP.ISSPWW",
                "status": "enable",
                "token": "070c01d7-a9c5-5614-b9ac-8f14b53ee5a7",
                "creator_id": "0",
                "updated_at": "2018-03-23 16:14:38",
                "created_at": "2018-03-23 16:14:38",
                "slug": "david_b",
                "excerpt": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "og_title": "\"David B\"",
                "og_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "meta_title": "\"David B\"",
                "meta_description": "曾旅居美國、英國、法國，為一專欄記者，目前擔任自由評論家，了解各地風情。",
                "avatar_title": "\"David B\"",
                "avatar_alt": "\"David B\""
            },
            "s3_url": "https://s3.ap-northeast-1.amazonaws.com/ea-stage1-dev-gallery",
            "url_path": "/英國留遊學-倫敦大學金匠學院-p8220321387778048"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


