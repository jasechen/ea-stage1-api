<?php

namespace App\Docs;

    /**
     * 使用者註冊
     *
     * @api {POST} /register 01. 註冊
     * @apiVersion 0.1.0
     * @apiDescription ・ 使用者註冊
     * @apiName PostUserRegister
     * @apiGroup Register
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    account                           使用者帳號
     * @apiParam {string}    password                          密碼
     * @apiParam {string}    password_repeat                   確認密碼
     * @apiParam {string}    first_name                        名
     * @apiParam {string}    last_name                         姓
     * @apiParam {string}    mobile_country_code               手機號碼國碼
     * @apiParam {string}    mobile_phone                      手機號碼
     *
     * @apiParamExample {json} Request
{
    "account"   : "service@showhi.co",
    "password"  : "12345678" ,
    "password_repeat" : "12345678",
    "first_name" : "John",
    "last_name"   : "Wu",
    "mobile_country_code" : "886",
    "mobile_phone" : "912345678",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 201,
    "comment": "register success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "token": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "account is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "first_name is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "last_name is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.06
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_country_code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.07
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_phone is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "account error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "password' length < 8"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "password / password_repeat is NOT equal"
}
     *
     * @apiErrorExample {json}  Response: 422.04
{
    "status": "fail",
    "code": 422,
    "comment": "mobile_phone format error"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "account error"
}
     *
     * @apiErrorExample {json}  Response: 409.02
{
    "status": "fail",
    "code": 409,
    "comment": "mobile_phone error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create user error"
}
     *
     */
