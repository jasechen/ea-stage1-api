<?php

namespace App\Docs;

    /**
     * 修改使用者角色
     *
     * @api {PUT} /user/{id}/role 12. 修改使用者角色
     * @apiVersion 0.1.0
     * @apiDescription ・ 修改使用者角色
     * @apiName PutUserUpdateRole
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             Token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
     *
     * @apiParam {string}    id                            使用者 ID
     * @apiParam {string}    role_ids                      角色 IDs
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
    "role_ids" : [
        "5324389482631168",
        "5324389604265984",
        "5324389721706496",
        "5324389788815360",
        "5324389847535616"
     ]
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.user_id        使用者 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "update user role success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "user_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "role_ids empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "updateData empty"
}
     *
     */



