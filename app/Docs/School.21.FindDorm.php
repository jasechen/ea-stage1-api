<?php

namespace App\Docs;

    /**
     * 查詢 單篇學校宿舍
     *
     * @api {GET} /school/dorm/{school_dorm_id} 21. 查詢 單篇學校宿舍
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單篇學校宿舍
     * @apiName GetSchoolFindDorm
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     school_dorm_id          學校宿舍 ID
     *
     * @apiParamExample {json} Request
{
    "school_dorm_id" : "12668924801978368"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.school_dorm         學校宿舍
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch school dorm success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorm": {
            "id": "12679249458761728",
            "school_id": "12658531849342976",
            "type": "dorm",
            "service": "serviceserviceserviceserviceserviceserviceservice",
            "facility": "facilityfacilityfacilityfacilityfacilityfacility",
            "updated_at": "2018-04-04 23:42:48",
            "created_at": "2018-04-04 23:42:48",
            "rooms": [
                {
                    "id": "12679249500704768",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "single",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                },
                {
                    "id": "12679249513287680",
                    "school_id": "12658531849342976",
                    "dorm_id": "12679249458761728",
                    "type": "twin",
                    "accessible": "0",
                    "smoking": "0",
                    "updated_at": "2018-04-04 23:42:48",
                    "created_at": "2018-04-04 23:42:48"
                }
            ]
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "school_dorm_id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


