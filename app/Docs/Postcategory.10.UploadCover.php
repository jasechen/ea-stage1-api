<?php

namespace App\Docs;

    /**
     * 上傳封面檔案
     *
     * @api {POST} /postcategory/cover    10. 上傳封面檔案
     * @apiVersion 0.1.0
     * @apiDescription ・ 上傳封面檔案
     * @apiName PostPostcategoryUploadCover
     * @apiGroup Postcategory
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             Token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
     *
     * @apiParam {string}            [owner_id="0"]       擁有者
     * @apiParam {string=public,private}            [privacy_status="public"]   隱私狀態
     * @apiParam {string=enable,disable,delete}           [status="disable"]    狀態
     * @apiParam {file}                            file                         檔案
     *
     * @apiParamExample {json} Request
{
    "owner_id" : "0",
    "privacy_status" : "public",
    "status" : "disable",
    "file" : ""
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.filename       檔案名稱
     * @apiSuccess (Success) {string[]} data.cover_links     各尺寸圖檔連結
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "upload cover success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "filename": "1517067835270712",
        "cover_links": {
            "c": "gallery/1517067835270712_c.jpeg",
            "z": "gallery/1517067835270712_z.jpeg",
            "n": "gallery/1517067835270712_n.jpeg",
            "q": "gallery/1517067835270712_q.jpeg",
            "sq": "gallery/1517067835270712_sq.jpeg",
            "o": "gallery/1517067835270712_o.jpeg"
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "file empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "privacy_status error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.04
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */


