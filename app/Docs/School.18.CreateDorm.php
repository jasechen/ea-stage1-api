<?php

namespace App\Docs;

    /**
     * 新增學校宿舍
     *
     * @api {POST} /school/course   18. 新增學校宿舍
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增學校宿舍
     * @apiName PostSchoolCreateDorm
     * @apiGroup School
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             Token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token": "",
}
     *
     * @apiParam {string}                                           school_id       學校 ID
     * @apiParam {string=dorm,hotel,homestay}                       type            宿舍類型
     * @apiParam {string}                                           service         服務
     * @apiParam {string}                                           facility        設施
     * @apiParam {string=[]}                                        rooms           房型
     * @apiParam {string='single','twin','triple','quadruple','six-people','eight-people','semi-double','double','suit-2','suit-3','suit-4'} rooms.N.type 房型類型
     * @apiParam {string=true,false}                                [rooms.N.accessible=true]        無障礙設施
     * @apiParam {string=true,false}                                [rooms.N.smoking=true]           吸菸
     *
     * @apiParamExample {json} Request
{
    "school_id" : "12658531849342976",
    "type" : "hotel",
    "service" : "course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1course 1",
    "facility" : "senior",
    "rooms" : [
        {
            "type" : "single"
            "accessible" : "false"
            "smoking" : "false"
        },
        {
            "type" : "twin"
            "accessible" : "true"
            "smoking" : "false"
        },
    ]
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.school_dorm_id      學校宿舍 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "create school dorm success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "school_dorm_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "school_id empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "service empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "facility empty"
}
     *
     * @apiErrorExample {json}  Response: 400.06
{
    "status": "fail",
    "code": 400,
    "comment": "rooms empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "type error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "all room type error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "school error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "school dorm error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create school dorm error"
}
     *
     */


