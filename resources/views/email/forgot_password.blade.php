<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <container>
        <header></header>
        <main>
            <p>Hi {{ $account }},</p>
            <p>
            歡迎你進入 English.Agency「忘記密碼」流程，為了重設你的密碼，請點選下方連結<br />
            <a href='{{ $link }}'/>{{ $link }}</a><br />
            進入「重設密碼頁」後，輸入此驗證碼「{{ $code }}」及你的新密碼。
            </p>
        </main>
        <footer>
            <p><strong>English.Agency 團隊</strong> 敬上</p>
            <p>-- 此信件由系統自動發信通知，請勿直接回覆 --</p>
        </footer>
        </container>
    </body>
</html>

