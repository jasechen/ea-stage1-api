<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <container>
        <header></header>
        <main>
            <p>Hi {{ $applicant->profile->last_name }}{{ $applicant->profile->first_name }},</p>
            <p>
            我已經發送通知給 {{ $agent->profile->last_name }}{{ $agent->profile->first_name }} ，將在 24 小時內聯繫你。<br />
            選校、選課、生活大小事，要在意的事情好多好多，與顧問專業聊聊，為你展開海外留遊學新視野！<br />
            以下為聯繫資訊：
            </p>
            <p>
            名字：{{ $agent->profile->last_name }}{{ $agent->profile->first_name }}<br />
            電話：{{ $agent->profile->mobile_country_code }} - {{ $agent->profile->mobile_phone }}<br />
            Email：{{ $agent->profile->email }}<br />
            LINE：{{ $agent->profile->line }}
            </p>
        </main>
        <footer>
            <p><strong>English.Agency 團隊</strong> 敬上</p>
            <p>-- 此信件由系統自動發信通知，請勿直接回覆 --</p>
        </footer>
        </container>
    </body>
</html>

