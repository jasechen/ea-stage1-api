<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <container>
        <header></header>
        <main>
            <p>{{ $agent->profile->last_name }}{{ $agent->profile->first_name }} 您好：</p>
            <p>
            恭喜您，已收到新的學生諮詢，請在 24 小時內盡快聯繫 {{ $applicant->profile->last_name }}{{ $applicant->profile->first_name }}，即時解答學生需求！<br />
            以下為聯繫資訊：
            </p>
            <p>
            名字：{{ $applicant->profile->last_name }}{{ $applicant->profile->first_name }}<br />
            電話：{{ $applicant->profile->mobile_country_code }} - {{ $applicant->profile->mobile_phone }}<br />
            Email：{{ $applicant->profile->email }}<br />
            有興趣的學校：{{ $applicant->school_name }}
            </p>
        </main>
        <footer>
            <p><strong>English.Agency 團隊</strong> 敬上</p>
            <p>-- 此信件由系統自動發信通知，請勿直接回覆 --</p>
        </footer>
        </container>
    </body>
</html>

