<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <container>
        <header></header>
        <main>
            <p>Hi {{ $user->profile->last_name }}{{ $user->profile->first_name }},</p>
            <p>
            歡迎你進入 English.Agency「開啟帳號」流程，為了開啟你的帳號，請點選下方連結<br />
            <a href='{{ $link }}'/>{{ $link }}</a><br />
            進入「開啟帳號頁」後，輸入此驗證碼「{{ $code }}」。
            </p>
            <p>選校、選課、生活大小事，要在意的事情好多好多，與顧問專業聊聊，為你展開海外留遊學新視野！</p>
        </main>
        <footer>
            <p><strong>English.Agency 團隊</strong> 敬上</p>
            <p>-- 此信件由系統自動發信通知，請勿直接回覆 --</p>
        </footer>
        </container>
    </body>
</html>

