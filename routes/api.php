<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::pattern('blog_id', '[0-9]+');
Route::pattern('code', '[0-9a-zA-Z_\-]+');
Route::pattern('company_contact_id', '[0-9]+');
Route::pattern('company_emer_id', '[0-9]+');
Route::pattern('company_id', '[0-9]+');
Route::pattern('company_location_id', '[0-9]+');
Route::pattern('company_user_id', '[0-9]+');
Route::pattern('id', '[0-9]+');
Route::pattern('order_way', '[aAsScC]{3}|[dDeEsScC]{4}');
Route::pattern('owner_id', '[0-9]+');
Route::pattern('page', '^-1$|[1-9]+');
Route::pattern('parent_type', '[0-9a-zA-Z_\-]+');
Route::pattern('parent_id', '[0-9]+');
Route::pattern('postcategory_id', '[0-9]+');
Route::pattern('school_course_id', '[0-9]+');
Route::pattern('school_dorm_id', '[0-9]+');
Route::pattern('school_user_id', '[0-9]+');
Route::pattern('status', '[a-zA-Z_\-]+');
Route::pattern('type', '[0-9a-zA-Z_\-]+');
Route::pattern('user_intro_id', '[0-9]+');
Route::pattern('user_recommend_id', '[0-9]+');


Route::group(['prefix' => 'company'], function () use ($router) {
    $router->post('',       'Company@create');
    $router->put('{id}',    'Company@update');
    $router->delete('{id}', 'Company@delete');

    $router->get('{id}', 'Company@find');
    $router->get('list/{status?}/{order_way?}/{page?}', 'Company@findList');

    $router->post('cover',     'Company@uploadCover');

    $router->post('bank',     'CompanyBank@create');
    $router->put('{id}/bank', 'CompanyBank@update');
    $router->get('{id}/bank', 'CompanyBank@find');

    $router->post('location',                      'CompanyLocation@create');
    $router->put('location/{company_location_id}',    'CompanyLocation@update');
    $router->delete('location/{company_location_id}', 'CompanyLocation@delete');
    $router->get('location/{company_location_id}',    'CompanyLocation@find');
    $router->get('{id}/location/{order_way?}/{page?}', 'CompanyLocation@findByCompanyId');

    $router->post('contact',                      'CompanyContact@create');
    $router->put('contact/{company_contact_id}',    'CompanyContact@update');
    $router->delete('contact/{company_contact_id}', 'CompanyContact@delete');
    $router->get('contact/{company_contact_id}',    'CompanyContact@find');
    $router->get('{id}/contact/{order_way?}/{page?}', 'CompanyContact@findByCompanyId');

    $router->post('emer',                      'CompanyEmerContact@create');
    $router->put('emer/{company_emer_id}',    'CompanyEmerContact@update');
    $router->delete('emer/{company_emer_id}', 'CompanyEmerContact@delete');
    $router->get('emer/{company_emer_id}',    'CompanyEmerContact@find');
    $router->get('{id}/emer/{order_way?}/{page?}', 'CompanyEmerContact@findByCompanyId');

    $router->post('user',                     'CompanyUser@create');
    $router->put('user/{company_user_id}',    'CompanyUser@update');
    $router->delete('user/{company_user_id}', 'CompanyUser@delete');
    $router->get('user/{company_user_id}',    'CompanyUser@find');
    $router->get('{id}/user/{order_way?}/{page?}', 'CompanyUser@findByCompanyId');


    $router->get('countries', 'Company@findCountries');
    $router->get('cities', 'Company@findCities');
    $router->get('locations', 'Company@findLocations');
});


Route::group(['prefix' => 'file'], function () use ($router) {
    // $router->post('', 'File@upload');
    $router->delete('{filename}', 'File@delete');
});


Route::group(['prefix' => 'filter'], function () use ($router) {
//     $router->post('',       'Filter@create');
//     $router->put('{id}',    'Filter@update');
//     $router->delete('{id}', 'Filter@delete');

    $router->get('{id}', 'Filter@find');
    $router->get('list/{order_way?}/{page?}', 'Filter@findList');
    $router->get('school/code/{code}/{order_way?}/{page?}', 'Filter@findSchoolByCode');
    $router->get('school/type/{parent_type}/{order_way?}/{page?}', 'Filter@findSchoolByParentType');
    $router->get('school/type/{parent_type}/id/{parent_id}/{order_way?}/{page?}', 'Filter@findSchoolByParentTypeAndParentId');
    $router->get('school/type/{parent_type}/code/{code}', 'Filter@findSchoolByParentTypeAndCode');
});


Route::group(['prefix' => 'locales'], function () use ($router) {
//     $router->post('',       'Locales@create');
//     $router->put('{id}',    'Locales@update');
//     $router->delete('{id}', 'Locales@delete');

    $router->get('{id}', 'Locales@find');
    $router->get('list/{order_way?}/{page?}', 'Locales@findList');
    $router->get('code/{code}', 'Locales@findByCode');
    $router->get('lang/{lang}/{order_way?}/{page?}', 'Locales@findByLang');
    $router->get('lang/{lang}/code/{code}', 'Locales@findByLangAndCode');
});


Route::post('login', 'Login@index');
Route::post('logout', 'Logout@index');


Route::group(['prefix' => 'post'], function () use ($router) {
    $router->post('',       'Post@create');
    $router->put('{id}',    'Post@update');
    $router->delete('{id}', 'Post@delete');

    $router->get('{id}', 'Post@find');
    $router->get('list/{status?}/{order_way?}/{page?}', 'Post@findList');

    $router->get('slug/{slug}', 'Post@findBySlug');
    $router->get('recommended/postcategory/{postcategory_id}/{status?}/{order_way?}/{page?}', 'Post@findRecommendedByPostcategoryId');

    $router->get('postcategory/{postcategory_id}/{status?}/{order_way?}/{page?}', 'Post@findByPostcategoryId');
    $router->get('postcategory/slug/{postcategory_slug}/{status?}/{order_way?}/{page?}', 'Post@findByPostcategorySlug');

    $router->post('cover',     'Post@uploadCover');

    $router->put('{id}/seo',    'PostSeo@update');
});


Route::group(['prefix' => 'postcategory'], function () use ($router) {
    $router->post('',       'Postcategory@create');
    $router->put('{id}',    'Postcategory@update');
    $router->delete('{id}', 'Postcategory@delete');

    $router->get('{id}', 'Postcategory@find');
    $router->get('list/{status?}/{order_way?}/{page?}', 'Postcategory@findList');
    $router->get('slug/{slug}', 'Postcategory@findBySlug');
    $router->get('type/{type}/{status?}/{order_way?}/{page?}', 'Postcategory@findByType');
    $router->get('owner/{owner_id}/{status?}/{order_way?}/{page?}', 'Postcategory@findByOwnerId');
    $router->get('type/{type}/owner/{owner_id}/{status?}/{order_way?}/{page?}', 'Postcategory@findByTypeAndOwnerId');

    $router->post('cover',     'Postcategory@uploadCover');

    $router->put('{id}/seo',    'PostcategorySeo@update');
});


Route::post('register', 'Register@index');


Route::group(['prefix' => 'role'], function () use ($router) {
//     $router->post('',       'Role@create');
//     $router->put('{id}',    'Role@update');
//     $router->delete('{id}', 'Role@delete');

    $router->get('{id}', 'Role@find');
    $router->get('list/{order_way?}/{page?}', 'Role@findList');
    $router->get('type/{type}/{order_way?}/{page?}', 'Role@findByType');
    $router->get('code/{code}/{order_way?}/{page?}', 'Role@findByCode');
    $router->get('type/{type}/code/{code}', 'Role@findByTypeAndCode');
});


Route::group(['prefix' => 'search'], function () use ($router) {
    $router->get('{term}/{page?}', 'Search@findByTerm');
});


Route::group(['prefix' => 'school'], function () use ($router) {
    $router->post('',       'School@create');
    $router->put('{id}',    'School@update');
    $router->delete('{id}', 'School@delete');

    $router->get('{id}', 'School@find');
    $router->get('list/{status?}/{order_way?}/{page?}', 'School@findList');
    $router->get('slug/{slug}', 'School@findBySlug');

    $router->post('cover',     'School@uploadCover');

    $router->put('{id}/seo',    'SchoolSeo@update');

    $router->put('{id}/image', 'School@uploadImage');

    $router->post('fee',     'SchoolFee@create');
    $router->put('{id}/fee', 'SchoolFee@update');
    $router->get('{id}/fee', 'SchoolFee@find');

    $router->post('course',                      'SchoolCourse@create');
    $router->put('course/{school_course_id}',    'SchoolCourse@update');
    $router->delete('course/{school_course_id}', 'SchoolCourse@delete');
    $router->get('course/{school_course_id}',    'SchoolCourse@find');
    $router->get('{id}/course/{order_way?}/{page?}', 'SchoolCourse@findBySchoolId');

    $router->post('dorm',                    'SchoolDorm@create');
    $router->put('dorm/{school_dorm_id}',    'SchoolDorm@update');
    $router->delete('dorm/{school_dorm_id}', 'SchoolDorm@delete');
    $router->get('dorm/{school_dorm_id}',    'SchoolDorm@find');
    $router->get('{id}/dorm/{order_way?}/{page?}', 'SchoolDorm@findBySchoolId');

    $router->post('user',                    'SchoolUser@create');
    $router->put('user/{school_user_id}',    'SchoolUser@update');
    $router->delete('user/{school_user_id}', 'SchoolUser@delete');
    $router->get('user/{school_user_id}',    'SchoolUser@find');
    $router->get('{id}/user/{order_way?}/{page?}', 'SchoolUser@findBySchoolId');
    $router->get('{id}/user/type/{type}/{order_way?}/{page?}', 'SchoolUser@findBySchoolIdAndType');

    $router->post('filter',     'SchoolFilter@create');
    $router->put('{id}/filter', 'SchoolFilter@update');
    $router->delete('{id}/filter', 'SchoolFilter@delete');
    $router->get('{id}/filter', 'SchoolFilter@find');
    $router->post('filter/search', 'SchoolFilter@search');

    $router->get('course/tas',      'SchoolCourse@findTAs');
    $router->get('dorm/room/types', 'SchoolDorm@findRoomTypes');

    $router->post('apply',     'School@apply');
});



Route::group(['prefix' => 'session'], function () use ($router) {
    $router->post('init',  'Session@init');
    $router->get('{code}', 'Session@findByCode');
});


Route::group(['prefix' => 'subscribe'], function () use ($router) {
    $router->post('', 'Subscribe@create');
    $router->get('subscribers/{order_way?}/{page?}', 'Subscribe@findByStatusSubscribe');
});


Route::group(['prefix' => 'user'], function () use ($router) {
    $router->post('',       'User@create');
    $router->put('{id}',    'User@update');
    $router->delete('{id}', 'User@delete');

    $router->get('{id}', 'User@find');
    $router->get('list/{status?}/{order_way?}/{page?}', 'User@findList');
    $router->get('slug/{slug}', 'User@findBySlug');
    $router->get('token/{token}', 'User@findByToken');

    $router->put('{id}/password', 'User@updatePassword');
    $router->put('{id}/profile',  'UserProfile@update');
    $router->put('{id}/avatar',   'UserProfile@updateAvatar');
    $router->put('{id}/seo',      'UserSeo@update');
    $router->put('{id}/role',     'UserRole@update');

    $router->get('{id}/email/notify', 'UserEmail@notify');
    $router->put('{id}/email/verify', 'UserEmail@verify');

    $router->get('{id}/mobile/notify', 'UserMobile@notify');
    $router->put('{id}/mobile/verify', 'UserMobile@verify');

    $router->post('password/notify',    'UserPassword@notify');
    $router->put('{id}/password/reset', 'UserPassword@reset');
/*
    $router->post('agent/country',        'UserCountry@create');
    $router->put('{id}/agent/country',    'UserCountry@update');
    $router->delete('{id}/agent/country', 'UserCountry@delete');
    $router->get('{id}/agent/country/{order_way?}/{page?}', 'UserCountry@findByUserId');

    $router->post('agent/location',        'UserLocation@create');
    $router->put('{id}/agent/location',    'UserLocation@update');
    $router->delete('{id}/agent/location', 'UserLocation@delete');
    $router->get('{id}/agent/location/{order_way?}/{page?}', 'UserLocation@findByUserId');

    $router->post('agent/school',        'UserSchool@create');
    $router->put('{id}/agent/school',    'UserSchool@update');
    $router->delete('{id}/agent/school', 'UserSchool@delete');
    $router->get('{id}/agent/school/{order_way?}/{page?}', 'UserSchool@findByUserId');
*/
    $router->post('agent/profile',        'AgentProfile@create');
    $router->put('{id}/agent/profile',    'AgentProfile@update');
    $router->delete('{id}/agent/profile', 'AgentProfile@delete');
    $router->get('{id}/agent/profile', 'AgentProfile@findByUserId');
    $router->get('agent/profile/countries', 'AgentProfile@findFamiliarCountries');
    $router->get('agent/profile/locations', 'AgentProfile@findServiceLocations');

    $router->post('agent/intro',                           'AgentIntro@create');
    $router->put('agent/intro/{user_intro_id}',    'AgentIntro@update');
    $router->delete('agent/intro/{user_intro_id}', 'AgentIntro@delete');
    $router->get('agent/intro/{user_intro_id}',    'AgentIntro@find');
    $router->get('{id}/agent/intro/{order_way?}/{page?}', 'AgentIntro@findByUserId');
    $router->post('agent/intro/image', 'AgentIntro@uploadImage');

    $router->post('agent/recommend',                           'AgentRecommend@create');
    $router->put('agent/recommend/{user_recommend_id}',    'AgentRecommend@update');
    $router->delete('agent/recommend/{user_recommend_id}', 'AgentRecommend@delete');
    $router->get('agent/recommend/{user_recommend_id}',    'AgentRecommend@find');
    $router->get('{id}/agent/recommend/{order_way?}/{page?}', 'AgentRecommend@findByUserId');
    $router->post('agent/recommend/image', 'AgentRecommend@uploadImage');

});

