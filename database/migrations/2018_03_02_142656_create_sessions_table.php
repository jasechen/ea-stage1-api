<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('sessions', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->char('code', 255);
            $table->bigInteger('device_id');
            $table->enum('status', ['init', 'login', 'logout'])->default('init');
            $table->integer('expire_time')->default(86400);
            $table->ipAddress('remote_address');
            $table->bigInteger('owner_id')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique('code');
            $table->index('device_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
