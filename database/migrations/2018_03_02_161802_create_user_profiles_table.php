<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('user_profiles', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('user_id');

            $table->char('first_name', 50);
            $table->char('last_name', 50);
            $table->char('nickname', 50)->nullable();

            $table->char('email', 50);
            $table->char('mobile_country_code', 5);
            $table->char('mobile_phone', 50);
            $table->char('line', 50)->nullable();

            $table->char('avatar', 50)->nullable();
            $table->char('birth', 10)->nullable();
            $table->text('bio')->nullable();
            $table->text('experience')->nullable();
            $table->integer('service_years')->default(1);


            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
