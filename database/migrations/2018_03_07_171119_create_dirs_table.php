<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('dirs', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->enum('parent_type', ['postcategory','school','company','blog', 'post', 'user', 'dir'])->default('blog');
            $table->unsignedBigInteger('parent_id');

            $table->enum('type', ['no_dir', 'avatar', 'cover', 'others'])->default('others');

            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('cover')->nullable();

            $table->enum('privacy_status', ['public', 'private'])->default('public');
            $table->enum('status', ['enable', 'delete', 'disable'])->default('enable');

            $table->boolean('is_album')->default(true);
            $table->boolean('is_default')->default(true);

            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('creator_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['parent_type', 'parent_id']);
            $table->index('privacy_status');
            $table->index('status');
            $table->index('is_album');
            $table->index('is_default');
            $table->index('owner_id');
            $table->index('creator_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dirs');
    }
}
