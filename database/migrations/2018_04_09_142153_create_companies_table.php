<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('companies', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->string('name', 255);
            $table->string('principal', 50);

            $table->char('incorporation_date', 10);
            $table->char('gui_number', 10);

            $table->char('cover', 255)->nullable();
            $table->enum('status', ['enable', 'disable', 'delete'])->default('enable');

            $table->unsignedBigInteger('creator_id')->default(0);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique('gui_number');
            $table->index('status');
            $table->index('creator_id');
        });

        Schema::connection('mysql')->create('company_contacts', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('company_id');

            $table->char('username', 50);

            $table->char('country', 10);
            $table->char('state', 20);
            $table->char('address', 50);
            $table->char('phone', 15);
            $table->char('fax', 15);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index('company_id');
        });

        Schema::connection('mysql')->create('company_locations', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('company_id');

            $table->char('code', 10);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index('company_id');
        });

        Schema::connection('mysql')->create('company_banks', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('company_id');

            $table->char('name', 50);

            $table->char('branch_name', 50);
            $table->char('account_name', 50);
            $table->char('account_number', 50);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique('company_id');
        });

        Schema::connection('mysql')->create('company_emer_contacts', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('company_id');

            $table->char('username', 50);

            $table->char('phone', 15);
            $table->char('email', 50);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index('company_id');
        });

        Schema::connection('mysql')->create('company_users', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('role_id');
            $table->enum('type', ['admin', 'agent'])->default('agent');

            $table->unsignedBigInteger('creator_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique(['company_id', 'user_id', 'role_id']);
            $table->index(['company_id', 'role_id']);
            $table->index(['user_id', 'role_id']);
            $table->index(['role_id']);
        });

        Schema::connection('mysql')->create('agent_profiles', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->string('expertly_school_types', 255);
            $table->string('familiar_countries', 255);
            $table->string('service_locations', 255);
            $table->integer('service_years');
            $table->text('experience')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id']);
        });


        Schema::connection('mysql')->create('agent_intros', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->string('image_title')->nullable();
            $table->string('image_alt')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id']);
        });

        Schema::connection('mysql')->create('agent_recommends', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->string('image_title')->nullable();
            $table->string('image_alt')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id']);
        });

/*
        Schema::connection('mysql')->create('user_locations', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->char('code', 10);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id', 'code']);
        });

        Schema::connection('mysql')->create('user_countries', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->char('code', 10);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id', 'code']);
        });

        Schema::connection('mysql')->create('user_schools', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->char('code', 10);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id', 'code']);
        });

        Schema::connection('mysql')->create('user_intros', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->string('image_title')->nullable();
            $table->string('image_alt')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id']);
        });

        Schema::connection('mysql')->create('user_recommends', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->string('image_title')->nullable();
            $table->string('image_alt')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['user_id']);
        });
*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
        Schema::dropIfExists('company_contacts');
        Schema::dropIfExists('company_locations');
        Schema::dropIfExists('company_banks');
        Schema::dropIfExists('company_emergency_contacts');
        Schema::dropIfExists('company_users');
        Schema::dropIfExists('agent_profiles');
        Schema::dropIfExists('agent_intros');
        Schema::dropIfExists('agent_recommends');

        // Schema::dropIfExists('user_locations');
        // Schema::dropIfExists('user_countries');
        // Schema::dropIfExists('user_schools');
        // Schema::dropIfExists('user_intros');
        // Schema::dropIfExists('user_recommends');

    }
}
