<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->char('title', 255);
            $table->text('content');
            $table->char('cover', 255);
            $table->enum('privacy_status', ['public', 'private'])->default('public');
            $table->enum('status', ['draft', 'publish', 'disable', 'delete'])->default('draft');
            $table->boolean('recommended')->defalt(false);

            $table->bigInteger('blog_id');
            $table->bigInteger('postcategory_id');

            $table->bigInteger('owner_id');
            $table->bigInteger('creator_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['owner_id','status']);
            $table->index(['status']);
            $table->index('postcategory_id');
            $table->index(['privacy_status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
