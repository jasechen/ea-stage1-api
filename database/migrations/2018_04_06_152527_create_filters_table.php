<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('filters', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->enum('type', ['school', 'company'])->default('school');

            $table->enum('parent_type', ['country', 'location', 'school-type', 'program-type', 'program', 'program-ta'])->default('country');
            $table->bigInteger('parent_id')->default(0);

            $table->char('code', 255);

            $table->bigInteger('creator_id')->default(0);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['type','parent_type','code']);
            $table->index(['type','parent_type','parent_id']);
            $table->index(['type','parent_id']);
            $table->index(['parent_type','parent_id']);
            $table->index(['code']);
        });

        Schema::connection('mysql')->create('filter_schools', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->bigInteger('filter_id');
            $table->bigInteger('school_id');

            $table->bigInteger('creator_id')->default(0);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['filter_id','school_id']);
            $table->index(['school_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
        Schema::dropIfExists('filter_schools');
    }
}
