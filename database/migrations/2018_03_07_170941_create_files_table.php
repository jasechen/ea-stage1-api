<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('files', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('dir_id');

            $table->enum('parent_type', ['postcategory','school','company','blog','post','user'])->default('post');
            $table->unsignedBigInteger('parent_id');

            $table->string('filename');
            $table->string('extension');
            $table->string('mime_type');
            $table->integer('size');

            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->enum('type', ['image', 'video', 'audio', 'others'])->default('image');

            $table->enum('privacy_status', ['public', 'private'])->default('public');
            $table->enum('status', ['enable', 'delete', 'disable'])->default('enable');

            $table->boolean('is_cover')->default(false);

            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('creator_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique('filename');
            $table->index('dir_id');
            $table->index(['parent_type', 'parent_id']);
            $table->index('privacy_status');
            $table->index('status');
            $table->index('is_cover');
            $table->index('owner_id');
            $table->index('creator_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
