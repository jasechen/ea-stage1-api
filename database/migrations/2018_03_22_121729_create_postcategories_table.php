<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('postcategories', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->enum('type', ['global', 'user'])->default('global');
            $table->char('name', 255);
            $table->char('cover', 255)->nullable();
            $table->enum('status', ['enable', 'disable', 'delete'])->default('enable');

            $table->integer('num_items')->default(0);

            $table->bigInteger('owner_id')->default(0);
            $table->bigInteger('creator_id')->default(0);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique(['type','owner_id','name']);
            $table->index(['type','owner_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postcategories');
    }
}
