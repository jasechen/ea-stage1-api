<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('schools', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->char('name', 255);
            $table->text('description')->nullable();
            $table->text('facility')->nullable();

            $table->char('cover', 255)->nullable();
            $table->enum('status', ['enable', 'disable', 'delete'])->default('enable');

            $table->bigInteger('creator_id')->default(0);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::connection('mysql')->create('school_dorms', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('school_id');

            $table->enum('type', ['dorm', 'hotel', 'homestay'])->default('dorm');
            $table->text('service')->nullable();
            $table->text('facility')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index('school_id');
        });

        Schema::connection('mysql')->create('school_dorm_rooms', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('dorm_id');

            $table->enum('type', ['single', 'twin', 'triple', 'quadruple', 'five-people', 'six-people', 'eight-people', 'semi-double', 'double', 'suit-2', 'suit-3', 'suit-4'])->default('single');
            $table->boolean('accessible')->defalt(false);
            $table->boolean('smoking')->defalt(false);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index(['school_id','dorm_id']);
            $table->index(['dorm_id']);
        });

        Schema::connection('mysql')->create('school_courses', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('school_id');

            $table->char('name', 255);
            $table->text('description')->nullable();
            // $table->enum('ta', ['child', 'parent-child', 'middle', 'senior', 'bachelor', 'master', 'doctoral', 'adult', 'senior-citizen'])->default('senior');
            $table->string('ta')->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index('school_id');
        });

        // Schema::connection('mysql')->create('school_fees', function (Blueprint $table) {
        //     $table->unsignedBigInteger('id')->primary();

        //     $table->unsignedBigInteger('school_id');

        //     $table->float('tuition', 10, 4)->nullable();
        //     $table->float('dorm', 10, 4)->nullable();
        //     $table->string('unit', 5)->nullable();
        //     $table->enum('currency', ['usd', 'gbp', 'eur', 'aud', 'twd', 'cny'])->default('usd');

        //     $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        //     $table->timestamp('created_at')->useCurrent();

        //     $table->unique('school_id');
        // });

        Schema::connection('mysql')->create('school_seos', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('school_id');
            $table->char('slug', 255);
            $table->text('excerpt');

            $table->char('og_title', 255);
            $table->text('og_description');

            $table->char('meta_title', 255);
            $table->text('meta_description');

            $table->char('cover_title', 255);
            $table->char('cover_alt', 255);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique('school_id');
            $table->unique('slug');
        });

        Schema::connection('mysql')->create('school_users', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->bigInteger('school_id');
            $table->bigInteger('user_id');
            $table->bigInteger('role_id');
            $table->enum('type', ['school', 'company', 'student'])->default('school');
            $table->bigInteger('creator_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['school_id', 'user_id', 'role_id']);
            $table->index(['school_id', 'role_id']);
            $table->index(['user_id', 'role_id']);
            $table->index(['role_id']);
        });

        Schema::connection('mysql')->create('school_applicants', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->bigInteger('school_id');
            $table->bigInteger('agent_id');
            $table->bigInteger('applicant_id');
            $table->enum('status', ['init', 'notify'])->default('init');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index(['school_id', 'agent_id', 'applicant_id']);
            $table->index(['agent_id', 'applicant_id']);
            $table->index(['applicant_id']);
            $table->index(['status']);
        });

        Schema::connection('mysql')->create('school_dorm_fees', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('dorm_id');
            $table->unsignedBigInteger('room_id');

            $table->float('price', 10, 4)->nullable();

            $table->integer('unit_no')->default(1);
            $table->enum('unit_opt', ['day', 'week', 'month', 'smesmter', 'year'])->default('week');
            $table->enum('currency', ['usd', 'gbp', 'eur', 'aud', 'twd', 'cny'])->default('usd');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['school_id', 'dorm_id', 'room_id']);
            $table->index(['dorm_id', 'room_id']);
            $table->index(['room_id']);
        });

        Schema::connection('mysql')->create('school_course_fees', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('course_id');

            $table->enum('type', ['register', 'tuition'])->default('register');
            $table->float('price', 10, 4)->nullable();

            $table->integer('unit_no')->default(1);
            $table->enum('unit_opt', ['day', 'week', 'month', 'smesmter', 'year'])->default('week');
            $table->enum('currency', ['usd', 'gbp', 'eur', 'aud', 'twd', 'cny'])->default('usd');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['school_id', 'course_id', 'type']);
            $table->index(['school_id', 'course_id']);
        });

        Schema::connection('mysql')->create('school_course_discounts', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('course_id');

            $table->enum('type', ['percent', 'number'])->default('percent');
            $table->float('limit_upper', 10, 4)->default(0);
            $table->float('limit_lower', 10, 4)->default(0);

            $table->enum('currency', ['usd', 'gbp', 'eur', 'aud', 'twd', 'cny'])->default('usd');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['school_id', 'course_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
        Schema::dropIfExists('school_dorms');
        Schema::dropIfExists('school_dorm_rooms');
        Schema::dropIfExists('school_courses');
        Schema::dropIfExists('school_fees');
        Schema::dropIfExists('school_seos');
        Schema::dropIfExists('school_users');
        Schema::dropIfExists('school_applicants');
    }
}
