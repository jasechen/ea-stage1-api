<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Storage;

use App\Services\CountryPropServ;

use App\Services\LocalesServ;
use App\Services\LocalesContentServ;

use App\Services\PostcategoryServ;
use App\Services\PostcategorySeoServ;

use App\Services\RoleServ;
use App\Services\PermissionServ;
use App\Services\RolePermissionServ;

use App\Services\UserServ;
use App\Services\UserProfileServ;
use App\Services\AgentProfileServ;
use App\Services\AgentIntroServ;
use App\Services\AgentRecommendServ;
use App\Services\UserSeoServ;
use App\Services\UserRoleServ;
use App\Services\UserCheckedOptionServ;
use App\Services\BlogServ;

use App\Services\PostServ;
use App\Services\PostSeoServ;

use App\Services\FileServ;
use App\Services\DirServ;

use App\Services\FilterServ;
use App\Services\FilterSchoolServ;

use App\Services\SchoolServ;
use App\Services\SchoolCourseServ;
use App\Services\SchoolDormServ;
use App\Services\SchoolDormRoomServ;
use App\Services\SchoolFeeServ;
use App\Services\SchoolSeoServ;
use App\Services\SchoolUserServ;

use App\Services\CompanyServ;
use App\Services\CompanyBankServ;
use App\Services\CompanyContactServ;
use App\Services\CompanyEmerContactServ;
use App\Services\CompanyLocationServ;
use App\Services\CompanyUserServ;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->importCountryPropTableSeeder();
        $this->importLocalesTableSeeder();
        $this->importRoleTableSeeder();
        $this->importPostcategoryTableSeeder();
        $this->importFilterTableSeeder();
        $this->importSchoolTableSeeder();
        $this->importCompanyTableSeeder();
        $this->importUserTableSeeder();
        $this->importPostTableSeeder();
    }


    public function importCountryPropTableSeeder()
    {

        $countryPropServ = app(CountryPropServ::class);

        $countryPropData = config('default_country_props');

        foreach ($countryPropData as $countryProp) {

            $item = $countryPropServ->findByCode($countryProp['code']);
            if ($item->isNotEmpty()) {continue;}

            $countryPropServ->create($countryProp['code'], $countryProp['calling_code'], $countryProp['tld'], $countryProp['lat'], $countryProp['lng']);
        } // END foreach
    } // END function


    public function importLocalesTableSeeder()
    {

        $localesServ = app(LocalesServ::class);
        $localesContentServ = app(LocalesContentServ::class);

        $localesData = config('default_locales');
        $localesContentData = config('default_locales_contents');

        foreach ($localesData as $locales) {
            $origLocalesId = $locales['id'];
            $item = $localesServ->findByCode($locales['code']);
            if ($item->isEmpty()) {
                $item = $localesServ->create($locales['code'], $locales['note'], 0);
                $localesId = $item->first()->id;
            } else {
                $localesId = $item->first()->id;
            } // END if

            foreach ($localesContentData as $localesContent) {

                if ($localesContent['locales_id'] != $origLocalesId) {continue;}

                $item = $localesContentServ->findByLangAndContent($localesContent['lang'], $localesContent['content']);
                if ($item->isEmpty()) {
                    $localesContentServ->create($localesId, $localesContent['lang'], $localesContent['content'], 0);
                } else {
                    $localesContentServ->update(['locales_id' => $localesId], ['id' => $item->first()->id]);
                } // END if
            } // END foreach
        } // END foreach
    } // END function


    public function importPostcategoryTableSeeder()
    {

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_POSTCATEGORY');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirCreatorId = 0;

        $fileParentType = config('tbl_files.FILES_PARENT_TYPE_POSTCATEGORY');
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');
        $filePrivacyStatus = config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC');
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');
        $fileOwnerId = 0;
        $fileCreatorId = 0;
        $fileIsCover = true;

        $dirServ = app(DirServ::class);
        $fileServ = app(FileServ::class);
        $postcategoryServ = app(PostcategoryServ::class);
        $postcategorySeoServ = app(PostcategorySeoServ::class);

        $postcategories = config('default_postcategories');
        $postcategorySeos = config('default_postcategory_seos');

        foreach ($postcategories as $postcategory) {

            $id = $postcategory['id'];
            $type = 'global';
            $name = $postcategory['name'];
            $cover = $postcategory['cover']; // https://tw.english.agency/gallery/category/47845375721213952/1519872277260477_o.png
            $status = $postcategory['status'];
            $numPosts = $postcategory['num_posts'];

            $postcategoryDatum = $postcategoryServ->findByTypeAndNameAndOwnerId($type, $name, 0);

            if ($postcategoryDatum->isEmpty()) {
                $postcategoryDatum = $postcategoryServ->create($type, $name, $cover, $status);
                $postcategoryId = $postcategoryDatum->first()->id;
            } else {
                $postcategoryId = $postcategoryDatum->first()->id;
            } // END if


            $dirParentId = $postcategoryId;
            $dirDatum = $dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

            if ($dirDatum->isEmpty()) {
                $dirDatum = $dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
            } // END if

            $dirId = $dirDatum->first()->id;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $pathExists = Storage::disk('gallery')->exists($tPath);
            if (empty($pathExists)) {
                Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
            } // END if

            if (!empty($cover)) {
                $ext = 'png';
                $coverUrl = 'https://tw.english.agency/gallery/category/'.$id.'/'.$cover.'_o.'.$ext;
                $fileContent = @file_get_contents($coverUrl);
                if ($fileContent === FALSE) {
                    $ext = 'jpeg';
                    $coverUrl = 'https://tw.english.agency/gallery/category/'.$id.'/'.$cover.'_o.'.$ext;
                    $fileContent = @file_get_contents($coverUrl);
                }

                $coverPath = '/tmp/'.$cover.'_o.'.$ext;
                file_put_contents($coverPath, $fileContent);

                $filename = $fileServ->convertImage($coverPath, $dirParentId, $dirParentType, $dirType);

                if (!empty($filename)) {
                    $updateFields = ['cover' => $filename];
                    $updateWhere = ['id' => $postcategoryId];
                    $postcategoryServ->update($updateFields, $updateWhere);

                    $fileParentId = $dirParentId;
                    $fileTitle = $cover.'_o.'.$ext;

                    $fileDatum = $fileServ->create($dirId, $fileParentType, $fileParentId, $filename, $ext, $fileType.'/'.$ext, filesize($coverPath), $fileTitle, '', $fileOwnerId, $fileIsCover, $fileType, $filePrivacyStatus, $fileStatus, $fileCreatorId);

                    if ($fileDatum->isNotEmpty()) {
                        unlink($coverPath);
                    } // END if
                } // END if
            } // END if


            $postcategoryServ->updateNumItems($postcategoryId, $numPosts);


            foreach ($postcategorySeos as $postcategorySeo) {
                if ($postcategorySeo['category_id'] != $id) {continue;}
                $postcategory['seo'] = $postcategorySeo;
            } // END foreach


            $slug = $postcategory['seo']['slug'];
            $excerpt = $postcategory['seo']['excerpt'];
            $ogTitle = $postcategory['seo']['og_title'];
            $ogDescription = $postcategory['seo']['og_description'];
            $metaTitle = $postcategory['seo']['meta_title'];
            $metaDescription = $postcategory['seo']['meta_description'];
            $coverTitle = $postcategory['seo']['cover_title'];
            $coverAlt = $postcategory['seo']['cover_alt'];

            $postcategorySeoDatum = $postcategorySeoServ->findBySlug($slug);

            if ($postcategorySeoDatum->isEmpty()) {
                $postcategorySeoServ->create($postcategoryId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);
            } // END if
        } // END foreach
    } // END function importPostcategoryTableSeeder


    public function importRoleTableSeeder()
    {
        $roleServ       = app(RoleServ::class);
        $permissionServ = app(PermissionServ::class);
        $rolePermissionServ = app(RolePermissionServ::class);

        $roles = config('default_role_permissions');

        foreach ($roles as $role) {

            $roleType = $role['type'];
            $roleCode = $role['code'];
            $roleNote = $role['note'];

            $roleDatum = $roleServ->findByTypeAndCode($roleType, $roleCode);

            if ($roleDatum->isEmpty()) {

                $roleDatum = $roleServ->create($roleType, $roleCode, $roleNote, 0);

                $roleId = $roleDatum->first()->id;
            } else {
                $roleId = $roleDatum->first()->id;
            } // END if


            $permissions = $role['permissions'];

            foreach ($permissions as $permission) {
                $permissionType = $permission['type'];
                $permissionCode = $permission['code'];
                $permissionNote = $permission['note'];

                $permissionDatum = $permissionServ->findByTypeAndCode($permissionType, $permissionCode);

                if ($permissionDatum->isEmpty()) {

                    $permissionDatum = $permissionServ->create($permissionType, $permissionCode, $permissionNote, 0);

                    $permissionId = $permissionDatum->first()->id;
                } else {
                    $permissionId = $permissionDatum->first()->id;
                } // END if

                $RPDatum = $rolePermissionServ->findByRoleIdAndPermissionId($roleId, $permissionId);

                if ($RPDatum->isEmpty()) {

                    $rolePermissionServ->create($roleId, $permissionId, 0);

                } // END if

            } // END foreach

        } // END foreach

    } // END function RolePermissionTableSeeder


    function importUserTableSeeder()
    {
        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_USER');
        $dirType = config('tbl_dirs.DIRS_TYPE_AVATAR');
        // $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirCreatorId = 0;
        $dirTitle = $dirParentType . ' ' . $dirType;

        $fileParentType = config('tbl_files.FILES_PARENT_TYPE_USER');
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');
        $filePrivacyStatus = config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC');
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');
        // $fileOwnerId = 0;
        $fileCreatorId = 0;
        $fileIsCover = false;

        $dirServ = app(DirServ::class);
        $fileServ = app(FileServ::class);
        $userServ = app(UserServ::class);
        $userProfileServ = app(UserProfileServ::class);
        $userSeoServ = app(UserSeoServ::class);
        $agentProfileServ = app(AgentProfileServ::class);
        $agentIntroServ = app(AgentIntroServ::class);
        $agentRecommendServ = app(AgentRecommendServ::class);
        $roleServ = app(RoleServ::class);
        $userRoleServ = app(UserRoleServ::class);
        $userCheckedOptionServ = app(UserCheckedOptionServ::class);
        $blogServ = app(BlogServ::class);
        $companyServ = app(CompanyServ::class);
        $companyUserServ = app(CompanyUserServ::class);
        $schoolSeoServ = app(SchoolSeoServ::class);
        $schoolUserServ = app(SchoolUserServ::class);


        $users = $userProfiles = $userSeos = [];
        $agentProfiles = $agentIntros = $agentRecommends = [];

        $defaultUsers = config('default_users');
        foreach ($defaultUsers as $defaultUser) {
            array_push($users, $defaultUser);
        }

        $defaultUserProfiles = config('default_user_profiles');
        foreach ($defaultUserProfiles as $defaultUserProfile) {
            array_push($userProfiles, $defaultUserProfile);
        }

        $defaultSeoUsers = config('default_user_seos');
        foreach ($defaultSeoUsers as $defaultSeoUser) {
            array_push($userSeos, $defaultSeoUser);
        }

        $defaultAgentProfiles = config('default_user_agent_profiles');
        foreach ($defaultAgentProfiles as $defaultAgentProfile) {
            array_push($agentProfiles, $defaultAgentProfile);
        }

        $defaultAgentIntros = config('default_user_agent_intros');
        foreach ($defaultAgentIntros as $defaultAgentIntro) {
            array_push($agentIntros, $defaultAgentIntro);
        }

        $defaultAgentRecommends = config('default_user_agent_recommends');
        foreach ($defaultAgentRecommends as $defaultAgentRecommend) {
            array_push($agentRecommends, $defaultAgentRecommend);
        }

        $companies = config('default_companies');
        $schoolSeos = config('default_school_seos');

        foreach ($users as $user) {
            $id = $user['id'];
            $account = $user['account'];
            $password = $user['password'];
            $creatorId = 0; //$user['creator_id'];

            $userDatum = $userServ->findByAccount($account);

            if ($userDatum->isNotEmpty()) {
                continue;
            } // END if

            $userDatum = $userServ->create($account, $password, 'enable', $creatorId);

            $userId = $userDatum->first()->id;

            foreach ($userProfiles as $userProfile) {
                if ($userProfile['id'] != $id) continue;
                $user['profile'] = $userProfile;
            } // END foreach

            foreach ($agentProfiles as $agentProfile) {
                if ($agentProfile['id'] != $id) continue;
                $user['agent_profile'] = $agentProfile;
            } // END foreach

            foreach ($agentIntros as $agentIntro) {
                if ($agentIntro['id'] != $id) continue;
                $user['agent_intro'][] = $agentIntro;
            } // END foreach

            foreach ($agentRecommends as $agentRecommend) {
                if ($agentRecommend['id'] != $id) continue;
                $user['agent_recommend'][] = $agentRecommend;
            } // END foreach

            // https://tw.english.agency/gallery/author/47845354464481280/1519231220525923_o.jpeg
            $email = $user['account'];
            $mobileCountryCode = empty($user['profile']['mobile_country_code']) ? '886' : $user['profile']['mobile_country_code'];
            $mobilePhone = empty($user['profile']['mobile_phone']) ? '912345678' : $user['profile']['mobile_phone'];
            $firstName = empty($user['profile']['first_name']) ? '' : $user['profile']['first_name'];
            $lastName = empty($user['profile']['last_name']) ? '' : $user['profile']['last_name'];
            $nickname = $user['profile']['nickname'];
            $birth = empty($user['profile']['birth']) ? '' : $user['profile']['birth'];
            $line = empty($user['profile']['line']) ? '' : $user['profile']['line'];
            $avatar = $user['profile']['avatar'];

            $userProfileDatum = $userProfileServ->create($userId, $email, $mobileCountryCode, $mobilePhone, $firstName, $lastName, $nickname, '', $birth, $line);
            if ($userProfileDatum->isNotEmpty()) {
                $userProfileServ->update(['bio' => $user['profile']['bio']],['id' => $userProfileDatum->first()->id]);
            }

            $dirParentId = $userId;
            $dirOwnerId = $userId;
            $dirDatum = $dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

            if ($dirDatum->isEmpty()) {
                $dirDatum = $dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
            } // END if

            $dirId = $dirDatum->first()->id;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $pathExists = Storage::disk('gallery')->exists($tPath);
            if (empty($pathExists)) {
                Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
            } // END if

            if (!empty($avatar)) {
                $ext = 'png';
                $coverUrl = 'https://tw.english.agency/gallery/author/'.$id.'/'.$avatar.'_o.'.$ext;
                $fileContent = @file_get_contents($coverUrl);
                if ($fileContent === FALSE) {
                    $ext = 'jpeg';
                    $coverUrl = 'https://tw.english.agency/gallery/author/'.$id.'/'.$avatar.'_o.'.$ext;
                    $fileContent = @file_get_contents($coverUrl);
                }

                $coverPath = '/tmp/'.$avatar.'_o.'.$ext;
                file_put_contents($coverPath, $fileContent);

                $filename = $fileServ->convertImage($coverPath, $dirParentId, $dirParentType, $dirType);

                if (!empty($filename)) {
                    $updateFields = ['avatar' => $filename];
                    $updateWhere = ['id' => $userProfileDatum->first()->id];
                    $userProfileServ->update($updateFields, $updateWhere);

                    $fileParentId = $dirParentId;
                    $fileOwnerId = $userId;
                    $fileTitle = $avatar.'_o.'.$ext;

                    $fileDatum = $fileServ->create($dirId, $fileParentType, $fileParentId, $filename, $ext, $fileType.'/'.$ext, filesize($coverPath), $fileTitle, '', $fileOwnerId, $fileIsCover, $fileType, $filePrivacyStatus, $fileStatus, $fileCreatorId);

                    if ($fileDatum->isNotEmpty()) {
                        unlink($coverPath);
                    } // END if
                } // END if
            } // END if


            $userRoleTypes = explode(',', $user['role_types']);
            $userAgentRoleType  = $user['agent_role_type'];
            $userSchoolRoleType = $user['school_role_type'];

            if (!empty($userRoleTypes)) {
                $roleType = 'general';
                foreach ($userRoleTypes as $roleCode) {

                    $roleDatum = $roleServ->findByTypeAndCode($roleType, $roleCode);

                    if ($roleDatum->isNotEmpty()) {
                        $roleId = $roleDatum->first()->id;

                        $userRoleDatum = $userRoleServ->findByUserIdAndRoleId($userId, $roleId);

                        if ($userRoleDatum->isEmpty()) {
                            $userRoleServ->create($userId, $roleId, 0);

                            if ($roleType == 'general' AND $roleCode == 'author') {
                                $blogDatum = $blogServ->create($userId, 0);
                                $blogId = $blogDatum->first()->id;

                                $coverDir  = $dirServ->initDefaultAlbum('blog', $blogId, 'cover', $userId);
                                $coverPath = $coverDir->first()->parent_type . '_' . $coverDir->first()->parent_id . '/' . $coverDir->first()->type;
                                Storage::disk('gallery')->makeDirectory($coverPath, 0755, true);
                                // Storage::disk('s3-gallery')->makeDirectory($coverPath, 0755, true);
                            } // END if

                            if ($roleType == 'general' AND $roleCode == 'member') {
                                $noDir  = $dirServ->initDefaultDir('user', $userId, $userId);
                                $noPath = $noDir->first()->parent_type . '_' . $noDir->first()->parent_id . '/' . $noDir->first()->type;
                                Storage::disk('gallery')->makeDirectory($noPath, 0755, true);
                                // Storage::disk('s3-gallery')->makeDirectory($noPath, 0755, true);

                                $avatarDir = $dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId('user', $userId, 'avatar', $userId);
                                if ($avatarDir->isEmpty()) {
                                    $avatarDir = $dirServ->initDefaultAlbum('user', $userId, 'avatar', $userId);
                                } // END if
                                $avatarPath = $avatarDir->first()->parent_type . '_' . $avatarDir->first()->parent_id . '/' . $avatarDir->first()->type;
                                $dirExists = Storage::disk('gallery')->exists($avatarPath);
                                if (empty($dirExists)) {
                                    Storage::disk('gallery')->makeDirectory($avatarPath, 0755, true);
                                    // Storage::disk('s3-gallery')->makeDirectory($avatarPath, 0755, true);
                                } // END if
                            } // END if

                        } // END if
                    } // END if
                } // END if
            } // END if

            if (!empty($userAgentRoleType)) {
                $companyId = empty($user['company_id']) ? 0 : $user['company_id'];

                if (!empty($companyId)) {
                    foreach ($companies as $company) {
                        if ($company['id'] != $companyId) continue;
                        $user['company'] = $company;
                    } // END foreach

                    $guiNumber = $user['company']['gui_number'];
                    $companyDatum = $companyServ->findByGuiNumber($guiNumber);
                    $companyId = $companyDatum->isEmpty() ? 0 : $companyDatum->first()->id;
                } // END if

                $schoolNewIds = [];
                $schoolIds = empty($user['school_ids']) ? 0 : $user['school_ids'];
                $schoolIds = explode(',', $schoolIds);
                if (!empty($schoolIds)) {
                    foreach ($schoolIds as $schoolId) {


                        foreach ($schoolSeos as $schoolSeo) {
                            if ($schoolSeo['id'] != $schoolId) continue;
                            $user['schools'][$schoolId]['seo'] = $schoolSeo;
                        } // END foreach

                        $schoolSlug = $user['schools'][$schoolId]['seo']['slug'];
                        $schoolSeoDatum = $schoolSeoServ->findBySlug($schoolSlug);
                        $schoolId = $schoolSeoDatum->isEmpty() ? 0 : $schoolSeoDatum->first()->school_id;

                        array_push ($schoolNewIds, $schoolId);
                    } // END foreach
                } // END if

                $roleType = 'company';
                $roleCode = $userAgentRoleType;

                $roleDatum = $roleServ->findByTypeAndCode($roleType, $roleCode);

                if ($roleDatum->isNotEmpty()) {
                    $roleId = $roleDatum->first()->id;
                    $userRoleDatum = $userRoleServ->findByUserIdAndRoleId($userId, $roleId);

                    if ($userRoleDatum->isEmpty()) {
                        $createUserRoleDatum = $userRoleServ->create($userId, $roleId, 0);

                        if ($createUserRoleDatum->isNotEmpty()) {
                            $companyUserServ->create($companyId, $userId, $roleId, $roleCode, 0);

                            if (!empty($user['agent_profile'])) {
                                $expertlySchoolTypes = $user['agent_profile']['expertly_school_types'];
                                $familiarCountries = $user['agent_profile']['familiar_countries'];
                                $serviceLocations = $user['agent_profile']['service_locations'];
                                $serviceYears = $user['agent_profile']['service_years'];
                                $experience = $user['agent_profile']['experience'];
                                $agentProfileServ->create($userId, $expertlySchoolTypes, $familiarCountries, $serviceLocations, $serviceYears, $experience);
                            } // END if

                            if (!empty($user['agent_intro'])) {
                                foreach ($user['agent_intro'] as $agentIntro) {
                                    $introTitle = $agentIntro['title'];
                                    $introContent = $agentIntro['content'];
                                    $introImage = $agentIntro['image'];
                                    $introImageTitle = $agentIntro['image_title'];
                                    $introImageAlt = $agentIntro['image_alt'];

                                    $agentIntroServ->create($userId, $introTitle, $introContent, $introImage, $introImageTitle, $introImageAlt);
                                } // END foreach
                            } // END if

                            if (!empty($user['agent_recommend'])) {
                                foreach ($user['agent_recommend'] as $agentRecommend) {
                                    $recommendTitle = $agentRecommend['title'];
                                    $recommendContent = $agentRecommend['content'];
                                    $recommendImage = $agentRecommend['image'];
                                    $recommendImageTitle = $agentRecommend['image_title'];
                                    $recommendImageAlt = $agentRecommend['image_alt'];

                                    $agentRecommendServ->create($userId, $recommendTitle, $recommendContent, $recommendImage, $recommendImageTitle, $recommendImageAlt);
                                } // END foreach
                            } // END if

                            if (!empty($schoolNewIds)) {
                                foreach ($schoolNewIds as $schoolNewId) {
                                    $schoolUserServ->create($schoolNewId, $userId, $roleId, $roleType, 0);
                                } // END foreach
                            } // ENd if

                        } // ENd if
                    } // END if
                } // END if
            } // END if

            if (!empty($userSchoolRoleType)) {
                $schoolNewIds = [];
                $schoolIds = empty($user['school_ids']) ? 0 : $user['school_ids'];
                $schoolIds = explode(',', $schoolIds);
                if (!empty($schoolIds)) {
                    foreach ($schoolIds as $schoolId) {

                        foreach ($schoolSeos as $schoolSeo) {
                            if ($schoolSeo['id'] != $schoolId) continue;
                            $user['schools'][$schoolId]['seo'] = $schoolSeo;
                        } // END foreach

                        $schoolSlug = $user['schools'][$schoolId]['seo']['slug'];
                        $schoolSeoDatum = $schoolSeoServ->findBySlug($schoolSlug);
                        $schoolId = $schoolSeoDatum->isEmpty() ? 0 : $schoolSeoDatum->first()->school_id;

                        array_push ($schoolNewIds, $schoolId);
                    } // END foreach
                } // END if

                $roleType = 'school';
                $roleCode = $userAgentRoleType;

                $roleDatum = $roleServ->findByTypeAndCode($roleType, $roleCode);

                if ($roleDatum->isNotEmpty()) {
                    $roleId = $roleDatum->first()->id;
                    $userRoleDatum = $userRoleServ->findByUserIdAndRoleId($userId, $roleId);

                    if ($userRoleDatum->isEmpty()) {
                        $createUserRoleDatum = $userRoleServ->create($userId, $roleId, 0);

                        if ($createUserRoleDatum->isNotEmpty()) {

                            if (!empty($schoolNewIds)) {
                                foreach ($schoolNewIds as $schoolNewId) {
                                    $schoolUserServ->create($schoolNewId, $userId, $roleId, $roleType, 0);
                                } // END foreach
                            } // ENd if

                        } // ENd if
                    } // END if
                } // END if
            } // END if


            $userCheckedOptionServ->create($userId, 'email', 'checked');
            $userCheckedOptionServ->create($userId, 'mobile_phone', 'checked');


            foreach ($userSeos as $userSeo) {
                if ($userSeo['id'] != $id) continue;
                $user['seo'] = $userSeo;
            } // END foreach

            $slug = $user['seo']['slug'];
            $excerpt = empty($user['seo']['excerpt']) ? '' : $user['seo']['excerpt'];
            $ogTitle = empty($user['seo']['og_title']) ? '' : $user['seo']['og_title'];
            $ogDescription = empty($user['seo']['og_description']) ? '' : $user['seo']['og_description'];
            $metaTitle = empty($user['seo']['meta_title']) ? '' : $user['seo']['meta_title'];
            $metaDescription = empty($user['seo']['meta_description']) ? '' : $user['seo']['meta_description'];
            $avatarTitle = empty($user['seo']['avatar_title']) ? '' : $user['seo']['avatar_title'];
            $avatarAlt = empty($user['seo']['avatar_alt']) ? '' : $user['seo']['avatar_alt'];

            $userSeoDatum = $userSeoServ->findBySlug($slug);

            if ($userSeoDatum->isEmpty()) {
                $userSeoServ->create($userId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $avatarTitle, $avatarAlt);
            } // END if
        } // END foreach
    } // END function importUserTableSeeder


    function importPostTableSeeder()
    {
        $problemCoverPostIds = ['47998948467675136', '47998975743234048', '47999008391696384', '47999037139456000', '47999069418819584', '47999371060580352'];

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_POST');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        // $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirCreatorId = 0;
        $dirTitle = $dirParentType . ' ' . $dirType;

        $fileParentType = config('tbl_files.FILES_PARENT_TYPE_POST');
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');
        $filePrivacyStatus = config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC');
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');
        // $fileOwnerId = 0;
        $fileCreatorId = 0;
        $fileIsCover = true;

        $dirServ = app(DirServ::class);
        $fileServ = app(FileServ::class);
        $userSeoServ = app(UserSeoServ::class);
        $postcategorySeoServ = app(PostcategorySeoServ::class);
        $postServ = app(PostServ::class);
        $postSeoServ = app(PostSeoServ::class);
        $blogServ = app(BlogServ::class);

        $posts = config('default_posts');
        $postSeos = config('default_post_seos');

        $userSeos = config('default_user_seos');
        $postcategorySeos = config('default_postcategory_seos');
        // https://tw.english.agency/gallery/post/61142325027540992/1522401456305941_o.png
        foreach ($posts as $post) {

            foreach ($postSeos as $postSeo) {
                if ($postSeo['post_id'] != $post['id']) continue;
                $post['seo'] = $postSeo;
            } // END foreach

            if (empty($post['seo']['slug'])) {
                continue;
            } // END if

            $postSeoDatum = $postSeoServ->findBySlug($post['seo']['slug']);

            if ($postSeoDatum->isNotEmpty()) {
                continue;
            } // END if


            foreach ($postcategorySeos as $postcategorySeo) {
                if ($postcategorySeo['category_id'] != $post['category_id']) continue;
                $postcategorySeoDatum = $postcategorySeoServ->findBySlug($postcategorySeo['slug']);
            } // END foreach

            foreach ($userSeos as $userSeo) {
                if ($userSeo['id'] != $post['author_id']) continue;
                $userSeoDatum = $userSeoServ->findBySlug($userSeo['slug']);
            } // END foreach

            $blogDatum = $blogServ->findByOwnerId($userSeoDatum->first()->user_id);

            $id = $post['id'];
            $title = $post['title'];
            $content = $post['content'];
            $blogId = $blogDatum->first()->id;
            $postcategoryId = $postcategorySeoDatum->first()->postcategory_id;
            $ownerId = $userSeoDatum->first()->user_id;
            $cover = $post['cover'];
            $status = $post['status'];
            $recommended = $post['recommended'];
            $creatorId = 0;

            $postDatum = $postServ->create($title, $content, $blogId, $postcategoryId, $ownerId, $cover, $status, $recommended, $creatorId);

            if ($postDatum->isEmpty()) {
                continue;
            } // END if

            $postId = $postDatum->first()->id;


            $slug = $post['seo']['slug'];
            $excerpt = empty($post['seo']['excerpt']) ? '' : $post['seo']['excerpt'];
            $canonicalUrl = empty($post['seo']['canonical_url']) ? '' : $post['seo']['canonical_url'];
            $ogTitle = empty($post['seo']['og_title']) ? '' : $post['seo']['og_title'];
            $ogDescription = empty($post['seo']['og_description']) ? '' : $post['seo']['og_description'];
            $metaTitle = empty($post['seo']['meta_title']) ? '' : $post['seo']['meta_title'];
            $metaDescription = empty($post['seo']['meta_description']) ? '' : $post['seo']['meta_description'];
            $coverTitle = empty($post['seo']['cover_title']) ? '' : $post['seo']['cover_title'];
            $coverAlt = empty($post['seo']['cover_alt']) ? '' : $post['seo']['cover_alt'];

            $postSeoServ->create($postId, $slug, $excerpt, $canonicalUrl, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);


            $dirParentId = $postId;
            $dirOwnerId = $ownerId;
            $dirDatum = $dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

            if ($dirDatum->isEmpty()) {
                $dirDatum = $dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
            } // END if

            $dirId = $dirDatum->first()->id;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $pathExists = Storage::disk('gallery')->exists($tPath);
            if (empty($pathExists)) {
                Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
            } // END if

            if (!empty($cover)) {
                $ext = 'png';
                $coverUrl = 'https://tw.english.agency/gallery/post/'.$id.'/'.$cover.'_o.'.$ext;
                $fileContent = @file_get_contents($coverUrl);
                if ($fileContent === FALSE) {
                    $ext = 'jpeg';
                    $coverUrl = 'https://tw.english.agency/gallery/post/'.$id.'/'.$cover.'_o.'.$ext;
                    $fileContent = @file_get_contents($coverUrl);
                }

                $coverPath = '/tmp/'.$cover.'_o.'.$ext;
                file_put_contents($coverPath, $fileContent);

                if (!in_array($id, $problemCoverPostIds)) {
                    $filename = $fileServ->convertImage($coverPath, $dirParentId, $dirParentType, $dirType);

                    if (!empty($filename)) {
                        $updateFields = ['cover' => $filename];
                        $updateWhere = ['id' => $postId];
                        $postServ->update($updateFields, $updateWhere);

                        $fileParentId = $dirParentId;
                        $fileOwnerId = $ownerId;
                        $fileTitle = $cover.'_o.'.$ext;

                        $fileDatum = $fileServ->create($dirId, $fileParentType, $fileParentId, $filename, $ext, $fileType.'/'.$ext, filesize($coverPath), $fileTitle, '', $fileOwnerId, $fileIsCover, $fileType, $filePrivacyStatus, $fileStatus, $fileCreatorId);

                        if ($fileDatum->isNotEmpty()) {
                            unlink($coverPath);
                        } // END if
                    } // END if
                } // END if
            } // END if

        } // END foreach
    } // END function importUserTableSeeder


    public function importFilterTableSeeder()
    {

        $filterServ = app(FilterServ::class);

        $filterData = config('default_filters');

        foreach ($filterData as $filter) {

            $filterDatum = $filterServ->findByTypeAndParentTypeAndCode($filter['type'], $filter['parent_type'], $filter['code']);
            if ($filterDatum->isNotEmpty()) {continue;}

            if (!empty($filter['parent_id'])) {

                foreach ($filterData as $parentFilter) {
                    if ($parentFilter['id'] == $filter['parent_id']) {
                        $parentFilterDatum = $filterServ->findByTypeAndParentTypeAndCode($parentFilter['type'], $parentFilter['parent_type'], $parentFilter['code']);
                        $filter['parent_id'] = $parentFilterDatum->isEmpty() ? $filter['parent_id'] : $parentFilterDatum->first()->id;
                        break;
                    }
                } // END foreach

            } // END if

            $filterServ->create($filter['code'], $filter['parent_type'], $filter['parent_id'], $filter['type'], $filter['creator_id']);
        } // END foreach
    } // END function


    public function importSchoolTableSeeder()
    {

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_SCHOOL');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirCreatorId = 0;

        $fileParentType = config('tbl_files.FILES_PARENT_TYPE_SCHOOL');
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');
        $filePrivacyStatus = config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC');
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');
        $fileOwnerId = 0;
        $fileCreatorId = 0;
        $fileIsCover = true;

        $dirServ = app(DirServ::class);
        $fileServ = app(FileServ::class);
        $schoolServ = app(SchoolServ::class);
        $schoolCourseServ = app(SchoolCourseServ::class);
        $schoolDormServ = app(SchoolDormServ::class);
        $schoolDormRoomServ = app(SchoolDormRoomServ::class);
        $schoolFeeServ = app(SchoolFeeServ::class);
        $schoolSeoServ = app(SchoolSeoServ::class);
        $filterServ = app(FilterServ::class);
        $filterSchoolServ = app(FilterSchoolServ::class);

        $schools = config('default_schools');
        $schoolCourses = config('default_school_courses');
        $schoolDorms = config('default_school_dorms');
        $schoolFees = config('default_school_fees');
        $schoolSeos = config('default_school_seos');
        $schoolFilters = config('default_school_filters');

        $finalSchools = [];
        foreach ($schools as $school) {

            foreach ($schoolSeos as $schoolSeo) {
                if ($schoolSeo['id'] != $school['id']) continue;
                $school['seo']['slug'] = $schoolSeo['slug'];
                $school['seo']['excerpt'] = $schoolSeo['excerpt'];
                $school['seo']['og_title'] = $schoolSeo['og_title'];
                $school['seo']['og_description'] = $schoolSeo['og_description'];
                $school['seo']['meta_title'] = $schoolSeo['meta_title'];
                $school['seo']['meta_description'] = $schoolSeo['meta_description'];
                $school['seo']['cover_title'] = $schoolSeo['cover_title'];
                $school['seo']['cover_alt'] = $schoolSeo['cover_alt'];
            } // END foreach

            if (empty($school['seo']['slug'])) {
                continue;
            } // END if

            $schoolSeoDatum = $schoolSeoServ->findBySlug($school['seo']['slug']);

            if ($schoolSeoDatum->isNotEmpty()) {
                continue;
            } // END if

            // fee
            foreach ($schoolFees as $schoolFee) {
                if ($schoolFee['id'] != $school['id']) continue;
                $school['fee']['tuition'] = $schoolFee['tuition'];
                $school['fee']['dorm'] = $schoolFee['dorm'];
                $school['fee']['unit'] = $schoolFee['unit'];
                $school['fee']['currency'] = $schoolFee['currency'];
            } // END foreach

            // course
            $cc = 0;
            foreach ($schoolCourses as $schoolCourse) {
                if ($schoolCourse['id'] != $school['id']) continue;
                $school['courses'][$cc]['name'] = $schoolCourse['name'];
                $school['courses'][$cc]['description'] = $schoolCourse['description'];
                $school['courses'][$cc]['ta'] = $schoolCourse['ta'];
                $cc++;
            } // END foreach

            // dorm
            $dd = 0;
            foreach ($schoolDorms as $schoolDorm) {
                if ($schoolDorm['id'] != $school['id']) continue;
                $school['dorms'][$dd]['type'] = $schoolDorm['type'];
                $school['dorms'][$dd]['service'] = $schoolDorm['service'];
                $school['dorms'][$dd]['facility'] = $schoolDorm['facility'];

                $rooms = explode(',', $schoolDorm['rooms']);
                foreach ($rooms as $ddd => $roomType) {
                    $school['dorms'][$dd]['rooms'][$ddd] = trim($roomType);
                } // END foreach
                $dd++;
            } // END foreach

            // filter
            $ee = 0;
            foreach ($schoolFilters as $schoolFilter) {
                if ($schoolFilter['id'] != $school['id']) continue;
                $school['filters'][$ee]['country'] = $schoolFilter['country'];
                $school['filters'][$ee]['state'] = $schoolFilter['state'];
                $school['filters'][$ee]['school_type'] = $schoolFilter['school_type'];
                $school['filters'][$ee]['program_type'] = $schoolFilter['program_type'];
                $school['filters'][$ee]['program'] = $schoolFilter['program'];
                $school['filters'][$ee]['program_ta'] = $schoolFilter['program_ta'];

                $ee++;
            } // END foreach

            array_push ($finalSchools, $school);
        } // END foreach

        foreach ($finalSchools as $school) {

            $schoolId = $school['id'];
            $schoolName = $school['name'];
            $schoolDescription = $school['description'];
            $schoolFacility = $school['facility'];
            $schoolCover = $school['cover'];

            $schoolSeoSlug = $school['seo']['slug'];
            $schoolSeoExcerpt = $school['seo']['excerpt'];
            $schoolSeoOgTitle = $school['seo']['og_title'];
            $schoolSeoOgDescription = $school['seo']['og_description'];
            $schoolSeoMetaTitle = $school['seo']['meta_title'];
            $schoolSeoMetaDescription = $school['seo']['meta_description'];
            $schoolSeoCoverTitle = $school['seo']['cover_title'];
            $schoolSeoCoverAlt = $school['seo']['cover_alt'];

            $schoolSeoDatum = $schoolSeoServ->findBySlug($schoolSeoSlug);
            if ($schoolSeoDatum->isNotEmpty()) continue;

            // school
            $schoolDatum = $schoolServ->create($schoolName, $schoolDescription, $schoolFacility, $schoolCover);
            if ($schoolDatum->isEmpty()) continue;

            $schoolNewId = $schoolDatum->first()->id;

            // cover
            $dirParentId = $schoolNewId;
            $dirDatum = $dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

            if ($dirDatum->isEmpty()) {
                $dirDatum = $dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
            } // END if

            $dirId = $dirDatum->first()->id;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $pathExists = Storage::disk('gallery')->exists($tPath);
            if (empty($pathExists)) {
                Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
            } // END if

            // seo
            $schoolSeoServ->create($schoolNewId, $schoolSeoSlug, $schoolSeoExcerpt, $schoolSeoOgTitle, $schoolSeoOgDescription, $schoolSeoMetaTitle, $schoolSeoMetaDescription, $schoolSeoCoverTitle, $schoolSeoCoverAlt);

            // fee
            $schoolFeeTuition = $school['fee']['tuition'];
            $schoolFeeDorm = $school['fee']['dorm'];
            $schoolFeeUnit = $school['fee']['unit'];
            $schoolFeeCurrency = $school['fee']['currency'];

            $schoolFeeServ->create($schoolNewId, $schoolFeeTuition, $schoolFeeDorm, $schoolFeeUnit, $schoolFeeCurrency);

            // courses
            foreach ($school['courses'] as $schoolCourse) {

                $schoolCourseName = $schoolCourse['name'];
                $schoolCourseDescription = $schoolCourse['description'];
                $schoolCourseTa = $schoolCourse['ta'];

                $schoolCourseServ->create($schoolNewId, $schoolCourseName, $schoolCourseTa, $schoolCourseDescription);
            } // END foreach

            // dorms
            foreach ($school['dorms'] as $schoolDorm) {

                $schoolDormType = $schoolDorm['type'];
                $schoolDormService = $schoolDorm['service'];
                $schoolDormFacility = $schoolDorm['facility'];

                $schoolDormDatum = $schoolDormServ->create($schoolNewId, $schoolDormType, $schoolDormService, $schoolDormFacility);
                $schoolDormId = $schoolDormDatum->first()->id;

                foreach ($schoolDorm['rooms'] as $roomType) {
                    $schoolDormRoomServ->create($schoolNewId, $schoolDormId, $roomType, false, false);
                } // END foreach
            } // END foreach

            // filter
            foreach ($school['filters'] as $schoolFilter) {

                $schoolFilterCountry = $schoolFilter['country'];
                $schoolFilterState = $schoolFilter['state'];
                $schoolFilterSchoolType = $schoolFilter['school_type'];
                $schoolFilterProgramType = $schoolFilter['program_type'];
                $schoolFilterProgram = $schoolFilter['program'];
                $schoolFilterProgramTa = $schoolFilter['program_ta'];

                $locationDatum = $filterServ->findByTypeAndParentTypeAndCode('school', 'location', $schoolFilterState);
                if ($locationDatum->isNotEmpty()) {
                    $locationParentFilterId = $locationDatum->first()->parent_id;
                    $locationFilterId = $locationDatum->first()->id;
                }
                $locationFilterId = empty($locationFilterId) ? 0 : $locationFilterId;

                $countryFilterDatum = $filterServ->findByTypeAndParentTypeAndCode('school', 'country', $schoolFilterCountry);
                if ($countryFilterDatum->isNotEmpty()) {
                    $countryFilterId = $countryFilterDatum->first()->id;
                }
                if(!empty($countryFilterId) AND !empty($locationParentFilterId)) {
                    $countryFilterId = $countryFilterId != $locationParentFilterId ? $locationParentFilterId : $countryFilterId;
                }
                if(!empty($countryFilterId) AND empty($locationParentFilterId)) {
                    $countryFilterId = $countryFilterId;
                }
                if(empty($countryFilterId) AND !empty($locationParentFilterId)) {
                    $countryFilterId = $locationParentFilterId;
                }
                if(empty($countryFilterId) AND empty($locationParentFilterId)) {
                    $countryFilterId = 0;
                }

                $schoolTypeDatum = $filterServ->findByTypeAndParentTypeAndCode('school', 'school-type', $schoolFilterSchoolType);
                if ($schoolTypeDatum->isNotEmpty()) {
                    $schoolTypeFilterId = $schoolTypeDatum->first()->id;
                }
                $schoolTypeFilterId = empty($schoolTypeFilterId) ? 0 :$schoolTypeFilterId;

                $programDatum = $filterServ->findByTypeAndParentTypeAndCode('school', 'program', $schoolFilterProgram);
                if ($programDatum->isNotEmpty()) {
                    $programParentFilterId = $programDatum->first()->parent_id;
                    $programFilterId = $programDatum->first()->id;
                }
                $programFilterId = empty($programFilterId) ? 0 : $programFilterId;

                $programTypeDatum = $filterServ->findByTypeAndParentTypeAndCode('school', 'program-type', $schoolFilterProgramType);
                if ($programTypeDatum->isNotEmpty()) {
                    $programTypeFilterId = $programTypeDatum->first()->id;
                }
                if(!empty($programTypeFilterId) AND !empty($programParentFilterId)) {
                    $programTypeFilterId = $programTypeFilterId != $programParentFilterId ? $programParentFilterId : $programTypeFilterId;
                }
                if(!empty($programTypeFilterId) AND empty($programParentFilterId)) {
                    $programTypeFilterId = $programTypeFilterId;
                }
                if(empty($programTypeFilterId) AND !empty($programParentFilterId)) {
                    $programTypeFilterId = $programParentFilterId;
                }
                if(empty($programTypeFilterId) AND empty($programParentFilterId)) {
                    $programTypeFilterId = 0;
                }

                $programTaDatum = $filterServ->findByTypeAndParentTypeAndCode('school', 'program-ta', $schoolFilterProgramTa);
                if ($programTaDatum->isNotEmpty()) {
                    $programTaFilterId = $programTaDatum->first()->id;
                }
                $programTaFilterId = empty($programTaFilterId) ? 0 : $programTaFilterId;


                if (!empty($countryFilterId)) {
                    $cDatum = $filterSchoolServ->findByFilterIdAndSchoolId($countryFilterId, $schoolNewId);
                    if ($cDatum->isEmpty()) {
                        $filterSchoolServ->create($countryFilterId, $schoolNewId, 0);
                    }
                }

                if (!empty($locationFilterId)) {
                    $lDatum = $filterSchoolServ->findByFilterIdAndSchoolId($locationFilterId, $schoolNewId);
                    if ($lDatum->isEmpty()) {
                        $filterSchoolServ->create($locationFilterId, $schoolNewId, 0);
                    } // END if
                }

                if (!empty($schoolTypeFilterId)) {
                    $stDatum = $filterSchoolServ->findByFilterIdAndSchoolId($schoolTypeFilterId, $schoolNewId);
                    if ($stDatum->isEmpty()) {
                        $filterSchoolServ->create($schoolTypeFilterId, $schoolNewId, 0);
                    } // END if
                }

                if (!empty($programTypeFilterId)) {
                    $ptDatum = $filterSchoolServ->findByFilterIdAndSchoolId($programTypeFilterId, $schoolNewId);
                    if ($ptDatum->isEmpty()) {
                        $filterSchoolServ->create($programTypeFilterId, $schoolNewId, 0);
                    } // END if
                }

                if (!empty($programFilterId)) {
                    $pDatum = $filterSchoolServ->findByFilterIdAndSchoolId($programFilterId, $schoolNewId);
                    if ($pDatum->isEmpty()) {
                        $filterSchoolServ->create($programFilterId, $schoolNewId, 0);
                    } // END if
                }

                if (!empty($programTaFilterId)) {
                    $ptDatum = $filterSchoolServ->findByFilterIdAndSchoolId($programTaFilterId, $schoolNewId);
                    if ($ptDatum->isEmpty()) {
                        $filterSchoolServ->create($programTaFilterId, $schoolNewId, 0);
                    } // END if
                }

            } // END foreach

        } // END foreach
    } // END function


    public function importCompanyTableSeeder()
    {

        $dirParentType = config('tbl_dirs.DIRS_PARENT_TYPE_COMPANY');
        $dirType = config('tbl_dirs.DIRS_TYPE_COVER');
        $dirOwnerId = 0;
        $dirPrivacyStatus = config('tbl_dirs.DIRS_PRIVACY_STATUS_PUBLIC');
        $dirStatus = config('tbl_dirs.DIRS_STATUS_ENABLE');
        $dirCreatorId = 0;

        $fileParentType = config('tbl_files.FILES_PARENT_TYPE_COMPANY');
        $fileType = config('tbl_files.FILES_TYPE_IMAGE');
        $filePrivacyStatus = config('tbl_files.FILES_PRIVACY_STATUS_PUBLIC');
        $fileStatus = config('tbl_files.FILES_STATUS_ENABLE');
        $fileOwnerId = 0;
        $fileCreatorId = 0;
        $fileIsCover = true;

        $dirServ = app(DirServ::class);
        $fileServ = app(FileServ::class);
        $companyServ = app(CompanyServ::class);
        $companyBankServ = app(CompanyBankServ::class);
        $companyContactServ = app(CompanyContactServ::class);
        $companyEmerContactServ = app(CompanyEmerContactServ::class);
        $companyLocationServ = app(CompanyLocationServ::class);

        $companies = config('default_companies');
        $companyBanks = config('default_company_banks');
        $companyContacts = config('default_company_contacts');
        $companyEmerContacts = config('default_company_emer_contacts');
        $companyLocations = config('default_company_locations');

        $finalCompanies = [];
        foreach ($companies as $company) {

            $companyDatum = $companyServ->findByGuiNumber($company['gui_number']);

            if ($companyDatum->isNotEmpty()) {
                continue;
            } // END if

            // bank
            foreach ($companyBanks as $companyBank) {
                if ($companyBank['id'] != $company['id']) continue;
                $company['bank']['name'] = $companyBank['name'];
                $company['bank']['branch_name'] = $companyBank['branch_name'];
                $company['bank']['account_name'] = $companyBank['account_name'];
                $company['bank']['account_number'] = $companyBank['account_number'];
            } // END foreach

            // contact
            foreach ($companyContacts as $companyContact) {
                if ($companyContact['id'] != $company['id']) continue;
                $company['contact']['username'] = $companyContact['username'];
                $company['contact']['country'] = $companyContact['country'];
                $company['contact']['state'] = $companyContact['state'];
                $company['contact']['address'] = $companyContact['address'];
                $company['contact']['phone'] = $companyContact['phone'];
                $company['contact']['fax'] = $companyContact['fax'];
            } // END foreach

            // emer
            foreach ($companyEmerContacts as $companyEmerContact) {
                if ($companyEmerContact['id'] != $company['id']) continue;
                $company['emer']['username'] = $companyEmerContact['username'];
                $company['emer']['phone'] = $companyEmerContact['phone'];
                $company['emer']['email'] = $companyEmerContact['email'];
            } // END foreach

            // location
            $dd = 0;
            foreach ($companyLocations as $companyLocation) {
                if ($companyLocation['id'] != $company['id']) continue;

                $locations = explode(',', $companyLocation['locations']);
                foreach ($locations as $ddd => $code) {
                    $company['locations'][$dd]['codes'][$ddd] = $code;
                } // END foreach
                $dd++;
            } // END foreach

            array_push ($finalCompanies, $company);
        } // END foreach

        foreach ($finalCompanies as $company) {

            $companyId = $company['id'];
            $companyName = $company['name'];
            $companyPrincipal = $company['principal'];
            $companyIncorporationDate = $company['incorporation_date'];
            $companyGuiNumber = $company['gui_number'];
            $companyCover = $company['cover'];

            $companyDatum = $companyServ->findByGuiNumber($companyGuiNumber);
            if ($companyDatum->isNotEmpty()) continue;

            $companyDatum = $companyServ->create($companyName, $companyPrincipal, $companyIncorporationDate, $companyGuiNumber, $companyCover);
            if ($companyDatum->isEmpty()) continue;

            $companyNewId = $companyDatum->first()->id;

            // cover
            $dirParentId = $companyNewId;
            $dirDatum = $dirServ->findByParentTypeAndParentIdAndTypeAndOwnerId($dirParentType, $dirParentId, $dirType, $dirOwnerId);

            if ($dirDatum->isEmpty()) {
                $dirDatum = $dirServ->initDefaultAlbum($dirParentType, $dirParentId, $dirType, $dirOwnerId, $dirPrivacyStatus, $dirStatus, $dirCreatorId);
            } // END if

            $dirId = $dirDatum->first()->id;

            $tPath = $dirParentType . '_' . $dirParentId . '/' . $dirType;
            $pathExists = Storage::disk('gallery')->exists($tPath);
            if (empty($pathExists)) {
                Storage::disk('gallery')->makeDirectory($tPath, 0755, true);
            } // END if

            // bank
            $companyBankName = $company['bank']['name'];
            $companyBankBranchName = $company['bank']['branch_name'];
            $companyBankAccountName = $company['bank']['account_name'];
            $companyBankAccountNumber = $company['bank']['account_number'];

            $companyBankServ->create($companyNewId, $companyBankName, $companyBankBranchName, $companyBankAccountName, $companyBankAccountNumber);

            // contact
            $contactUsername = $company['contact']['username'];
            $contactCountry = $company['contact']['country'];
            $contactState = $company['contact']['state'];
            $contactAddress = $company['contact']['address'];
            $contactPhone = $company['contact']['phone'];
            $contactFax = $company['contact']['fax'];

            $companyContactServ->create($companyNewId, $contactUsername, $contactCountry, $contactState, $contactAddress, $contactPhone, $contactFax);

            // emer
            $emerUsername = $company['emer']['username'];
            $emerPhone = $company['emer']['phone'];
            $emerEmail = $company['emer']['email'];

            $companyEmerContactServ->create($companyNewId, $emerUsername, $emerPhone , $emerEmail);

            // locations
            foreach ($company['locations'] as $location) {
                foreach ($location['codes'] as $code) {
                    $companyLocationServ->create($companyNewId, $code);
                } // END foreach
            } // END foreach

        } // END foreach

    } // END function

}
