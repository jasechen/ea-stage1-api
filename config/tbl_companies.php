<?php
$fields = [
    'COMPANIES_STATUS_ENABLE' => 'enable',
    'COMPANIES_STATUS_DELETE' => 'delete',
    'COMPANIES_STATUS_DISABLE' => 'disable',
];

$fields['DEFAULT_COMPANIES_STATUS'] = $fields['COMPANIES_STATUS_ENABLE'];
$fields['COMPANIES_STATUS'] = [
    $fields['COMPANIES_STATUS_ENABLE'],
    $fields['COMPANIES_STATUS_DELETE'],
    $fields['COMPANIES_STATUS_DISABLE']
];

return $fields;