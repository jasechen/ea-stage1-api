<?php
$fields = [
    'SCHOOL_USERS_TYPE_SCHOOL' => 'school',
    'SCHOOL_USERS_TYPE_COMPANY' => 'company',
    'SCHOOL_USERS_TYPE_STUDENT' => 'student',
];

$fields['DEFAULT_SCHOOL_USERS_TYPE'] = $fields['SCHOOL_USERS_TYPE_SCHOOL'];
$fields['SCHOOL_USERS_TYPES'] = [
    $fields['SCHOOL_USERS_TYPE_SCHOOL'],
    $fields['SCHOOL_USERS_TYPE_COMPANY'],
    $fields['SCHOOL_USERS_TYPE_STUDENT']
];

return $fields;