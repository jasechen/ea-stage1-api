<?php
$fields = [
    'USERS_STATUS_INIT' => 'init',
    'USERS_STATUS_ENABLE' => 'enable',
    'USERS_STATUS_DISABLE' => 'disable',
    'USERS_STATUS_DELETE' => 'delete',
];

$fields['DEFAULT_USERS_STATUS'] = $fields['USERS_STATUS_INIT'];
$fields['USERS_STATUS'] = [
    $fields['USERS_STATUS_INIT'],
    $fields['USERS_STATUS_ENABLE'],
    $fields['USERS_STATUS_DISABLE'],
    $fields['USERS_STATUS_DELETE']
];

return $fields;