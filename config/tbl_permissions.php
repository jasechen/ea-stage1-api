<?php
$fields = [
    'PERMISSIONS_TYPE_GENERAL' => 'general',
    'PERMISSIONS_TYPE_ADMIN' => 'admin',
];

$fields['DEFAULT_PERMISSIONS_TYPE'] = $fields['PERMISSIONS_TYPE_GENERAL'];
$fields['PERMISSIONS_TYPES'] = [
    $fields['PERMISSIONS_TYPE_GENERAL'],
    $fields['PERMISSIONS_TYPE_ADMIN']
];

return $fields;