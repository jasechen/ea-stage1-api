<?php
$fields = [
    'SCHOOL_COURSES_TA_CHILD' => 'child',
    'SCHOOL_COURSES_TA_PARENT-CHILD' => 'parent-child',
    'SCHOOL_COURSES_TA_MIDDLE' => 'middle',
    'SCHOOL_COURSES_TA_SENIOR' => 'senior',
    'SCHOOL_COURSES_TA_BACHELOR' => 'bachelor',
    'SCHOOL_COURSES_TA_MASTER' => 'master',
    'SCHOOL_COURSES_TA_DOCTORAL' => 'doctoral',
    'SCHOOL_COURSES_TA_ADULT' => 'adult',
    'SCHOOL_COURSES_TA_SENIOR-CITIZEN' => 'senior-citizen'
];

$fields['DEFAULT_SCHOOL_COURSES_TA'] = $fields['SCHOOL_COURSES_TA_SENIOR'];
$fields['SCHOOL_COURSES_TAS'] = [
    $fields['SCHOOL_COURSES_TA_PARENT-CHILD'],
    $fields['SCHOOL_COURSES_TA_MIDDLE'],
    $fields['SCHOOL_COURSES_TA_SENIOR'],
    $fields['SCHOOL_COURSES_TA_BACHELOR'],
    $fields['SCHOOL_COURSES_TA_MASTER'],
    $fields['SCHOOL_COURSES_TA_DOCTORAL'],
    $fields['SCHOOL_COURSES_TA_ADULT'],
    $fields['SCHOOL_COURSES_TA_SENIOR-CITIZEN']
];

return $fields;