<?php
$fields = [
    'FILES_PARENT_TYPE_POSTCATEGORY' => 'postcategory',
    'FILES_PARENT_TYPE_SCHOOL' => 'school',
    'FILES_PARENT_TYPE_COMPANY' => 'company',
    'FILES_PARENT_TYPE_BLOG' => 'blog',
    'FILES_PARENT_TYPE_POST' => 'post',
    'FILES_PARENT_TYPE_USER' => 'user',

    'FILES_TYPE_IMAGE' => 'image',
    'FILES_TYPE_VIDEO' => 'video',
    'FILES_TYPE_AUDIO' => 'audio',
    'FILES_TYPE_OTHERS' => 'others',

    'FILES_PRIVACY_STATUS_PUBLIC' => 'public',
    'FILES_PRIVACY_STATUS_PRIVATE' => 'private',

    'FILES_STATUS_ENABLE'  => 'enable',
    'FILES_STATUS_DISABLE' => 'disable',
    'FILES_STATUS_DELETE'  => 'delete',

    'FILES_IS_COVER_TRUE' => 'true',
    'FILES_IS_COVER_FALSE' => 'false',
];

$fields['DEFAULT_FILES_PARENT_TYPE'] = $fields['FILES_PARENT_TYPE_POST'];
$fields['FILES_PARENT_TYPES'] = [
    $fields['FILES_PARENT_TYPE_POSTCATEGORY'],
    $fields['FILES_PARENT_TYPE_SCHOOL'],
    $fields['FILES_PARENT_TYPE_COMPANY'],
    $fields['FILES_PARENT_TYPE_BLOG'],
    $fields['FILES_PARENT_TYPE_POST'],
    $fields['FILES_PARENT_TYPE_USER']
];

$fields['DEFAULT_FILES_TYPE'] = $fields['FILES_TYPE_IMAGE'];
$fields['FILES_TYPES'] = [
    $fields['FILES_TYPE_IMAGE'],
    $fields['FILES_TYPE_VIDEO'],
    $fields['FILES_TYPE_AUDIO'],
    $fields['FILES_TYPE_OTHERS']
];

$fields['DEFAULT_FILES_PRIVACY_STATUS'] = $fields['FILES_PRIVACY_STATUS_PUBLIC'];
$fields['FILES_PRIVACY_STATUS'] = [
    $fields['FILES_PRIVACY_STATUS_PUBLIC'],
    $fields['FILES_PRIVACY_STATUS_PRIVATE']
];

$fields['DEFAULT_FILES_STATUS'] = $fields['FILES_STATUS_ENABLE'];
$fields['FILES_STATUS'] = [
    $fields['FILES_STATUS_ENABLE'],
    $fields['FILES_STATUS_DISABLE'],
    $fields['FILES_STATUS_DELETE']
];

$fields['DEFAULT_FILES_IS_COVER'] = $fields['FILES_IS_COVER_FALSE'];
$fields['FILES_IS_COVERS'] = [
    $fields['FILES_IS_COVER_TRUE'],
    $fields['FILES_IS_COVER_FALSE']
];

return $fields;