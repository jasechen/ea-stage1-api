<?php
$fields = [
    'DIRS_PARENT_TYPE_POSTCATEGORY' => 'postcategory',
    'DIRS_PARENT_TYPE_SCHOOL' => 'school',
    'DIRS_PARENT_TYPE_COMPANY' => 'company',
    'DIRS_PARENT_TYPE_BLOG' => 'blog',
    'DIRS_PARENT_TYPE_POST' => 'post',
    'DIRS_PARENT_TYPE_USER' => 'user',
    'DIRS_PARENT_TYPE_DIR' => 'dir',

    'DIRS_TYPE_NO_DIR' => 'no_dir',
    'DIRS_TYPE_AVATAR' => 'avatar',
    'DIRS_TYPE_COVER' => 'cover',
    'DIRS_TYPE_OTHERS' => 'others',

    'DIRS_PRIVACY_STATUS_PUBLIC' => 'public',
    'DIRS_PRIVACY_STATUS_PRIVATE' => 'private',

    'DIRS_STATUS_ENABLE'  => 'enable',
    'DIRS_STATUS_DISABLE' => 'disable',
    'DIRS_STATUS_DELETE'  => 'delete',

    'DIRS_IS_ALBUM_TRUE' => 'true',
    'DIRS_IS_ALBUM_FALSE' => 'false',

    'DIRS_IS_DEFAULT_TRUE' => 'true',
    'DIRS_IS_DEFAULT_FALSE' => 'false',
];

$fields['DEFAULT_DIRS_PARENT_TYPE'] = $fields['DIRS_PARENT_TYPE_BLOG'];
$fields['DIRS_PARENT_TYPES'] = [
    $fields['DIRS_PARENT_TYPE_POSTCATEGORY'],
    $fields['DIRS_PARENT_TYPE_SCHOOL'],
    $fields['DIRS_PARENT_TYPE_COMPANY'],
    $fields['DIRS_PARENT_TYPE_BLOG'],
    $fields['DIRS_PARENT_TYPE_POST'],
    $fields['DIRS_PARENT_TYPE_USER'],
    $fields['DIRS_PARENT_TYPE_DIR']
];

$fields['DEFAULT_DIRS_TYPE'] = $fields['DIRS_TYPE_OTHERS'];
$fields['DIRS_TYPES'] = [
    $fields['DIRS_TYPE_NO_DIR'],
    $fields['DIRS_TYPE_AVATAR'],
    $fields['DIRS_TYPE_COVER'],
    $fields['DIRS_TYPE_OTHERS']
];

$fields['DEFAULT_DIRS_PRIVACY_STATUS'] = $fields['DIRS_PRIVACY_STATUS_PUBLIC'];
$fields['DIRS_PRIVACY_STATUS'] = [
    $fields['DIRS_PRIVACY_STATUS_PUBLIC'],
    $fields['DIRS_PRIVACY_STATUS_PRIVATE']
];

$fields['DEFAULT_DIRS_STATUS'] = $fields['DIRS_STATUS_ENABLE'];
$fields['DIRS_STATUS'] = [
    $fields['DIRS_STATUS_ENABLE'],
    $fields['DIRS_STATUS_DISABLE'],
    $fields['DIRS_STATUS_DELETE']
];

$fields['DEFAULT_DIRS_IS_ALBUM'] = $fields['DIRS_IS_ALBUM_TRUE'];
$fields['DIRS_IS_ALBUMS'] = [
    $fields['DIRS_IS_ALBUM_TRUE'],
    $fields['DIRS_IS_ALBUM_FALSE']
];

$fields['DEFAULT_DIRS_IS_DEFAULT'] = $fields['DIRS_IS_DEFAULT_TRUE'];
$fields['DIRS_IS_DEFAULTS'] = [
    $fields['DIRS_IS_DEFAULT_TRUE'],
    $fields['DIRS_IS_DEFAULT_FALSE']
];

return $fields;