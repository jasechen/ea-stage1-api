<?php
$fields = [
    'POSTS_PRIVACY_STATUS_PUBLIC' => 'public',
    'POSTS_PRIVACY_STATUS_PRIVATE' => 'private',

    'POSTS_STATUS_DRAFT' => 'draft',
    'POSTS_STATUS_PUBLISH' => 'publish',
    'POSTS_STATUS_DELETE' => 'delete',
    'POSTS_STATUS_DISABLE' => 'disable',
];

$fields['DEFAULT_POSTS_PRIVACY_STATUS'] = $fields['POSTS_PRIVACY_STATUS_PUBLIC'];
$fields['POSTS_PRIVACY_STATUS'] = [
    $fields['POSTS_PRIVACY_STATUS_PUBLIC'],
    $fields['POSTS_PRIVACY_STATUS_PRIVATE']
];

$fields['DEFAULT_POSTS_STATUS'] = $fields['POSTS_STATUS_DRAFT'];
$fields['POSTS_STATUS'] = [
    $fields['POSTS_STATUS_DRAFT'],
    $fields['POSTS_STATUS_PUBLISH'],
    $fields['POSTS_STATUS_DELETE'],
    $fields['POSTS_STATUS_DISABLE']
];

return $fields;