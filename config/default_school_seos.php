<?php

$fields = [

['id' => '1',
'slug' => 'smeag-global-education',
'excerpt' => '最多台灣學生的選擇，想找考試課程，同時學校又是官方考場，免去舟車勞頓的困擾，SMEAG您會喜歡',
'og_title' => 'SMEAG Global Education - English.Agency',
'og_description' => '最多台灣學生的選擇，想找考試課程，同時學校又是官方考場，免去舟車勞頓的困擾，SMEAG您會喜歡',
'meta_title' => 'SMEAG Global Education - English.Agency',
'meta_description' => '最多台灣學生的選擇，想找考試課程，同時學校又是官方考場，免去舟車勞頓的困擾，SMEAG您會喜歡',
'cover_title' => '菲律賓宿霧 SMEAG 語言學校',
'cover_alt' => '菲律賓宿霧 SMEAG 語言學校'],

['id' => '2',
'slug' => 'cebu-english-language-center',
'excerpt' => '斯巴達式的課程安排，若想借助外在的環境給予自己學習動力，CELC每天11堂以上的課程與嚴格執行的English Only規則可以幫的上你的忙',
'og_title' => 'Cebu English Language Center - English.Agency',
'og_description' => '斯巴達式的課程安排，若想借助外在的環境給予自己學習動力，CELC每天11堂以上的課程與嚴格執行的English Only規則可以幫的上你的忙',
'meta_title' => 'Cebu English Language Center - English.Agency',
'meta_description' => '斯巴達式的課程安排，若想借助外在的環境給予自己學習動力，CELC每天11堂以上的課程與嚴格執行的English Only規則可以幫的上你的忙',
'cover_title' => '菲律賓宿霧 CELC 語言學校',
'cover_alt' => '菲律賓宿霧 CELC 語言學校'],

['id' => '3',
'slug' => 'kredo-it-abroad',
'excerpt' => '成為通過學習“英語×IT”，可以活躍在“未來世界”的人，是Kredo IT主打的特色，本校適合未來想在跨國環境經商，又了解網頁程式的人就讀。',
'og_title' => 'Kredo IT Abroad - English.Agency',
'og_description' => '成為通過學習“英語×IT”，可以活躍在“未來世界”的人，是Kredo IT主打的特色，本校適合未來想在跨國環境經商，又了解網頁程式的人就讀。',
'meta_title' => 'Kredo IT Abroad - English.Agency',
'meta_description' => '成為通過學習“英語×IT”，可以活躍在“未來世界”的人，是Kredo IT主打的特色，本校適合未來想在跨國環境經商，又了解網頁程式的人就讀。',
'cover_title' => '菲律賓宿霧 Kredo 語言學校',
'cover_alt' => '菲律賓宿霧 Kredo 語言學校'],

['id' => '4',
'slug' => 'first-english',
'excerpt' => '若想要找一所非斯巴達教育，周遭環境又單純沒有誘惑，可以靜下心來學習英文的學校，宿霧地區真的沒有比First English更適合了。',
'og_title' => 'First English - English.Agency',
'og_description' => '若想要找一所非斯巴達教育，周遭環境又單純沒有誘惑，可以靜下心來學習英文的學校，宿霧地區真的沒有比First English更適合了。',
'meta_title' => 'First English - English.Agency',
'meta_description' => '若想要找一所非斯巴達教育，周遭環境又單純沒有誘惑，可以靜下心來學習英文的學校，宿霧地區真的沒有比First English更適合了。',
'cover_title' => '菲律賓宿霧 First English 語言學校',
'cover_alt' => '菲律賓宿霧 First English 語言學校'],

['id' => '5',
'slug' => 'howdy-english',
'excerpt' => '這是一間於2015年，以日式軟硬體，原汁原味建造的全新學校，除了日式餐點美味、校舍新穎之外，Howdy提供學生一般英文課程，以及親子課程供選擇',
'og_title' => 'HOWDY English - English.Agency',
'og_description' => '這是一間於2015年，以日式軟硬體，原汁原味建造的全新學校，除了日式餐點美味、校舍新穎之外，Howdy提供學生一般英文課程，以及親子課程供選擇',
'meta_title' => 'HOWDY English - English.Agency',
'meta_description' => '這是一間於2015年，以日式軟硬體，原汁原味建造的全新學校，除了日式餐點美味、校舍新穎之外，Howdy提供學生一般英文課程，以及親子課程供選擇',
'cover_title' => '菲律賓宿霧 HOWDY 語言學校',
'cover_alt' => '菲律賓宿霧 HOWDY 語言學校'],

];

return $fields;