<?php

$fields = [

  ['id' => '1', 'country' => 'ph', 'state' => 'ceb', 'school_type' => 'language', 'program_type' => 'humanities-and-social-sciences', 'program' => 'english', 'program_ta' => 'adult'],

  ['id' => '2', 'country' => 'ph', 'state' => 'ceb', 'school_type' => 'language', 'program_type' => 'humanities-and-social-sciences', 'program' => 'english', 'program_ta' => 'adult'],

  ['id' => '3', 'country' => 'ph', 'state' => 'ceb', 'school_type' => 'language', 'program_type' => 'humanities-and-social-sciences', 'program' => 'english', 'program_ta' => 'adult'],
  ['id' => '3', 'country' => 'ph', 'state' => 'ceb', 'school_type' => 'language', 'program_type' => 'applied-science', 'program' => 'computer-science', 'program_ta' => 'adult'],

  ['id' => '4', 'country' => 'ph', 'state' => 'ceb', 'school_type' => 'language', 'program_type' => 'humanities-and-social-sciences', 'program' => 'english', 'program_ta' => 'adult'],

  ['id' => '5', 'country' => 'ph', 'state' => 'ceb', 'school_type' => 'language', 'program_type' => 'humanities-and-social-sciences', 'program' => 'english', 'program_ta' => 'adult'],

];

return $fields;