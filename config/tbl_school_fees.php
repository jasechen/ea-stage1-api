<?php
$fields = [
    'SCHOOL_FEES_UNIT_WEEK' => 'week',
    'SCHOOL_FEES_UNIT_MONTH' => 'month',
    'SCHOOL_FEES_UNIT_YEAR' => 'year',
    'SCHOOL_FEES_UNIT_ACAD-TERM' => 'acad-term',

    'SCHOOL_FEES_CURRENCY_USD' => 'usd',
    'SCHOOL_FEES_CURRENCY_GBP' => 'gbp',
    'SCHOOL_FEES_CURRENCY_EUR' => 'eur',
    'SCHOOL_FEES_CURRENCY_AUD' => 'aud',
    'SCHOOL_FEES_CURRENCY_TWD' => 'twd',
    'SCHOOL_FEES_CURRENCY_CNY' => 'cny'
];

$fields['DEFAULT_SCHOOL_FEES_UNIT'] = $fields['SCHOOL_FEES_UNIT_MONTH'];
$fields['SCHOOL_FEES_UNITS'] = [
    $fields['SCHOOL_FEES_UNIT_WEEK'],
    $fields['SCHOOL_FEES_UNIT_MONTH'],
    $fields['SCHOOL_FEES_UNIT_YEAR'],
    $fields['SCHOOL_FEES_UNIT_ACAD-TERM'],
];

$fields['DEFAULT_SCHOOL_FEES_CURRENCY'] = $fields['SCHOOL_FEES_CURRENCY_USD'];
$fields['SCHOOL_FEES_CURRENCYS'] = [
    $fields['SCHOOL_FEES_CURRENCY_USD'],
    $fields['SCHOOL_FEES_CURRENCY_GBP'],
    $fields['SCHOOL_FEES_CURRENCY_EUR'],
    $fields['SCHOOL_FEES_CURRENCY_AUD'],
    $fields['SCHOOL_FEES_CURRENCY_TWD'],
    $fields['SCHOOL_FEES_CURRENCY_CNY']
];

return $fields;