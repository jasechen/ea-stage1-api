<?php

$fields = [

    'CITY_TW_CHY' => 'chy',
    'CITY_TW_CPS' => 'cps',
    'CITY_TW_CWH' => 'cwh',
    'CITY_TW_CWS' => 'cws',
    'CITY_TW_CYI' => 'cyi',
    'CITY_TW_DLS' => 'dls',

    'CITY_TW_HSC' => 'hsc',
    'CITY_TW_HSH' => 'hsh',
    'CITY_TW_HWA' => 'hwa',
    'CITY_TW_HWC' => 'hwc',
    'CITY_TW_ILC' => 'ilc',
    'CITY_TW_ILN' => 'iln',

    'CITY_TW_IUH' => 'iuh',
    'CITY_TW_KHH' => 'khh',
    'CITY_TW_KLU' => 'klu',
    'CITY_TW_KMN' => 'kmn',
    'CITY_TW_LNN' => 'lnn',
    'CITY_TW_MAC' => 'mac',

    'CITY_TW_MAL' => 'mal',
    'CITY_TW_MZG' => 'mzg',
    'CITY_TW_NTC' => 'ntc',
    'CITY_TW_NTO' => 'nto',
    'CITY_TW_PEH' => 'peh',
    'CITY_TW_PTS' => 'pts',

    'CITY_TW_PZC' => 'pzc',
    'CITY_TW_TBC' => 'tbc',
    'CITY_TW_TFC' => 'tfc',
    'CITY_TW_TNN' => 'tnn',
    'CITY_TW_TPE' => 'tpe',
    'CITY_TW_TPH' => 'tph',

    'CITY_TW_TTC' => 'ttc',
    'CITY_TW_TTT' => 'ttt',
    'CITY_TW_TXG' => 'txg',
    'CITY_TW_TYC' => 'tyc',
    'CITY_TW_YLN' => 'yln',
    'CITY_TW_YUN' => 'yun',

    'CITY_AU_SYD' => 'syd',

    'CITY_CA_YOW' => 'yow',
    'CITY_CA_YVR' => 'yvr',
    'CITY_CA_YYZ' => 'yyz',

    'CITY_PH_BAG' => 'bag',
    'CITY_PH_BCD' => 'bcd',
    'CITY_PH_CEB' => 'ceb',
    'CITY_PH_CRK' => 'crk',
    'CITY_PH_MPH' => 'mph',

];

$fields['SERVICE_LOACTIONS'] = [
    $fields['CITY_TW_CHY'],
    $fields['CITY_TW_CPS'],
    $fields['CITY_TW_CWH'],
    $fields['CITY_TW_CWS'],
    $fields['CITY_TW_CYI'],
    $fields['CITY_TW_DLS'],

    $fields['CITY_TW_HSC'],
    $fields['CITY_TW_HSH'],
    $fields['CITY_TW_HWA'],
    $fields['CITY_TW_HWC'],
    $fields['CITY_TW_ILC'],
    $fields['CITY_TW_ILN'],

    $fields['CITY_TW_IUH'],
    $fields['CITY_TW_KHH'],
    $fields['CITY_TW_KLU'],
    $fields['CITY_TW_KMN'],
    $fields['CITY_TW_LNN'],
    $fields['CITY_TW_MAC'],

    $fields['CITY_TW_MAL'],
    $fields['CITY_TW_MZG'],
    $fields['CITY_TW_NTC'],
    $fields['CITY_TW_NTO'],
    $fields['CITY_TW_PEH'],
    $fields['CITY_TW_PTS'],

    $fields['CITY_TW_PZC'],
    $fields['CITY_TW_TBC'],
    $fields['CITY_TW_TFC'],
    $fields['CITY_TW_TNN'],
    $fields['CITY_TW_TPE'],
    $fields['CITY_TW_TPH'],

    $fields['CITY_TW_TTC'],
    $fields['CITY_TW_TTT'],
    $fields['CITY_TW_TXG'],
    $fields['CITY_TW_TYC'],
    $fields['CITY_TW_YLN'],
    $fields['CITY_TW_YUN'],
];

$fields['CITIES_AU'] = [
    $fields['CITY_AU_SYD'],
];

$fields['CITIES_CA'] = [
    $fields['CITY_CA_YOW'],
    $fields['CITY_CA_YVR'],
    $fields['CITY_CA_YYZ'],
];

$fields['CITIES_PH'] = [
    $fields['CITY_PH_BAG'],
    $fields['CITY_PH_BCD'],
    $fields['CITY_PH_CEB'],
    $fields['CITY_PH_CRK'],
    $fields['CITY_PH_MPH'],
];

$fields['CITIES_TW'] = [
    $fields['CITY_TW_CHY'],
    $fields['CITY_TW_CPS'],
    $fields['CITY_TW_CWH'],
    $fields['CITY_TW_CWS'],
    $fields['CITY_TW_CYI'],
    $fields['CITY_TW_DLS'],

    $fields['CITY_TW_HSC'],
    $fields['CITY_TW_HSH'],
    $fields['CITY_TW_HWA'],
    $fields['CITY_TW_HWC'],
    $fields['CITY_TW_ILC'],
    $fields['CITY_TW_ILN'],

    $fields['CITY_TW_IUH'],
    $fields['CITY_TW_KHH'],
    $fields['CITY_TW_KLU'],
    $fields['CITY_TW_KMN'],
    $fields['CITY_TW_LNN'],
    $fields['CITY_TW_MAC'],

    $fields['CITY_TW_MAL'],
    $fields['CITY_TW_MZG'],
    $fields['CITY_TW_NTC'],
    $fields['CITY_TW_NTO'],
    $fields['CITY_TW_PEH'],
    $fields['CITY_TW_PTS'],

    $fields['CITY_TW_PZC'],
    $fields['CITY_TW_TBC'],
    $fields['CITY_TW_TFC'],
    $fields['CITY_TW_TNN'],
    $fields['CITY_TW_TPE'],
    $fields['CITY_TW_TPH'],

    $fields['CITY_TW_TTC'],
    $fields['CITY_TW_TTT'],
    $fields['CITY_TW_TXG'],
    $fields['CITY_TW_TYC'],
    $fields['CITY_TW_YLN'],
    $fields['CITY_TW_YUN'],
];

return $fields;



