<?php
$fields = [
    'SCHOOLS_STATUS_ENABLE' => 'enable',
    'SCHOOLS_STATUS_DELETE' => 'delete',
    'SCHOOLS_STATUS_DISABLE' => 'disable',
];

$fields['DEFAULT_SCHOOLS_STATUS'] = $fields['SCHOOLS_STATUS_ENABLE'];
$fields['SCHOOLS_STATUS'] = [
    $fields['SCHOOLS_STATUS_ENABLE'],
    $fields['SCHOOLS_STATUS_DELETE'],
    $fields['SCHOOLS_STATUS_DISABLE']
];

return $fields;