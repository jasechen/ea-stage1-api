<?php

$fields = [

['id' => '1',
'type' => 'dorm',
'service' => '洗衣服務,房間清潔服務',
'facility' => '交誼廳, 冰箱, 飲水機, 浴廁',
'rooms' => 'single, twin, triple, quadruple, five-people'],

['id' => '2',
'type' => 'dorm',
'service' => '洗衣服務,房間清潔服務',
'facility' => '交誼廳, 冰箱, 飲水機, 浴廁',
'rooms' => 'single, twin, triple, quadruple, six-people'],

['id' => '3',
'type' => 'dorm',
'service' => '房間清潔服務',
'facility' => '廚房, 冰箱, 洗衣機, 飲水機, 浴廁',
'rooms' => 'single, twin'],
['id' => '3',
'type' => 'dorm',
'service' => '房間清潔服務',
'facility' => '飲水機, 冰箱, 浴廁',
'rooms' => 'single, twin, quadruple'],

['id' => '4',
'type' => 'dorm',
'service' => '洗衣服務, 托育服務, 房間清潔服務',
'facility' => '冰箱, 浴廁',
'rooms' => 'single, twin, triple, quadruple, five-people'],

['id' => '5',
'type' => 'dorm',
'service' => '房間清潔服務',
'facility' => '飲水機, 冰箱, 浴廁',
'rooms' => 'single, twin, quadruple'],

];

return $fields;