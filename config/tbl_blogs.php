<?php
$fields = [
    'BLOGS_STATUS_ENABLE'  => 'enable',
    'BLOGS_STATUS_DISABLE' => 'disable',
    'BLOGS_STATUS_DELETE'  => 'delete',
];

$fields['DEFAULT_BLOGS_STATUS'] = $fields['BLOGS_STATUS_ENABLE'];
$fields['BLOGS_STATUS'] = [
    $fields['BLOGS_STATUS_ENABLE'],
    $fields['BLOGS_STATUS_DISABLE'],
    $fields['BLOGS_STATUS_DELETE']
];

return $fields;