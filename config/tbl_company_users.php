<?php
$fields = [
    'COMPANY_USERS_TYPE_ADMIN' => 'admin',
    'COMPANY_USERS_TYPE_AGENT' => 'agent'
];

$fields['DEFAULT_COMPANY_USERS_TYPE'] = $fields['COMPANY_USERS_TYPE_AGENT'];
$fields['COMPANY_USERS_TYPES'] = [
    $fields['COMPANY_USERS_TYPE_ADMIN'],
    $fields['COMPANY_USERS_TYPE_AGENT']
];

return $fields;