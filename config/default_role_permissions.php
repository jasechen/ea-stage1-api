<?php

$fields = [

// general
    [
        'type' => 'general',
        'code' => 'admin',
        'note' => '網站最高管理員',
        'permissions' => [
            ['type' => 'admin', 'code' => 'role_create', 'note' => '新增角色'],
            ['type' => 'admin', 'code' => 'role_update', 'note' => '更新角色'],
            ['type' => 'admin', 'code' => 'role_delete', 'note' => '刪除角色'],

            ['type' => 'admin', 'code' => 'locales_create', 'note' => '新增語系'],
            ['type' => 'admin', 'code' => 'locales_update', 'note' => '更新語系'],
            ['type' => 'admin', 'code' => 'locales_delete', 'note' => '刪除語系'],
        ]
    ],

    [
        'type' => 'general',
        'code' => 'manager',
        'note' => '網站管理員',
        'permissions' => [
            ['type' => 'admin', 'code' => 'user_create', 'note' => '新增使用者'],
            ['type' => 'admin', 'code' => 'user_update', 'note' => '更新使用者'],
            ['type' => 'admin', 'code' => 'user_delete', 'note' => '刪除使用者'],

            ['type' => 'admin', 'code' => 'postcategory_create', 'note' => '新增文章分類'],
            ['type' => 'admin', 'code' => 'postcategory_update', 'note' => '更新文章分類'],
            ['type' => 'admin', 'code' => 'postcategory_delete', 'note' => '刪除文章分類'],
        ]
    ],

    [
        'type' => 'general',
        'code' => 'editor',
        'note' => '網站編輯',
        'permissions' => [
            ['type' => 'admin', 'code' => 'blog_update', 'note' => '更新網誌資料'],

            ['type' => 'admin', 'code' => 'post_create', 'note' => '新增文章'],
            ['type' => 'admin', 'code' => 'post_update', 'note' => '更新文章'],
            ['type' => 'admin', 'code' => 'post_delete', 'note' => '刪除文章'],
        ]
    ],

    [
        'type' => 'general',
        'code' => 'author',
        'note' => '網站作者',
        'permissions' => [
            ['type' => 'general', 'code' => 'post_create', 'note' => '新增文章'],
            ['type' => 'general', 'code' => 'post_update', 'note' => '更新文章'],
            ['type' => 'general', 'code' => 'post_delete', 'note' => '刪除文章'],
        ]
    ],

    [
        'type' => 'general',
        'code' => 'member',
        'note' => '網站會員',
        'permissions' => [
            ['type' => 'general', 'code' => 'member_update', 'note' => '更新個人資料'],
            ['type' => 'general', 'code' => 'file_upload', 'note' => '上傳檔案'],
            ['type' => 'general', 'code' => 'password_change', 'note' => '變更密碼'],
            ['type' => 'general', 'code' => 'quotation_fill', 'note' => '填寫報價單'],
        ]
    ],

// company
    [
        'type' => 'company',
        'code' => 'admin',
        'note' => '公司管理員',
        'permissions' => [
            ['type' => 'company', 'code' => 'report_show', 'note' => '觀看統計數據'],
            ['type' => 'company', 'code' => 'quotation_create', 'note' => '新增學生報名報價單'],
            ['type' => 'company', 'code' => 'quotation_show', 'note' => '觀看每筆報名的狀態'],
        ]
    ],
    [
        'type' => 'company',
        'code' => 'agent',
        'note' => '公司仲介',
        'permissions' => [
            ['type' => 'company', 'code' => 'quotation_create', 'note' => '新增學生報名報價單'],
            ['type' => 'company', 'code' => 'quotation_show', 'note' => '觀看每筆報名的狀態'],
        ]
    ],

// school
    [
        'type' => 'school',
        'code' => 'admin',
        'note' => '學校管理員',
        'permissions' => [
            ['type' => 'school', 'code' => 'report_show', 'note' => '觀看統計數據'],
            ['type' => 'school', 'code' => 'quotation_verify', 'note' => '審核學生報名報價單'],
        ]
    ],
    [
        'type' => 'school',
        'code' => 'contact',
        'note' => '學校經辦員',
        'permissions' => [
            ['type' => 'school', 'code' => 'quotation_verify', 'note' => '審核學生報名報價單'],
        ]
    ],

];

return $fields;