<?php
$fields = [
    'FILTERS_TYPE_SCHOOL' => 'school',
    'FILTERS_TYPE_COMPANY' => 'company',

    'FILTERS_PARENT_TYPE_COUNTRY' => 'country',
    'FILTERS_PARENT_TYPE_LOCATION' => 'location',
    'FILTERS_PARENT_TYPE_SCHOOL-TYPE' => 'school-type',
    'FILTERS_PARENT_TYPE_PROGRAM-TYPE' => 'program-type',
    'FILTERS_PARENT_TYPE_PROGRAM' => 'program',
    'FILTERS_PARENT_TYPE_PROGRAM-TA' => 'program-ta',
];

$fields['DEFAULT_FILTERS_TYPE'] = $fields['FILTERS_TYPE_SCHOOL'];
$fields['FILTERS_TYPES'] = [
    $fields['FILTERS_TYPE_SCHOOL'],
    $fields['FILTERS_TYPE_COMPANY']
];

$fields['DEFAULT_FILTERS_PARENT_TYPE'] = $fields['FILTERS_PARENT_TYPE_COUNTRY'];
$fields['FILTERS_PARENT_TYPES'] = [
    $fields['FILTERS_PARENT_TYPE_COUNTRY'],
    $fields['FILTERS_PARENT_TYPE_LOCATION'],
    $fields['FILTERS_PARENT_TYPE_SCHOOL-TYPE'],
    $fields['FILTERS_PARENT_TYPE_PROGRAM-TYPE'],
    $fields['FILTERS_PARENT_TYPE_PROGRAM'],
    $fields['FILTERS_PARENT_TYPE_PROGRAM-TA']
];

return $fields;