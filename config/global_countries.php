<?php

$fields = [
    'COUNTRY_AU' => 'au',
    'COUNTRY_CA' => 'ca',
    'COUNTRY_PH' => 'ph',
    'COUNTRY_TW' => 'tw',
    'COUNTRY_GB' => 'gb',
    'COUNTRY_US' => 'us',
];

$fields['FAMILIAR_COUNTRIES'] = [
    $fields['COUNTRY_AU'],
    $fields['COUNTRY_CA'],
    $fields['COUNTRY_PH'],
    $fields['COUNTRY_TW'],
    $fields['COUNTRY_GB'],
    $fields['COUNTRY_US'],
];


return $fields;