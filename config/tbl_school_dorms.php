<?php
$fields = [
    'SCHOOL_DORMS_TYPE_DORM' => 'dorm',
    'SCHOOL_DORMS_TYPE_HOTEL' => 'hotel',
    'SCHOOL_DORMS_TYPE_HOMESTAY' => 'homestay',
];

$fields['DEFAULT_SCHOOL_DORMS_TYPE'] = $fields['SCHOOL_DORMS_TYPE_DORM'];
$fields['SCHOOL_DORMS_TYPES'] = [
    $fields['SCHOOL_DORMS_TYPE_DORM'],
    $fields['SCHOOL_DORMS_TYPE_HOTEL'],
    $fields['SCHOOL_DORMS_TYPE_HOMESTAY']
];

return $fields;