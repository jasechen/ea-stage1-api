<?php

$fields = [
    'LOCALES_CONTENTS_LANG_EN' => 'en',
    'LOCALES_CONTENTS_LANG_TW' => 'tw',
    'LOCALES_CONTENTS_LANG_CN' => 'cn',
];

$fields['DEFAULT_LOCALES_CONTENTS_LANG'] = $fields['LOCALES_CONTENTS_LANG_EN'];
$fields['LOCALES_CONTENTS_LANGS'] = [
    $fields['LOCALES_CONTENTS_LANG_EN'],
    $fields['LOCALES_CONTENTS_LANG_TW'],
    $fields['LOCALES_CONTENTS_LANG_CN']
];

return $fields;
