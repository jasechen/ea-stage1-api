<?php
$fields = [
    'ROLES_TYPE_GENERAL' => 'general',
    'ROLES_TYPE_COMPANY' => 'company',
    'ROLES_TYPE_SCHOOL' => 'school',
];

$fields['DEFAULT_ROLES_TYPE'] = $fields['ROLES_TYPE_GENERAL'];
$fields['ROLES_TYPES'] = [
    $fields['ROLES_TYPE_GENERAL'],
    $fields['ROLES_TYPE_COMPANY'],
    $fields['ROLES_TYPE_SCHOOL']
];

return $fields;