<?php

$fields = [
    'LANG_EN' => 'en',
    'LANG_TW' => 'tw',
];

$fields['CURRENT_LANGUAGES'] = [
    $fields['LANG_EN'],
    $fields['LANG_TW'],
];


return $fields;